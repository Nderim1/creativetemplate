var config = require('../../app/config')
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill(config.mandrill.api_key);

var message = {
    "subject": "example subject",
    "from_email": "info@" + config.primary_domain,
    "from_name": "Example Name",
    "to": [{
            "email": "mattia.borini@gmail.com",
            "name": "Mattia",
            "type": "to"
        }],
    "headers": {
        "Reply-To": "message.reply@" + config.primary_domain
    },
    "merge_language": "mailchimp",
    "global_merge_vars": [{
        "name": "NOME",
        "content": "Pippo"
    }]
};
mandrill_client.messages.sendTemplate({
      "template_name": 'Majeeko_welcome_2',
      "template_content": [{
          "name": "example name",
          "content": "example content"
      }],
      "message": message,
      "async": false
    },
  function(result) {
    console.log(result);
}, function(e) {
    console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
});