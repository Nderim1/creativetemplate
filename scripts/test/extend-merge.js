var obj = {
	propA: {
		subpropA: "hello"
	}
}

var _ = require('lodash')
_.defaults = require('merge-defaults');

console.log(_.defaults(obj, {
	propA: 'hi'
}))

console.log(_.defaults(obj, {
	propA: {
		subpropB: "ciao"
	},
	propB: 'hi'
}))