var mail = require('../../app/utils/mail')
  , swig = require('swig')

swig.setDefaults({ 
	varControls: ['{=', '=}']
});

// process.env.MAIL_ACCOUNTS_MANAGER = 'simone.golcic@synkysite.com'
process.env.MAIL_ACCOUNTS_MANAGER = 'mattia.borini@gmail.com'

var attempts = 10
  , domain = 'www.somedomain.com'
  , problem = "Dopo " + attempts + " tentativi è risultato impossibile da parte del server Majeeko confermare l'avvenuta selezione della pagina da abbinare al dominio \"" + domain + "\". Si conferma che la pagina è online ed è possibile procedere con l'impostazione dei DNS."

mail.sendMailForTophostProblem(problem)