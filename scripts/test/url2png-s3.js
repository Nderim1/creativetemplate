var id = '240860025962354'
  , url = 'http://' + id + '.majeeko-test.com'
  , config = require('../../app/config')
  
  , url2png = require('url2png')(config.URL2PNG_APIKEY,config.URL2PNG_SECRET)
  , url2pngOptions = { fullpage:true, thumbnail_max_width: 600, protocol: 'http' }
  
  , AWS = require('aws-sdk')

  , stream = require('stream')

AWS.config.update({
	accessKeyId: config.AWS_ACCESS_KEY_ID,
	secretAccessKey: config.AWS_SECRET_ACCESS_KEY
});

var s3 = new AWS.S3({
	params: {
		Bucket: 'majeeko',
		Key: 'screenshots/' + id + '.png'
	}
});

// var screenshotStream = url2png.readURL(url, url2pngOptions).pipe(new stream.PassThrough());

var fs = require('fs')
var screenshotStream = fs.createReadStream(__dirname + '/../public/images/screenshots/134312802006.png')

var upload = s3.upload({
	Body: screenshotStream
})

upload.send(function (err,data) {
	if(!err) {
		console.log("Successfully uploaded data to ", data.Location);
	}
})