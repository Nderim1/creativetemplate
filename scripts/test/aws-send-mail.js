var config = require('../../app/config')
  , AWS = require('aws-sdk')

AWS.config.update({
	accessKeyId: config.AWS_ACCESS_KEY_ID,
	secretAccessKey: config.AWS_SECRET_ACCESS_KEY
	// region: 'eu-west-1'
});

var ses = new AWS.SES();

var params = {
  Destination: {
    // BccAddresses: [ 'STRING_VALUE' ],
    // CcAddresses: [ 'STRING_VALUE' ],
    ToAddresses: [ 'mattia.borini@gmail.com' ]
  },
  Message: {
    Body: {
      Html: {
        Data: 'Hello there! This is a test message!'
        // Charset: 'STRING_VALUE'
      }
      // Text: {
      //   Data: 'STRING_VALUE',
      //   Charset: 'STRING_VALUE'
      // }
    },
    Subject: {
      Data: 'TEST MESSAGE'
      // Charset: 'STRING_VALUE'
    }
  },
  Source: 'STRING_VALUE', /* required */
  ReplyToAddresses: [
    'STRING_VALUE',
    /* more items */
  ],
  ReturnPath: 'STRING_VALUE'
};
ses.sendEmail(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(data);           // successful response
});