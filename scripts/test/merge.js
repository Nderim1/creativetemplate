var _ = require('lodash')
  , R = require('ramda')

var oldPosts = [{
	id: 1
  , title:'old title'
  , updated_at: 2
  , exclude: true
},{
	id: 3
  , updated_at: 5
}]

var newPosts = [{
	id: 1
  , title:'new title'
  , updated_at: 7
},{
	id: 2
  , title:'other title'
  , updated_at: 7
}]

var mergePosts = function (olds, news) {
	olds = olds || []
	news = news || []
	var ids = _.union( _.map(news, R.prop('id')), _.map(olds, R.prop('id')) )
	return _.map(ids, function(id) {
		var oldOne = _.findWhere(olds, {id:id}) || {}
		  , newOne = _.findWhere(news, {id:id}) || {}
		return _.extend({},oldOne,_.omit(newOne,['exclude']))
	})
}

console.log(mergePosts(oldPosts,newPosts))