var config = require('../../app/config')
  , AWS = require('aws-sdk')

AWS.config.update({
	accessKeyId: config.AWS_ACCESS_KEY_ID,
	secretAccessKey: config.AWS_SECRET_ACCESS_KEY
	// region: 'eu-west-1'
});

var s3 = new AWS.S3({
	params: {
		Bucket: 'majeeko',
		Key: 'test'
	}
});
// s3.createBucket(function(err) {
// 	if (err) {
// 		console.log("Error:", err);
// 	} else {
		s3.upload({
			Body: 'Hello!'
		}, function(err, data) {
			if(!err) {
				console.log("Successfully uploaded data to ", data.Location);
			}
		});
// 	}
// });