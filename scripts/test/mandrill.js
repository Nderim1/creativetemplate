var config = require('../../app/config')
var mandrill = require('mandrill-api/mandrill');
var mandrill_client = new mandrill.Mandrill(config.mandrill.api_key);

var message = {
    "html": "<p>Example HTML content</p>",
    "text": "Example text content",
    "subject": "example subject",
    "from_email": "info@" + config.primary_domain,
    "from_name": "Example Name",
    "to": [{
            "email": "mattia.borini@gmail.com",
            "name": "Recipient Name",
            "type": "to"
        }],
    "headers": {
        "Reply-To": "message.reply@" + config.primary_domain
    },
    // "important": false,
    // "track_opens": null,
    // "track_clicks": null,
    // "auto_text": null,
    // "auto_html": null,
    // "inline_css": null,
    // "url_strip_qs": null,
    // "preserve_recipients": null,
    // "view_content_link": null,
    // "bcc_address": "message.bcc_address@" + config.primary_domain,
    // "tracking_domain": null,
    // "signing_domain": null,
    // "return_path_domain": null,
    // "merge": true,
    // "merge_language": "mailchimp",
  //   "global_merge_vars": [{
  //           "name": "merge1",
  //           "content": "merge1 content"
  //       }],
  //   "merge_vars": [{
  //           "rcpt": "recipient.email@" + config.primary_domain,
  //           "vars": [{
  //                   "name": "merge2",
  //                   "content": "merge2 content"
  //               }]
  //       }],
  //   "tags": [
  //       "password-resets"
  //   ],
  //   "subaccount": "customer-123",
  //   "google_analytics_domains": [
		// config.primary_domain
  //   ],
  //   "google_analytics_campaign": "message.from_email@" + config.primary_domain,
  //   "metadata": {
  //       "website": "www." + config.primary_domain
  //   },
  //   "recipient_metadata": [{
  //           "rcpt": "recipient.email@" + config.primary_domain,
  //           "values": {
  //               "user_id": 123456
  //           }
  //       }],
  //   "attachments": [{
  //           "type": "text/plain",
  //           "name": "myfile.txt",
  //           "content": "ZXhhbXBsZSBmaWxl"
  //       }],
  //   "images": [{
  //           "type": "image/png",
  //           "name": "IMAGECID",
  //           "content": "ZXhhbXBsZSBmaWxl"
  //       }]
};
var async = false;
var ip_pool = "Main Pool";
var send_at = "example send_at";
mandrill_client.messages.send({
	"message": message, 
	"async": async
	// "ip_pool": ip_pool, 
	// "send_at": send_at
}, function(result) {
    console.log(result);
    /*
    [{
            "email": "recipient.email@" + config.primary_domain,
            "status": "sent",
            "reject_reason": "hard-bounce",
            "_id": "abc123abc123abc123abc123abc123"
        }]
    */
}, function(e) {
    // Mandrill returns the error as an object with name and message keys
    console.log('A mandrill error occurred: ' + e.name + ' - ' + e.message);
    // A mandrill error occurred: Unknown_Subaccount - No subaccount exists with the id 'customer-123'
});