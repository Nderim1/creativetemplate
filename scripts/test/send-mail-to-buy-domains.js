var mail = require('../../app/utils/mail')
  , swig = require('swig')

swig.setDefaults({ 
	varControls: ['{=', '=}']
});

// var toName = 'Fabio'
//   , toEmail = 'fabio@falardesign.it'
var order = {
    "page_id" : "523319834457224",
    "dominio" : "amicicicicicic",
    "domains" : [ 
        "amicicicicicic.it"
    ],
    "ragione_sociale" : "Ragione sociale srl",
    "partita_iva" : "013456789",
    "nome" : "Simone",
    "cognome" : "Golcic",
    "codice_fiscale" : "GLCSMN83L19L483H",
    "telefono" : "0432322222",
    "email" : "simone@hellobrand.it",
    "indirizzo" : "via G. Giolitti 34",
    "cap" : "00185",
    "comune" : "Udine",
    "provincia" : "UD",
    "coupon" : "onlyforroberto",
    "totalPrice" : 1,
    "_id" : "555b76bc392a505a6b755fb1",
    "loop" : null,
    "domain" : null,
    "__k" : null
}

// process.env.MAIL_ACCOUNTS_MANAGER = 'simone.golcic@synkysite.com'
process.env.MAIL_ACCOUNTS_MANAGER = 'mattia.borini@gmail.com'

mail.sendMailToBuyDomains(order)