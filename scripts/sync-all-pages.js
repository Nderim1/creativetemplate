require('dotenv').load()
var syncer = require('../app/sync/facebook')

syncer.syncAllPages()
.then(function() {
	console.log('All pages synced from script')
})
.catch(function (err) {
	console.log('Error syncing pages from script', err)	
})