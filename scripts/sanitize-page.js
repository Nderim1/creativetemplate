require('dotenv').load();
var 	_ = require("lodash")
	,	Q = require('q')
	,	db = require('../app/db')
	,	logger = require('../app/utils/logger');

/*
	Updates page props if missing
*/
var sanitizePageConfigById = function(pageId) {
	var deferred = Q.defer();

	db.getPageById(pageId)
		.then(function(page) {
			if(page) {
				var defaultConfiguration = require('../app/templates/hellobrand/default-configuration');

				if(_.has(page, 'configuration.settings')) {
					if(!_.has(page, 'configuration.settings.menu')) {
						_.set(page, 'configuration.settings.menu', defaultConfiguration.settings.menu);
					}
					if(!_.has(page, 'configuration.settings.social')) {
						var social = {
							skype: "",
							twitter: "",
							youtube: "",
							vimeo: ""
						}
						_.set(page, 'configuration.settings.social', social);
					}

				} else {
					_.set(page, "configuration.settings", defaultConfiguration.settings);
				}

				if(_.has(page, 'configuration.graphics')) {
					if(!_.has(page, 'configuration.graphics.columns')) {
						_.set(page, 'configuration.graphics.columns', defaultConfiguration.graphics.columns);
					}
					if(!_.has(page, 'configuration.graphics.colors')) {
						_.set(page, 'configuration.graphics.colors', defaultConfiguration.graphics.colors);
					}
				} else {
					_.set(page, 'configuration.graphics', defaultConfiguration.graphics);
				}

				if(_.has(page, 'configuration.contents')) {
					if(!_.has(page, 'configuration.contents.extra_pages')) {
						_.set(page, 'configuration.contents.extra_pages', defaultConfiguration.contents.extra_pages);
					}
				} else {
					_.set(page, 'configuration.contents', defaultConfiguration.contents);
				}

				return db._raw.pages.update({ 'facebook_id': pageId }, { $set: {"configuration": page.configuration}})
							.then(function() {
								logger.info('Page "' + page.username + '" sync complete!')
								deferred.resolve(page);
							});
				deferred.resolve(page);
			} else {
				deferred.reject();
			}
		})
		.catch(function(err) {
			logger.error('Error getting page ', err)
			deferred.reject();
		})
		return deferred.promise;
}

module.exports = {
	sanitizePageConfigById: sanitizePageConfigById
}
