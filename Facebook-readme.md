# Facebook Notes

- When requesting `albums` you can't include `photos` (eg. `?fields=albums{[...],photos,[...]}`) if you don't have a user access token. If you do you'll get **Authentication Error**.
	
	Test queries:
	- 492408757485618?fields=albums{photos}
	- 492408757485618/albums?fields=photos
	
	
### Facebook sync queries

#### Page

minimal:

	?fields=name,username,picture.width(200).height(200){url},cover,description,location,phone,category,website,link,emails,likes,posts{type,updated_time,attachments,likes,shares,comments{id}},albums{name,cover_photo,updated_time,type,count,photos,description},events{cover,location,name,attachments,description,updated_time,invited_count,start_time,end_time,ticket_uri,attending_count,maybe_count,place}

full:

	?fields=name,username,picture.width(200).height(200){url},cover,description,location,phone,category,website,link,emails,likes,posts.limit(6){type,updated_time,attachments,likes,shares,comments{id}},albums.limit(6){name,cover_photo,updated_time,type,count,description},events.limit(6){cover,location,name,attachments,description,updated_time,invited_count,start_time,end_time,ticket_uri,attending_count,maybe_count,place}