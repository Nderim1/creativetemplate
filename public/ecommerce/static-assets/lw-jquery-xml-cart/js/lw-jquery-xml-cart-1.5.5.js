/*!
	jQuery XML Store / Shop - Shopping Cart
	Created by LivelyWorks - http://livelyworks.net
	Ver. 1.5.5 - 02 DEC 2014
*/
(function loadStore($) {
	// helper functions
	if(!Object.keys) {
		Object.keys = (function() {
			'use strict';
			var hasOwnProperty = Object.prototype.hasOwnProperty,
				hasDontEnumBug = !({
					toString: null
				}).propertyIsEnumerable('toString'),
				dontEnums = [
					'toString',
					'toLocaleString',
					'valueOf',
					'hasOwnProperty',
					'isPrototypeOf',
					'propertyIsEnumerable',
					'constructor'
				],
				dontEnumsLength = dontEnums.length;

			return function(obj) {
				if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
					throw new TypeError('Object.keys called on non-object');
				}

				var result = [],
					prop, i;

				for (prop in obj) {
					if (hasOwnProperty.call(obj, prop)) {
						result.push(prop);
					}
				}

				if (hasDontEnumBug) {
					for (i = 0; i < dontEnumsLength; i++) {
						if (hasOwnProperty.call(obj, dontEnums[i])) {
							result.push(dontEnums[i]);
						}
					}
				}
				return result;
			};
		}());
	}

	$.fn.serializeObject = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

	function getParameterByName(name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	// end helper functions

	$(document).ready(function(){
	"use strict";

		_.templateSettings.variable = "_oData";

		/*
			Configuration Options
		*/

		var configOptions = {
			configJSONFile 			: "ecommerce/data-provider/config.json",
			productsJSONFile 		: "ecommerce/data-provider/products.json",
			storeName 				: "Default Majeeko Store",
			logoImage 				: "",
			currencySymbol 			: "€",
			currency 				: "EUR",
			businessEmail 			: "",
			usePaypal 				: false,
			useSubmitOrderByEmail 	: false,
			shippingCharges 		: 0,
			searchProductDetails	: true,
			bs3Theme 				: true,
			paypalBaseURL 			: "https://www.paypal.com/cgi-bin/webscr?cmd=_cart&rm=2&upload=1&charset=utf-8&currency_code=",
			paypalNotifyURL			: "&notify_url=" + notify_url,
			paypalReturnURL			: "&return=" + return_url + "&rm=2",
			submitOrderBaseURL 		: '/submit-order?currency_code='
		},
		/*
			General Variables
		*/
		allProductsCollection		= {},
		categoriesCollection		= {},
		currentProductCollection	= {},
		oCurrentProductData			= {},
		searchedProductsCollection	= {},
		cartProductsCollection		= [],
		DateTime					= new Date(),
		cartStats					= {},
		totalBtnMarkup				= '',
		selectedProductOptions		= {
			color:null,
			size:null
		},
		nProductInCart				= false,
		generalVars = {
			categoryIdentfierInURL	: "uid-",
			isStoreLoaded			: false,
			lastAccessedCategory 	: null,
			hashChanged 			: false,
			preventHashChangedAction: false,
			cartStorageName			: 'store-cart-storage'+window.location.hostname,
			qtyUpdateTimeout		: null,
			searchDelayTimeout		: null,
			showSubmitOrderTimeout 	: null,
			enableOrderBtn 			: true,
			isDemoActivate 			: false,
			preventHashChange 		: false
		},
		/*
			DOM elements
		*/
		$domElements = {
			storeLogo					: $('#storeLogo'),
			checkoutWithPaypalBtn 		: $('#checkoutWithPaypal'),
			checkoutSubmitOrderBtn		: $('#checkoutSubmitOrder'),
			loaderContainer				: $('#loaderContainer'),
			mainContainer				: $('#mainContainer'),
			modalCommon					: $('#commonModal'),
			modalContainer 				: configOptions.bs3Theme ? $('.common-modal-content') : $('#commonModal'),
			categoriesList 				: $('#categoriesList'),
			storeLoaderStatusText		: $('.lw-loading-status'),
			productsContainer 			: $('#productsContainer'),
			storeWaitingText 			: $('.lw-waiting-text'),
			addToCartBtnContainer 		: $('#addToCartBtnContainer'),
			productsBreadcrumb 			: $('#productsBreadcrumb'),
			shoppingCartBtnContainer 	: $('.shopping-cart-btn-container'),
			searchInput 				: $('input.search-product'),
			clearSearchBtn				: $('.clear-search-result-btn'),
			footerStoreName				: $('.footer-store-name'),
			goToTop						: $('.go-to-top'),
			searchedProductCounts 		: $('#searchedProductCounts')
		},

		/*
			Templates to process as _ (underscore templates)
		*/
		_templates = {
			sidebarCategories		: _.template( $("script.sidebar-catgegories-template").html() ),
			productsGrid			: _.template( $("script.products-grid-template").html() ),
			productsDetailsModal	: _.template( $("script.products-details-modal-template").html() ),
			shoppingCartModal		: _.template( $("script.shopping-cart-template").html() ),
			addToCartBtn 	 		: _.template( $("script.add-to-cart-btn-template").html() ),
			itemsInCart 	 		: _.template( $("script.products-details-modal-items-in-cart").html() ),
			shoppingCartBtn 	 	: _.template( $("script.shopping-cart-btn-template").html() ),
			submitOrderFormModal 	: _.template( $("script.submit-order-form-template").html() ),
			termsModal 				: _.template( $("script.terms-conditions").html() ),
			thanksModal 			: _.template( $("script.thanks").html() )
		},

		/*
			Object contains miscellaneous functions as helpers
		*/
		fnMisc = {
			/*
				Format amount using currency symbol
			*/
			fullFormatAmount	: function(amt) {
				return configOptions.currencySymbol+" "+Number(amt).toFixed(2)+" "+configOptions.currency;
			},
			/*
				Format amount using currency symbol & code
			*/
			formatAmount	: function(amt) {
				return configOptions.currencySymbol+" "+Number(amt).toFixed(2);
			},
			/*
				Create url friendly string
			*/
			convertToSlug	: function(string) {
			    return string
			        .toLowerCase()
			        .replace(/ /g,'-')
			        .replace(/[^\w-]+/g,'')
			        ;
			},
			/*
				extract data from URL & convert it to object
			*/
			dataFromURL		: function() {
				return _.object(
					_.compact(
						_.map(location.hash.slice(1).split('/'), function(urlItem) {
						 if (urlItem) {
						 	return urlItem.split("id-");
						 }
						}))
					);
			},
			/*
				Go to top method
			*/
			goToTop			: function(e) {

				if(e) {
					e.preventDefault();
				}

				$("html, body").animate({
		            scrollTop: "0px"
		        }, {
		            duration: 200,
		            easing: "swing"
		        });
			},
			/*
				On resize
			*/
			resizeNPositioin	: function() {

				$('head').append(
					'<style> .bs-2 .modal-body { max-height:'+( $(window).height() * 0.4 )+'px;} </style>'
					);

			 	$domElements.loaderContainer.css({
			 		top: ( $(window).height() * 0.5 ) - ( $domElements.loaderContainer.height() * 0.5 ),
			 		left: ( $(window).width() * 0.5 ) - ( $domElements.loaderContainer.width() * 0.5 )
			 	});
			}
		};

		var storeFuncs = {
			/*
				setup categories
			*/
			setupCategories : function() {

			$domElements.categoriesList.find(".active-category").after(
					_templates.sidebarCategories ( {categoriesCollection:categoriesCollection} )
				);

				storeFuncs.setupStore();
			},

			/*
				Retrive Cart from local storage & update cart
			*/
			loadExistingCartItems 	: function(){

			    var sRetrivedExistingCartCollation = $.jStorage.get(generalVars.cartStorageName),
			    	retrivedExistingCartCollation = $.parseJSON(sRetrivedExistingCartCollation);
			    if( retrivedExistingCartCollation && retrivedExistingCartCollation.length ){
			        cartProductsCollection = retrivedExistingCartCollation;
			    }

				storeFuncs.updateCart();
			    storeFuncs.setupCategories();
			},
			/*
				setup products
			*/
			setupStore 				: function() {

				storeFuncs.onAllComplete();
			},
			/*
				setup products
			*/
			categoryLinkAction 		: function(e) {
				generalVars.preventHashChangedAction = false;
			},
			/*
				load products for current catgeory
			*/
			loadCategoryProducts : function( sCategoryID ) {

				storeFuncs.clearSearchResult( true );

				if(sCategoryID == 'all') {
					currentProductCollection = allProductsCollection;
					storeFuncs.updateBreadCrumb('all');
				} else {
					currentProductCollection = _.filter(allProductsCollection, function(productObj){
					if(productObj.categoryIndex == sCategoryID) {
							return productObj;
						}
					});

					storeFuncs.updateBreadCrumb(categoriesCollection[sCategoryID]);
				}

				//fnMisc.goToTop();

				$domElements.categoriesList.find( 'li' ).removeClass( 'active-category' );
				$domElements.categoriesList.find( '.category-list-item-'+sCategoryID ).addClass('active-category');

				generalVars.lastAccessedCategory = sCategoryID;

				storeFuncs.generateProductsThumbs();

			},
			/*
				List out the products on page
			*/
			generateProductsThumbs 	: function() {

				/*if($domElements.productsContainer.data('masonry'))
				{
					$domElements.productsContainer.masonry( 'destroy' );
				}*/

				$domElements.productsContainer.html(
					_templates.productsGrid( {currentProductCollection:currentProductCollection} )
				);
				dynamicThumbnailHeight();
				$("#productsContainer").eqHeight(".product-item");


				$domElements.storeLoaderStatusText.remove();
				$domElements.loaderContainer.show();

				$domElements.productsContainer.imagesLoaded( function() {
			       $('.product-item').addClass('fade-in');
			       $domElements.loaderContainer.hide();

			    });


			},
			/*
				On serach click
			*/
			onSearch 		: function() {

				clearTimeout(generalVars.searchDelayTimeout);

				/*
					wait for some time if user still typing
				*/
				generalVars.searchDelayTimeout 	= setTimeout(function() {

				if($domElements.searchInput.val() === ""){
					return false;
				}

				$domElements.clearSearchBtn.removeAttr('disabled');

				var oURLData = fnMisc.dataFromURL();

				if(oURLData.hasOwnProperty( 'search' )) {
					if ( generalVars.preventHashChangedAction ) {
						generalVars.preventHashChangedAction = false;
						return false;
					}
					storeFuncs.searchProduct();
				} else {
					location.hash = "#/search";
				}

				}, 300);

			},
			/*
				Clear search result
			*/
			clearSearchResult 	: function(preventSearchResult) {

				$domElements.searchInput.val("");
				$domElements.clearSearchBtn.attr('disabled', '');
				$domElements.searchedProductCounts.html('');

				if(!preventSearchResult)
				{
					storeFuncs.searchProduct();
				}
			},
			/*
				Search for product
			*/
			searchProduct 	: function() {

				$domElements.categoriesList.find('li').removeClass('active-category');

				var sSearchTerm 	= $domElements.searchInput.val(),
					aSeachTerm 		= sSearchTerm.toLowerCase().split(' ');


				searchedProductsCollection = allProductsCollection;
				var tempSearchProductCollection = [];

				for ( var i = 0; i < aSeachTerm.length; i++ ) {

					var sCurrentSearchTermWord = aSeachTerm[i];

					tempSearchProductCollection = [];

					for ( var nProductItem in searchedProductsCollection ) {

						var oProduct = searchedProductsCollection[nProductItem],
							sProductString = oProduct.name.toLowerCase();

							if( configOptions.searchProductDetails ) {
								sProductString += oProduct.details.toLowerCase();
							}


						if ( sProductString.indexOf( sCurrentSearchTermWord ) > -1 ) {
							tempSearchProductCollection.push(oProduct);
						}
					}

					searchedProductsCollection = tempSearchProductCollection;

				}

				generalVars.lastAccessedCategory = 'search';

				if(searchedProductsCollection.length > 1){
					$domElements.searchedProductCounts.html(searchedProductsCollection.length + store_texts.product_found__plural);
				}else if(searchedProductsCollection.length === 1){
					$domElements.searchedProductCounts.html(searchedProductsCollection.length + store_texts.product_found);
				}else{
					$domElements.searchedProductCounts.html(store_texts.product_not_found);
				}

				if( ! _.isEqual(currentProductCollection, searchedProductsCollection) ) {

					currentProductCollection = searchedProductsCollection;
					storeFuncs.generateProductsThumbs();

				}
			},
			/*
				show product details
			*/
			productDetails : function( nProdductIndexID ) {

				oCurrentProductData 	= allProductsCollection[nProdductIndexID];

				if( !oCurrentProductData ) {
					return false;
				}

				selectedProductOptions 	= {
					color 	: ( oCurrentProductData.options.colors && oCurrentProductData.options.colors[0] ) ? oCurrentProductData.options.colors[0].value : null,
					size 	: ( oCurrentProductData.options.sizes && oCurrentProductData.options.sizes[0] ) ? oCurrentProductData.options.sizes[0].value : null
				};

				nProductInCart = storeFuncs.itemExistInCart();


				$domElements.modalContainer.html(
					_templates.productsDetailsModal( {
						oCurrentProductData:oCurrentProductData,
						categoriesCollection:categoriesCollection
					} )
				);
				console.log(oCurrentProductData);

				 storeFuncs.updateAddToCartBtn();
				 storeFuncs.openModal('lg');
			},
			//showThanksModal: function (oOptions) {
			//	alert("Hello, it's me..");
			//	$domElements.modalContainer.html(
			//		_templates.shoppingCartModal( {
			//			cartProductsCollection: cartProductsCollection,
			//			allProductsCollection: allProductsCollection,
			//			configOptions: configOptions,
			//			fnMisc: fnMisc,
			//			generalVars: generalVars,
			//			cartStats: cartStats
			//		} )
			//	);
            //
			//	if(oOptions && oOptions.preventModelReLoad) {
			//		return false;
			//	}
            //
			//	cartProductsCollection = [];
			//	storeFuncs.updateCart();
			//	storeFuncs.openModal();
			//	storeFuncs.updateAddToCartBtn();
            //
			// 	if( !generalVars.isStoreLoaded ) {
			//		storeFuncs.loadCategoryProducts( 'all' );
			//	}
			//}
			showThanksModal: function(){

				storeFuncs.closeAllModals();

				generalVars.preventHashChange = true;

				clearTimeout( generalVars.showSubmitOrderTimeout );
				generalVars.showSubmitOrderTimeout 	= setTimeout( function() {

					$domElements.modalContainer.html(
						_templates.thanksModal
					);
					storeFuncs.openModal();

				}, 500);
			}
			,
			/*
				show shopping cart
			*/
			showShoppingCart : function( oOptions ) {
				$domElements.modalContainer.html(
					_templates.shoppingCartModal( {
						cartProductsCollection: cartProductsCollection,
						allProductsCollection: allProductsCollection,
						configOptions: configOptions,
						fnMisc: fnMisc,
						generalVars: generalVars,
						cartStats: cartStats
					} )
				);

				if(oOptions && oOptions.preventModelReLoad) {
					return false;
				}

				storeFuncs.openModal();

				storeFuncs.updateAddToCartBtn();


			 	if( !generalVars.isStoreLoaded )
				{
					storeFuncs.loadCategoryProducts( 'all' );
				}
			},
			/*
				let the system know that you back from any of the modal functionality
				& it don't need to rearange products of that particuler category
			*/
			backFromModal 			: function() {

				$domElements.mainContainer.removeClass('main-container-addtions');

				if( generalVars.preventHashChange ) {
					generalVars.preventHashChange = false;
					return false;
				}

				generalVars.preventHashChangedAction = true;

				if( generalVars.lastAccessedCategory == 'search' ) {
					location.hash = "#/search";
				} else {
					location.hash = "#/category/uid-"+generalVars.lastAccessedCategory;
				}
			},
			/*
				Update add to cart button based on the existance of that product with selected categories
			*/
			updatedSelectedOption 	: function( e ) {
				e.preventDefault();

				var $this 				= $(this),
				bProductSizeSelector 	= $this.hasClass('product-size-selector'),
				sCurrentOptionSelected 	= $this.find('option:selected').val();

				if(bProductSizeSelector) {
					selectedProductOptions.size = sCurrentOptionSelected;
				} else {
					selectedProductOptions.color = sCurrentOptionSelected;
				}

				return storeFuncs.updateAddToCartBtn();
			},
			/*
				add (or increment product quantity if already in cart) product to cart
			*/
			addToCart 	: function( e ) {
				e.preventDefault();

	            for ( var nCartItem in cartProductsCollection ) {
	            	var oCurrentCartItem = cartProductsCollection[nCartItem];

	                if ( oCurrentCartItem.index == oCurrentProductData.index &&
	                	oCurrentCartItem.size == selectedProductOptions.size &&
	                	oCurrentCartItem.color == selectedProductOptions.color ) {
	                	/*
							Update if already in cart
	                	*/
	                    oCurrentCartItem.qty++;
	            		oCurrentProductData.quantity--;

	                	storeFuncs.updateCart();
	                    return storeFuncs.updateAddToCartBtn();
	                }
	            }

	            /*
					Its not in the cart, lets add it.
	        	*/
	            cartProductsCollection.push({
	            	index 	: oCurrentProductData.index,
	            	size 	: selectedProductOptions.size,
	            	color 	: selectedProductOptions.color,
	            	qty 	: 1
	            });

	            storeFuncs.updateCart();
	            oCurrentProductData.quantity--;
	            storeFuncs.updateAddToCartBtn();

				return storeFuncs.updateAddToCartBtn();
			},
			/*
				Update Shopping cart
			*/
			updateCart 		: function(){

				cartStats.totalItems 	= 0;
				cartStats.subTotal 		= 0;
				/*
					Store cart in storage, so on refresh of page we can get it again
				*/
	            $.jStorage.set( generalVars.cartStorageName, $.toJSON( cartProductsCollection ) );


	            for (var nCartItem in cartProductsCollection) {
	            	var oCurrentCartItem 		= cartProductsCollection[nCartItem],
	            		oCurrentProductItem 	= allProductsCollection[oCurrentCartItem.index];

	            		if( !oCurrentProductItem ) {
	            			cartProductsCollection = [];
	            			break;
	            		}

	                	cartStats.totalItems 	+= oCurrentCartItem.qty;
	                	cartStats.subTotal 	 	+= (oCurrentProductItem.price * oCurrentCartItem.qty);
	            }

	            cartStats.amountFormatted 	= fnMisc.fullFormatAmount(cartStats.subTotal);

	            generalVars.enableOrderBtn = (cartProductsCollection.length > 0 ) ? true : false;

	            $domElements.shoppingCartBtnContainer.html(
					_templates.shoppingCartBtn( {cartStats:cartStats} )
				);

			},
			/*
				Update product qty from the cart
			*/
			updateCartItemQty 		: function() {
				clearTimeout( generalVars.qtyUpdateTimeout );
				var $this 			= $(this),
					nQtyValue 		= Number( $this.val() ),
					nCartRowIndex 	= $this.data('cartrowindex');

					if(nQtyValue < 1) {
						$this.val(1);
						return false;
					}

					generalVars.qtyUpdateTimeout 	= setTimeout(function() {
					cartProductsCollection[nCartRowIndex].qty = nQtyValue;
					storeFuncs.updateCart();

					storeFuncs.showShoppingCart( { preventModelReLoad:true } );
				}, 300);
			},
			/*
				Remove product from cart
			*/
			removeCartItem 		: function(e) {
				var nCartRowIndex 		= $(this).data('cartrowindex');

					cartProductsCollection.splice(nCartRowIndex, 1);
					storeFuncs.updateCart();
					storeFuncs.showShoppingCart( { preventModelReLoad:true } );
			},
			/*
				Update add to cart button to update
			*/
			updateAddToCartBtn 		: function() {
				$domElements.addToCartBtnContainer 	= $('#addToCartBtnContainer');
				nProductInCart = storeFuncs.itemExistInCart();

				$('#addToCartBtnContainer').html(
					_templates.addToCartBtn( { nProductInCart: nProductInCart } )
				);


				// added to cart
				$domElements.itemsInCartContainer 	= $('.confirm-added-to-cart');

				if(nProductInCart>0){
					$('.confirm-added-to-cart').html(
						_templates.itemsInCart({nProductInCart: nProductInCart})
					);
				} else {
					$('.confirm-added-to-cart').html("");
				}
				if(oCurrentProductData.quantity <= 0) {
					return $('#addToCartBtnContainer').html("Prodotto non disponibile");
				}
				return nProductInCart;
			},
			/*
				Check if the product already in cart with selected options
			*/
			itemExistInCart 	: function() {
				for (var nCartItem in cartProductsCollection) {
	            	var oCurrentCartItem = cartProductsCollection[nCartItem];

	                if ( oCurrentCartItem.index == oCurrentProductData.index &&
	                	oCurrentCartItem.size == selectedProductOptions.size &&
	                	oCurrentCartItem.color == selectedProductOptions.color ) {
	                    return oCurrentCartItem.qty;
	                }
	            }

	            return false;
			},
			/*
				Breadcrumb on product Mouseover
			*/
			updateBreadCrumbOnOver 	: function(){
			var nMouseOveredProductIndexID 		= $(this).data('productindex'),
				getMouseOveredProudct 			= allProductsCollection[nMouseOveredProductIndexID],
				getMouseOveredProudctCategory	= categoriesCollection[getMouseOveredProudct.categoryIndex];

				storeFuncs.updateBreadCrumb(getMouseOveredProudctCategory, getMouseOveredProudct);
			},
			/*
				Update product breadcrumb values
			*/
			updateBreadCrumb 	: function(oProudctCategory, oProduct){

				var liAll = '<li><a data-categoryindex="all" href="#/category/uid-all" class="category-link-all category-link">' + store_texts.all_products + '</a></li>';
				var divider = ' <span class="divider">/</span> ';
				if( oProudctCategory == 'all') {
					$domElements.productsBreadcrumb.html(liAll);
				} else {
					var liCategory = '<li><a data-categoryindex="all" href="#/category/uid-'+ oProudctCategory.index + '" class="category-link-'+ oProudctCategory.index + ' category-link">'+ oProudctCategory.name +'</a></li>';
					$domElements.productsBreadcrumb.html(liAll + divider + liCategory);
				}
			},
			/*
				Go to submit order form
			*/
			proceedToOrderByEmail 	: function(e) {
				e.preventDefault();

				generalVars.preventHashChange = true;

				if(!generalVars.enableOrderBtn) {
					return false;
				} else {
					storeFuncs.closeAllModals();

					clearTimeout(generalVars.showSubmitOrderTimeout);
					generalVars.showSubmitOrderTimeout = setTimeout(function() {

						$domElements.modalContainer.html(
							_templates.submitOrderFormModal
						);

						storeFuncs.openModal();

						$('#submitOrderForm').validate();
						$('.required').on('keyup change', storeFuncs.validateSubmitOrderForm);

					}, 500);
				}
			},
			openTermsModal: function(){

				storeFuncs.closeAllModals();

				generalVars.preventHashChange = true;

				clearTimeout( generalVars.showSubmitOrderTimeout );
				generalVars.showSubmitOrderTimeout 	= setTimeout( function() {

					$domElements.modalContainer.html(
						_templates.termsModal
					);
					storeFuncs.openModal();

				}, 500);
			},
			/*
				Submit Order
			*/
			submitOrder 			: function(e) {
				e.preventDefault();

				if( !generalVars.enableOrderBtn ) {
					return false;
				} else if( storeFuncs.validateSubmitOrderForm() ) {

					if(generalVars.isDemoActivate) {
						return storeFuncs.onOrderSubmitted( {
							adminMailSent:1,
							customerMailSent:1
						} );
					}

					generalVars.enableOrderBtn = false;

					var submitOrderURL = configOptions.submitOrderBaseURL +
						'&cartLength=' + (cartProductsCollection.length + 1) + '&storeID=' + storeID;


					$('.submittingOrder').show();
					$.ajax({
						url: submitOrderURL,
						type: 'POST',
						dataType: 'JSON',
						contentType:"application/json",
						data: storeFuncs.generatePostBody()
					})
					.done(function(returnData) {

						storeFuncs.onOrderSubmitted( returnData );
						$('.submittingOrder').hide();

					})
					.fail(function() {

						generalVars.enableOrderBtn = false;
						$('.order-page-body').html("Order Submission failed, try again");
					});

				}

			},

			generatePostBody: function () {
				var body = {};
					body.customer = $('#submitOrderForm').serializeObject();
					body.cart = [];
				for(var i = 0; i < cartProductsCollection.length; i++) {
					var currentCartItem = cartProductsCollection[i],
						currentProductData = allProductsCollection[currentCartItem.index],
						cartItem = {
							name: currentProductData.name,
							size: (currentCartItem.size && (currentCartItem.size != 'NA')) ? (currentCartItem.size) : '',
							color: (currentCartItem.color && (currentCartItem.color != 'NA')) ? (currentCartItem.color) : '',
							productID: currentProductData.id,
							price: currentProductData.price,
							quantity: currentCartItem.qty
						};
					body.cart.push(cartItem);
				}
				return JSON.stringify(body);
			},

			onOrderSubmitted : function( oReturnData ) {

				storeFuncs.closeAllModals();
				setTimeout( function() {
					window.location.replace("/store/thanks");
				}, 500);

				// if( oReturnData.adminMailSent ) {

				// 			$('.order-page-header').html("Your Order has been placed.");
				// 		}

				// if( oReturnData.customerMailSent ) {

				// 	$('.order-page-body').html("Thank you for your Order, <br /> Order details has been sent to your email.");
				// }

				// $('#backToCartBtn, #submitOrderBtn').hide();
				// $('.order-page-close-btn').show();

				// cartProductsCollection = [];
				// storeFuncs.updateCart();
			},
			/*
				Check if the form is Validated or not
			*/
			validateSubmitOrderForm 	: function() {

				var isSubmitFormValid = $('#submitOrderForm').valid();

				if( isSubmitFormValid ) {
					$('#submitOrderBtn').removeAttr('disabled');
				} else {
					$('#submitOrderBtn').attr('disabled', 'disabled');
				}

				return isSubmitFormValid;

			},
			/*
				User back from Order submit form Modal to Cart
			*/
			backToCartFromSubmitForm 	: function(e) {

				e.preventDefault();
				storeFuncs.closeAllModals();

				generalVars.preventHashChange = true;

				clearTimeout( generalVars.showSubmitOrderTimeout );
				generalVars.showSubmitOrderTimeout 	= setTimeout( function() {

					storeFuncs.showShoppingCart( { preventModelReLoad:true } );
					storeFuncs.openModal();

				}, 500);
			},
			/*
				PayPal Checkout
			*/
			paypalCheckout 	: function( e ){

	    		e.preventDefault();

	    		if(!generalVars.enableOrderBtn) {
	    			return false;
	    		}

	    		var paypalCheckoutURL = configOptions.paypalBaseURL+storeFuncs.cartProductsURLGeneration();

		        window.location.replace( paypalCheckoutURL );
			    return true;

			 },
			/*
				Generating URL to send products to mailer script or PayPal
			*/
			cartProductsURLGeneration : function() {

			    var itemsCartIndex = 1,
			    	sCartProductsURL = "";

		        for ( var nCartItem in cartProductsCollection ) {
	            	var oCurrentCartItem 	= cartProductsCollection[nCartItem],
	            		oCurrentProductData = allProductsCollection[oCurrentCartItem.index];

	                sCartProductsURL += '&item_name_'+itemsCartIndex + '=' + oCurrentProductData.name;
	                sCartProductsURL += (oCurrentCartItem.size && (oCurrentCartItem.size != 'NA')) ? ( ' Size: ' + oCurrentCartItem.size ) : '';
	                sCartProductsURL += (oCurrentCartItem.color && (oCurrentCartItem.color != 'NA')) ? ( ' Color: '+ oCurrentCartItem.color ) : '';
		            sCartProductsURL += '&item_number_'+itemsCartIndex + '=' + oCurrentProductData.id;
		            sCartProductsURL += '&amount_'+itemsCartIndex + '=' + oCurrentProductData.price;
		            sCartProductsURL += '&quantity_'+itemsCartIndex+'=' + oCurrentCartItem.qty;

		            itemsCartIndex ++;
	            }

		        return sCartProductsURL;
			},
			/*
				Close all opened Modals
			*/
			closeAllModals 		: function(){
				$domElements.modalCommon.modal('hide');
				$('.modal-backdrop').remove();

			},
			/*
				Open Modal
			*/
			openModal 		: function(size){
				storeFuncs.closeAllModals();

				if(size == "lg"){
					$('.modal-dialog').addClass("modal-lg");
				}else{
					$('.modal-dialog').removeClass("modal-lg");
				}

				$domElements.modalCommon.modal();
			},
			/*
				Load category based on hash value
			*/
			categoryCalled 		: function( oGetURLData ) {

					if( !oGetURLData.u ) {
						oGetURLData.u = 'all';
					}

					oGetURLData.u = ( categoriesCollection[oGetURLData.u] ) ? oGetURLData.u : 'all';

				 	storeFuncs.loadCategoryProducts( oGetURLData.u );
			},
			/*
				Load product details based on hash value
			*/
			productCalled 		: function( oGetURLData ) {

				if( oGetURLData.u ) {
					storeFuncs.productDetails( oGetURLData.u );

					$domElements.mainContainer.addClass('main-container-addtions');

					if( !allProductsCollection[oGetURLData.u] ) {

						storeFuncs.loadCategoryProducts( 'all' );
						return false;
					}

					var nCategoryIndex = allProductsCollection[oGetURLData.u].categoryIndex;

					if( !generalVars.isStoreLoaded )
					{
						storeFuncs.loadCategoryProducts( nCategoryIndex );
					}

				} else {

				 storeFuncs.loadCategoryProducts( 'all' );
				}
			},

			onAllComplete 		: function() {

				storeFuncs.closeAllModals();

				var oURLData = fnMisc.dataFromURL();

				if( oURLData.hasOwnProperty( 'category' ) ) {

					if ( generalVars.preventHashChangedAction ) {
						generalVars.preventHashChangedAction = false;
						return false;
					}

					storeFuncs.categoryCalled( oURLData );

				} else if( oURLData.hasOwnProperty( 'search' ) ) {

					if ( generalVars.preventHashChangedAction ) {
						generalVars.preventHashChangedAction = false;
						return false;
					}

					storeFuncs.searchProduct();

				} else if( oURLData.hasOwnProperty( 'product' ) ) {

					storeFuncs.productCalled( oURLData );

				} else if( oURLData.hasOwnProperty( 'shopping-cart' ) ) {

					storeFuncs.showShoppingCart();

					/*if(oURLData.u == 'show') {

					 storeFuncs.showShoppingCart();
					 }*/
				}else if( oURLData.hasOwnProperty( 'terms' ) ) {
					storeFuncs.loadCategoryProducts( 'all' );
					storeFuncs.openTermsModal();

				} else if(oURLData.hasOwnProperty( 'thanks' )){
					cartProductsCollection = [];
					storeFuncs.updateCart();
					setTimeout( function() {
						storeFuncs.loadCategoryProducts( 'all' );
						storeFuncs.showThanksModal();
					}, 500);
				} else {
					storeFuncs.loadCategoryProducts( 'all' );
				}

				if( !generalVars.isStoreLoaded )
				{
					generalVars.isStoreLoaded = true;
				}
			}
		};

		fnMisc.resizeNPositioin();
		$domElements.storeLoaderStatusText.html('Loading configurations...');

		/*
			Load Config Data from JSON
		*/
		setStoreConfiguration(storeData);
		function setStoreConfiguration(configData) {

			$domElements.storeLoaderStatusText.html('Configuration loaded...');

			var configuration = _.get(configData, "configuration", {});

			/*
				logo image
			*/
			configOptions.logoImage = _.get(configuration, 'logoImage'); //

			configOptions.storeName = _.get(configuration, 'storeName') === "" ? configOptions.storeName : _.get(configuration, 'storeName');

			$domElements.footerStoreName.html(configOptions.storeName);

			/*
				currency Symbol
			*/
			if(_.has(configuration, 'currencySymbol')) {
				configOptions.currencySymbol = _.get(configuration, 'currencySymbol', "€");
			}
			/*
				Currency
			*/
			if(_.has(configuration, 'currency')) {
				configOptions.currency = _.get(configuration, 'currency', "EUR");
			}

			/*
				Business Email
			*/
			if(_.has(configuration, 'businessEmail')) {
				configOptions.businessEmail = _.get(configuration, 'businessEmail');
			}

			/*
				Check if allows PayPal
			*/
			if(_.has(configuration, 'UsePaypal')) {
				configOptions.usePaypal = _.get(configuration, 'UsePaypal');
			}

			/*
				Check if allows Submit Order by Email
			*/
			if(_.has(configuration, 'UseSubmitOrder')) {
				configOptions.useSubmitOrderByEmail = _.get(configuration, 'UseSubmitOrder');
			}

			/*
				Fixed Shipping Charges
			*/
			if(_.has(configuration, 'ShippingCharges')) {
				configOptions.shippingCharges = parseFloat(_.get(configuration, 'ShippingCharges'));
			}

			/*
				Set logo
			*/
			$domElements.storeLogo.attr('src', configOptions.logoImage);
			/*
				Disable PayPal Checkout button
			*/
			if(!configOptions.usePaypal) {
				$domElements.checkoutWithPaypalBtn.hide();
			}

			/*
				Disable Checkout button
			*/
			if(!configOptions.useSubmitOrderByEmail) {
				$domElements.checkoutSubmitOrderBtn.hide();
			}

			/*
				basic urls for PayPal & submit order
			*/
			configOptions.paypalBaseURL = configOptions.paypalBaseURL + configOptions.currency + '&business='+ configOptions.businessEmail + '&handling_cart=' + configOptions.shippingCharges + configOptions.paypalNotifyURL + configOptions.paypalReturnURL;


			configOptions.submitOrderBaseURL = configOptions.submitOrderBaseURL + configOptions.currency + '&business='+ configOptions.businessEmail + '&handling_cart=' + configOptions.shippingCharges;

			/*
				Update Status
			*/
			$domElements.storeLoaderStatusText.html('Loading products data...');

			/*
				Lets load products data from XML file
			*/
			setStoreProducts(storeData);
		    function setStoreProducts(productsData) {

				var nCategoryIndex = 0,
					nProdductIndex = 0,
					products = productsData.products || [],
					categories = _.get(products, 'category', []);

				$('#mainContainer').show();
				$domElements.storeLoaderStatusText.html('Intializating ...');

				/*
					loop through the categories
		    	*/
				_.forEach(categories, function(category) {

					var categoryName = _.get(category, 'categoryName'),
						categoryID = _.get(category, 'categoryID') ? fnMisc.convertToSlug(category.categoryID) : nCategoryIndex,
						products = _.get(category, 'products', []);

					categoriesCollection[categoryID] = {
						name: categoryName,
						index: categoryID,
						slug: fnMisc.convertToSlug(categoryName),
						products_length: _.get(category, 'products', []).length
					};

					/*
						loop through the products of this category
		        	*/
					_.forEach(products, function(product) {

						var oldPrice = _.get(product, 'oldPrice'),
							productName = _.get(product, 'productName'),
							productID = _.get(product, 'productID') ? fnMisc.convertToSlug(product.productID) : nProdductIndex,
							productPrice = _.get(product, 'productPrice');

						/*
							Products
		        		*/
						var oThisProduct = allProductsCollection[productID] = {
							name: productName,
							slug: fnMisc.convertToSlug(productName),
							thumbPath: _.get(product, 'thumbPath'),
							price: _.get(product, 'productPrice'),
							quantity: _.get(product, 'quantity', 0),
							formattedPrice: fnMisc.formatAmount(productPrice),
							fullFormattedPrice: fnMisc.fullFormatAmount(productPrice),
							oldPrice: oldPrice ? {
								fullFormatted: fnMisc.fullFormatAmount(oldPrice),
								formatted: fnMisc.formatAmount(oldPrice),
								price: oldPrice
							} : null,
							id: _.get(product, 'productID'),
							index: productID,
							categoryIndex: categoryID,
							categoryName: categoryName,
							details: _.get(product, 'details'),
							options: {
								sizes: [],
								colors: [],
								extraOptions: {}
							}
						};

						/*
							product size options
		        		*/
						_.forEach(_.get(product, 'sizes'), function(size) {

							oThisProduct.options.sizes.push({
								name: size,
								value: (size.value ? size.value : size)
							});
						});

						/*
							product color options
		        		*/
						_.forEach(_.get(product, 'colors'), function(color) {

							oThisProduct.options.colors.push({
								name: color,
								value: color
							});
						});

						/*
							increment product index
		        		*/
						nProdductIndex++;
					});

					/*
						increment category index
	        		*/
					nCategoryIndex++;
				});
				/*
					we have all the data lets setup a store
				*/
				storeFuncs.loadExistingCartItems();

	        }

		}

		$(window).on('hashchange', function() {
		  generalVars.hashChanged = true;
		  storeFuncs.onAllComplete();
		});

		$(window).on( 'resize', fnMisc.resizeNPositioin );

		$domElements.categoriesList.on( 'click', '.category-link', storeFuncs.categoryLinkAction );

		$domElements.modalContainer.on( 'click', '.add-to-cart-btn', storeFuncs.addToCart );

		$domElements.modalContainer.on( 'click', '.product-img', function() {
			$( this ).toggleClass( "open" );
		} );

		$domElements.searchInput.on( 'keyup', storeFuncs.onSearch );

		$domElements.clearSearchBtn.on( 'click',
			function(){
				storeFuncs.clearSearchResult( false );
			});

		$domElements.modalContainer.on( 'change',
			'.option-selector', storeFuncs.updatedSelectedOption );

		$domElements.modalContainer.on( 'keyup change',
			'input.cart-product-qty', storeFuncs.updateCartItemQty );

		$domElements.modalContainer.on( 'click',
			'.delete-product-from-cart', storeFuncs.removeCartItem );

		$domElements.modalContainer.on( 'click',
			'#checkoutWithPaypalBtn', storeFuncs.paypalCheckout );

		$domElements.modalContainer.on( 'click',
			'#checkoutSubmitOrderBtn', storeFuncs.proceedToOrderByEmail );

		$domElements.modalContainer.on('click',
			'#submitOrderBtn', storeFuncs.submitOrder );

		$domElements.modalContainer.on( 'click',
			'#backToCartBtn', storeFuncs.backToCartFromSubmitForm );

		$domElements.modalContainer.on( 'click',
			'.go-to-cart', storeFuncs.backToCartFromSubmitForm );

		$domElements.modalContainer.on( 'click',
			'#open-terms', storeFuncs.openTermsModal );

		$domElements.goToTop.on( 'click', fnMisc.goToTop);

		//$domElements.productsContainer.on( 'mouseover',
		//	'.product-item', storeFuncs.updateBreadCrumbOnOver );

		$domElements.modalCommon.on( 'hidden hidden.bs.modal', storeFuncs.backFromModal );
	});
})(jQuery);
