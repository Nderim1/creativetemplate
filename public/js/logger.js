window.onerror = function(message, file, line, col, error) {
	reportError(message, file, line, col, error);
};

function reportError (message, file, line, col, error) {
	var xhr;

	if (window.XMLHttpRequest) {
		// code for IE7+, Firefox, Chrome, Opera, Safari
		xhr = new XMLHttpRequest();
	} else {
		// code for IE6, IE5
		xhr = new ActiveXObject("Microsoft.XMLHTTP");
	}

	xhr.open("POST", "/api/report-violation", true);
	xhr.setRequestHeader("Content-Type", "application/json; charset=utf-8");
	var stack = error ? error.stack : undefined;
	if(message === "Script error.") {
		message = "Same-origin policy error.";
	}
	xhr.send(JSON.stringify({"error-report": { "message": message, "file": file, "line": line, "column": col, "stack": stack}}));
}