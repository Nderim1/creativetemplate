angular.module('majeeko.main', ['main','page-preview-generator'])



$(document).ready(function(){
	function resizeBanner () {
		if($(window).width() >= 992) {
			$(".window-height").css('height',$(window).height());
		} else {
			$(".window-height").css('height','auto');
		}
	}
	resizeBanner()
	$(window).resize(resizeBanner);
});
