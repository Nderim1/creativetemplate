

angular.module('main', [
	'ui.bootstrap'
  , 'contacts'
  , 'ngCookies'
  , 'rx'
])

.service('cookiesService', function (
	$cookies
) {
	this.cookiesAccepted = $cookies.get('cookies_accepted')
	this.acceptCookies = function () {
		$cookies.put('cookies_accepted', true, { expires: moment().add(1,'year').toISOString() })
	}
})

.controller('pageCtrl', function (
	$scope,
	$modal,
	$cookies,
  	$timeout,
  	$window,
  	rx,
  	cookiesService
) {
	$scope.cookiesAccepted = cookiesService.cookiesAccepted
	$scope.acceptCookies = function () {
		cookiesService.acceptCookies()
		$scope.cookiesAccepted = true
	}
	
	if(!$scope.cookiesAccepted) {
		rx.Observable.fromEvent(angular.element($window), 'scroll')
		.take(1)
		.subscribe(function() {
			$timeout($scope.acceptCookies)
		})
	}
	
	$scope.openContactsModal = function() {
		console.log('contacts')
		$modal.open({
			templateUrl: 'main-contacts.tpl'
		  , controller: 'contactsCtrl'
		  , size: 'lg'
		})
	}
})

.directive("disableNgAnimate", function ($animate) {
	return {
		restrict: 'AC',
		link: function (scope, element) {
	        $animate.enabled(false, element);
	    }
	}
})
