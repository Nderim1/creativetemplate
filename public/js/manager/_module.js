angular.module('manager', [
	'ngAnimate'
	, 'ui.bootstrap'
])

.config(function($animateProvider) { $animateProvider.classNameFilter(/maj-animate/); })

.filter('sortPages', function() {
	var statusOrder = ['pro', 'paid', 'synced', 'unsynced'];
	var sortFunction = function(a,b) {
		if(a.isPro){a.status = "pro";}
		if(b.isPro){b.status = "pro";}
		var orderA = _.indexOf(statusOrder, a.status)
		  , orderB = _.indexOf(statusOrder, b.status);
		if(orderA < orderB) return -1
		else if(orderA > orderB) return 1
		else if(a.name < b.name) return -1
		else if(a.name > b.name) return 1
		else return 0
	}
	return function (input) {
		if(!input) return false
		return input.sort(sortFunction)
	}
})

.controller('managerCtrl', function(
	$scope
  , $http
  , $timeout
  , $filter
  , Pages
  , $modal
  , $sce
) {
	$scope.filter = 'all';
	$scope.loading = true;

	var loadPages = function() {
		Pages
			.load()
			.then(function(pages) {
				if($scope.pages) {
					pages = pages.filter(function(page) {
						return !_.contains($scope.pages.map(function(_page) {
							return _page.facebook_id
						}), page.facebook_id)
					})
				} else {
					pages = $filter('sortPages')(pages)
				}
				console.debug(pages)
				$scope.pages = ($scope.pages || []).concat(pages);
				$scope.freePages = $scope.pages.filter(function(page) { return !page.paid_on }).length;
				$scope.paidPages = $scope.pages.filter(function(page) { return page.paid_on && !page.isPro}).length;
				$scope.proPages = $scope.pages.filter(function(page) { return page.isPro }).length;
				$scope.loading = false;
			}, function(err) {
				$timeout(loadPages, 30000)
			})
	}

	loadPages();

	$scope.sync = function (account) {
		account.loading = true
		console.debug(account.facebook_id,account.username);
		$http.post('/api/sync', { id: account.facebook_id }).then(function(res) {
			//console.debug(res.data)
			delete account.loading
			if(res.data.message === 'done') {
				_.extend(_.findWhere($scope.pages, {facebook_id: account.facebook_id}),res.data.page);
				_.findWhere($scope.pages, {facebook_id: account.facebook_id}).generated_for_preview=false;
				console.log($scope.pages);
			}
		})
	}

	$scope.viewWidget = function(link){
		// window.open(link,'Analytics','scrollbars=no,resizable=yes, width=600,height=600,status=no,location=no,toolbar=no,directories=0');
		$scope.activeUrl = $sce.trustAsResourceUrl(link);
		$modal.open({
			templateUrl:'analytics_modal',
			scope:$scope
		})


	}
})

.filter('getAccountStatus', function() {
	return function(account) {
		if(account.isPro){
			return 'pro';
		}
		return account.paid_on ? 'synced' : 'pending';
	}
})

.filter('filterPageByStatus', function($log) {
	return function(accounts, filter) {
		if(!accounts) return false;
		return accounts.filter(function(account) {
			return filter == 'all' || (account.paid_on && !account.isPro && filter == 'synced') || (account.isPro && filter == 'pro') || (!account.paid_on && filter == 'pending');
		})
	}
})

.service('Pages', function($q, $http, $timeout) {
	this.load = function() {
		var attemts = 0;
		var deferred = $q.defer();

		var __loadPages = function() {
			attemts++;
			$http
				.get('/api/me/pages')
				.then(function(res) {
					deferred.resolve(res.data);
				}, function(err) {
					if(attemts < 10) {
						$timeout(__loadPages, 1000);
					} else {
						deferred.reject();
					}
				})
		}

		__loadPages();
		return deferred.promise;
	}
})
