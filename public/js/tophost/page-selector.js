angular.module('tophost.page_selector', [
	'ngAnimate'
])

.config(function($animateProvider) { $animateProvider.classNameFilter(/maj-animate/); })

.controller('pageSelectorCtrl', function(
	$scope
  , $http
  , $timeout
  , $filter
  , $window
  , Pages
) {
	$scope.loading = true;

	var loadPages = function() {
		Pages
			.load()
			.then(function(pages) {
				if($scope.pages) {
					pages = pages.filter(function(page) {
						return !_.contains($scope.pages.map(function(_page) {
							return _page.facebook_id
						}), page.facebook_id)
					})
				}
				console.debug(pages)
				$scope.pages = ($scope.pages || []).concat(pages);
				$scope.loading = false;
			}, function(err) {
				$timeout(loadPages, 30000)
			})
	}

	loadPages();

	$scope.bindPage = function (account) {
		$scope.binding = true
		$http.post('/api/bind-page-to-domain', { id: account.facebook_id }).then(function(res) {
			console.debug(res.data)
			if(res.status == 200 || res.status == 403) {
				$window.location = '/'
			} else {
				$scope.binding = false
			}
		})
	}
})
