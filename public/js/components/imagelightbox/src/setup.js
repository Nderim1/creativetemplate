jQuery(document).ready(function ($) {
	initLightbox()
});
var lightboxInstance;

function initLightbox() {
	$("[data-toggle=tooltip]").tooltip();
	$("[data-toggle=popover]").popover();
	$('.dropdown').on('show.bs.dropdown', function () {
		$(this).find('.dropdown-menu').slideDown(250);
	});
	$('.dropdown').on('hide.bs.dropdown', function () {
		$('.dropdown-menu').slideUp();
	});
	$('.carousel').carousel();
	$('.carousel .carousel-control.left').click(function (e) {
		$(this).parent().carousel('prev');
		e.stopImmediatePropagation();
		e.preventDefault();
	});
	$('.carousel .carousel-control.right').click(function (e) {
		$(this).parent().carousel('next');
		e.stopImmediatePropagation();
		e.preventDefault();
	});
	$('.carousel').on('slide.bs.carousel', function () {
		$('.carousel-caption').animate({'opacity': '0'}, 300);
	});
	$('.carousel').on('slid.bs.carousel', function () {
		$('.carousel-caption').animate({'opacity': '1'}, 300);
	});

	//// ImageLightbox

	var arrowsOn = function (instance, selector) {
		var $arrows = $('<button type="button" class="imagelightbox-arrow imagelightbox-arrow-left"><i class="fa fa-chevron-left"></i></button><button type="button" class="imagelightbox-arrow imagelightbox-arrow-right"><i class="fa fa-chevron-right"></i></button>');

		$arrows.appendTo('body');
		$('.imagelightbox-arrow').show();

		$arrows.on('click touchend', function (e) {
			e.preventDefault();

			var $this = $(this),
				$target = $(selector + '[href="' + $('#imagelightbox').attr('src') + '"]'),
				index = $target.index(selector);

			if ($this.hasClass('imagelightbox-arrow-left')) {
				index = index - 1;
				if (!$(selector).eq(index).length)
					index = $(selector).length;
			} else {
				index = index + 1;
				if (!$(selector).eq(index).length)
					index = 0;
			}

			instance.switchImageLightbox(index);
			return false;
		});
	};
	var arrowsOff = function () {
		$('.imagelightbox-arrow').remove();
	};

	var captionOn = function(instance)
	{
		var target = $('[href="' + $( '#imagelightbox' ).attr( 'src' ) + '"]' );

		if(! target){
			return false;
		}

		var description = target.attr( 'data-caption' );

		if( !description || ! (description.length > 0) ){
			return false;
		}

		var descriptionMini = description.substring(0,200);
		var descriptionFull = description.replace(/(?:\r\n|\r|\n)/g, '<br />');

		var captionBox = $('<div id="caption_box_container"></div>');
		var captionTexts = $('<div id="caption_texts_container"></div>');
		var captionOpen = $('<div class="toggle-icon"><a href="#" onclick="toggleOpen()"><i class="fa fa-plus"></i></a></div>');
		//var captionClose = $('<div class="close-icon showOpen"><a href="#" onclick="toggleOpen()"><i class="fa fa-times"></i></a></div>');
		var captionMini = $('<div id="caption_text_mini" class="showClosed"><div class="truncate">'+ descriptionMini +'</div></div>');
		var captionFull = $('<div id="caption_text_full" class="showOpen">'+ descriptionFull +'</div>');

		captionTexts
			.append(captionOpen)
			//.append(captionClose)
			.append(captionMini)
			.append(captionFull);

		var elementImgLightbox = $('#imagelightbox').detach();
		captionBox.append(elementImgLightbox);
		captionBox.append(captionTexts);
		captionBox.appendTo("body");

		instance.captionObj = {};
		instance.captionObj.imageSelector = '#imagelightbox';

		initCaption(instance.captionObj);
		$(window).on('resizedLightboxImg', function(){
			setSize(instance.captionObj);
		});
	};
	var captionOff = function(instance){
		$('#caption_box_container').fadeOut('fast',function(){
			$('#caption_box_container').remove();
		});
		instance.captionObj = {};
	};


	lightboxInstance = $(".imagelightbox").imageLightbox({
		allowedTypes: 'png|jpg|jpeg||gif',    // string;
		animationSpeed: 250,
		preloadNext: true,
		onLoadStart: function(){
			captionOff(lightboxInstance);
		},
		onLoadEnd: function(){
			captionOn(lightboxInstance);
		},
		onStart: function () {
			$('<div id="imagelightbox-overlay"></div>').appendTo('body');  // overlay
			$('<button type="button" id="imagelightbox-close" title="Close"></button>').appendTo('body').on('click touchend', function () {
				$(this).remove();
				lightboxInstance.quitImageLightbox();
				return false;
			});  // close button
			if ($(".imagelightbox").length > 1) arrowsOn(lightboxInstance, ".imagelightbox");
		},
		onEnd: function () {
			captionOff(lightboxInstance);
			$('#imagelightbox-overlay').remove(); // overlay off
			$('#imagelightbox-close').remove(); // close button off
			$('#imagelightbox-caption').remove(); // caption off
			arrowsOff();
		}
	});

}


function toggleOpen(){
	if(lightboxInstance.captionObj.caption_open){
		setBase(lightboxInstance.captionObj);
	}else{
		setOpen(lightboxInstance.captionObj);
	}
}

function initCaption(captionObj){
	setSize(captionObj);
	$("#caption_texts_container").animate({opacity:1},400);
	setSize(captionObj);
}

function setMargin(captionObj){
	if(!captionObj.caption_open){
		captionObj.textsMarginTop = $("#caption_box_container").outerHeight() - $("#caption_text_mini").outerHeight();
	}else{
		captionObj.textsMarginTop = Math.max(0, $("#caption_box_container").outerHeight() - $("#caption_text_full").outerHeight());
	}
}

function setSize(captionObj){
	$("#caption_box_container")
		.height($(captionObj.imageSelector).height())
		.width($(captionObj.imageSelector).width())
		.css("top", $(captionObj.imageSelector).css("top"))
		.css("left", $(captionObj.imageSelector).css("left"));

	if($("#caption_text_full").outerHeight() > $("#caption_texts_container").outerHeight()){
		$("#caption_text_full").css("height", $("#caption_texts_container").outerHeight());
	}

	setMargin(captionObj);

	$("#caption_texts_container")
		.height($(captionObj.imageSelector).height())
		.css("marginTop", captionObj.textsMarginTop);
}

function setBase(captionObj){
	captionObj.caption_open = false;
	setMargin(captionObj);

	$("#caption_texts_container").removeClass("open");
	$("#caption_texts_container").animate({marginTop: captionObj.textsMarginTop}, 400);
}

function setOpen(captionObj){
	captionObj.caption_open = true;
	setMargin(captionObj);

	$("#caption_texts_container").animate({marginTop: captionObj.textsMarginTop}, 400);
	$("#caption_texts_container").addClass("open");
}
