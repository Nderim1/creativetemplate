var editorCtrlFactory = function(isTophost) {
	return function(
		$scope,
		$rootScope,
		$http,
		pageData,
		pageService,
		$HideShowPost,
		$modal,
		messagesText,
		userData,
		$timeout,
		getEmbeddedSiteUrl
	) {

		$scope.isSaving = true;

		$scope.siteUrl = getEmbeddedSiteUrl.getValidUrl();
		$rootScope.$on('pageSaved', function(evt, res) {
			$scope.selectedProfilePicture = $scope.pageData.profile_pictures.filter(function(elem) {
				return elem.id === $scope.pageData.configuration.graphics.logoId;
			})[0];
			refreshPreview();
		});

		$frame = angular.element('#app_preview')[0];

		$frame.addEventListener('load', function() {
			console.log("loaded");
			$scope.$apply(function() {
				$scope.isSaving = false;
			});
		});

		$rootScope.$on('saving', function(evt, res) {
			$scope.isSaving = true;
		});

		$rootScope.$on('pageResetted', function(evt, res) {
			refreshEditor();
		});

		$rootScope.hasStore = function hasStore() {
			return !!_.find(pageData.configuration.contents.extra_pages, function(item) {
				return item.type == "store";
			});
		};

		$scope.syncing = false;
		var sideBarWidth = $('.sidebar').width();
		var editorPanelWidth = $('.editor-panel').width();
		var editorNavbarHeight = $('#editor-navbar-top').height();
		var windowWidth = $(window).width();
		var windowHeight = $(window).height();
		$scope.save = pageService.save
		$scope.sendPayload = pageService.sendPayload
		$scope.default = pageService.default
		$scope.reset = pageService.reset
		$scope.openEditorPanel = true;
		$scope.currentEditorPanel = "menu";
		$scope.publish = function() {
			var returned = pageService.publish();
		}

		$rootScope.toggleEditorPanel = function(panel, goingBack) {
			$(document).ready(function() {
				if (panel == 'buy') {
					$modal.open({
						templateUrl: 'purchase-modal.tpl',
						size: "lg",
						$scope: $rootScope
					});
					trackEventModal('open_buy');
				} else if (panel == 'panel') {
					if (!$scope.openEditorPanel) { // opens the sidebar and shrink the topbar
						$scope.openEditorPanel = true;
						$scope.initEditorPanel();
						trackEventSidebar(true);
					} else { // close the sidebar and set the topbar to fullwidth
						$scope.initEditorPanel();
						$scope.openEditorPanel = false;
						trackEventSidebar(false);
					}
				} else {
					$timeout(function() {
						previousPanel = $scope.currentEditorPanel ? $scope.currentEditorPanel : "menu";
						$scope.currentEditorPanel = ($scope.currentEditorPanel == panel) ? ($scope.currentEditorPanel == 'menu') ? null : 'menu' : panel;

						if (goingBack) {
							$("#editor-nav-sidebar-" + previousPanel).removeClass("selected-sub-panel").removeClass("to-left");
						} else {
							$("#editor-nav-sidebar-" + previousPanel).removeClass("selected-sub-panel").addClass("to-left");
						}
						$("#editor-nav-sidebar-" + panel).addClass("selected-sub-panel").removeClass("to-left");
						trackEventMenuPanel(panel);
					}, 0)
				};

			})
		};

		$scope.initEditorPanel = function(panel) {
			$scope.currentEditorPanel = panel ? panel : "menu";
			$(".panel-step").removeClass("selected-sub-panel").removeClass("to-left");
			$("#editor-nav-sidebar-" + $scope.currentEditorPanel).addClass("selected-sub-panel");
		};


		var refreshPreview = function() {
			$frame.contentWindow.location = $frame.contentWindow.location.href;
		};

		var refreshEditor = function() {
			window.location.reload();
		};

		$scope.sync = function(facebookId) {
			$scope.syncing = true;
			$scope.isSaving = true;
			$http.post('/api/sync', {
				id: facebookId
			})
			.then(function(res) {
				console.debug(res.data);
				$scope.syncing = false;
				if (res.data.message === 'done') {
					refreshPreview();
					if (!_.get(pageData, "paid")) {
						$modal.open({
							templateUrl: 'sync-done-buy.tpl',
							size: "sm",
							scope: $scope
						});
					}
				}
			});
			trackEventSync();
		};

		$scope.showHiddenPosts = function() {
			$modal.open({
				templateUrl: 'show-hidden-modal.tpl',
				scope: $scope
			});
			trackEventModal('open_hidden_post');
		};
		$scope.showStoreAdmin = function() {
			$modal.open({
				templateUrl: 'store-admin-modal.tpl',
				scope: $rootScope,
				windowClass: "fullscreen-modal"
			});
		};

		$scope.saveHiddenPosts = function($event) {
			console.log('save hidden posts');
			console.log($scope.hiddenData);
			var keys = Object.keys($scope.hiddenData);
			var data = {};
			keys.forEach(function(elem) {
				data[elem] = $scope.hiddenData[elem].filter(function(data) {
						return data.show === true;
					})
					.map(function(elem) {
						return elem.id;
					});
			});
			$HideShowPost.hidePost(data).then(function(obj) {
					refreshPreview();
					return $HideShowPost.getHiddenData();
				})
				.then(function(data) {
					$scope.hiddenData = data;
					console.log(data);
					if (($scope.hiddenData.hasOwnProperty('news') && $scope.hiddenData.news.length > 0) || ($scope.hiddenData.hasOwnProperty('event') && $scope.hiddenData.event.length > 0) || ($scope.hiddenData.hasOwnProperty('album') && $scope.hiddenData.album.length > 0))
						$scope.showHiddenPostsModal = true;
					else
						$scope.showHiddenPostsModal = false;
				})
				.catch(function(err) {
					console.log(err);
				});
		};

		$HideShowPost.getHiddenData().then(function(obj) {
			$scope.hiddenData = obj;
			if (($scope.hiddenData.hasOwnProperty('news') && $scope.hiddenData.news.length > 0) || ($scope.hiddenData.hasOwnProperty('event') && $scope.hiddenData.event.length > 0) || ($scope.hiddenData.hasOwnProperty('album') && $scope.hiddenData.album.length > 0)) $scope.showHiddenPostsModal = true;
		});

		window.addEventListener("message", function(event) {
			console.log(event);
			//if(event.origin != window.location.origin && /paypal/gi.test(event.origin) === false && /chrome/gi.test(event.origin) === false){
			if (/.*majeeko.*/.test(event.origin) && event.origin != window.location.origin) {
				bootbox.dialog({
					message: messagesText.show_hide_message,
					title: messagesText.show_hide_title,
					buttons: {
						danger: {
							label: messagesText.show_hide_cancel,
							className: 'btn-default'
						},
						success: {
							label: messagesText.show_hide_confirm,
							className: 'btn-primary',
							callback: function() {
								$scope.$apply(function() {
									$HideShowPost.hidePost(event.data)
										.then(function(response) {
											if (response != undefined && response.status === 200) refreshPreview();
											return $HideShowPost.getHiddenData();
										})
										.then(function(data) {
											$scope.hiddenData = data;
											if (($scope.hiddenData.hasOwnProperty('news') && $scope.hiddenData.news.length > 0) || ($scope.hiddenData.hasOwnProperty('event') && $scope.hiddenData.event.length > 0) || ($scope.hiddenData.hasOwnProperty('album') && $scope.hiddenData.album.length > 0)) $scope.showHiddenPostsModal = true;
										})
										.catch(function(err) {
											console.log(err);
										});
								});
							}
						}
					}
				});

			}

		}, false);
		$scope.pageData = pageData;
		$scope.canSubmit = 0;
		$scope.countHidden = function(toggle) {
			$scope.canSubmit = toggle === true ? $scope.canSubmit + 1 : $scope.canSubmit - 1;
		};
		$scope.selectedProfilePicture = $scope.pageData.profile_pictures.filter(function(elem) {
			return elem.id === $scope.pageData.configuration.graphics.logoId
		})[0];

		$rootScope.trackEventPublishBtn = function(a,b) {
			trackEventPublishBtn(a, b);
		};
		$rootScope.trackEventButton = function(a,b) {
			trackEventButton(a, b);
		};
		$('<img />').attr('src',"/images/banner/default.jpg").appendTo('body').css('display','none');
	};
};

angular.module('editor', [
	'ngAnimate',
	'ng-sortable',
	'colorpicker.module',
	'rx',
	'majeeko.utils',
	'ui.bootstrap',
	'textAngular',
	'minicolors',
	'ui.mask',
	'slick',
	'store'
])

.config(function($animateProvider) {
	$animateProvider.classNameFilter(/maj-animate/);
})

.constant('clientTokenPath', '/client_token')

.run(function() {
	console.log('running angular module editor');
})

.service('apiInterceptor', function($q, $rootScope, messagesText) {
	var that = this;

	var deferred = $q.defer();
	this.responseError = function(response) {
		if (response.status == 401) {
			bootbox.dialog({
				"message": messagesText.session_expired_body,
				"title": messagesText.session_expired_title,
				"buttons": {
					success: {
						"label": messagesText.session_expired_do_access,
						"className": "btn-success",
						callback: function() {
							console.log('callback fired');
							window.location.href = window.location.origin + "/auth/facebook";
						}
					}
				}
			});
			deferred.reject('invalid auth');
			return deferred;
		}
		return response;
	};
})

.config(function($httpProvider, $compileProvider) {
	$httpProvider.interceptors.push('apiInterceptor');
	$compileProvider.debugInfoEnabled(false);
})

.service('pageService', function($http, $rootScope, messagesText, pageData) {
	this.isSaving = false;

	this.selectNewLogo = function(logoId) {
		pageData.configuration.graphics.logoId = logoId;
		trackEventAction("logo", "save");
		this.save();
	};
	this.selectNewBanner = function(selected_images_flags, do_sync) {
		if(do_sync){
			pageData.configuration.graphics.bannerId = false;
			trackEventAction("banner", "sync");
		}else{
			var selected_banners_id = [];
			for (var banner_id in selected_images_flags) {
				if (selected_images_flags[banner_id]) {
					selected_banners_id.push(banner_id);
				}
			}
			pageData.configuration.graphics.bannerId = selected_banners_id.join(',');
			trackEventAction("banner", "custom_select");
		}
		this.save();
		trackEventAction("banner", "save");
	};
	this.save = function() {
		trackEventAction("save");
		this.sendPayload("save");
	};
	this.publish = function() {
		that = this;

		var bootbox_buttons = {
			danger: {
				label: messagesText.publish_alert_no,
				className: 'btn-default',
				callback: function() {
					$rootScope.$apply();
					$(window).trigger('resize');
				}
			},
			success: {
				label: messagesText.publish_alert_yes,
				className: 'btn-primary',
				callback: function() {
					trackEventAction("publish");
					that.sendPayload("publish");
					$(window).trigger('resize');
				}
			}
		};

		if (!_.get(pageData, "paid")) {
			bootbox_buttons.goOnline = {
				label: "<i class='glyphicon glyphicon-globe'></i> " + messagesText.go_online,
				className: 'btn btn-buy pull-left',
				callback: function() {
					$rootScope.toggleEditorPanel("buy");
				}
			};
		}

		var box = bootbox.dialog({
			message: messagesText.publish_alert_body,
			title: messagesText.publish_alert_title,
			buttons: bootbox_buttons
		});

		box.on('hidden.bs.modal', function (e) {
			if($('.modal.in').css('display') == 'block'){
				$('body').addClass('modal-open');
			}
			box.off('hidden.bs.modal');
		});

		return box;
	};
	this.colorExtractor = function() {
		this.sendPayload("ce");
	};
	this.reset = function() {
		trackEventAction("reset");
		this.sendPayload("reset");
	};
	this.default = function() {
		this.sendPayload("cancel");
	};
	this.saveStore = function saveStore() {
		console.log("saving the store");
		this.save();
	};
	this.sendPayload = function(action) {
		this.isSaving = true;
		$rootScope.$broadcast('saving');
		var configuration = pageData.configuration;

		configuration.settings.menu.map(function(elem) {
			if (elem.label === '')
				elem.label = elem.real_name;
			return elem;
		});

		console.log(configuration);

		var payload = {
			username: pageData.username,
			configuration: pageData.configuration
		};
		payload.action = action;
		console.debug(payload);
		var broadcastAction;
		switch (action) {
			case "reset":
			case "ce":
				broadcastAction = 'pageResetted';
				break;
			default:
				broadcastAction = 'pageSaved';
		}
		return $http.post('/api/editor', angular.toJson(payload))
			.success(function(data) {
				$rootScope.$broadcast(broadcastAction, data);
				return data;
			})
			.finally(function() {
				this.isSaving = false;
			});
	};
})

.controller('editorCtrl', editorCtrlFactory())

.controller('tophostEditorCtrl', editorCtrlFactory(true))

.controller('editorSettingsCtrl', function($scope, pageData) {
	$scope.choices = pageData.configuration;
})

.controller('editorGraphicsCtrl', function($scope, $modal, pageData, messagesText, pageService, fonts) {
	$scope.fonts = fonts;
	$scope.options = {};
	$scope.newLogoId = false;
	$scope.newLogoUseSync = false;

	$scope.fontClickHandler = function(value) {
		$scope.choices.graphics.fonts = value;
		trackEventAction("select_font", value);
	};

	$scope.selectTheme = function(value) {
		$scope.choices.graphics.template = value;
		trackEventAction("select_theme", value);
	};
	$scope.openChangeLogoPopup = function() {
		$scope.newLogoIdDefault = _.get(_.first(pageData.profile_pictures),"id",false);
		$scope.newLogoUseSync = ! _.get(pageData.configuration.graphics,"logoId");
		if($scope.newLogoUseSync){
			$scope.newLogoId = false;
		}else{
			$scope.newLogoId = _.get(pageData.configuration.graphics, "logoId");
		}
		$modal.open({
			templateUrl: 'modify-logo-modal.tpl',
			scope: $scope
		});
		trackEventModal("open_change_logo");
	};

	$scope.checkNewLogo = function(id){
		$scope.newLogoId = id;
		trackEventAction("logo", "select");
	};

	$scope.toggleLogoSync = function(){
		if(!$scope.newLogoUseSync){
			$scope.newLogoId = false;
			trackEventAction("logo", "sync");
		}else{
			$scope.newLogoId = $scope.newLogoIdDefault;
		}
	}

	$scope.selectNewLogo = pageService.selectNewLogo;

	$scope.banner_selected_images_flags = {};
	$scope.banner_selection_valid = false;
	$scope.banner_use_sync = false;

	$scope.openChangeBannerPopup = function() {
		var selected_images = [];
		if ($scope.pageData.configuration.graphics.bannerId) {
			var banner_id_string = "" + $scope.pageData.configuration.graphics.bannerId;
			selected_images = banner_id_string.split(',');
			$scope.banner_use_sync = false;
		} else {
			selected_images = [];
			$scope.banner_use_sync = true;
		}

		var count = 0;
		for (var c_img in $scope.pageData.cover_pictures) {
			var image_id = $scope.pageData.cover_pictures[c_img].id;
			if(selected_images.length == 0 && count == 0){
				$scope.banner_selected_images_flags[image_id] = true;
			} else {
				$scope.banner_selected_images_flags[image_id] = selected_images.indexOf(image_id) >= 0;
			}
			count += 1;
		}
		var modalInstance = $modal.open({
			templateUrl: 'modify-banner-modal.tpl',
			scope: $scope
		});
		$scope.validBannerSelection();
		trackEventModal("open_change_banner");
	};
	$scope.toggleBannerSelection = function(bannerId) {
		$scope.banner_selected_images_flags[bannerId] = !$scope.banner_selected_images_flags[bannerId];
		$scope.validBannerSelection();
		trackEventAction("banner", "select");
	};

	$scope.validBannerSelection = function() {
		console.log("validating", $scope.banner_use_sync);
		if($scope.banner_use_sync){
			$scope.banner_selection_valid = true;
			return true;
		}
		console.log("init loop");
		for (var b_id in $scope.banner_selected_images_flags) {
			console.log(b_id);
			console.log($scope.banner_selected_images_flags[b_id]);
			if ($scope.banner_selected_images_flags[b_id] === true) {
				$scope.banner_selection_valid = true;
				return true;
			}
		}
		$scope.banner_selection_valid = false;
	};

	$scope.cleanSelectedBanners = function() {
		for (var c_img in $scope.pageData.cover_pictures) {
			var image_id = $scope.pageData.cover_pictures[c_img].id;
			$scope.banner_selected_images_flags[image_id] = false;
		}
		$scope.validBannerSelection();
	};

	$scope.validBannerSelection();

	$scope.selectNewBanner = pageService.selectNewBanner;

	$scope.choices = pageData.configuration;

	$scope.feelLucky = pageService.colorExtractor;

})
.controller('editorContentsCtrl', function($scope, $rootScope, pageData, $modal, messagesText, pageService) {

	$scope.choices = pageData.configuration;
	$scope.sortableOptions = {
		handle: '.drag-handle',
		animation: 50,
		chosenClass: "sortable-chosen",
		ghostClass: "sortable-ghost",
		onUpdate: function (evt) {
			trackEventAction("move", _.get(evt, "models["+evt.newIndex+"].id"));
		}
	};
	$scope.newPage = {
		step: 'init',
		type: ''
	};

	$scope.menuInitialParser = function(){
		for(var i = $scope.choices.settings.menu.length -1 ; i>=0; i--){
			var name = "";
			if(_.has(messagesText.page_name, $scope.choices.settings.menu[i].id)){
				name = _.get(messagesText.page_name, $scope.choices.settings.menu[i].id);
			}else{
				var type = _.get(_.first(_.filter($scope.choices.contents.extra_pages, function(e){return e.id == $scope.choices.settings.menu[i].id;})), "type");

				name = name = _.get(messagesText.page_name, type, _.get(messagesText.page_name, "extra-page"));
			}
			$scope.choices.settings.menu[i].real_name_default = name;
		}
	};
	$scope.menuInitialParser();

    $scope.canShow= function(item){
        if(item.id == "extra-page-1" || item.id == "extra-page-2") {
            return !!item.inUse || item.active;
        }
        return true;
    };

	$scope.canAdd = function canAddMorePages() {
		return $scope.choices.settings.menu.filter(function(elem) {
			return (elem.id == "extra-page-1" || elem.id == "extra-page-2") && (elem.inUse || elem.active);
		}).length === 2 ? false : true;
	};

	$scope.addExtraPage = function(newPage) {
		var extraMenu = _.find($scope.choices.settings.menu, function(elem) {
			return (elem.id == "extra-page-1" || elem.id == "extra-page-2") && !elem.inUse;
		});
		var extraContents = _.find($scope.choices.contents.extra_pages, function(elem) {
			return elem.id == extraMenu.id;
		});

		var extraToAssign = {
			text: newPage.text,
			real_name: newPage.titolo,
			type: newPage.type,
			inUse: true
		};

		if (newPage.type === 'album') {
			extraToAssign.album = newPage.album;
		}

		extraMenu = _.assign(extraMenu, {
			active: true,
			label: newPage.titolo,
			real_name: newPage.titolo,
			slug: 'changed',
			inUse: true
		});

		extraContents = _.assign(extraContents, extraToAssign);
		if(newPage.type === "store" && !_.has($scope.choices, "contents.store.configuration")) {
			_.set($scope.choices,
				"contents.store.configuration",
			{
					"storeName": "",
					"logoImage": "ecommerce/logo.png",
					"currency": "EUR",
					"currencySymbol": "€",
					"businessEmail": "sample@website.com",
					"UsePaypal": true,
					"UseSubmitOrder": true,
					"ShippingCharges": "9"
			});
		}
		$scope.newPage = {
			type: ''
		};
		pageService.save();
		trackEventNewPage(newPage.type);
	};

	$scope.initModal = function(step, type) {
		$scope.newPage = {
			step: (step ? step : 'init'),
			type: (type ? type : '')
		};
	};

	$scope.removeExtraPage = function(id) {
		var extraMenu = _.find($scope.choices.settings.menu, function(elem) {
			return elem.id == id;
		});
		var extraContents = _.find($scope.choices.contents.extra_pages, function(elem) {
			return elem.id == id;
		});

		var extraToAssign = {
			text: '',
			real_name: id,
			album: '',
			type: '',
			inUse: false
		};
		extraMenu = _.assign(extraMenu, {
			active: false,
			label: id,
			real_name: id,
			slug: id,
			inUse: false
		});
		extraContents = _.assign(extraContents, extraToAssign);
		pageService.save();
		trackEventAction("delete_extra_page");
	};

	$scope.options = {
		albums: pageData.albums
	};

	$scope.openAddPageModal = function() {
		$scope.initModal();
		$modal.open({
			templateUrl: 'add-page.tpl',
			scope: $scope
		});
		trackEventModal("open_add_page");
	};

	$scope.openModifyPage = function(id) {
		if (id == 'home') {
			$scope.editingSettings = angular.copy($scope.choices.settings);
			$scope.editingHomepage = angular.copy($scope.choices.contents.homepage);
			$scope.page_title_default = messagesText.page_name.home;
			$modal.open({
				templateUrl: 'modify-page-home.tpl',
				scope: $scope
			});
			trackEventModal("open_edit_home");
		} else if (id == 'news' || id == 'albums' || id == 'events') {
			pageAttributes = $scope.whatPageAttributes(id);
			$scope.page_title_default = messagesText.page_name[id];
			$scope.editingSettings = {
				type: 'basic',
				id: id,
				menuIndex: $scope.whatPageIndex(id),
				active: pageAttributes.active,
				label: pageAttributes.label,
				columns: $scope.choices.graphics.columns[id]
			};
			$modal.open({
				templateUrl: 'modify-page-basicPage.tpl',
				scope: $scope
			});
			trackEventModal("open_edit_" + id);
		} else if (id == 'contacts') {
			pageAttributes = $scope.whatPageAttributes(id);
			$scope.page_title_default = messagesText.page_name.contacts;
			$scope.editingSettings = {
				type: 'contacts',
				id: id,
				menuIndex: $scope.whatPageIndex(id),
				active: pageAttributes.active,
				label: pageAttributes.label,
				email: $scope.choices.settings.mail
			};
			$modal.open({
				templateUrl: 'modify-page-contactsPage.tpl',
				scope: $scope
			});
			trackEventModal("open_edit_" + id);
		} else {
			pageAttributes = $scope.whatPageAttributes(id);
			currentPage = $scope.whatPage(id);
			$scope.editingSettings = {
				type: 'extra',
				id: id,
				menuIndex: $scope.whatPageIndex(pageAttributes.id),
				extraPageIndex: $scope.whatExtraPageIndex(currentPage.id),
				active: pageAttributes.active,
				label: pageAttributes.label,
				content: currentPage
			};
			if (currentPage.type == "album") {
				$scope.editingSettings.type = 'extra-album';
				$scope.editingSettings.step = 'init';
				$scope.editingSettings.newAlbum = {};
				$modal.open({
					templateUrl: 'modify-page-albumPage.tpl',
					scope: $scope,
					size: "lg"
				});
				trackEventModal("open_edit_extra_album");
			} else {
				$scope.editingSettings.type = 'extra-text';
				$modal.open({
					templateUrl: 'modify-page-textPage.tpl',
					scope: $scope
				});
				trackEventModal("open_edit_extra_text");
			}
		}
	};

	$scope.saveModifyPage = function(id) {
		var toggleVisibility = false;
		if (id == 'home') {
			$scope.choices.settings = angular.copy($scope.editingSettings);
			$scope.choices.contents.homepage = angular.copy($scope.editingHomepage);
		} else if (id == 'news' || id == 'albums' || id == 'events') {
			if($scope.choices.graphics.columns[id] != $scope.editingSettings.columns){
				trackEventAction("set_cols_" + id, $scope.editingSettings.columns);
			}
			if($scope.choices.settings.menu[$scope.editingSettings.menuIndex].active != $scope.editingSettings.active){
				toggleVisibility = true;
			}
			$scope.choices.settings.menu[$scope.editingSettings.menuIndex].active = $scope.editingSettings.active;
			$scope.choices.settings.menu[$scope.editingSettings.menuIndex].label = $scope.editingSettings.label;
			$scope.choices.graphics.columns[id] = $scope.editingSettings.columns;
		} else if (id == 'contacts') {
			if($scope.choices.settings.menu[$scope.editingSettings.menuIndex].active != $scope.editingSettings.active){
				toggleVisibility = true;
			}
			$scope.choices.settings.menu[$scope.editingSettings.menuIndex].active = $scope.editingSettings.active;
			$scope.choices.settings.menu[$scope.editingSettings.menuIndex].label = $scope.editingSettings.label;
			$scope.choices.settings.mail = $scope.editingSettings.email;
		} else if (id = "extra-text") { // saving extra text page
			if($scope.choices.settings.menu[$scope.editingSettings.menuIndex].active != $scope.editingSettings.active){
				toggleVisibility = true;
			}
			$scope.choices.settings.menu[$scope.editingSettings.menuIndex].active = $scope.editingSettings.active;
			$scope.choices.settings.menu[$scope.editingSettings.menuIndex].label = $scope.editingSettings.label;
			$scope.choices.contents.extra_pages[$scope.editingSettings.extraPageIndex] = $scope.editingSettings.content;
		} else if (id = "extra-album") { // saving extra text album
			if($scope.choices.settings.menu[$scope.editingSettings.menuIndex].active != $scope.editingSettings.active){
				toggleVisibility = true;
			}
			$scope.choices.settings.menu[$scope.editingSettings.menuIndex].active = $scope.editingSettings.active;
			$scope.choices.settings.menu[$scope.editingSettings.menuIndex].label = $scope.editingSettings.label;
			$scope.choices.contents.extra_pages[$scope.editingSettings.extraPageIndex] = $scope.editingSettings.content;
		}
		this.save();
		$scope.editingSettings = {};
		trackEventAction("save_from_edit_page");
		if(toggleVisibility){
			trackEventAction("toggle_visibility", id);
		}
		//$scope.currentPage = angular.copy(toSavePage);
	};

	$scope.whatPage = function(id) {
		return $scope.choices.contents.extra_pages.filter(function(elem) {
			return elem.id === id;
		})[0];
	};

	$scope.whatPageIndex = function(id) {
		for (var i = 0; i < ($scope.choices.settings.menu).length; i++) {
			if ($scope.choices.settings.menu[i].id == id) {
				return i;
			}
		}
		return -1;
	};

	$scope.whatExtraPageIndex = function(id) {
		for (var i = 0; i < ($scope.choices.contents.extra_pages).length; i++) {
			if ($scope.choices.contents.extra_pages[i].id == id) {
				return i;
			}
		}
		return -1;
	};

	$scope.whatPageAttributes = function(id) {
		return $scope.choices.settings.menu.filter(function(elem) {
			return elem.id === id;
		})[0];
	};

	$scope.doEditAlbum = function(editingSettings) {
		editingSettings.newAlbum.album = editingSettings.content.album;
		editingSettings.step = 'selectNewAlbum';
	};
	$scope.updateAlbum = function(editingSettings, replace) {
		editingSettings.content.album = editingSettings.newAlbum.album;
		if (replace) {
			editingSettings.label = editingSettings.newAlbum.titolo;
			editingSettings.content.text = editingSettings.newAlbum.text;
		}
		editingSettings.step = 'init';
		trackEventAction("extra_change_album", replace ? "replace":"no-replace");
	};
	$scope.setContentsByAlbum = function(newAlbum) {
		if(!newAlbum.album){
			$scope.editingSettings.content.album = "";
			$scope.updateAlbum($scope.editingSettings,false);
		}else{
			var album = _.findWhere(pageData.albums, {
				'value': newAlbum.album
			});
			if(album){
				newAlbum.titolo = album.label ? album.label : "";
				newAlbum.albumTitle = album.label ? album.label : "";
				newAlbum.text = (album.description ? album.description : "").replace(/(?:\r\n|\r|\n)/g, '<br>');
			}
		}
	};

	$scope.updateContentsByAlbum = function(newAlbum){
		if(!newAlbum.album){
			$scope.editingSettings.content.album = "";
			$scope.updateAlbum($scope.editingSettings,false);
			$scope.editingSettings.step='init';
		}else{
			$scope.setContentsByAlbum(newAlbum);
			$scope.editingSettings.step='setTextNewAlbum';
		}
	};

	$scope.getAlbumTitle = function(value) {
		for (var i = 0; i < ($scope.options.albums).length; i++) {
			if ($scope.options.albums[i].value == value) {
				return $scope.options.albums[i].label;
			}
		}
		return messagesText.no_album_selected;
	};

	$scope.pageCanBeDisplayed = function(pageId, pageData) {
		switch (pageId) {
			case 'news':
				return pageData.num_posts > 0;
			case 'events':
				return pageData.num_events > 0;
			case 'albums':
				return pageData.num_albums > 0;
			default:
				return true;
		}
	};

	$scope.isActive = function(id) {
		return $scope.choices.settings.menu.filter(function(elem) {
			return elem.id == id;
		})[0].active;
	};

	$scope.trackEventAction = function(a,b) {
		trackEventAction(a, b);
	};
})

.service('getEmbeddedSiteUrl', function(
	$sce,
	embeddedWebsiteURL
) {
	this.getValidUrl = function() {
		return $sce.trustAsResourceUrl(embeddedWebsiteURL);
	};
})

.service('$HideShowPost', function($http, $q, pageData) {
	this.getHiddenData = function() {
		return $http.get('/api/toggle-post/' + pageData.id)
			.then(function(response) {
				return response.data.data;
			});
	};

	this.hidePost = function(data) {

		data.pageId = pageData.id;
		return $http.post('/api/toggle-post', data);
	};
});

$(document).ready(function() {
	$(".window-height").css('height', $(window).height());
	$(window).resize(function() {
		$(".window-height").css('height', $(window).height());
	});
});
