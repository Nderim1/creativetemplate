angular.module('schemaForm').config(['schemaFormDecoratorsProvider',
	function(schemaFormDecoratorsProvider) {
		schemaFormDecoratorsProvider.addMapping(
			'bootstrapDecorator',
			'textangular',
			'textangulartemplate.tpl'
		);
		schemaFormDecoratorsProvider.addMapping(
			'bootstrapDecorator',
			'switch',
			'switchtemplate.tpl'
		);
		schemaFormDecoratorsProvider.addMapping(
			'bootstrapDecorator',
			'singleimage',
			'singleimagetemplate.tpl'
		);
		schemaFormDecoratorsProvider.addMapping(
			'bootstrapDecorator',
			'arraytable',
			'arraytabletemplate.tpl'
		);
		schemaFormDecoratorsProvider.addMapping(
			'bootstrapDecorator',
			'displayonly',
			'displayonlytemplate.tpl'
		);
		schemaFormDecoratorsProvider.addMapping(
			'bootstrapDecorator',
			'displayonlydate',
			'displayonlydatetemplate.tpl'
		);
		schemaFormDecoratorsProvider.addMapping(
			'bootstrapDecorator',
			'displayonlywithcurrency',
			'displayonlywithcurrencytemplate.tpl'
		);
	}]);
angular.module('store',[
	'schemaForm',
	'ui.grid',
	'ui.grid.pagination'
])
.service('storeProvider', function(pageData) {
	this.id = pageData.id;
	this.mail = pageData.mail;
	this.page_name = pageData.pageName;
	this.albums = pageData.albums;
	this.urls =  {
		products: '/store/products/' + this.id,
		categories: '/store/categories/' + this.id,
		orders: '/store/orders/' + this.id,
		configuration: '/store/configuration/' + this.id,
		album: '/api/album/' + this.id
	};
})
// providers
.service('productProvider', function ($http, storeProvider) {
	this.type = "product";
	this.getCollection = function getProductCollection(filters) {
		// need to add filters to the server call
		// pagination and search
		return $http.get(storeProvider.urls.products);
	};
	this.get = function getProduct(productID) {
		return $http.get(storeProvider.urls.products + '/' + productID);
	};
	this.save = function saveProduct(product) {
		return $http.post(storeProvider.urls.products, { product: product });
	};
	this.delete = function deleteProduct(product) {
		return $http.post(storeProvider.urls.products, { product: product, action: "delete" });
	};
})
.service('categoryProvider', function ($http, storeProvider) {
	this.type = "category";
	this.getCollection = function getCategoryCollection(filters) {
		// need to add filters to the server call
		// pagination and search
		return $http.get(storeProvider.urls.categories);
	};
	this.get = function getCategory(categoryID) {
		return $http.get(storeProvider.urls.categories + '/' + categoryID);
	};
	this.save = function saveCategory(category) {
		return $http.post(storeProvider.urls.categories, { category: category });
	};
	this.delete = function deleteCategory(category) {
		return $http.post(storeProvider.urls.categories, { category: category, action: "delete" });
	};
})
.service('orderProvider', function ($http, storeProvider) {
	this.type = "order";
	this.getCollection = function getOrderCollection(filters) {
		return $http.get(storeProvider.urls.orders);
	};
	this.get = function getOrder(orderID) {
		return $http.get(storeProvider.urls.orders + '/' + orderID);
	};
	this.save = function saveOrder(order) {
		return $http.post(storeProvider.urls.orders , {order: order});
	};
})
.service('configurationProvider', function ($http, storeProvider) {
	this.type = "configuration";
	// provides async call to get configuration from the server
 	this.get = function getConfiguration() {
		return $http.get(storeProvider.urls.configuration);
 	};
 	// provides async call to set configuration on the server
 	this.save = function saveConfiguration(configuration) {
		return $http.post(storeProvider.urls.configuration, { configuration: configuration });
 	};
})
.factory('albumProvider', function ($http, storeProvider) {
	return function (scope){
		this.scope = scope;
		this.getAlbum = function getAlbum() {
			return $http.get(storeProvider.urls.album + "/" + _.get(this.scope, "selectedAlbum.value"));
		};
	};
})
// grids
.factory('storeGrid', function($http, uiGridConstants) {
	return function(scope, collection, paginationOptions) {
		paginationOptions = paginationOptions || {
			pageNumber: 1,
			pageSize: 25,
			sort: null
		};
		var self = this;
		this.collection = collection;
		this.onRegisterApi = function(gridApi) {
			gridApi.core.on.sortChanged(scope, function(grid, sortColumns) {
				if (sortColumns.length === 0) {
					paginationOptions.sort = null;
				} else {
					paginationOptions.sort = sortColumns[0].sort.direction;
				}
				self.getPage();
			});
			gridApi.pagination.on.paginationChanged(scope, function(newPage, pageSize) {
				//debugger
				paginationOptions.pageNumber = newPage;
				paginationOptions.pageSize = pageSize;
				self.getPage();
			});
		};
		this.getPage = function() {
			$scope.isLoading = true;
			self.collection.getCollection().then(function (collection) {
				$scope.isLoading = false;
				collection = collection.data || collection;
				self.config.totalItems = collection.totalItems;
				var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
				self.config.data = collection.data.slice(firstRow, firstRow + paginationOptions.pageSize);
			});
		};
		this.config = {
			paginationPageSizes: [25, 50, 75],
			paginationPageSize: paginationOptions.pageSize,
			useExternalPagination: true,
			useExternalSorting: true,
			columnDefs: [{
				name: 'name',
				headerCellClass: 'red'
			}, {
				name: 'gender',
				enableSorting: false,
				headerCellClass: 'red'
			}, {
				name: 'company',
				enableSorting: false,
				headerCellClass: 'red'
			}],
			onRegisterApi: this.onRegisterApi
		};
	};
})
// main controller
.controller('storeAdminCtrl', function($scope, $rootScope, $modal, messagesText, pageService, productForm, categoryForm, orderForm, configurationForm, storeGrid, storeProvider, productProvider, categoryProvider, orderProvider, configurationProvider, albumProvider, messagesText) {
	$scope.products = [];
	$scope.productForm = productForm;
	$scope.categoryForm = categoryForm;
	$scope.configurationForm = configurationForm;
	$scope.orderForm = orderForm;
	$scope.albums = storeProvider.albums;

	$scope.vm = {
		selectedAlbum: {},
		album: {},
		store: storeProvider,
		totalItems: function (collection) {
			return collection ? $scope[collection].length : $scope.products.length;
		},
		currentPage: 1,
		numPerPage: 10,
		collectionType: "products",
		providers: {
			product: productProvider,
			category: categoryProvider,
			order: orderProvider,
			configuration: configurationProvider
		},
		isSaving: false,
		isFailed: false
	};

	// $scope.storeProductsGrid = new storeGrid($scope, productProvider);
	// $scope.storeProductsGrid.getPage();

	var albumService = new albumProvider($scope.vm);
	$scope.paginate = function paginate(value) {
		var begin, end, index;
		begin = ($scope.vm.currentPage - 1) * $scope.vm.numPerPage;
		end = begin + $scope.vm.numPerPage;
		if ($scope[$scope.vm.collectionType]) {
			index = $scope[$scope.vm.collectionType].indexOf(value);
			return ((begin <= index) && (index < end));
		}
		return undefined;
	};

	/**
	 * TABS for navigation
	 */
	$scope.tabs = [
	{
		title: 'Prodotti',
		url: 'products.tpl',
		visible: false
	}, {
		title: 'Categorie',
		url: 'categories.tpl',
		visible: false
	}, {
		title: 'Ordini',
		url: 'orders.tpl',
		visible: false
	}, {
		title: 'Configurazione',
		url: 'configuration.tpl',
		visible: true
	}];

	$scope.onClickTab = function onClickTab(tab) {
		$scope.vm.currentPage = 1;
		if(tab.url == "products.tpl") {
			$scope.vm.collectionType = "categories";
			$scope.isLoading = true;
			categoryProvider.getCollection().then(function (payload) {
				$scope.isLoading = false;
				$scope.categories = payload.data;
				$scope.canAddProduct = $scope.categories.length > 0;
			});
			$scope.vm.collectionType = "products";
			$scope.isLoading = true;
			productProvider.getCollection().then(function (payload) {
				$scope.isLoading = false;
				$scope.products = payload.data;
				$scope.currentTab = tab.url;
			});
		} else if(tab.url == "categories.tpl") {
			$scope.vm.collectionType = "categories";
			$scope.isLoading = true;
			categoryProvider.getCollection().then(function (payload) {
				$scope.isLoading = false;
				$scope.categories = payload.data;
				$scope.currentTab = tab.url;
			});
		} else if(tab.url == "orders.tpl") {
			$scope.vm.collectionType = "orders";
			$scope.isLoading = true;
			orderProvider.getCollection().then(function (payload) {
				$scope.isLoading = false;
				$scope.orders = payload.data;
				_.forEach($scope.orders, function(l){
					l.date = moment(l.date).format("L LT");
					l.status = messagesText.status_title[l.status];
				});
				$scope.currentTab = tab.url;
			});
		} else if(tab.url == "configuration.tpl") {
			$scope.vm.collectionType = undefined;
			$scope.isLoading = true;
			configurationProvider.get().then(function (payload) {
				$scope.isLoading = false;
				$scope.configurationForm.model = payload.data;
				if(!$scope.configurationForm.model.ownerEmail) {
					$scope.configurationForm.model.ownerEmail = storeProvider.mail;
				}
				//if(!$scope.configurationForm.model.businessEmail) {
				//	$scope.configurationForm.model.businessEmail = storeProvider.mail;
				//}
				if(!$scope.configurationForm.model.storeName) {
					$scope.configurationForm.model.storeName = storeProvider.page_name + " Store";//storeProvider.mail;
				}
				$scope.currentTab = tab.url;
			});
		}
	};
	$scope.isActiveTab = function isActiveTab(tabUrl) {
		return tabUrl == $scope.currentTab;
	};

	$scope.onClickTab($scope.tabs[0]);
	//END TABS

	/**
		Functions to handle adding removing elements from the store
	 */
	$scope.back = function back(element) {
		$scope.currentTab = element;
	};
	$scope.editCategoryTab = function editCategoryTab(category) {
		$scope.updatingElement = Boolean(category);
		$scope.categoryForm.model = category || {};
		$scope.currentTab = "add_category.tpl";
	};
	$scope.editProductTab = function editProductTab(product) {
		$scope.isLoading = true;
		categoryProvider.getCollection().then(function (payload) {
			$scope.isLoading = false;
			if(!payload || !payload.data){
				return;
			}
			$scope.productForm.schema.properties.productCategory.enum = payload.data.map(function (element) {
				return element.categoryName ? element.categoryName : undefined;
			});
			$scope.updatingElement = Boolean(product);
			$scope.productForm.model = product || { availability: true};

			$scope.currentTab = "add_product.tpl";
		});
	};

	//
	// add support for order status and order date
	// add support to edit order
	//
	$scope.showOrderTab = function showshowOrderTab(order) {
		$scope.updatingElement = Boolean(order);
		$scope.isLoading = true;
		orderProvider.get(order.orderID).then(function(payload){
			$scope.isLoading = false;
			$scope.orderForm.model = payload.data || {};
			$scope.currentTab = "show_order.tpl";
		});
	};

	function setModalStep(step) {
		$scope.chooseImage = {
			step: (step ? step : 'step1')
		};
	}

	$scope.modalStep = function modalStep(step) {
		switch(step) {
			case 'step2':
				albumService.getAlbum()
				.then(function(res) {
					$scope.vm.album = res.data;
					setModalStep(step);
				});
				break;
			default:
				setModalStep(step);
		}
	};

	$scope.openImagePicker = function openImagePicker(event) {
		console.log("event:", event);
		$scope.modalStep();
		$modal.open({
			templateUrl: 'store-image-picker.tpl',
			scope: $scope
		});
	};

	$scope.selectProductImage = function selectProductImage(image) {
		image.src = image.source;
		delete image.source;

		productForm.model.thumbPath = image;

		if(image.description && (productForm.model.productName === undefined || productForm.model.productName == "")){
			productForm.model.productName = image.description;
		}
	};

	$scope.vm.saveModel = function saveModel(form, provider, model) {
		$scope.$broadcast('schemaFormValidate');
		if (!form.$valid) {
			return;
		}
		$scope.vm.isSaving = true;
		$scope.isLoading = true;
		provider.save(model)
			.then(function (response) {
				$scope.isLoading = false;
				if(response.status == 404 || response.status == 403 ) {
					$scope.vm.isFailed = true;
				}
				$scope.vm.isSaving = false;
				switch(provider.type) {
					case "category":
						$scope.onClickTab($scope.tabs[1]);
						break;
					case "order":
						$scope.onClickTab($scope.tabs[2]);
						break;
					default:
						$scope.onClickTab($scope.tabs[0]);
				}
			});
	};

	$scope.vm.deleteModel = function deleteModel(provider, model) {
		$scope.vm.isSaving = true;
		var message;
		if(provider.type == "category") {
			message = "Sei sicuro di voler cancellare questa categoria? <br> Quest'azione comporterà l'eliminazione di tutti i prodotti che appartengono a questa categoria!";
		} else if(provider.type == "product") {
			message = "Sei sicuro di voler cancellare questo prodotto?";
		}

		var box = bootbox.dialog({
			message: message,
			title: "Cancella elemento",
			buttons: {
				danger: {
					label: messagesText.show_hide_cancel,
					className: 'btn-default'
				},
				success: {
					label: messagesText.show_hide_confirm,
					className: 'btn-primary',
					callback: function() {
						provider.delete(model)
							.then(function (response) {
								if(response.status == 404 || response.status == 403 ) {
									$scope.vm.isFailed = true;
								}
								$scope.vm.isSaving = false;
								switch(provider.type) {
									case "category":
										$scope.onClickTab($scope.tabs[1]);
										break;
									case "order":
										$scope.onClickTab($scope.tabs[2]);
										break;
									default:
										$scope.onClickTab($scope.tabs[0]);
								}
							});
					}
				}
			}
		});
		box.on('hidden.bs.modal', function (e) {
			if($('.modal.in').css('display') == 'block'){
				$('body').addClass('modal-open');
			}
			box.off('hidden.bs.modal');
		});
	};
})
.filter('formattedDate', function() {
	return function(input) {
		return moment(input).format("lll");
	};
})

// forms
.factory('productForm', function (storeProvider) {
	var productForm = {
		schema: {
			type: "object",
			properties: {
				productCategory: {
					type: "string",
					title: "Categoria",
					"x-schema-form":{
						feedback: true
					}
				},
				thumbPath: {
					title: "Immagine prodotto",
					type: "string",
					"x-schema-form": {
						type: "singleimage",
						onClick: "openImagePicker(event)"
					}
				},
				productName: {
					type: "string",
					title: "Nome Prodotto",
					"x-schema-form": {
						validationMessage: "Inserire un nome per questo prodotto",
						feedback: true,
						htmlClass: "has-feedback"
					}
				},
				details: {
					type: "string",
					title: "Dettagli Prodotto",
					"x-schema-form": {
						type: "textangular"
					}
				},
				productID: {
					type: "string",
					title: "ID Prodotto",
					readonly: true,
					"x-schema-form": {
						htmlClass: "hidden"
					}
				},
				productPrice: {
					type: "string",
					title: "Prezzo",
					"x-schema-form": {
						onChange:  function onChange(modelValue,form) {
							var validatedPrice = modelValue ? modelValue.replace(',', '.').replace(/[^0-9\.]+/i, "") : 0;
							productForm.model.productPrice = validatedPrice;
						},
						feedback: true,
						htmlClass: "has-feedback"
					}
				},
				quantity: {
					type: "string",
					title: "Quantità Disponibile",
					"x-schema-form": {
						feedback: false
					}
				}/*,
				availability: {
					type: "boolean",
					title: "Disponibile per l'ordine",
					"x-schema-form": {
						type: "switch"
					}
				},
				 colors: {
				 type: "string",
				 title: "Colori disponibili",
				 description: "Separare i valori con una virgola",
				 placeholder: "es. blu, rosso, bianco"
				 },
				 sizes: {
				 type: "string",
				 title: "Taglie disponibili",
				 description: "Separare i valori con una virgola",
				 placeholder: "es. small, large, extra large"
				 }*/
			},
			required: ["productName", "productCategory", "productPrice", "quantity"]
		},
		form: [
			/*{
			 key: "productCategory",
			 feedback: true
			 }, {
			 key: "thumbPath",
			 type: "singleimage"
			 }, {
			 key: "productName",
			 type: "text",
			 validationMessage: "Inserire un nome per questo prodotto",
			 feedback: true,
			 htmlClass: "has-feedback"
			 }, {
			 key: "details",
			 type: "textangular"
			 }, {
			 key: "productPrice",
			 onChange:  function onChange(modelValue,form) {
			 var validatedPrice = modelValue ? modelValue.replace(',', '.').replace(/[^0-9\.]+/i, "") : 0;
			 productForm.model.productPrice = validatedPrice;
			 },
			 feedback: true,
			 htmlClass: "has-feedback"
			 }, {
			 type: "fieldset",
			 htmlClass: "row",
			 items: [
			 {
			 key: "quantity",
			 type: "string",
			 htmlClass: "col-sm-6",
			 feedback: false,
			 onChange:  function onChange(modelValue,form) {
			 var validatedQt = modelValue ? modelValue.replace(/[^0-9]+/i, "") : 0;
			 productForm.model.quantity = validatedQt;
			 }
			 }, {
			 type: "fieldset",
			 title: "Disponibilità",
			 notitle: false,
			 htmlClass: "col-sm-6 control-label",
			 items: [{
			 key: "availability",
			 type:"switch"
			 }]
			 }
			 ]
			 },*/
			"*",{
				"type": "submit",
				"style": "pull-right btn-info",
				"title": "Salva"
			}
		],
		model: {}
	};

	return productForm;
})
.factory('categoryForm', function (storeProvider) {
	return {
		schema: {
			type: "object",
			properties: {
				"categoryName": {
					type: "string",
					title: "Nome Categoria"
				},
				"categoryID": {
					type: "string",
					title: "ID Cateogria",
					readonly: true
				},
				required: ["categoryName", "categoryID"]
			}
		},
		form: [
			{
				key: "categoryName",
				type: "string",
				feedback: true,
				htmlClass: "has-feedback"
			}, {
				"type": "submit",
				"style": "btn-info",
				"title": "Salva"
			}
		],
		model: {}
	};
})
.factory('orderForm', function (storeProvider) {
	// order status:  canceled, pending, paid, shipped
	return {
		schema: {
			type: "object",
			properties: {
				"orderID": {
					type: "string",
					title: "ID Ordine",
					readonly: true
				},
				"date": {
					type: "string",
					title: "Data",
					readonly: true
				},
				"status": {
					type: "string",
					title: "Stato",
					enum: [
						"pending",
						"paid",
						"shipped",
						"canceled"
					]
				},
				"customer": {
					type: "object",
					properties: {
						"name": {
							type: "string",
							title: "Nome"
						},
						"last_name": {
							type: "string",
							title: "Cognome"
						},
						"email": {
							type: "string",
							title: "Email"
						},
						"phone": {
							type: "string",
							title: "Telefono"
						},
						"address": {
							type: "string",
							title: "Indirizzo"
						},
						"city": {
							type: "string",
							title: "Città"
						},
						"province": {
							type: "string",
							title: "Provincia"
						},
						"zip": {
							type: "string",
							title: "CAP"
						},
						"country": {
							type: "string",
							title: "Stato"
						}
					}
				}/*,
				 "currency": {
				 type: "string",
				 title: "Valuta",
				 enum: [
				 "EUR",
				 "DOLLAR",
				 "POUND"
				 ]
				 }*/,
				"message": {
					type: "string",
					title: "Messaggio cliente",
					readonly: true
				},
				"shipping": {
					type: "string",
					title: "Costi di spedizione"
				},
				"items": {
					type: "array",
					title: "Prodotti",
					"items": {
						type: "object",
						title: "Prodotto",
						properties: {
							"name": {
								type: "string",
								Title: "Nome Prodotto"
							},
							"size": {
								type: "string",
								title: "Taglia"
							},
							"color": {
								type: "string",
								title: "Colore"
							},
							"productID": {
								type: "string",
								title: "ID Prodotto"
							},
							"price": {
								type: "string",
								title: "Prezzo"
							},
							"quantity": {
								type: "string",
								title: "Quantità"
							}
						}
					}
				},
				"total": {
					type: "string",
					title: "TOTALE"
				}
			}
		},
		form: [
			{
				type: "fieldset",
				htmlClass: "row",
				items: [
					{
						type: "fieldset",
						title: "Info sull'ordine",
						htmlClass: "col-xs-12 col-md-4",
						items: [
							{
								key: "orderID",
								readonly: true,
								type: "displayonly"
							}, {
								key: "date",
								readonly: true,
								type: "displayonlydate"
							}, {
								key: "status",
								type: "select",
								titleMap: [
									{ value: "pending", name: "In attesa di pagamento" },
									{ value: "paid", name: "Pagato" },
									{ value: "shipped", name: "Spedito" },
									{ value: "canceled", name: "Cancellato" }
								]
							},
							{
								"type": "submit",
								"style": "btn-info pull-right",
								"title": "Salva"
							}
						]
					},
					{
						type: "fieldset",
						title: "Cliente",
						readonly: true,
						htmlClass: "col-xs-12 col-md-8",
						items: [
							{
								key: "customer.name",
								type: "displayonly"
							}, {
								key: "customer.last_name",
								type: "displayonly"
							}, {
								key: "customer.email",
								type: "displayonly"
							}, {
								key: "customer.phone",
								type: "displayonly"
							}, {
								key: "customer.address",
								type: "displayonly"
							}, {
								key: "customer.city",
								type: "displayonly"
							}, {
								key: "customer.province",
								type: "displayonly"
							}, {
								key: "customer.zip",
								type: "displayonly"
							}, {
								key: "customer.country",
								type: "displayonly"
							}, {
								key: "message",
								type: "displayonly"
							}
						]
					}
				]
			},
			{
				type: "fieldset",
				title: "Composizione dell'ordine",
				items: [
					{
						key: "items",
						readonly: true,
						type: "arraytable"
					},
					{
						key: "shipping",
						type: "displayonlywithcurrency"
					},
					{
						key: "total",
						type: "displayonlywithcurrency"
					}
				]
			}
		],
		model: {}
	};
})
.factory('configurationForm', function () {
		var configurationForm  ={
			schema: {
				type: "object",
				properties: {
					storeName: {
						type: "string",
						minLength: 2,
						title: "Nome Negozio",
						feedback: true
					},
					ownerEmail: {
						type: "string",
						title: "Email di Lavoro",
						feedback: true
					},
					UsePaypal: {
						type: "boolean",
						title: "Accetta Paypal"
					},
					businessEmail: {
						type: "email",
						title: "Indirizzo Email Paypal",
						feedback: true
					},
					UseSubmitOrder: {
						type: "boolean",
						title: "Accetta Bonifico Bancario"
					},
					BankTransferDetails: {
						type: "string",
						title: "Dettagli bonifico bancario"
					},
					ShippingCharges: {
						type: "string",
						title: "Costi di spedizione"
					}
				},
				"required": ["storeName","businessEmail","currency", "currencySymbol"]
			},
			form: [
				{
					key:"storeName",
					feedback: true,
					htmlClass: "has-feedback",
					validationMessage: "Inserire un nome per il negozio"
				},
				{
					key:"ownerEmail",
					feedback: true,
					htmlClass: "has-feedback",
					validationMessage: "Indirizzo email non valido",
					description: "Indirizzo dove riceverai le notifiche degli acquisti."
				},
				{
					key: "UsePaypal",
					type: "switch"
				}, {
					key: "businessEmail",
					type: "text",
					title: "Account PayPal",
					condition: "configurationForm.model.UsePaypal",
					description: "Indirizzo email dell'account PayPal dove ricevere i pagamenti. <a target='_blank' href='http://go.majeeko.com/paypal_create'>Apri un account in pochi minuti</a>.",
					feedback: true,
					htmlClass: "has-feedback"
				}, {
					key: "UseSubmitOrder",
					type: "switch"
				}, {
					key: "BankTransferDetails",
					condition: "configurationForm.model.UseSubmitOrder",
					type: "textarea",
					description: "Specificare IBAN, CIN/ABI/CAB/BIC, Banca di riferimento...",
					feedback: true,
					htmlClass: "has-feedback"
				},
				{
					key: "ShippingCharges",
					onChange:  function onChange(modelValue, form) {
						var validatedPrice = modelValue ? modelValue.replace(',', '.').replace(/[^0-9\.]+/i, "") : 0;
						configurationForm.model.ShippingCharges = validatedPrice;
					},
					feedback: true,
					htmlClass: "has-feedback"
				},
				{
					"type": "submit",
					"style": "btn-info",
					"title": "Salva"
				}
			],
			model: {}
		};

		return configurationForm;
	});
