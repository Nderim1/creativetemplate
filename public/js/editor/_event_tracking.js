var timers = {
	openModal: {
		'buy': 0,
		'extraAlbum': 0,
		'addPage': 0
	},
	buy: {}
};

function trackEventSync() {
	ga('send', {
		hitType: 'event',
		eventCategory: 'ButtonAction',
		eventAction: 'sync',
		eventLabel: 'sync'
	});
}

function trackEventPublishBtn(action) {
	ga('send', {
		hitType: 'event',
		eventCategory: 'ButtonAction',
		eventAction: "publish_"+action,
		eventLabel: action
	});
}

function trackEventAction(name, label) {
	ga('send', {
		hitType: 'event',
		eventCategory: 'GenericAction',
		eventAction: 'performed_' + name,
		eventLabel: label || name
	});
	if (name == "extra_change_album") {
		ga('send', {
			hitType: 'timing',
			timingCategory: 'EditAlbum',
			timingVar: 'totalEditAlbumTime',
			timingValue: Date.now() - timers.openModal.extraAlbum
		});
	}
}

function trackEventMenuPanel(name) {
	ga('send', {
		hitType: 'event',
		eventCategory: 'MenuPanel',
		eventAction: 'panel_' + name,
		eventLabel: 'panel_' + name
	});
}

function trackEventSidebar(open) {
	ga('send', {
		hitType: 'event',
		eventCategory: 'ButtonAction',
		eventAction: 'sidebar',
		eventLabel: open ? 'sidebar_open' : 'sidebar_close'
	});
}

function trackEventButton(action) {
	ga('send', {
		hitType: 'event',
		eventCategory: 'ButtonAction',
		eventAction: action,
		eventLabel: action
	});
}

function trackEventModal(name) {
	if (name == "open_buy") {
		trackEventBuy("start");
	}
	if (name == "open_edit_extra_album") {
		timers.openModal.extraAlbum = Date.now();
	}
	if (name == "open_add_page") {
		timers.openModal.addPage = Date.now();
	}
	ga('send', {
		hitType: 'event',
		eventCategory: 'OpenModal',
		eventAction: "modal_" + name,
		eventLabel: name
	});
}

function trackEventNewPage(type) {
	ga('send', {
		hitType: 'timing',
		timingCategory: 'AddPage',
		timingVar: 'totalAddPageTime_' + type,
		timingValue: Date.now() - timers.openModal.addPage
	});
}

function trackEventBuy(name, from) {
	if (name == "start") {
		timers.openModal.buy = Date.now();
		timers.buy.start = Date.now();

		$("#purchaseForm").append("<img height='1' width='1' border='0' src='https://www.facebook.com/tr?id=421915611342521&ev=InitiateCheckout&noscript=1' />");
	}else if (name == "close") {
		timeDiffmill = Date.now() - timers.buy.start;
		timeDiff10sec = Math.round(timeDiffmill / 10000) * 10;
		ga('send', {
			hitType: 'timing',
			timingCategory: 'Buy',
			timingVar: 'timeToClose',
			timingValue: Date.now() - timers.openModal.buy
		});
		ga('send', {
			hitType: 'event',
			eventCategory: 'BuyModal',
			eventAction: 'timeToClose',
			eventLabel: timeDiff10sec
		});
	}else {
		timers.buy[name] = Date.now();
		if(from){
			timeDiffmill = timers.buy[name] - timers.buy[from];
			timeDiff10sec = Math.round(timeDiffmill / 10000) * 10;
			ga('send', {
				hitType: 'timing',
				timingCategory: 'Buy',
				timingVar: 'time_' + from + "_to_" + name,
				timingValue: timeDiffmill
			});
			ga('send', {
				hitType: 'event',
				eventCategory: 'BuyModal',
				eventAction: 'time_' + from + "_to_" + name,
				eventLabel: timeDiff10sec
			});
		}
	}
	ga('send', {
		hitType: 'event',
		eventCategory: 'BuyModal',
		eventAction: name,
		eventLabel: name
	});
}
