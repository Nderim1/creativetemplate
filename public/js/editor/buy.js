angular.module('editor')

	.service('domainService', function ($http, $q, rx) {
		this.suggestionsStream = function (query) {
			console.info('Fetching domain name suggestions for query:', query);
			query = query.split(".")[0]; // stripping tld
			return rx.Observable
				.fromPromise($http.get('/api/domain/suggest?q=' + query))
				.map(function (response) {
					if (!response) {
						return "ERROR:CLIENT_GOT_NO_RESPONSE";
					}
					if (!response.data) {
						return "ERROR:CLIENT_GOT_NO_RESPONSE_DATA";
					}
					if (response.data.length > 0) {
						return response.data;
					}
					return "ERROR:CLIENT_GOT_NO_RESPONSE";
				});
		};
	})
	.service("$PaymentApiService", function($http, apiUrl, pageData){
		this.getOrder = function(type){
			var api_url = apiUrl.order_base;
			return $http.post(api_url, {
				query:{
					facebook_page_id: pageData.id,
					type: type
				}
			});
		};
		this.setOrder = function(obj){
			var api_url = apiUrl.order_setup + obj._id;
			return $http.post(api_url, obj);
		};
	})

	.controller('editorPublishCtrl', function ($scope,
											   $PaymentApiService,
											   $scope,
											   $http,
											   $q,
											   pageData,
											   domainService,
											   observeOnScope,
											   apiUrl,
											   userData) {
		//$(".body-row").eqHeight(".body-col");
		// Init Purchase Info

		$scope.demodomain = pageData.username.replace('-', '').replace('_', '') + ".it";
		$scope.pms = { // purchase modal scope
			inputDomain: $scope.demodomain,
			step: "loading",
			purchaseInfo : {
				email: angular.copy(_.get(pageData.configuration, "settings.mail", _.get(userData, "facebook.profile._json.email"))) ///userData.facebook.profile._json.email) || angular.copy(_.get($scope.choices, "settings.mail"))) || ''
			}
		};

		var type = "new";
		if(pageData.paid){
			type = "upgrade";
		}
		$PaymentApiService.getOrder(type).then(function(order_data){
			var orderObj = order_data.data.order;
			$scope.pms.orderObj = orderObj;
			$scope.pms.order_url = apiUrl.order_base + "render/" + orderObj._id;
			if(orderObj.status == "new"){
				if(orderObj.type == "upgrade"){
					// First purchase
					$scope.demodomain = _.first(_.get(pageData,"domains", ["nome-dominio.it"]));
					$scope.pms.step = "upgrade";
					$scope.pms.purchaseInfo.service = 'upgrade';
				}else{
					// First purchase
					$scope.demodomain = pageData.username.replace('-', '').replace('_', '') + ".it";
					$scope.pms.suggestions = [];
					$scope.pms.loadingSuggestions = false;
					$scope.pms.inputDomainValid = true;
					$scope.pms.inputDomainAvailable = false;
					$scope.pms.errorDomainGeneric = false;
					$scope.pms.inputDomain = $scope.demodomain;
					$scope.pms.step = "domain";

					$scope.pms.purchaseInfo.service = 'pro';
					if(_.has(orderObj,"billing_info.email")){
						$scope.pms.purchaseInfo.email = _.get(orderObj,"billing_info.email");
					}
					if(_.first(_.get(orderObj,"domains"))){
						$scope.demodomain = _.first(_.get(orderObj,"domains"));
						$scope.pms.inputDomain = angular.copy($scope.demodomain);
					}
				}
			}else if(orderObj.status == "pending"){
				$scope.pms.step = "pending";
			}else if(orderObj.status == "pending_billing" || orderObj.status == "paid"){
				$scope.pms.step = "pending_billing";
			}else if(orderObj.status == "pending_payment"){
				$scope.pms.step = "pending_payment";
			}else if(orderObj.status == "completed"){
				$scope.pms.step = "completed";
			}else{
				try{
					$zopim.livechat.addTags("errore_ordine_corrotto", "errore_ordine_id_"+orderObj._id, "errore_ordine_status_" + orderObj.status, "errore_ordine_type_" + orderObj.type);
				}catch (e){}
			}
			try {
				$zopim.livechat.addTags("ordine_id_" + orderObj._id);
			}catch (e){}
			console.log("Order ID:",orderObj._id);
			trackEventBuy("service:"+orderObj.type + "-status:"+orderObj.status);

		}, $scope.orderError);

		$scope.confirmOrder = function(){
			var setup_info = {
				_id: _.get($scope.pms,"orderObj._id"),
				email: _.get($scope.pms,"purchaseInfo.email"),
				service: _.get($scope.pms,"purchaseInfo.service"),
				domains: []
			};

			if(_.get($scope.purchaseForm, "domain.$valid")) {
				setup_info.domains.push(_.get($scope.pms,"inputDomain"));
			}

			$scope.pms.suggestions.filter(function(d){return d.active;}).forEach(function(d){
				setup_info.domains.push(d.domainName);
			});

			$PaymentApiService.setOrder(setup_info).then(function(response){
				if(_.get(response, "data.error")){
					var error_type = _.get(response, "data.type");
					if(error_type == "INVALID-DOMAIN"){
						$scope.pms.step = 'domain';
						$scope.pms.errorDomainGeneric = true;
					}else{
						alert("Errore in fase di inserimento dell'ordine. Riprovare più tardi.");
					}
					return;
				}
				trackEventBuy("SUBMIT-"+"service:"+$scope.pms.orderObj.type, "start" );
				window.location.replace(apiUrl.order_display + $scope.pms.orderObj._id);
			}, $scope.orderError);


		};

		$scope.orderError = function(error){
			console.log(error);
		};

		var domainSearchStream = observeOnScope($scope, 'pms.inputDomain')
			.filter(function (searchString) {
				return searchString.newValue && searchString.newValue.length >= 3;
			})
			.map(R.prop('newValue'))
			.do(function (searchString) {
				$scope.pms.loadingSuggestions = true;
				$scope.pms.suggestions = [];

				if(!$scope.$$phase) $scope.$apply();
			})
			.debounce(500)
			.flatMapLatest(domainService.suggestionsStream)
			.subscribe(function (suggestions) {
				if(_.get($scope.pms,"purchaseInfo.service") == "upgrade"){
					return;
				}
				if (['ERROR:DOMAIN_GENERIC_ERROR', 'ERROR:INVALID_DOMAIN'].indexOf(suggestions) > -1) {
					$scope.pms.inputDomainValid = false;
					return;
				}
				$scope.pms.inputDomainValid = true;
				if (['ERROR:NO_SUGGESTIONS', 'ERROR:UNABLE_TO_SERVE_SUGGESTIONS', "ERROR:CLIENT_GOT_NO_RESPONSE", "ERROR:CLIENT_GOT_NO_RESPONSE_DATA"].indexOf(suggestions) > -1 || !Array.isArray(suggestions)) {
					$scope.pms.errorDomainGeneric = true;
					return;
				}
				$scope.pms.errorDomainGeneric = false;
				$scope.pms.suggestions = [];
				_.forEach(suggestions, function(suggestion){
					if(_.get(suggestion,"domainName") === _.get($scope, 'pms.inputDomain')){
						$scope.pms.inputDomainAvailable = _.get(suggestion, "availability") == "AVAILABLE";
					} else if(_.get(suggestion, "availability") == "AVAILABLE"){
						$scope.pms.suggestions.push(suggestion);
					}
				});
				$scope.pms.loadingSuggestions = false;
				console.info('Successfully fetched domain name suggestions', suggestions);

			}, function (err) {
				console.error('Error fetching domain name suggestions', err);
				$scope.loadingSuggestions = false;
				$scope.pms.errorDomainGeneric = true;
			});

		$scope.trackEventBuy = function (a, b) {
			trackEventBuy(a, b);
		};

	});
