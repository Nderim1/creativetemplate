$(function () {
	$('[data-toggle="popover"]').popover({
		trigger: 'hover',
		viewport: { selector: 'body', padding: -100 }
	})
})


angular.module('orderpage', ['credit-cards'])
	.controller('PaymentController', ['$scope', '$PaymentService', "orderDetails", function($scope, $PaymentService, orderDetails) {

		$scope.loading = false;
		$scope.display_coupon_form = false;
		$scope.paymentMethod = "credit_card";
		$scope.creditCardMonthOptions = [];
		for(var i = 1; i <= 12; i++) $scope.creditCardMonthOptions.push(pad(i));
		$scope.creditCardYearOptions = [];
		var thisMonth = pad(new Date().getMonth() + 1);
		var thisYear = new Date().getFullYear();
		for(var j = thisYear; j <= thisYear + 18; j++) $scope.creditCardYearOptions.push(pad(j));

		$scope.creditCard = {
			number: "",
			expire_month: thisMonth,
			expire_year: pad(thisYear),
			cvv: ""
		};

		var checkout;
		var braintree_token;

		// INIT for PayPal
		$PaymentService.getBraintreeToken().then(function(response){
			braintree_token = response.data;
			braintree.setup(braintree_token, "custom", {
				onReady: function (integration) {
					checkout = integration;
				},
				paypal: {
					container: "paypalContainer",
					amount: orderDetails.total_amount,
					currency: "EUR",
					locale: "it_it",
					displayName: "Majeeko - Sync s.r.l.",
					singleUse: true,
					onUnsupported: handlePaymentError,
					onCancelled: handlePaymentError,
					enableShippingAddress: 'true',
					enableBillingAddress: 'true',
					headless: true
				},
				onPaymentMethodReceived: function(obj){
					$PaymentService.executePayment("paypal", obj).then(handlePaymentResponse, handlePaymentError);
				}
			});
		}, function(e){handlePaymentError(e)});

		$scope.submit = function() {
			if($scope.paymentMethod == "credit_card"){
				if(!$scope.creditCardForm.$valid){
					return;
				}

				$scope.loading = true;
				var client = new braintree.api.Client({clientToken: braintree_token});
				client.tokenizeCard({
					number: $scope.creditCard.number,
					expirationMonth: $scope.creditCard.expire_month,
					expirationYear: $scope.creditCard.expire_year,
					cvv: $scope.creditCard.cvv
				}, function (err, nonce) {

					if(err){
						handlePaymentError(err);
						return false;
					}
					$PaymentService.executePayment("braintree", {nonce: nonce}).then(handlePaymentResponse, handlePaymentError);
				});
			}else if($scope.paymentMethod == "paypal"){
				checkout.paypal.initAuthFlow();
			}else if($scope.paymentMethod == "banktransfer"){
				$scope.loading = true;
				$PaymentService.executePayment("banktransfer").then(handlePaymentResponse, handlePaymentError);
			}
		};

		$scope.paymentFailed = function(error){
			$scope.loading = false;
			console.log("ERROR PAYMENT");
			console.log(error);
			var message = "Errore nel pagamento.";
			if(error.status == 500){
				message = "Impossibile procedere con il pagamento online.";
			}else if(error.data.payment_status == "failed"){
				message = "Errore nel processo di pagamento.";
			}
			if(error.data.braintree_result){
				message = "Impossibile completare il pagamento con carta di credito.";
				if( error.data.braintree_result.message){
					message = message + "<br><small><i>" + error.data.braintree_result.message + "</i></small>";
				}
			}
			$("#error_container").html('<div class="alert alert-dismissible alert-danger"><button type="button" class="close" data-dismiss="alert"><i class="fa fa-times"></i></button>' + message + '</div>');
		};
		$scope.paymentSucceeded = function(){
			location.reload();
		};

		function handlePaymentResponse(response){
			if(response.data && response.data.payment_status == "success"){
				$scope.paymentSucceeded();
			}else{
				$scope.paymentFailed(response);
			}
		}
		function handlePaymentError(error){
			$scope.paymentFailed(error);
		}

	}])
	.controller('BillingController', ['$scope', "orderDetails", function($scope, orderDetails){
		$scope.billing_info = orderDetails.billing_info;
	}])
	.service("$PaymentService", function($http, orderDetails){
		this.getBraintreeToken = function(){
			return $http.get('/client_token');
		};
		this.executePayment = function(paymentMethod, apiResponse){
			var api_url = "/purchase/orders/pay";
			if(paymentMethod == "banktransfer"){
				api_url = "/purchase/orders/moneytransfer";
			}
			return $http.post(api_url, {
				apiResponse: apiResponse || null,
				orderId: orderDetails.id,
				paymentMethod: paymentMethod
			});
		};
	});

function pad(n) {
	return (n < 10) ? ("0" + n) : n.toString();
}
