angular.module('page-preview-generator', ['ui.bootstrap', 'ui.bootstrap.typeahead'])

.controller('previewGeneratorCtrl', function (
	$scope
  , $http
  , $sce
) {
	$scope.searchPage = function (query) {
		console.debug('Searching page...', query);
		return $http.get('/api/page-search', {
			params: {
				q: query
			}
		}).then(function (res) {
			console.debug('Page search results:', res.data.data);
			return res.data.data.slice(0,5);
		})
	}

	$scope.createPreview = function (form) {
		console.debug('Create preview for page with id:', $scope.selectedPage.id)
		$scope.state = 'generating-preview';
		return $http.post('/api/generate-preview', {
			id: $scope.selectedPage.id
		}).then(function (res) {
			$scope.preview = {
				page_name: $scope.selectedPage.name,
				url: $sce.trustAsResourceUrl(res.data.url)
			}
			console.debug('Preview generated');
			$scope.state = 'preview-ready';
		})
	}
})
