var parseLinks = function (text) {
	return text.replace(/(https{0,1}:\/\/[a-zA-Z0-9\-_\.\/]+)/gmi,'<a href="$1" target="_blank">$1</a>')
}

var parseHashtags = function (text) {
	return text.replace(/#([a-zA-Z0-9\-_\.\/]+)/gmi, '<a href="https://twitter.com/search?q=%23$1" target="_blank">#$1</a>')
}

var parseDescription = function (text) {
	return R.compose(parseHashtags, parseLinks)(text)
}

var stripslashes = function (str) {
	str = str.replace(/\\'/g, '\'');
	str = str.replace(/\\"/g, '"');
	str = str.replace(/\\0/g, '\0');
	str = str.replace(/\\\\/g, '\\');
	return str;
}

var equalHeigh = function(a,b){
	if(a.height()< b.height()){
		a.height(b.height());
	}else{
		b.height(a.height());
	}
}
