angular.module('template', [
	'template.pages',
	'template.contacts',
	'template.modals',
	'directives.clamp'
])

angular.module('template.pages', [
	'ngCookies',
	'rx'
])

.service('cookiesService', function (
	$cookies
) {
	this.cookiesAccepted = $cookies.get('cookies_accepted')
	this.acceptCookies = function () {
		$cookies.put('cookies_accepted', true, { expires: moment().add(1,'year').toISOString() })
	}
})

.controller('page', function(
	$scope,
	$modal,
	$window,
	$timeout,
  	cookiesService,
  	rx
) {
	$scope.cookiesAccepted = cookiesService.cookiesAccepted
	$scope.acceptCookies = function () {
		cookiesService.acceptCookies()
		$scope.cookiesAccepted = true
	}

	if(!$scope.cookiesAccepted) {
		rx.Observable.fromEvent(angular.element($window), 'scroll')
		.take(1)
		.subscribe(function() {
			$timeout($scope.acceptCookies)
		})
	}

	$scope.openModal = function (url,title,templateUrl,containerTemplateUrl) {
		$modal.open({
			templateUrl: templateUrl || 'standard-modal.tpl'
		  , scope: _.extend($scope.$new(true), {
		  		title: title
			  , url: url
			})
		  , windowTemplateUrl: containerTemplateUrl || null
		})
	}
	// $scope.openModal('/news/321447531391992_363902443813167', 'Some title')
})

.directive('postDescription', function () {
	return {
		restrict: 'C',
		link: function (scope,el,attrs) {
			// console.log('here i am', parseDescription, el)
			el.html(parseDescription(el.html()))
		}
	}
})
.directive('displaytime', [function() {
return {
	template: function(element, attrs){
		/* powered by momentJs */

		/* Getting the desired format */
		var custom_format = "short";
		if (attrs.customFormat){
			custom_format = attrs.customFormat;
		}

		/* Detecting single or multiple days/times in a day */
		var startTime = moment(attrs.displaytime, "YYYY-MM-DD\THH:mm:ssZ");
		var endTime = false;
		if (attrs.endTime){
			endTime = moment(attrs.endTime, "YYYY-MM-DD\THH:mm:ssZ");
		}

		if(!endTime){
			// single time, single day
			if(custom_format == "short"){
				return moment(startTime).format('l');
				//return moment(startTime).format('l LT');
			}else{
				return moment(startTime).format('LLLL');
			}
		}
		if(moment(startTime).dayOfYear() === moment(endTime).dayOfYear() && moment(startTime).year() === moment(endTime).year() ){
			// multiple times - same day
			if(custom_format == "short"){
				return moment(startTime).format('l LT') + " - " + moment(endTime).format('LT');
			}else{
				return moment(startTime).format('LLLL') + " - " + moment(endTime).format('LT');
			}
		}else{
			// multiple times - different days
			if(custom_format == "short"){
				return moment(startTime).format('l') + " - " + moment(endTime).format('l');
			}else{
				return moment(startTime).format('LLL') + " - " + moment(endTime).format('LLL');
			}
		}
	}
};
}])

// template.contacts module

angular.module('template.contacts', ['serverSideData'])

.controller('contactsCtrl', function (
	$scope
  , $http
  , CAPTCHA_TOKEN
) {
	$scope.contactFormData = { captcha_token : CAPTCHA_TOKEN }
	$scope.sendContactForm = function() {
		$scope.formSuccess = $scope.formError = false;
		$scope.sending = true
		console.debug('sending contact form', $scope.contactForm, $scope.contactFormData)
		$http.post('/send-contact-form', $scope.contactFormData)
			.success(function (res) {
				console.debug(res)
				if(res.success) {
					$scope.formSuccess = true;
				} else {
					$scope.formError = true;
				}
				$scope.sending = false
			})
			.error(function (err) {
				console.debug('Error', err)
				$scope.formError = true;
				$scope.sending = false
			})
	}
})

// template.modals module

angular.module('template.modals', ['ui.bootstrap'])

.directive('modal', function() {
	return {
		restrict: 'A'
	  , link: function(scope,el,attrs) {
	  	el.bind('click', function(event) {
	  		scope.openModal(attrs.href,stripslashes(attrs.title),attrs.modal,attrs.modalContainer)
	  		return false
	  	})
	  }
	}
})
