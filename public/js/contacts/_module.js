function CustomMarker(latlng, map) {
	this.latlng_ = latlng;
	this.setMap(map);
}

CustomMarker.prototype = new google.maps.OverlayView();

CustomMarker.prototype.draw = function() {
	var me = this;
	var div = this.div_;
	if (!div) {
		div = this.div_ = document.createElement('DIV');
		div.className = "map_marker";
		div.style.border = "none";
		div.style.position = "absolute";
		div.style.paddingLeft = "0px";
		div.style.cursor = 'pointer';

		var img = document.createElement("img");
		img.src = "/images/logo_mono.png";
		img.style.width = "210px";
		img.style.height = "71px";
		div.appendChild(img);

		google.maps.event.addDomListener(div, "click", function(event) {
			google.maps.event.trigger(me, "click");
		});

		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}

	var point = this.getProjection().fromLatLngToDivPixel(this.latlng_);
	if (point) {
		div.style.left = point.x + 'px';
		div.style.top = point.y + 'px';
	}
};

CustomMarker.prototype.remove = function() {
	if (this.div_) {
		this.div_.parentNode.removeChild(this.div_);
		this.div_ = null;
	}
};

CustomMarker.prototype.getPosition = function() {
	return this.latlng_;
};

angular.module('contacts', ['vcRecaptcha'])

.controller('contactsCtrl', function (
	$scope
  , $http
) {

	/* Flags */
	$scope.contactCaptchaValid = false;
	$scope.contactFormValid = false;
	$scope.contactFormShowError = {
		validation: false,
		privacy:false,
		captcha: false,
		emailaddress: false,
		cannotsend: false
	};
	$scope.contactFormSuccess = false;
	$scope.contactFormSending = false;


	/* Initialising all inputs */
	$scope.contactFormData = {};

	var fields = ['name','surname','email','message'];
	for (var i = 0; i< fields.length; i++){
		var f = fields[i];
		$scope.contactFormData[f] = {
			'content' : "",
			'valid' : false
		};
	}
	$scope.contactFormData.privacy = false;

	/* Validation function */
	$scope.validateContactForm = function(){
		$scope.cleanErrors();
		$scope.contactFormValid = true;

		if(!$scope.contactCaptchaValid){
			$scope.contactFormValid = false;
		}

		for (var i = 0; i< fields.length; i++){
			var f = fields[i];

			if ($scope.contactFormData[f].content == undefined){
				$scope.contactFormData[f].valid = false;
				$scope.contactFormValid = false;
			}else{
				if($scope.contactFormData[f].content.length<3){
					$scope.contactFormData[f].valid = false;
					$scope.contactFormValid = false;
				}else{
					$scope.contactFormData[f].valid = true;
				}
			}
		}

		var regex_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
		if(!($scope.contactFormData.email.content == undefined) && !regex_email.test($scope.contactFormData.email.content)){
			$scope.contactFormData.email.valid = false;
			$scope.contactFormValid = false;
		}
	};

	/* Recaptcha validation */
	$scope.recaptchaSuccess = function (recaptchaResponseCode) {
		$scope.contactFormData['g-recaptcha-response'] = recaptchaResponseCode;
		$scope.contactCaptchaValid = true;
		$scope.validateContactForm();
	};
	$scope.recaptchaExpire = function () {
		$scope.contactFormData['g-recaptcha-response'] = null;
		$scope.contactCaptchaValid = false;
		$scope.validateContactForm();
	};
	/* Sending the form */
	$scope.sendContactForm = function() {
		$scope.cleanErrors();
		if(!$scope.contactFormValid){
			$scope.contactFormShowError.validation = true;
			return false;
		}
		if(!$scope.contactFormData.privacy){
			$scope.contactFormShowError.privacy = true;
			return false;
		}
		$scope.contactFormSending = true;

		$http.post('/send-contact-form', $scope.contactFormData)
			.success(function (res) {
				$scope.resultForm(res.success, res.message);
			})
			.error(function (err) {
				$scope.resultForm(false, err.message);
		});
	};
	/* Showing result */
	$scope.resultForm = function(success, message){
		$scope.contactFormSending = false;
		if(success){
			$scope.contactFormSuccess = true;
		}else{
			if(message === "error-captcha"){
				$scope.contactFormShowError.captcha = true;
			}else if(message === "error-email-address"){
				$scope.contactFormShowError.emailaddress = true;
			}else if(message === "error-sending-mail"){
				$scope.contactFormShowError.cannotsend = true;
			}else{
				$scope.contactFormShowError.cannotsend = true;
			}
		}
	};
	$scope.cleanErrors = function(){
		for(e in $scope.contactFormShowError){
			$scope.contactFormShowError[e] = false;
		}
	};
})

.directive('gmap', function() {
	return {
		restrict: 'A',
		link: function (scope,el,attrs) {
			var styles = [
				{stylers: [{hue: "#4ca2bf"}, {saturation: -70}]},
				{featureType: "road",elementType: "geometry",stylers: [{lightness: 0}, {visibility: "simplified"}]},
				{featureType: "road",elementType: "labels",	stylers: [{visibility: "on"}]}];
	  		var opts = {
	  			zoom: 17,
	  			center: new google.maps.LatLng( 41.8989167,12.5033611 ),
	  			mapTypeId: google.maps.MapTypeId.ROADMAP,
	  			styles: styles,
	  			disableDefaultUI: false,
	  			scrollwheel: false,
	  			navigationControl: true,
	  			mapTypeControl: false,
	  			scaleControl: true,
	  			// draggable: false,
	  		};
			var majeeko_position = new google.maps.LatLng( 41.8989167,12.5033611 );
			setTimeout(function(){
				var map = new google.maps.Map(el[0], opts);
				var marker = new google.maps.Marker({
					position: majeeko_position,
					map: map,
					icon: '/images/maps/spotlight-poi-orange.min.png'
				});
				var infowindow = new google.maps.InfoWindow({
					content: '<div style="color: #F90; font-weight: bold; font-size: 1.2em; ">Sync s.r.l.</div><div style="color:#333;">Via Giovanni Giolitti 34 c/o Luiss Enlabs<br>00185 Roma Italia<br>p.iva 13278551000</div>'
				});
				infowindow.open(map, marker);
				marker.addListener('click', function() {
					infowindow.open(map, marker);
				});
				//google.maps.event.trigger(map, 'resize');
				map.setCenter(majeeko_position);

			},5);
		}
	}
})
