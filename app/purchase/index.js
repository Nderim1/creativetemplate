var moneyTransferMethod = require('./methods/money-transfer'),
	braintree = require('./braintree');

module.exports.createMoneyTransfer = moneyTransferMethod.create;
module.exports.braintree = braintree;