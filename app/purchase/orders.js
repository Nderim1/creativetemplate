var db = require('../db'),
	moment = require('moment'),
	coupons = require('../utils/coupons'),
	prices = require('./prices'),
	_ = require('lodash'),
	Q =  require('q'),
	logger = require('../utils/logger'),
	mail = require('../utils/mail');

function Order(options) {
	if (this instanceof Order) {
		this.user_id = options.user_id || undefined;
		this.page_id = options.page_id || undefined;
		this.facebook_page_id = options.facebook_page_id || undefined;
		this.billing_info = options.billing_info || null;
		this.cart = options.cart || [];
		this.total = options.total || 0;
		this.payment_info = options.payment_info || null;
		this.type = options.type || "";
		this.status = options.status || "new";
		this.insert_date = options.insert_date;
		this.date = options.date || options.time || moment().toISOString();
		this.affiliate = options.affiliate || null;
		this.domains = options.domains || [];
		this._id = options._id;
		logger.info("Order Instance " + this._id + " created!");
	} else {
		return new Order(options);
	}
}

Order.prototype.save = function save() {
	var that = this;
	return this.calculateTotalPrice()
		.then(function(){
			that.date = moment().toISOString();
			return db._raw.orders.findAndModify({ query: {_id: db._raw.ObjectId(that._id)}, update: { $set: _.omit(that, ["_id"]) }, new: true }).then(function(order) {
				that.updateInstance(order);
				return that;
			});
		}).catch(function (err) {
			logger.error(err);
		});
};

Order.prototype.updateInstance = function updateInstance(order) {
	for(var prop in order) {
		if(this.hasOwnProperty(prop)) {
			this[prop] = order[prop];
		}
	}
	return this;
};

Order.prototype.complete = function complete() {
	var that = this;
	this.status = "completed";
	this.payment_info.payment_received_on = moment().toISOString();
	return this.save().then(function (order) {
		mail.sendMailToBuyDomains(order);
		mail.sendThanksToCustomer(order);
		return order;
	});
};

Order.prototype.calculateTotalPrice = function calculateTotalPrice() {
	var that = this;
	logger.info("Calculating total for " + this._id +" order.");
	var coupon = _.findWhere(this.cart, { type: "coupon"}),
		deferred = Q.defer(),
		total = 0;

	var domainIteration = 0;

	that.domains = [];

	_.forEach(this.cart, function(item){
		item.price = undefined;
		if(item.type == "service"){
			item.price = prices[item.value];
		}else if(item.type == "domain" || item.type == "domain_transfer"){
			item.price = (domainIteration > 0) ? prices.domain : 0;
			that.domains.push(item.value);
			domainIteration += 1;
		}
		if(item.price){
			total += item.price;
		}
	});

	if(coupon) {
		coupons.getDiscount(coupon.value, {return_obj: true})
		.then(function(coupon_obj){
			if(that.isApplicableCoupon(coupon_obj)){
				var discount = Math.abs(coupon_obj.value);
				coupon.price = discount * -1;
				that.total = Math.max(0, total - discount);
				deferred.resolve(total);
			}else{
				_.remove(that.cart, function(i){
					return i.type == "coupon";
				});
				deferred.resolve(total);
			}

		});
	} else {
		this.total = total;
		deferred.resolve(total);
	}
	return deferred.promise;
};

Order.prototype.getKind = function getKind() {
	for(var i = 0; i < this.cart.length; i++){
		if(this.cart[i].type == "service"){
			return "" + this.type + "-" + this.cart[i].value;
		}
	}
	return this.type;
};

Order.prototype.isApplicableCoupon = function isApplicableCoupon(coupon_obj) {
	var coupon_available_kind = coupon_obj.appliedTo;
	if ( coupon_available_kind.indexOf("upgrade") >= 0){
		coupon_available_kind.push("upgrade-upgrade");
	}
	return coupon_available_kind.indexOf(this.getKind()) >= 0;
};

Order.prototype.applyCoupon = function applyCoupon(coupon_obj) {
	var coupon_name = coupon_obj.couponStr;
	if (this.isApplicableCoupon(coupon_obj)){
		var coupon_index = _.findIndex(this.cart, function(item) { return item.type == 'coupon'; });
		if(coupon_index == -1){
			this.cart.push({
				type: "coupon",
				value: coupon_name
			});
		}else{
			this.cart[coupon_index].value = coupon_name;
		}
		return true;
	}else{
		return false;
	}
};

Order.getById = function getById(id) {
	var deferred = Q.defer();
	try{
		var _id =  db._raw.ObjectId(id);
	}catch (e){
		deferred.reject(e);
	}
	db._raw.orders.findOne({_id: _id}).then(function (order) {
		order = new Order(order);
		deferred.resolve(order);
	}).catch(function(err){
		logger.error("Majeeko order not found.");
		deferred.reject(err);
	});
	return deferred.promise;
};

Order.getCollection = function getCollection(options) {
	options = options || {};

	return db._raw.orders.find(options).then(function (orders) {
		if(orders) {
			var orderInstances = [];
			orders.forEach(function (order, orderIndex, ordersArray) {
				ordersArray[orderIndex] = new Order(order);
			});
			return orders;
		} else {
			if(_.has(options, "id")){
				throw Error("Can find the order by id: " + options.id);
			}else{
				throw Error("Can find the order by options: " + options);
			}
		}
	}).catch(function (err) {
		logger.error(err);
		return err;
	});
};

Order.findOrCreate = function create(options) {
	if(options) {
		if(options.query) {
			var query = {
				facebook_page_id: options.query.facebook_page_id,
				type: options.query.type
			};

			return db._raw.orders.findAndModify({
				query: query,
				update: {
					$setOnInsert: { status: "new", facebook_page_id: options.query.facebook_page_id, type: options.query.type, user_id: options.user_id, insert_date: moment().toISOString()}
				},
				upsert: true,
				new: true
			})
			.then(function (order) {
				return new Order(order.value);
			});
		} else {
			return db._raw.orders.insert({status: "new", user_id: options.user_id, insert_date: moment().toISOString()}).then(function (response) {
				return response;
			});
		}
	}
};

module.exports = Order;
