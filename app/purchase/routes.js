var _ = require("lodash"),
	db = require("../db"),
	router = require('express').Router(),
	Order = require('./orders'),
	logger = require('../utils/logger'),
	braintree = require('./braintree'),
	coupons = require('../utils/coupons'),
	moneytransfer = require('./methods/money-transfer'),
	pageUtils = require('../utils/pages'),
	domainService = require('../utils/domain'),
	moment = require('moment'),
	validator = require('validator');
	mail = require('../utils/mail');


router.post('/orders/pay', braintree.pay);

router.post('/orders/moneytransfer', moneytransfer.create);

router.get('/orders/render/:id', function (req, res, next){

	res.set('maxAge', '0');
	res.set('cache-control', 'private, max-age=0, no-cache');

	if (!(req.params && req.params.id)) {
		return res.status(400).send();
	}

	Order.getById(req.params.id).then(function (order) {
		if (!_.has(order, "status")) {
			return res.status(400).send();
		}

		var page_data = {
			invalidCoupon: _.get(req.query, "r") == "notValidCoupon",
			notAppliedCoupon: _.get(req.query, "r") == "notAppliedCoupon",
			insertedCoupon: _.get(req.query, "coupon"),
			order: order
		};
		if (_.get(order, "status") == "new") {
			page_data.title = 'Conferma pagamento - Inizio Ordine | Gestione Ordini Majeeko';
			res.render('order/cart-payment', page_data);
		} else if (_.get(order, "status") == "paid" || _.get(order, "status") == "pending_billing" || _.get(order, "status") == "pending") {
			page_data.title = 'Inserimento dati ordine | Gestione Ordini Majeeko';
			res.render('order/billing', page_data);
		} else if (_.get(order, "status") == "pending_payment") {
			page_data.title = 'Attesa pagamento ordine | Gestione Ordini Majeeko';
			res.render('order/pending-payment', page_data);
		} else if (_.get(order, "status") == "completed") {
			page_data.title = 'Ordine Completato | Gestione Ordini Majeeko';
			res.render('order/completed', page_data);
		} else {
			logger.error("Tried to display a order with an invalid status", {
				order: order
			});
			res.status(400).send();
		}
	}).catch(function (err) {
		logger.error(err);
		res.status(400).send();
	});
});

router.get('/orders/render/:id/changePaymentMethod', function (req, res, next){
	if (!(req.params && req.params.id)){
		return res.status(400).send();
	}
	var order_id = req.params.id;
	Order.getById(order_id).then(function (order) {
		if(!_.has(order, "status")){
			return res.status(400).send();
		}
		if (!(_.get(order, "status") == "pending" || (_.get(order, "status") == "pending_payment"))) {
			logger.error("Tried to change payment method of not pending order.", {order: order});
			return res.redirect('/purchase/orders/render/' + order_id + "?i=error_not_status");
		}
		order.status = "new";
		order.payment_info.payment_method = null;
		order.save().then(function () {
			return res.redirect('/purchase/orders/render/' + order_id + "?i=saved");
		}).catch(function (err) {
			logger.error(err);
			return res.redirect('/purchase/orders/render/' + order_id + "?i=errorsave");
		});
	});
});

router.post('/orders/render/applyCoupon', function (req, res, next) {
	var options = req.body;
	if (!options.orderId) {
		res.status(400).send();
		return;
	}
	var coupon_input = options.coupon.trim();
	if(!coupon_input) {
		return res.redirect('/purchase/orders/render/' + options.orderId + "?i=notcoupon");
	}
	return coupons.getDiscount(coupon_input, {return_obj: true})
		.then(function(coupon_obj){
			if(!coupon_obj) {
				return res.redirect('/purchase/orders/render/' + options.orderId + "?r=notValidCoupon&coupon=" + coupon_input);
			}
			// the coupon does exist, so fetch the order
			Order.getById(options.orderId).then(function (order) {
				if (!order.applyCoupon(coupon_obj)) { // cannot apply coupon
					return res.redirect('/purchase/orders/render/' + options.orderId + "?r=notAppliedCoupon&coupon=" + coupon_input);
				}
				// applied coupon
				order.save().then(function () {
					return res.redirect('/purchase/orders/render/' + options.orderId + "?i=ordersaved&r" + Date.now());
				}).catch(function (err) {
					logger.error(err);
					return res.redirect('/purchase/orders/render/' + options.orderId + "?i=notsavedcoupon");
				});
			}).catch(function(err){
				logger.error(err);
				return res.status(400).send();
			});
		});
});

router.post('/orders/render/submit', function (req, res, next){
	var options = req.body;

	if(!options.orderId){
		res.status(400).send();
		return;
	}

	// all this stuff could be moved elsewhere
	var valid_billing_info = {};
	valid_billing_info.first_name = _.get(options, "first_name", "");
	valid_billing_info.last_name = _.get(options, "last_name", "");
	valid_billing_info.email = _.get(options, "email", "");
	valid_billing_info.phone = _.get(options, "phone", "");
	valid_billing_info.address = _.get(options, "address", "");
	valid_billing_info.city = _.get(options, "city", "");
	valid_billing_info.province = _.get(options, "province", "");
	valid_billing_info.ssn = _.get(options, "ssn", "");
	valid_billing_info.business_name = _.get(options, "business_name", "");
	valid_billing_info.zip = _.get(options, "zip", "");
	valid_billing_info.vat = _.get(options, "vat", "");

	Order.getById(options.orderId).then(function (order) {
		if(!_.has(order, "status")){
			res.status(400).send();
		}else{
			if(order.status == "pending"){
				order.status = "pending_payment";
			} else if(order.status == "paid" || order.status == "pending_billing"){
				order.billing_info = valid_billing_info;
				order.complete().then(function () {
					db._raw.pages.findOne({facebook_id: order.facebook_page_id}).then(function (page) {
						var service = _.findWhere(order.cart, { type: "service" });
						var isPro = (service.value == "pro" || service.value == "upgrade") ? true : false;
						pageUtils.activatePageForDomains(page.facebook_id, order.domains, undefined, isPro);
						return res.redirect('/purchase/orders/render/' + options.orderId);
					});
				});
			} else {
				logger.error("Tried to submit a not paid order",{
					order: order
				});
				return res.status(400).send();
			}
			order.billing_info = valid_billing_info;
			order.save().then(function () {
				return res.redirect('/purchase/orders/render/' + options.orderId);
			});
		}
	});
});

router.route('/orders/setup/:id')
	.post(function (req, res, next) {
		if (!req.body) {
			return res.status(400).send();
		}

		//check if it is a valid order submission
		var _id = _.get(req.body, "_id");
		var email = _.get(req.body, "email");
		var service = _.get(req.body, "service");
		var domains = _.get(req.body, "domains",[]);
		var isNew = (["premium","pro"].indexOf(service)) > -1;

		if(!_id || !email || !service || (isNew && domains.length < 1)){
			return res.send({error: true, type:"INVALID-ORDER", d: "Init Valid"});
		}

		Order.getById(_id).then(function (order) {
			if(!(isNew && order.type == "new" || (service=="upgrade" && order.type == "upgrade") || order.status == "new" )){
				return res.send({error: true, type:"INVALID-ORDER", d: "Order type"});
			}

			order.domains = [];
			order.cart = [];
			order.cart.push({
				type: "service",
				value: service
			});

			_.set(order, "billing_info.email", email);

			if(isNew) {
				domainService.suggest(_.first(_.first(domains).split("."))) // suggestion based on the base name of the domain
					.then(function (suggestions) {
						if (!suggestions || _.some(suggestions, _.has("error"))) {
							return res.send({error: true, type: "INVALID-DOMAIN", d: "Not suggestions"});
						}
						suggestions = suggestions.filter(function (suggestion) {
							return _.contains(['AVAILABLE', 'UNAVAILABLE'], suggestion.availability);
						});

						domains.forEach(function(domain){
							var validated_domain = _.first(suggestions.filter(function(s) {return s.domainName == domain}));
							if(!_.has(validated_domain, "availability")){
								return res.send({error: true, type: "INVALID-DOMAIN", d:"Not validated domain"});
							}
							order.cart.push({
								type: _.get(validated_domain, "availability") == "UNAVAILABLE" ? "domain_transfer": "domain" ,
								value: domain
							});
							order.domains.push(domain);
						});
						order.save().then(function () {
							return res.json({order: order});
						});
					})
					.catch(function (err) {
						logger.error('Error getting domain suggestions', err);
						return res.send({error: true, type: "INVALID-DOMAIN", d:"Not fetching domain"});
					});
			}else{
				order.save().then(function () {
					return res.json({order: order});
				});
			}
		}).catch(function(err){
			logger.error('Error getting order', err);
			return res.send({error: true, type: "INVALID-ORDER", d:"Not fetching order"});
		});
	});

router.post('/orders/add/:id', function (req, res, next){
	if(req.body.item) {
		Order.findOrCreate({_id: req.params.id}).then(function (order) {
			order.cart.push(item);
			order.save().then(function () {
				res.render("fatto");
			});
		});
	} else {
		res.status(404).send();
	}
});
router.post('/orders/remove/:id', function (req, res, next){
	if(req.body.item) {
		Order.findOrCreate({_id: req.params.id}).then(function (order) {
			_.remove(order.cart, function(item) {
				return (req.body.item == item.value) && (req.body.type ==item.type);
			});
			order.save().then(function () {
				res.render("fatto");
			});
		});
	} else {
		res.status(404).send();
	}
});

router.route('/orders/')
.get(function (req, res, next) {
	Order.getCollection()
	.then(function (orders) {
		res.json({ orders: orders });
	})
	.catch(function (error) {
		res.status(500).send();
	});
})
.post(function (req, res, next) {
	logger.info("Creating new order");
	var options = req.body;
	options.user_id = req.user.id;
	Order.findOrCreate(options)
	.then(function (order) {
		res.json({ order: order });
	});
});

router.route('/orders/:id')
.get(function (req, res, next) {
	if(req.params && req.params.id) {
		Order.getById(req.params.id).then(function (order) {
			res.json({ order: order });
		});
	} else {
		res.status(400).send();
	}
})
.post(function (req, res, next) {
	if(req.body && req.body.order) {
		var order = new Order(req.body.order);
		order.save().then(function () {
			res.json({ order: order });
		});
	} else {
		res.status(400).send();
	}
});


module.exports = router;
