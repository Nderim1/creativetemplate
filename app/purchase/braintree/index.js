var braintree = require('braintree');
var Order = require('../orders');
var mail = require('../../utils/mail');
var db = require('../../db');
var pageUtils = require('../../utils/pages');
var Q = require('q');
var _ = require('lodash');
var logger = require('../../utils/logger');
var gateway = braintree.connect({
    environment:  braintree.Environment[process.env.BRAINTREE_ENVIRONMENT] || braintree.Environment.Sandbox,
    merchantId:   process.env.BRAINTREE_MERCHANT_ID,
    publicKey:    process.env.BRAINTREE_PUBLIC_KEY,
    privateKey:   process.env.BRAINTREE_PRIVATE_KEY
});

module.exports.generateClientToken = gateway.clientToken.generate.bind(gateway.clientToken);

module.exports.pay = function(req, res) {
	// apiResponse ---> nounce, billing_info(only paypal)
	if(!req.body.apiResponse && !req.body.orderId) {
		res.status(400).send({error:'Data is incomplete'});
		return;
	}
	Order.getById(req.body.orderId).then(function (order) {
		gateway.transaction.sale({
			amount: Math.round(order.total * 100) / 100,
			paymentMethodNonce: req.body.apiResponse.nonce
		}, function(err, result) {
			if (result.success) {
				order.payment_info = {
					braintree_result: result.transaction,
					payment_method: req.body.paymentMethod
				};
				order.status = "paid";
				//salva dati vendita in db
				order.save().then(function () {
					db._raw.pages.findOne({facebook_id: order.facebook_page_id}).then(function (page) {
						var service = _.findWhere(order.cart,{ type: "service" });
						var isPro = (service.value == "pro" || service.value == "upgrade") ? true : false;
						pageUtils.activatePageForDomains(page.facebook_id, order.domains, 'braintree', isPro);

						res.status(200).send({
							payment_status: "success",
							status: true
						});
					});
				});
				// res.status(200).send({message:"successful payment"});
			} else {
				logger.error("Error during braintree payment", {
					error: err,
					error_message: result.message
				});
				res.send({
					payment_status: "failed",
					braintree_error: err,
					braintree_result: result
				});
			}
		});
	});
};


