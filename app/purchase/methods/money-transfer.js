var Order = require('../orders'),
	mail = require('../../utils/mail');

module.exports.create = function (req,res) {
	if(!req.body.apiResponse && !req.body.orderId) {
		res.status(400).send({error:'Data is incomplete'});
		return;
	}


	Order.getById(req.body.orderId).then(function (order) {
		order.status = "pending";
		order.payment_info = {
					payment_method: req.body.paymentMethod
				};
		order.save().then(function () {
			mail.sendMoneyTransferInstructionsToCustomer(order.billing_info.email, "Utente", order, order.domains);
			var responseData = {
				payment_status: "success"
			};
			res.send(responseData);
		});
	});
};
