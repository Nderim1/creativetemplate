var crypto = require('crypto')
  , Q = require('q')
var TOKEN_LENGTH = 32;
 
exports.createToken = function(callback) {
	var deferred = Q.defer()
    crypto.randomBytes(TOKEN_LENGTH, function(ex, token) {
        if (ex) deferred.reject(ex);
 
        if (token) deferred.resolve(token.toString('hex'));
        else deferred.reject(new Error('Problem when generating token'));
    });
    return deferred.promise
};