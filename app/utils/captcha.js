var config = require('../config')
  , request = require('request-promise')
  , logger = require('./logger')

module.exports.verify = function(code,remoteIp) {
	return request.post('https://www.google.com/recaptcha/api/siteverify')
		.form({
			secret: config.recaptcha.secret
		  , response: code
		  , remoteip: remoteIp
		})
		.then(function (res) {
			logger.debug(res)
			return JSON.parse(res).success
		})
		.catch(function (err) {
			logger.error(err)
			return false
		})
}