var config = require('../config'),
	db = require('../db'),
	Q = require('q'),
	_ = require('lodash'),
	logger = require('./logger'),
	templateUtils = require('./templates'),
	appAccessToken = config.facebook.clientID + "|" + config.facebook.clientSecret,
	ce = require('./colour-extractor'),
 	fs = require('fs'),
	facebook = require('../facebook'),
	moment = require('moment'),
	pwAnalytics = require('./analytics/piwik'),
	siteMap = require('./google-sitemap/api');

var completePageCreation = function (page,template) {
	var deferred = Q.defer();
	logger.debug(page.pageData.picture);
	ce.extractColorsFromURL(page.pageData.picture)
		.then(function(colors) {
			page.configuration.graphics.colors.outer_background = colors[0] || page.configuration.graphics.colors.outer_background;
			page.configuration.graphics.colors.titles = colors[1] || page.configuration.graphics.colors.titles;
			logger.info('Page colors for page', page.facebook_id, colors, page.configuration.graphics.colors);
		})
		.catch(function(err) {
			logger.error(err);
		})
		.finally(function() {
			logger.info('Page colors for page (second)', page.facebook_id, page.configuration.graphics.colors);
			templateUtils.renderTemplateStyles(template,page.configuration.graphics)
			    .then(function(output) {
			        page.configuration._css = output.css;
			        db._raw.pages.update({facebook_id:page.facebook_id},page,{upsert:true})
						.then(function(page) {
							deferred.resolve(page);
						});
				});
		});
	return deferred.promise;
};

var setDefaultColors = module.exports.setDefaultColors = function (page, template) {
	var deferred = Q.defer();
	var pagePicture = _.get(page, "pageData.picture.data.url") || _.get(page, "pageData.picture");
	var templateConfiguration = _.cloneDeep(require('../templates/' + template + '/default-configuration'));
	logger.debug(pagePicture);
	ce.extractColorsFromURL(pagePicture)
		.then(function(colors) {
			var preview = {};
			_.set(preview, "graphics.colors", templateConfiguration.graphics.colors);
			preview.graphics.colors.outer_background = colors[0];
			preview.graphics.colors.titles = colors[1];
			logger.info('Page colors for page', page.facebook_id, colors, preview.graphics.colors);
			templateUtils.renderTemplateStyles(template, preview.graphics)
			    .then(function(output) {
			        preview._css = output.css;
					return deferred.resolve(preview);
				});
		})
		.catch(function(err) {
			logger.error(err);
			return deferred.reject();
		});
	return deferred.promise;
};

var getCoverPhotoDim = function(cover,albums){
	try {
	var photoId = cover.id;
	var coverPhotos = albums.data.filter(function(elem){
		return elem.name == 'Cover Photos';
	});

	var dims = coverPhotos[0].photos.data.filter(function(elem){
		return elem.id == photoId;
	}).map(function(elem){
		return {
			height:elem.height,
			width:elem.width
		};
	});
	return dims[0];
	}
	catch (e) {

		logger.info('no cover photo, returning values to use temple template');
		return {
			height:300,
			width:200
		};
	}
};

var createRaw = module.exports.createRaw = function (pageId, pageAccessToken, account) {
	var deferred = Q.defer();

	pageAccessToken = pageAccessToken || appAccessToken;

	db.getPageById(pageId)
		.then(function (page) {
			var template;
			if(!page) {

				template = _.sample(['opus','innovation', 'temple', 'parallelo']);
				page = require('../sync/default-page')(pageId, pageAccessToken, template);

				page.created_at = moment().toISOString();

				if(account) {
					page.pageData = {
						id: account.id,
						picture: account.picture.data.url,
						name: account.name,
						category: account.category
					};
					completePageCreation(page,template).then(deferred.resolve);

				} else {
					facebook.fetchMinimalPageData(pageId, pageAccessToken)
					.then(function(pageData) {
						pageData.picture = pageData.picture.data.url;
						page.pageData = pageData;
						return completePageCreation(page,template).then(deferred.resolve);
					})
					.catch(deferred.reject);
				}
			} else {
				db._raw.pages.update({facebook_id:pageId},{$set:{'fb_access_token':pageAccessToken}});
				page.fb_access_token = pageAccessToken;
				deferred.resolve(page);
			}
		})
		.catch(function (err) {
			logger.error('Error creating page', err);
		});

	return deferred.promise;
};

var createFromAccount = module.exports.createFromAccount = function (account) {
	logger.info('Creating page from account', account.id);
	return createRaw(account.id, account.access_token, account);
};

var generateWebsiteDomain = function (identifier) {
	return identifier + '.website.' + config.primary_domain;
};

var getGeneratedWebsiteDomain = module.exports.getGeneratedWebsiteDomain = function (page, args) {
	args = args || {};
	var goodDomains = page.domains ? page.domains.filter(function(d){return /^www\./.test(d);}) : false;
	if(page.paid_on && goodDomains && goodDomains.length) {
		return goodDomains[0];
	} else {
	    var identifier = !args.useId ? page.username : page.facebook_id;
	    if(!identifier) return null;
	    return generateWebsiteDomain(identifier);
	}
};

var getGeneratedWebsiteUrl = module.exports.getGeneratedWebsiteUrl = function (page, args) {
	args = args || {};
	var protocol = args.protocol || 'http',
		domain = getGeneratedWebsiteDomain(page, args);
	if(domain) {
    	return protocol + '://' + getGeneratedWebsiteDomain(page, args);
	} else {
		return null;
	}
};

var getEmbeddedWebsiteUrl = module.exports.getEmbeddedWebsiteUrl = function (page, args) {
	args = args || {};
	var protocol = args.protocol || 'http',
		domain = page.username + '.embedded.' + config.primary_domain;
	if(domain) {
    	return protocol + '://' + domain;
	} else {
		return null;
	}
};

var getGeneratedWebsiteUrlByPageUsername = module.exports.getGeneratedWebsiteUrlByPageUsername = function (pageUsername, protocol) {
	protocol = protocol || 'http';

	return protocol + '://' + generateWebsiteDomain(pageUsername);
};

var getGeneratedWebsiteUrlByPageId = module.exports.getGeneratedWebsiteUrlByPageId = function (pageId, protocol) {
	protocol = protocol || 'http';

	return protocol + '://' + generateWebsiteDomain(pageId);
};

var getEmbeddedWebsiteUrlByPageId = module.exports.getEmbeddedWebsiteUrlByPageId = function (pageId, protocol) {
	return (protocol || 'http') + '://' + pageId + '.embedded.' + config.primary_domain;
};

function activatePageForDomains(pageFacebookId, domains, activator, isPro) {
	return db
		.activatePageForDomains(pageFacebookId,domains,activator, isPro)
		.then(function(result) {
			pwAnalytics.registerSiteToPiwik(pageFacebookId,domains);
			domains
				.filter(function(domain) {
					return !(/www/.test(domain));
				})
				.forEach(function(domain){
					siteMap.activateSiteMap(domain,pageFacebookId);
				});
		})
		.catch(function(err){
			console.log('final catch',err.stack);
			return err;
		});
}
module.exports.activatePageForDomains = activatePageForDomains;
