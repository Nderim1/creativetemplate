var config = require('../../config'),
	mandrill = require('mandrill-api/mandrill'),
	mandrill_client = new mandrill.Mandrill(config.mandrill.api_key),
	Q = require('q'),
	logger = require('../logger'),
	swig = require('swig'),

	majeeko_mandrill = require('./mandrill'),
	accounting = require('accounting'),
  	_ = require('lodash');

function send(message) {
	var deferred = Q.defer();
	mandrill_client.messages.send({
		"message": message,
		"async": false
	}, function(result) {
	    logger.info(result);
	    deferred.resolve(result);
	}, function(err) {
		logger.error('A mandrill error occurred: ' + err.name, err.message);
		deferred.reject(err);
	});
	return deferred.promise;
}

module.exports.sendGeneratedWebsiteContactForm = function (to,domain,firstname,lastname,email,message,generated_domain) {
	firstname = firstname || "-";
	lastname = lastname || "-";

	message = {
		"html": "<p>Nuovo contatto dal sito " + generated_domain + ".</p><p>Nome: "+firstname+"</p><p>Cognome: "+lastname+"</p><p>Email: "+email+"</p><p>Messaggio: " + message + "</p><p>Messaggio inviato da <a href=\"http://" + domain +"\">Majeeko</a></p>",
	    "subject": "Contatto dal sito " + generated_domain,
	    "from_email": "no-reply@" + domain,
	    // "from_name": "Example Name",
	    "to": [{
            "email": to
        }],
	    "headers": {
	        "Reply-To": email
	    }
	};
	return send(message);
};

module.exports.sendContactForm = function (to,domain, firstname, lastname, email, message, generated_domain) {
	firstname = firstname || "-";
	lastname = lastname || "-";

	message = {
		"html": "<p>Nuovo contatto dal sito " + generated_domain + ".</p><p>Nome: "+firstname+"</p><p>Cognome: "+lastname+"</p><p>Email: "+email+"</p><p>Messaggio: " + message + "</p><p>Messaggio inviato da <a href=\"http://" + domain +"\">Majeeko</a></p>",
	    "subject": "Contatto dal sito " + generated_domain,
	    "from_email": "no-reply@" + domain,
	    // "from_name": "Example Name",
	    "to": [{
            "email": to
        }],
	    "headers": {
	        "Reply-To": email
	    }
	};
	return send(message);
};

module.exports.sendMailForTophostProblem = function(problem) {
	var recipients = [process.env.MAIL_ACCOUNTS_MANAGER];
	if(process.env.TOPHOST_ADMIN_EMAIL) recipients.push(process.env.TOPHOST_ADMIN_EMAIL);
	var html = swig.renderFile(__dirname + '/../../mail-templates/tophost-problem.html', {problem:problem,admin:process.env.MAIL_ACCOUNTS_MANAGER});
	var message = {
	    "html": html,
	    "subject": "Problema tecnico nella comunicazione tra Tophost e Majeeko",
	    "from_email": 'info@majeeko.com',
	    "from_name": 'Tech @ Majeeko!',
	    "to": recipients.map(function (address) { return { email: address }; })
	};
	return send(message);
};

module.exports.sendMailForTophostActivation = function (domain,pageId) {
	var message = {
	    "html": "<p>Nuova attivazione tophost:<br>Domain: " + domain + "<br>PageId: " + pageId + "</p>",
	    "subject": "Attivazione pagina da parte di Tophost",
	    "from_email": 'info@majeeko.com',
	    "from_name": 'Tech @ Majeeko!',
	    "to": [process.env.MAIL_ACCOUNTS_MANAGER].map(function (address) { return { email: address }; })
	};
	return send(message);
};

module.exports.sendMailForTophostDeactivation = function (domain,pageId) {
	var message = {
	    "html": "<p>Disattivata una pagina da tophost:<br>Domain: " + domain + "<br>PageId: " + pageId + "</p>",
	    "subject": "Disattivazione pagina da parte di Tophost",
	    "from_email": 'info@majeeko.com',
	    "from_name": 'Tech @ Majeeko!',
	    "to": [process.env.MAIL_ACCOUNTS_MANAGER].map(function (address) { return { email: address }; })
	};
	return send(message);
};

module.exports.sendMailToBuyDomains = function(order) {
	var html = swig.renderFile(__dirname + '/../../mail-templates/buy-domains.html', order);
	var message = {
	    "html": html,
	    "subject": "Nuovo cliente su Majeeko! Procedere con acquisto domini.",
	    "from_email": 'info@majeeko.com',
	    "from_name": 'Majeeko!',
	    "to": [{
            "email": process.env.MAIL_ACCOUNTS_MANAGER || 'mattia.borini@gmail.com'
        }]
	};
	return send(message);
};

module.exports.sendThanksToCustomer = function (order) {
	logger.info('Sending thank you e-mail to', order.billing_info.email, order.billing_info.first_name);
	return majeeko_mandrill.sendThankYouMail(order)
		.then(function(result) {
			logger.info('Successfully sent thank you e-mail');
			return result;
		});
};

module.exports.sendMoneyTransferInstructionsToCustomer = function (email, name, order, domains) {
	var price = accounting.formatNumber(order.total, {
        decimal : ",",
     	thousand: ".",
     	precision : 2
    });

	logger.info('Sending money transfer instructions e-mail to', email, order, order._id, price);
	return majeeko_mandrill.sendMoneyTransferReminder(email, order, order._id, price, domains)
		.then(function(result) {
			logger.info('Successfully sent money transfer instructions e-mail');
			return result;
		});
};

module.exports.sendWelcomeMessage = function (email,name,familyName,gender) {
	logger.info('Sending welcome e-mail to', email, name,familyName,gender);
	return majeeko_mandrill.sendWelcomeMail(email,name,familyName,gender)
		.then(function(result) {
			logger.info('Successfully sent welcome e-mail');
			return result;
		});
};

module.exports.sendNewOrderMessage = function (to, email, order, page, type, customer) {
	var html;
	var contex = {
		order: order,
		store: _.get(page, "modules.store.configuration"),
		domain: page.domains ? (page.domains[1] || page.domains[0]) : "",
		logo: _.get(page, "pageData.picture.data.url")
	};

	if(type == "money_transfer") {
		html = swig.renderFile(__dirname + '/../../mail-templates/bonifico.html', contex);
	} else if(type == "paypal") {
		html = swig.renderFile(__dirname + '/../../mail-templates/paypal.html', contex);
	}
	var message = {
		"html": html,
	    "subject": "Nuovo Acquisto su " + _.get(page, "modules.store.configuration.storeName"),
	    "from_email": email,
	    "from_name": customer ? page.modules.store.configuration.storeName : order.customer.name + " " + order.customer.last_name,
	    "to": [{
            "email": to
        }],
	    "headers": {
	        "Reply-To": email
	    }
	};
	return send(message);
};