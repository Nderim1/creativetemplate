var _ = require('lodash'),
	Q = require('q'),
	db = require('../../db'),
	logger = require('../logger');

module.exports.sendWelcomeMail = function (toAddr, toName, familyName, gender) {

	var welcome = gender == 'male' ? 'Benvenuto' : gender == 'female' ?  'Benvenuta': 'Benvenuto/a';

	return sendTemplate({
		template: 'Majeeko-welcome-gendered',
		subject: welcome +' su Majeeko!',
		toAddr: toAddr,
		toName: toName,
		merge_vars: {
			NOME: toName,
			COGNOME: familyName,
			WELCOME:welcome
		}
	});
};

module.exports.sendMoneyTransferReminder = function (toAddr, order, orderId, price,domains) {
	var template = order.type =="new" ? 'Majeeko_bonifico' :'Majeeko_bonifico_2';

	return db._raw.users.findOne({"facebook.profile.id": order.user_id}).then(function (user) {
		if(user) {
			return sendTemplate({
				template: template,
				subject: "Completa l'acquisto su Majeeko.com",
				toAddr: toAddr,
				toName: _.get(user, "facebook.profile.name.givenName"),
				merge_vars: {
					NOME: _.get(user, "facebook.profile.name.givenName"),
					CAUSALE: orderId,
					PREZZO: price,
					domains: domains.map(function(domain) { return { domain:domain }; }),
					multi_domain: domains.length > 1,
				},
				merge_language:'handlebars'
			});
		} else {
			return sendTemplate({
				template: template,
				subject: "Completa l'acquisto su Majeeko.com",
				toAddr: toAddr,
				toName: "",
				merge_vars: {
					NOME: "",
					CAUSALE: orderId,
					PREZZO: price,
					domains: domains.map(function(domain) { return { domain:domain }; }),
					multi_domain: domains.length > 1,
				},
				merge_language:'handlebars'
			});
		}
	}).catch(function (err) {
		logger.error(err);
	});
};

module.exports.sendThankYouMail = function (order) {
	return Q.Promise(function(resolve,reject){
		var template = order.type == "new" ? 'Majeeko_grazie_acquisto_2' : 'Majeeko_grazie_acquisto_3';
		try {
			resolve(sendTemplate({
				template: template,
				subject: 'Grazie per aver acquistato Majeeko!',
				toAddr: order.billing_info.email,
				toName: order.billing_info.first_name,
				merge_vars: {
					NOME: order.billing_info.first_name,
					NOME_MAIL: order.billing_info.first_name.toLowerCase(),
					DOMINIO: order.domains[0] ? order.domains[0].replace('www.','') : "",
					multi_domain: order.domains.length > 1,
					domains: order.domains.map(function(domain) { return { domain:domain }; }),
				},
				merge_language: 'handlebars'
			}));
		} catch(err) {
			logger.error(err);
			reject(err);
		}
	});
};

var sendTemplate = module.exports.sendTemplate = function (options) {
	options = options || {};
	var deferred = Q.defer();
	var config = require('../../config'),
		mandrill = require('mandrill-api/mandrill'),
		mandrill_client = new mandrill.Mandrill(config.mandrill.api_key),

		merge_language = options.merge_language || 'mailchimp';

	var merge_vars = _.zipWith(_.keys(options.merge_vars),_.values(options.merge_vars), function(a,b) { return { name:a, content:b }; });
	var message = {
	    "subject": options.subject,
	    "from_email": options.fromAddr || "info@" + config.primary_domain,
	    "from_name": options.fromName || "Majeeko",
	    "to": [{
	            "email": options.toAddr,
	            "name": options.toName,
	            "type": "to"
	        }],
	    // "bcc_address": "message.bcc_address@" + config.primary_domain,
	    // "bcc_address": "mattia.borini@gmail.com",
	    "headers": {
	        "Reply-To": options.replyTo || ("message.reply@" + config.primary_domain)
	    },
	    "merge_language": merge_language,
	    "global_merge_vars": merge_vars
	};
	if(options.bccToMajeeko !== false) {
		_.extend(message, {
	    	"bcc_address": options.bccToMajeeko || "info@majeeko.com"
		});
	}
	// console.log(message)
	// return
	mandrill_client.messages.sendTemplate({
	      "template_name": options.template,
	      "template_content": [{}],
	      "message": message,
	      "async": false
	    },
	  function(result) {
	    console.log(result);
	    deferred.resolve(result);
	}, function(err) {
	    console.log('A mandrill error occurred: ' + err.name + ' - ' + err.message);
	    deferred.reject(err);
	});
	return deferred.promise;
};
