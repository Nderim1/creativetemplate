var _ = require('lodash');


var fonts = {
	// "elegant": {
	// 	"texts": "'Open Sans', Helvetica, Century Gothic, Arial, sans-serif",
	// 	"titles": "'Arvo', serif"
	// },
	// "neat": {
	// 	"texts": "'Raleway', sans-serif",
	// 	"titles": "'Source Sans Pro', sans-serif"
	// },
	// "restaurant": {
	// 	"texts": "'Raleway', sans-serif",
	// 	"titles": "'Arvo', serif"
	// },
	"fashion": {
		"texts": "'Raleway', sans-serif",
		"titles": "'Montserrat', Helvetica, Arial, sans-serif",
		"to_load":"Montserrat:300,400,500,700|Raleway:300,400,500,700",
		"label": "Fashion"
	},
	"hand": {
		"texts": "'Merriweather', Georgia, serif",
		"titles": "'Kaushan Script', sans-serif",
		"to_load":"Kaushan+Script:300,400,500,700|Merriweather:300,400,500,700",
		"label": "Hand!"
	},
	"elegant": {
		"texts": "'Roboto Slab', serif",
		"titles": "'Roboto Slab', serif",
		"to_load":"Roboto+Slab:300,400,500,700",
		"label": "Elegant"
	},
	"funny": {
		"texts": "'Raleway', sans-serif",
		"titles": "'Amatic SC', sans-serif",
		"to_load":"Amatic+SC:300,400,500,700|Raleway:300,400,500,700",
		"label": "Funny"
	},
	"innovation": {
		"texts": "'Open Sans', Helvetica, Century Gothic, Arial, sans-serif",
		"titles": "'Source Sans Pro', sans-serif",
		"to_load":"Source+Sans+Pro:300,400,500,700|Open+Sans:300,400,500,700",
		"label": "Harry"
	},
	"opus": {
		"texts": "'Open Sans', Helvetica, Arial, sans-serif",
		"titles": "'Oswald', Helvetica, Arial, sans-serif",
		"to_load":"Oswald:300,400,500,700|Open+Sans:300,400,500,700",
		"label": "Merlin"
	},
	"parallelo": {
		"texts": "'Lato', Helvetica, Arial, sans-serif",
		"titles": "'Roboto Slab', Helvetica, Arial, sans-serif",
		"to_load":"Lato:300,400,500,700|Roboto+Slab:300,400,500,700",
		"label": "Circe"
	},
	"hellobrand": {
		"texts": "'Open Sans', Helvetica, Arial, sans-serif",
		"titles": "'Oxygen', Helvetica, Arial, sans-serif",
		"to_load":"Open+Sans:300,400,500,700|Oxygen:300,400,500,700",
		"label": "Hellobrand"
	},
	"portfolio": {
		"texts": "'Open Sans', Helvetica, Arial, sans-serif",
		"titles": "'Montserrat', Helvetica, Arial, sans-serif",
		"to_load":"Montserrat:300,400,500,700|Open+Sans:300,400,500,700",
		"label": "Portfolio"
	},
	"salazar": {
		"texts": "'Montserrat', Helvetica, Arial, sans-serif",
		"titles": "'Lato', Helvetica, Arial, sans-serif",
		"to_load":"Lato:300,400,500,700|Montserrat:300,400,500,700",
		"label": "Salazar"
	},
	"temple": {
		"texts": "'Fjord One', Georgia, serif",
		"titles": "'Fjord One', Georgia, serif",
		"to_load":"Fjord+One:300,400,500,700",
		"label": "Temple"
	},
	"simposio": {
		"texts": "'Droid Sans', Helvetica, Arial, sans-serif",
		"titles": "'Droid Sans', Helvetica, Arial, sans-serif",
		"to_load":"Droid+Sans:300,400,500,700",
		"label": "Simposio"
	},
	"default": {
		"texts": "'Open Sans', Helvetica, Century Gothic, Arial, sans-serif",
		"titles": "'Source Sans Pro', sans-serif",
		"to_load":"Source+Sans+Pro:300,400,500,700|Open+Sans:300,400,500,700",
		"label": "default"
	}
};

var omitDefault = _.omit(fonts, 'default');

module.exports.getAllFonts = function() {
	return omitDefault;
}

function getFontCombination(combination) {
	combination = combination.toLowerCase();
	var theFont = _.get(fonts, combination, fonts.default);   ///to clean
	if(!theFont || theFont.length != fonts.default.length){
		return fonts.default;
	}
	return theFont;
}

module.exports.getFontCombination = getFontCombination;

module.exports.feelLucky = function() {
	var result = {};
	result['texts'] = _.sample(omitDefault, 1)[0].texts;
	result['titles'] = _.sample(omitDefault, 1)[0].titles;
	return result;
}

function getFontWithoutFallback(combination) {
	var result = getFontCombination(combination);
	result.texts = result.texts.split(',')[0];
	result.titles = result.titles.split(',')[0];
	return result;
}

module.exports.getFontWithoutFallback = getFontWithoutFallback;

function getAllFontsWithoutFallback(){
	var keys = Object.keys(fonts);
	var result =  keys.map(function(elem){
		return getFontWithoutFallback(elem);
	})
	return result;
}


module.exports.getAllFontsWithoutFallback = getAllFontsWithoutFallback;
