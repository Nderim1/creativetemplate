var db = require('../db')
  , async = require('async')
  , facebook = require('../facebook')
  , facebookSync = require('../sync/facebook')
  , logger = require('./logger')

  , pageUtils = require('../utils/pages')

  , R = require('ramda')

var impersonate = function(userId) {
  return function(__shortLiveAccessToken__, refreshToken, __profile__, done) {
    db.getUserByFacebookId(userId)
      .then(function (user) {
        if(user) {
          var longLiveAccessToken = user.facebook.access_token
            , profile = user.facebook.profile

          logger.info('impersonate: fetching user data')
          facebook.fetchUserData(profile.id, longLiveAccessToken)
            .then(function(userData) {
              logger.info('impersonate: saving user data')
              facebookSync.saveUserData(profile, userData, longLiveAccessToken)
                .then(function() {
                  logger.info('impersonate: update user ready')
                  db.updateUserReady(profile.id, true);
                  var accounts = userData.accounts.data.filter(R.prop('is_published'))
                  async.each(accounts, function(account, callback) {
                    logger.info('impersonate: creating page for account', account.id)
                    pageUtils.createFromAccount(account)
                    // facebookSync.createPage(account.id, account.access_token)
                    //   .finally(function() {
                    //     facebookSync.syncPage(account.id, account.access_token)
                    //       .then(function() {
                    //         logger.info('All page data downloaded for page "' + account.name + '"')
                    //         callback();
                    //       })
                    //       .catch(function(err) {
                    //         logger.info('Error', err)
                    //       })
                    //   })
                  }, function(err) {
                    logger.info('All went well')
                  }, function() {
                    logger.info('Errors')
                  })
                });
            })
          process.nextTick(function() {
            done(null, profile);
          });
        } else {
          logger.error('User not in db');
          done(null, false, {message:"User not in db"});
        }
      })
      .catch(function(err) {
        logger.error(err)
      })
  }
}

module.exports = impersonate