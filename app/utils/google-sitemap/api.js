var request = require('google-oauth-jwt').requestWithJWT();
var Q = require('q');
var db = require('../../db/db');
var logger = require('../logger');
var moment = require('moment');
var token = {
	email: '520076305922-98fpjh16v89riotqm0bbdt9c7u0a99vn@developer.gserviceaccount.com',
	keyFile: __dirname + '/majeeko-key.pem',
	scopes: ['https://www.googleapis.com/auth/webmasters', 'https://www.googleapis.com/auth/siteverification']
};
var exp = {};

var getGoogleTkn = function(link) {
	return Q.Promise(function(resolve, reject) {
		link = /www\./.test(link) ? link : "www."+link;
		var data = {
			site: {
				identifier: link,
				type: 'SITE'
			},
			verificationMethod: 'META'
		};
		data = JSON.parse(JSON.stringify(data));
		request({
			url: 'https://www.googleapis.com/siteVerification/v1/token',
			method: 'POST',
			json: data,
			jwt: token,
		}, function(err, res, body) {
			// If successful, this method returns a response body with the following structure:
			// {
			//   "method": string,
			//   "token": string
			// }
			if (err) reject(err);
			else resolve({
				body: body
			});
		});
	})
}

var verify = function(link) {
	return Q.Promise(function(resolve, reject) {
		link = /www\./.test(link) ? link : "www."+link;
		var data = {
			site: {
				identifier: link,
				type: 'SITE'
			}
		};
		data = JSON.parse(JSON.stringify(data));
		request({
			url: 'https://www.googleapis.com/siteVerification/v1/webResource?verificationMethod=META?fields=id',
			method: 'POST',
			json: data,
			jwt: token,
		}, function(err, res, body) {
			// If successful the response body should be
			// 	{
			//   "id": string,
			//   "site": {
			//     "type": string,
			//     "identifier": string
			//   }
			// }
			if (err) reject(err);
			else resolve({
				body: body
			});
		});
	})
}

var delSiteMap = function(id) {
	return Q.Promise(function(resolve, reject) {
		request({
			url: 'https://www.googleapis.com/siteVerification/v1/webResource/' + id,
			method: 'DELETE',
			jwt: token,
		}, function(err, res, body) {
			console.log(err, res, body);
			//If successful, this method returns the HTTP 204 No Content status code and an empty response body.
			if (err) reject(err);
			else resolve({
				body: body
			});
		});
	})
}

var addSiteMap = function(link) {
	return Q.Promise(function(resolve, reject) {
		link = /www\./.test(link) ? link : "www."+link;
		var urlToAppend = encodeURIComponent(link) + '/sitemaps/' + encodeURIComponent(link + '/sitemap');
		request({
			url: 'https://www.googleapis.com/webmasters/v3/sites/' + urlToAppend,
			method: 'PUT',
			jwt: token,
		}, function(err, res, body) {
			// If successful, this method returns an empty response body.
			if (err) reject(err);
			else resolve({
				body: body
			});
		});
	})
}

var addSite = function(link) {

	return Q.Promise(function(resolve, reject) {
		link = /www\./.test(link) ? link : "www."+link;
		var urlToAppend = 'https://www.googleapis.com/webmasters/v3/sites/' + encodeURIComponent(link);
		request({
			url: urlToAppend,
			method: 'PUT',
			jwt: token,
		}, function(err, res, body) {
			// If successful, this method returns an empty response body.
			if (body == '' && res.toJSON().statusCode == '204') resolve({
				body: body
			});
			if (err) reject(err);
		});
	})
}

exp.activateSiteMap = function(link, id) {
	var link = link ? link : '';
	console.log(link);
	var toSet = {};
	toSet["siteMap." + link] = {};
	return addSite(link)
		.then(function(data) {
			logger.info('sitemap: addsite');
			return getGoogleTkn(link);
		})
		.then(function(obj) {
			logger.info('sitemap: getGoogleTkn');
			toSet["siteMap." + link].meta = obj.body.token,
				toSet["siteMap." + link].updated = moment().toISOString();
			return db.pages.update({
				'facebook_id': id
			}, {
				$set: toSet
			})
		})
		.then(function(data) {
			return verify(link);
		})
		.then(function(data) {
			logger.info('sitemap: verified site');
			toSet["siteMap." + link].id = data.body.id;
			return db.pages.update({
				'facebook_id': id
			}, {
				$set: toSet
			})
		})
		.then(function(data) {
			logger.info('sitemap: saved id into db');
			return addSiteMap(link)
		})
		.then(function() {
			return 'succesfully added sitemap for link:'+link+" id:"+id;
			logger.info('sitemap succesfully added')
		})
		.catch(function(err) {
			logger.error('Error sending or adding sitemap to google', {
				error: err,
				stack: err.stack,
				filename: __filename
			})
		})
}

// exp.updateSitemap = function(link){
// 	return addSiteMap(link)
// }


exp.generateSiteMap = function(urls) {
	var first = "<?xml version='1.0' encoding='UTF-8'?><urlset xmlns='http://www.sitemaps.org/schemas/sitemap/0.9'>"
	var dataStart = "<url><loc>";
	var dataEnd = "</loc></url>";
	var last = "</urlset>";
	var final = first + urls.map(function(elem) {
		return dataStart + "'" + elem + "'" + dataEnd;
	}).join('') + last;
	return final;
}
module.exports = exp;
