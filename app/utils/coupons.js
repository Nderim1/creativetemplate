// caratteristiche degli sconti
// - codice
// - uno o + fb id a cui è riservato il coupon (opzionale)
// - scadenza (opzionale)
// - numero max di utilizzi
// - prezzo

var Q = require('q');
var _ = require('lodash');
var logger = require('./logger');
var db = require('../db');
var moment = require('moment');

function Coupon (couponStr, value, options) {
	this.couponStr = couponStr;
	this.value = value;
	options = options || {};
	if(options.startDate) this.startDate = moment(options.startDate,'YYYY-MM-DD');
	if(options.endDate) this.endDate = moment(options.endDate,'YYYY-MM-DD');
	this.caseSensitive = options.caseSensitive;
	this.appliedTo = ['new-premium', 'new-pro']
	if(options.appliedTo)this.appliedTo = options.appliedTo;
}

Coupon.prototype.test = function (couponStr) {
	try {
		var now = moment();

		if(this.startDate && (!this.startDate.isValid() || now < this.startDate)) return false;
		if(this.endDate && (!this.endDate.isValid() || now > this.endDate.add(1,'day'))) return false;
		return this.caseSensitive ? (this.couponStr === couponStr) : (this.couponStr.toLowerCase() === couponStr.toLowerCase());
	} catch(err) {
		logger.error(err);
		return false;
	}
}

function getDiscount (couponStr, options) {
	var deferred = Q.defer();
	options = options || {};
	if(!couponStr) {
		deferred.resolve(0);
	} else {
		try {
			db._raw.coupons.find().toArray()
				.then(function(coupons) {
					return coupons.map(function(coupon) {
						return new Coupon(coupon.string, coupon.value, coupon.options);
					})
				})
				.then(function(coupons) {
					var coupon = _.find(coupons, function(coupon) { return coupon.test(couponStr) });
					if( !coupon ) deferred.resolve(0);
					else if(_.get(options, "return_obj")){
						deferred.resolve(coupon);
					}else{
						deferred.resolve(coupon.value);
					}
				})
				.catch(function(err) {
					logger.error(err);
					deferred.resolve(0);
				})
		} catch (err) {
			logger.error(err);
			deferred.resolve(0);
			return deferred.promise;
		}
	}
	return deferred.promise;
}

exports.getDiscount = getDiscount;
