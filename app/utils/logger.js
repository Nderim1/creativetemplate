var winston = require('winston');
require('winston-logstash');
winston.emitErrs = true;

var logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: './logs/all-logs.log',
            handleExceptions: true,
            json: true,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        }),
        new winston.transports.Logstash({
		    port: process.env.LOGSTASH_PORT || 5000,
		    node_name: 'majeeko-node',
		    host: process.env.LOGSTASH_HOST || '192.168.99.100',
		    meta: {
		    	env: process.env.NODE_ENV
		    }
		})
    ],
    exitOnError: false
});

function makeErrorPropertyEnumerable(errorObject,propName) {
	if(errorObject && errorObject[propName] && !Object.propertyIsEnumerable(errorObject, propName)) {
		var value = errorObject[propName];
		delete errorObject[propName];
		errorObject[propName] = value;
	}
}

logger.addRewriter(function (level, msg, meta) {
	if(level !== 'error') return meta;
	if(meta instanceof Error) {
		makeErrorPropertyEnumerable(meta,'name');
		makeErrorPropertyEnumerable(meta,'message');
		makeErrorPropertyEnumerable(meta,'stack');
	}
	return meta;
});

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};
