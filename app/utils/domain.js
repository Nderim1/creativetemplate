var R = require('ramda')
  , config = require('../config')
  , AWS = require('aws-sdk')
  , Q = require('q')
  , async = require('async')
  , _ = require('lodash')

var tlds = [
	'it'
  , 'com'
  , 'net'
  , 'info'
  , 'org'
  , 'biz'
  // , 'me'
  // , 'mobi'
]

var suggest = module.exports.suggest = function (username) {
	var possibleDomainNames = R.map(R.concat(username + '.'), tlds)
	return Q.all(R.map(checkAvailability, possibleDomainNames)) //.then(R.map(R.prop('domainName'),R.filter(R.propEq('availability','AVAILABLE'))))
}

var checkAvailability = module.exports.checkAvailability = function (domainName) {
	var deferred = Q.defer()
	
	if(process.env.FAKE_DOMAIN_AVAILABILITY_CHECK) {
		deferred.resolve({ domainName: domainName, availability: _.sample(['AVAILABLE','UNAVAILABLE']) })
		return deferred.promise
	}

	AWS.config.update({
		accessKeyId: config.AWS_ACCESS_KEY_ID,
		secretAccessKey: config.AWS_SECRET_ACCESS_KEY,
		region: 'us-east-1'
	});

	var params = {
	  DomainName: domainName
	};

	var route53domains = new AWS.Route53Domains();
	route53domains.checkDomainAvailability(params, function (err, data) {
		if (err) {
			deferred.resolve({ error: err });
		} else {
			deferred.resolve({ domainName: domainName, availability: data.Availability })
		}
	});
	return deferred.promise
}
