var request = require('request-promise');
var serialize = require('qs');
var md5 = require('md5');

var piwickUrl = process.env.PIWIK_URL + ':' + process.env.PIWIK_PORT;
var pwToken = process.env.PIWIK_TOKEN;
var logger = require('../../logger');
var Q = require('q');
var rx = require('rx');

function rq () {
	var deferred = Q.defer();
	try {
		var args = Array.prototype.slice.call(arguments);
		var promise = request.apply(this,args);
		return rx.Observable.fromPromise(promise).timeout(2000).toPromise();
	} catch(err) {
		logger.error({
			message: 'Error executing promise in piwik api',
			info: {
				arguments: arguments
			},
			stack: err.stack
		});
		deferred.reject(err);
		return deferred.promise;
	}
}

var exp = {
	addSite: function(name, urls) {

		logger.info('piwik API: addSite', name, urls);

		var obj = {
			module: 'API',
			format: 'JSON',
			token_auth: pwToken,
			method: 'SitesManager.addSite',
			siteName: name,
			'urls[0]': urls[0]
		};

		var request = {
			uri: piwickUrl + '/index.php?' + serialize.stringify(obj),
			method: 'GET'
		};
		return rq(request)
			.catch(function(err) {
				logger.error('piwik API Error: addSite', name, urls);
				err.message = err.message + ": piwik API Error: addSite";
				throw err;
			});

	},
	trackAction: function(siteId, url, action, ip) {

		logger.info('piwik API: trackAction', siteId, url, action, ip);

		var obj = {
			idsite: siteId,
			'url': url,
			rec: 1,
			rand: Math.floor(Math.random() * 10) + 1,
			cip: ip,
			token_auth: pwToken,
		};

		if (action) obj.action_name = action;

		var request = {
			uri: piwickUrl + '/piwik.php?' + serialize.stringify(obj),
			method: 'GET'
		};

		return rq(request)
			.catch(function(err) {
				logger.error('piwik API Error: trackAction', siteId, url, action, ip);
				err.message = err.message + ": piwik API Error: trackAction";
				throw err;
			});
	},
	addUser: function(userLogin, userMail, userPassword) {

		logger.info('piwik API: addUser', userLogin, userMail, userPassword);

		var obj = {
			module: 'API',
			format: 'JSON',
			token_auth: pwToken,
			method: 'UsersManager.addUser',
			email: userMail,
			userLogin: userLogin,
			password: userPassword
		};

		var request = {
			uri: piwickUrl + '/index.php?' + serialize.stringify(obj),
			method: 'GET'
		};

		return rq(request)
			.catch(function(err) {
				logger.error('piwik API Error: addUser', userLogin, userMail, userPassword);
				err.message = err.message + ": piwik API Error: addUser";
				throw err;
			});

	},
	setUserAccess: function(userLogin, access, idSite) {

		logger.info('piwik API: setUserAccess', userLogin, access, idSite);

		var obj = {
			module: 'API',
			format: 'JSON',
			token_auth: pwToken,
			method: 'UsersManager.setUserAccess',
			userLogin: userLogin,
			access: access,
			idSites: idSite
		};

		var request = {
			uri: piwickUrl + '/index.php?' + serialize.stringify(obj),
			method: 'GET'
		};

		return rq(request)
			.catch(function(err) {
				logger.error('piwik API Error: setUserAccess', userLogin, access, idSite);
				err.message = err.message + ": piwik API Error: setUserAccess";
				throw err;
			});
	},
	getWidgetUrl: function(userLogin, password, idSite) {

		logger.info('piwik API: getWidgetUrl', userLogin, password, idSite);

		var obj = {
			module: 'API',
			format: 'JSON',
			method: 'UsersManager.getTokenAuth',
			userLogin: userLogin,
			md5Password: md5(password)
		};

		var request = {
			uri: piwickUrl + '/index.php?' + serialize.stringify(obj),
			method: 'GET'
		};

		return rq(request)
			.then(function(obj) {
				obj = JSON.parse(obj);
				return piwickUrl + '/index.php?module=Widgetize&action=iframe&widget=1&moduleToWidgetize=VisitsSummary&actionToWidgetize=index&idSite=' + idSite + '&period=day&date=today&disableLink=1&widget=1&token_auth=' + obj.value;
			})
			.catch(function(err) {
				logger.error('piwik API Error: getWidgetUrl', {
					error:err,
					stack:err.stack,
					filename: __filename,
					meta: {
						userLogin: userLogin,
						password: password,
						idSite: idSite
					}
				});
				err.message = err.message + ": piwik API Error: getWidgetUrl";
				throw err;
			});
	}
};

module.exports = exp;
