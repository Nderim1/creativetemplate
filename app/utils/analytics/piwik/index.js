var api    = require('./api');
var _      = require('lodash');
var Q      = require('q');
var logger = require('../../logger');
var db     = require('../../../db');

function registerSiteToPiwik(facebookPageId,domains) {
	logger.info({message:'Registering site to piwik',info:{page_facebook_id:facebookPageId,domains:domains}});
	return db.getPageById(facebookPageId)
		.then(function(page) {
			if(!page) throw new Error("Page with id " + facebookPageId + " doesn't exist");
			if(!page.username) throw new Error("Page with id " + facebookPageId + " doesn't have a username");
			var deferred = Q.defer();
			if(domains) {
				api.addSite(page.username,domains)
				.then(function(idSite){
					idSite = _.get(JSON.parse(idSite),'value');
					return api.addUser(page.username,page.facebook_id+'@majeeko.com',page.facebook_id)
					.then(function(data){
						return api.setUserAccess(page.username,'admin',idSite)
						.then(function(){
							db._raw.pages.update({facebook_id:page.facebook_id},{$set:{'analytics':{
								piwik: {
									idSite: idSite,
									username: page.username,
									password: page.facebook_id
								}
							}}});
							deferred.resolve({page:page,idSite:idSite.value});
						})
						.catch(function(){
							throw new Error('failed to add user to piwik', {
								login:page.username,
								idSite:idSite
							});
						});
					})
					.catch(function(){
						throw new Error('failed to add user to piwik', {
							login:page.username,
							password:page.facebook_id,
							email:'info@majeeko.com'
						});
					});
				})
				.catch(function(error){
					logger.error(error);
					console.log(error);
					deferred.resolve( { page: page });
				});
			} else {
				deferred.resolve({ page: page });
			}
			return deferred.promise;
		})
		.catch(function(err) {
			logger.error('Piwik',err)
			throw err
		})
}

exports.trackAction = api.trackAction;
exports.registerSiteToPiwik = registerSiteToPiwik;
exports.getWidgetUrl = api.getWidgetUrl;
