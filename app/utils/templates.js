var fs = require('fs'),
	less = require('less'),
	Q = require('q'),
	db = require('../db'),
	_ = require('lodash'),
	logger = require('./logger'),
	moment = require('moment'),
	LessPluginAutoPrefix = require('less-plugin-autoprefix'),
	LessPluginCleanCSS = require('less-plugin-clean-css'),
	fonts = require('./fonts')

function renderTemplateStyles(template, graphics) {
	var lessUrl = __dirname + '/../templates/' + template + '/styles/style.less'
	  , deferred = Q.defer()
	fs.readFile(lessUrl, "utf8", function(err, data) {
		if(!err) {
			if(!_.has(graphics, "fonts")){
				_.set(graphics, "fonts", (template||"default"));
			}
			var customFonts = fonts.getFontCombination(graphics.fonts);
			// var customFonts = fonts.feelLucky();
			var autoprefixPlugin = new LessPluginAutoPrefix({browsers: ["> 1%"]});
			var cleanCSSPlugin = new LessPluginCleanCSS({advanced: true, keepSpecialComments:0});
			less.render(data, {
				filename: lessUrl,
				plugins: [autoprefixPlugin,cleanCSSPlugin],
				modifyVars: {
					outer_background: graphics.colors.outer_background || null,
					inner_background: graphics.colors.inner_background || null,
					titles: graphics.colors.titles || null,
					texts: graphics.colors.texts || null,
					'custom-font-text':customFonts.texts || null,
					'custom-font-title':customFonts.titles || null
				}
			})
				.then(deferred.resolve)
				.catch(function(e) {
					logger.error("Error in compiling LESS:",e);
					deferred.reject(e);
			});
		} else {
			logger.error(err)
			deferred.reject(err)
		}
	})
	return deferred.promise
}

function compileAllSitesDynamicCss() {
	var time = moment();
	return db._raw.pages.find({},{facebook_id:1})
	.toArray()
	.then(function(ids){
		return Q.all(ids.map(function(id){
			return compileSiteDynamicCss(id.facebook_id)
			.then(function(result){
				logger.info(result)
				return result
			})
			.catch(function(err){
				logger.error(err)
				return null
			})
		}))
		.then(function(result){
			logger.info('All sites less recompiled in ', moment().diff(time), 's')
			return result;
		})
	})
}

function compileSiteDynamicCss(fbPageId) {
	return db._raw.pages.findOne({facebook_id:fbPageId},{'configuration':1})
	.then(function(page){
		if(!page) {
			throw new Error('No page found!!!')
		} else {
			var updates = {};
			var promises = [];
			if(_.has(page, 'configuration.editor.preview')) {

				promises.push(renderTemplateStyles(page.configuration.editor.preview.graphics.template, page.configuration.editor.preview.graphics)
				.then(function(output) {
					updates['preview'] = output.css;
				}))
			}
			if(_.has(page,'configuration.editor.default')) {
				promises.push(renderTemplateStyles(page.configuration.editor.default.graphics.template, page.configuration.editor.default.graphics)
				.then(function(output) {
					updates['default'] = output.css;
				}))
			}
			promises.push(renderTemplateStyles(page.configuration.graphics.template, page.configuration.graphics)
				.then(function(output) {
					updates['active'] = output.css;
				}))
			return Q.all(promises)
			.then(function() {
				var updateObj = {
					'configuration._css':updates['active']
				}
				if (updates['preview']) updateObj['configuration.editor.preview._css'] = updates['preview'];
				if (updates['default']) updateObj['configuration.editor.default._css'] = updates['default'];
				return db._raw.pages.update({'facebook_id':fbPageId},{$set:updateObj});
			})
		}
	})
}

module.exports = {
	renderTemplateStyles: renderTemplateStyles,
	compileSiteDynamicCss: compileSiteDynamicCss,
	compileAllSitesDynamicCss: compileAllSitesDynamicCss
}
