var ce = require('colour-extractor')
  , request = require('request-promise')
  , Q = require('q')
  , logger = require('../utils/logger')
  , _ = require('lodash')

var extractColorsFromURL = function (url,numColors) {
	logger.info('Extracting colors from image at url', url)
	numColors = numColors || 5;
	var deferred = Q.defer();
	request.get({url:url,encoding:null})
		.then(function (body) {
			// console.log(body)
			// ce.colourKey(body, function (colours) {
			// 	try {
			// 		var selectedColors = colours.slice(0,numColors).map(function(color) {
			// 		  	return ce.rgb2hex(color[1])
			// 		})
			// 		deferred.resolve(selectedColors);
			// 	} catch (err) {
			// 		logger.error('Error fetching color', err)
			// 		deferred.reject('Error fetching color')
			// 	}
			// });
			try {
				ce.topColours(body, true, function (colours) {
					deferred.resolve(_.sample(colours.map(function(color) {
					  	return ce.rgb2hex(color[1])
					}), numColors));
				});
			} catch(e) {
				deferred.reject('Unable to extract colours');
			}
		})
	return deferred.promise;
}

module.exports.extractColorsFromURL = extractColorsFromURL
