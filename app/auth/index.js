var basicAuth = require('basic-auth');
var moment = require('moment');

module.exports.ensureAuthenticated = function(req, res, next) {
	if (req.user === undefined) {
		console.log('user undefined. req.method: ', req.method);
		req.session.destroy();
		if (req.method == 'POST') return res.status(401).send();
		else return res.redirect(req.protocol + '://' + process.env.HOST + '/');
	} else if (moment().diff(moment(req.session.cookie._expires)) > 0) {
		console.log('user undefined. req.method: ', req.method);
		req.session.destroy();
		if (req.method == 'POST') return res.status(401).send();
		else return res.redirect(req.protocol + '://' + process.env.HOST + '/');
	} else if (req.isAuthenticated()) {
		return next();
	}
	res.redirect('/');
};

module.exports.requireBasicAuth = function(req, res, next) {
	var user = basicAuth(req);
	if (process.env.BASIC_AUTH_NAME && process.env.BASIC_AUTH_PASS &&
		user && user.name && user.pass &&
		user.name == process.env.BASIC_AUTH_NAME && user.pass == process.env.BASIC_AUTH_PASS) {
		next();
	} else {
		res.send(403);
	}
};
