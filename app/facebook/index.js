var facebookGraphAPI = "https://graph.facebook.com",
	apiVersion = '2.3',
	config = require('../config'),
	request = require('request-promise'),
	colors = require('colors'),
	qs = require('qs'),
	colors = require('colors'),
	logger = require('../utils/logger'),
	Q = require('q'),
	_ = require('lodash');

var appAccessToken = config.facebook.clientID + "|" + config.facebook.clientSecret;

// Prepare url for Facebook graph API query
var prepareGraphApiQuery = function(objectId, token, query, edge) {
	edge = edge ? '/' + edge : "";
    var url = facebookGraphAPI + '/v' + apiVersion + '/' + objectId  + edge + '?access_token=' + token;
    if(query) url += "&" + query;
    return url;
};

// Fetch app access token
var fetchAppAccessToken = function () {
    var url = facebookGraphAPI + '/v' + apiVersion + '/oauth/access_token?client_id=' + config.facebook.clientID + '&client_secret=' + config.facebook.clientSecret + '&grant_type=client_credentials';
    return request.get(url)
        .then(function (tokenStr) {
            var accessToken = qs.parse(tokenStr).access_token;
            return accessToken;
        })
        .catch(function (err) {
            logger.error("Error while fetching access token", err);
            throw err;
        });
};

// Exchange short live with long live Facebook user's access token
var exchangeUserToken = function(config, shortLiveAccessToken, callback) {
    logger.info("Exchanging Facebook user token:", shortLiveAccessToken);

    var url = facebookGraphAPI + '/oauth/access_token?grant_type=fb_exchange_token&client_id=' + config.facebook.clientID + '&client_secret=' + config.facebook.clientSecret + '&fb_exchange_token=' + shortLiveAccessToken;

    return request.get(url)
        .then(function (data) {
            var longLiveAccessToken = qs.parse(data).access_token;
            logger.info("Successfully exchanged Facebook user access token:", longLiveAccessToken);
            return longLiveAccessToken;
        })
        .catch(function (err) {
            logger.error("Error while fetching user long live token", err);
            throw err;
        });
};

// Fetch minimal user's data from Facebook
var fetchUserData = function(facebookUserId, shortLiveAccessToken) {
    logger.info("Fetching minimal Facebook user data");

    var url = prepareGraphApiQuery(facebookUserId, shortLiveAccessToken, 'fields=picture.height(200).width(200){url},gender,accounts.limit(100){id,username,picture.width(200).height(200){url},name,category,access_token,is_published}');

    return request.get(url)
        .then(function (data) {
            logger.info("Successfully fetched minimal Facebook user data", {meta:{data:data}});
            return JSON.parse(data);
        })
        .catch(function (err) {
            // logger.info(String(res.statusCode))
            // logger.info(JSON.stringify(res.headers, null, 2))
            logger.error('Error fetching user data', {error:err,meta:{url:url}});
            throw err;
        });
};

// Search facebook pages based on query
var searchPages = function (query,limit) {

    logger.info("Searching page");

    // default limit is 10
    limit = limit || 10;

    var url = facebookGraphAPI + '/v' + apiVersion + '/search?q=' + query + '&type=page&access_token=' + appAccessToken;

    // query fields
    url += '&fields=id,name,category,likes,link,picture.width(100).height(100){url}';

    if(limit !== 0) url += '&limit=' + limit;

    return request.get(url)
        .then(JSON.parse)
        .catch(function (err) {
            logger.error("Error during facebook page search", {error:err});
            throw err;
        });
};

// for preview generation purpose
var fetchMinimalPageData = function (facebookPageId,pageAccessToken) {

    logger.info("Fetching Facebook page data");

    // logger.debug(appAccessToken);

    pageAccessToken = pageAccessToken || appAccessToken;

    // exclude album pphotos as it requires user permission
    var url = prepareGraphApiQuery(facebookPageId, pageAccessToken, 'fields=' +
    	'name,username,picture.width(200).height(200){url},cover,about,description,location,phone,category,website,link,emails,likes,' +
    	'posts.limit(6){type,updated_time,attachments,likes{id},shares,comments{id},message,caption,description,name,link,source,status_type,story},' +
    	'albums.limit(6){name,cover_photo,picture,updated_time,type,count,description,likes{id},photos.limit(10)},' +
    	'events.limit(6){cover,name,attachments,description,updated_time,invited_count,start_time,end_time,ticket_uri,attending_count,maybe_count,place,likes{id}}'
    );

    return request.get(url)
        .then(function (data) {
            logger.info("Successfully fetched Facebook page data");
            return JSON.parse(data);
        })
        .catch(function (err) {
            logger.error("Error while fetching page data:", err);
            throw err;
        });
};

function fetchPageData(facebookPageId,pageAccessToken) {
		return _fetchPageData(facebookPageId,pageAccessToken)
		.catch(function(err){
			logger.error("Error on request with pageAccessToken(expired?), trying with appAccessToken");
			return _fetchPageData(facebookPageId,appAccessToken);
		});
}

// Fetch Facebook page data
function _fetchPageData(facebookPageId,accessToken) {

    logger.info("Fetching Facebook page data");

    // var url = prepareGraphApiQuery(facebookPageId, accessToken, 'fields=name,username,picture.width(200).height(200){url},cover,about,description,location,phone,category,website,link,emails,likes,posts{type,updated_time,attachments,likes{id},shares,comments{id},message},albums{name,cover_photo,picture,updated_time,type,count,photos,description,likes{id}},events{cover,name,attachments,description,updated_time,invited_count,start_time,end_time,ticket_uri,attending_count,maybe_count,place,likes{id}}');
    var url = prepareGraphApiQuery(facebookPageId, accessToken, 'fields=' +
        'name,username,picture.width(200).height(200){url},cover,about,description,location,phone,category,website,link,emails,likes,hours'
    );

        return Q.all([
        	request.get(url),
        	fetchPosts(facebookPageId, accessToken),
       		fetchAlbums(facebookPageId, accessToken),
       		fetchEvents(facebookPageId, accessToken)
        	]).spread(function (data, posts, albums, events) {
        		logger.info("Successfully fetched Facebook page data");
        		data = JSON.parse(data);
                if(data.hours != undefined && data.hours != null){
                    var daysObj = {
                        mon:[],
                        tue:[],
                        wed:[],
                        thu:[],
                        fri:[],
                        sat:[],
                        sun:[]
                    }
                    var openDays = _.pairs(data.hours);
                    openDays.forEach(function(elem){
                        daysObj[elem[0].slice(0,3)].push(elem);
                    })
                    Object.keys(daysObj).forEach(function(elem){
                        daysObj[elem].sort(function(a,b){
                            return a - b ;
                        })
                    })
                    data.hours = daysObj;
                }
        		data.albums = {};
        		data.albums.data = albums;
        		_.assign(data,_.omit(posts, "id"));
        		_.assign(data, _.omit(events, "id"));
        		return data;
        	})
        .catch(function (err) {
            logger.error("Error while fetching page data:\n", err);
            logger.error("Request URL:", url);

            throw err;
        });
}

function fetchPosts(facebookPageId, accessToken) {
	logger.info("Fetching POSTS for " + facebookPageId + "page");
    var url = prepareGraphApiQuery(facebookPageId, accessToken, 'fields=' +
    	'posts.limit(100){type,updated_time,attachments,likes{id},shares,comments{id},message,caption,description,name,link,source,status_type,story}'
    );
    return request.get(url)
        .then(function (data) {
            logger.info("Successfully fetched POSTS");
            return JSON.parse(data);
        })
        .catch(function (err) {
            logger.error("Error while fetching POSTS:\n", err);
            logger.error("Request URL:", url);

            throw err;
        });
}

function fetchAlbums(facebookPageId, accessToken) {
	logger.info("Fetching ALBUMS for " + facebookPageId + "page");
	var albums = [];
	var counter = 1;
	var deferred = Q.defer();

	var url = prepareGraphApiQuery(facebookPageId, accessToken, 'fields=' +
    	'name,cover_photo,picture,updated_time,type,count,photos,description,likes{id}&limit=20', "albums"
    );

	function _fetchAlbums(facebookPageId, accessToken, url) {
	    return request.get(url)
	        .then(function (data) {
	            logger.info("Successfully fetched ALBUMS");
	            var current_album = JSON.parse(data);
	            albums = albums.concat(current_album.data);
	            if(current_album.paging.next && counter < 3) {
	            	counter = counter + 1;
	            	return _fetchAlbums(facebookPageId, accessToken, current_album.paging.next);
	            } else {
	            	deferred.resolve(albums);
	            	return deferred.promise;
	            }
	        })
	        .catch(function (err) {
	            logger.error("Error while fetching ALBUMS:\n", err);
	            logger.error("Request URL:", url);

	            throw err;
	        });
	}
    return _fetchAlbums(facebookPageId, accessToken, url);
}

function fetchEvents(facebookPageId, accessToken) {
	logger.info("Fetching EVENTS for " + facebookPageId + "page");
    var url = prepareGraphApiQuery(facebookPageId, accessToken, 'fields=' +
    	'events.limit(100){cover,name,attachments,description,updated_time,invited_count,start_time,end_time,ticket_uri,attending_count,maybe_count,place,likes{id}}'
    );
    return request.get(url)
        .then(function (data) {
            logger.info("Successfully fetched EVENTS");
            return JSON.parse(data);
        })
        .catch(function (err) {
            logger.error("Error while fetching EVENTS:\n", err);
            logger.error("Request URL:", url);

            throw err;
        });
}

function checkMajeekoPermissions (user) {
    return request.get("https://graph.facebook.com/v2.4/" + user.id + "/permissions?access_token=" + config.facebook.clientID +"|"+ config.facebook.clientSecret)
        .then(function (permissions) {
            permissions = JSON.parse(permissions);
            if(permissions.data && permissions.data.length > 0) {
                var status = _.get(_.findWhere(permissions.data, {"permission": "manage_pages"}), "status");
                if (status === "granted") {
                    return true;
                }
                return false;
            }
        })
        .catch(function (error) {
            logger.error(error);
            return false;
        });
}

module.exports = {
    prepareGraphApiQuery: prepareGraphApiQuery,

	fetchAppAccessToken: fetchAppAccessToken,

	exchangeUserToken: exchangeUserToken,
	fetchUserData: fetchUserData,

	searchPages: searchPages,
	fetchMinimalPageData: fetchMinimalPageData,
	fetchPageData: fetchPageData,
    checkMajeekoPermissions: checkMajeekoPermissions
};
