module.exports = function(router) {

	var db = require('../db'),
		auth = require('../auth'),
		R = require('ramda'),
		_ = require('lodash'),
		fs = require('fs')

	  , logger = require('../utils/logger')
	  , tophost = require('./api')
	  , config = require('../config')
	  , commonRoutes = require('../routes/common')
	  , facebookSync = require('../sync/facebook')
	  , tokenUtils = require('./tokens')

	  , mail = require('../utils/mail')

	router.use(function (req,res,next) {
		if((/^\/(api)\/.+/).test(req.originalUrl))
			return next()

		var domain, token;
		switch(req.method) {
			case 'GET':
				domain = req.query.domain
				token = req.query.token
				action = req.query.action
				break
			case 'POST':
				logger.info('tophost request body\n',req.body)
				domain = req.body.domain
				token = req.body.token
				action = req.body.action
				break
			default:
				return res.redirect(config.tophost_redirect)
		}

		if(domain && token && action) {
			if(!_.contains(['create','manage','destroy'],action))
				return res.send(400, 'Error: wrong action!')

			tophost.validateCredentials(domain,token,action)
			.then(function(success) {
				logger.info('Tophost:', 'successfully validated credentials', 'success', success)
				if(success) {
					req.session.tophost = {
			            domain: domain,
			            token: token
			        }

					db.getPageByDomain(domain)
					.then(function (page) {
						logger.info('Tophost:', 'page found')
						if(page) {
							switch(action) {
					        	case 'create':
					        		// return res.send(400, 'Error: domain already bound to a page')
								case 'manage':
									req.session.tophost.currentPage = {
										facebook_id: page.facebook_id,
										username: page.username
									}
									return res.redirect('/')
								case 'destroy':
									db.unbindPageFromTophostDomain(page,domain)
									.then(function () {
										mail.sendMailForTophostDeactivation(domain,page._id)
										logger.info('Page', page.username, 'unbound from domain', domain)
										res.send(200)
									}, function () {
										res.send(403)
									})
									try {
										delete req.session.tophost
										req.session.destroy()
									} catch(err) {}
							}
						} else {
							switch(action) {
					        	case 'create':
					        		return res.redirect('/page-selection')
								case 'manage':
								case 'destroy':
									return res.send(404, 'Error: domain not found')
							}
						}
					})
				} else {
					return action == 'destroy' ? res.send(403) : res.redirect(config.tophost_redirect)
				}
			})
		} else if(req.session && req.session.tophost && req.session.tophost.domain && req.session.tophost.token)  {
			if(!req.isAuthenticated()) return res.redirect('/auth/facebook')
			if(req.session.tophost.currentPage) {
				if(!(/\/editor\/.+/).test(req.originalUrl))
					return res.redirect('/editor/' + req.session.tophost.currentPage.username)
				if(req.session.tophost.user_owns_page) next()
				else {
					db.checkUserOwnsPage(req.user.id, req.session.tophost.currentPage.facebook_id)
					.then(function (owns) {
						logger.info('OWNS:', owns)
						if(owns) {
							req.session.tophost.user_owns_page = true
							next()
						} else {
							req.session.destroy()
							res.render('error', {message: "L'attuale utente facebook non è autorizzato a gestire questa pagina."})
						}
					})
					.catch(function (err) {
						logger.error(err)
						res.sendStatus(500)
					})
				}
			} else {
				if(!(/\/page-selection\/{0,1}/).test(req.originalUrl))
					return res.redirect('/page-selection')
				next()
			}
		} else {
			return res.redirect(config.tophost_redirect)
		}
	})

	router.get('/page-selection', auth.ensureAuthenticated, function(req, res) {
		db.getUserByFacebookId(req.user.id)
		    .then(function(user) {
		    	user.facebook.accounts = user.facebook.accounts.filter(R.prop('is_published'))
		        res.render('tophost-page-selection', {
		        	title: 'Majeeko | Tophost Page Selection'
		          , user: user
		          , first_time: req.user.firstLogin
		          , is_tophost: !!req.session.tophost
		        });
		        delete req.user.firstLogin
		    })
	});

	router.get('/editor/:pageName', commonRoutes.editor)

	router.post('/api/bind-page-to-domain', auth.ensureAuthenticated, function (req,res) {
		if(!req.session.tophost || req.session.tophost.binding_page) return res.send(403)
		if(!req.body.id) return res.send(400)
		req.session.tophost.binding_page = true
		req.session.save()
		return db.checkUserOwnsPage(req.user.id,req.body.id)
		.then(function (owns) {
			if(!owns) {
				delete req.session.tophost.binding_page
				req.session.save()
				return res.send(403)
			}

			facebookSync.syncPage(req.body.id)
			.then(function () {
				mail.sendMailForTophostActivation(req.session.tophost.domain,req.body.id)
				return db.bindPageToTophostDomain(req.body.id, req.session.tophost.domain)
				.then(function() {
					tophost.confirmPage(req.session.tophost.domain,req.session.tophost.token)
					return db.getPageByDomain(req.session.tophost.domain)
					.then(function(page) {
						if(!page) {
							return res.send(404);
						}
						req.session.tophost.currentPage = page
						delete req.session.tophost.binding_page
						req.session.save()
						res.send(200)
					})
					.catch(function(err) {
						logger.error(err)
						delete req.session.tophost.binding_page
						req.session.save()
						res.send(500)
					})
				})
				.catch(function (err) {
					logger.error(err)
					delete req.session.tophost.binding_page
					req.session.save()
					res.send(500)
				})
			})
			.catch(function (err) {
				logger.error(err)
				delete req.session.tophost.binding_page
				req.session.save()
				res.send(500)
			})
		})
		.catch(function (err) {
			logger.error(err)
			delete req.session.tophost.binding_page
			req.session.save()
			return res.send(500)
		})
	})

	// router.post('/api/tophost', function (req,res) {
	// 	res.send('here i am')
	// 	if(!req.body.token || !req.body.domain || !req.body.action)
	// 		return res.send(400)

	// 	logger.info(req.body)

	// 	switch(req.body.action) {
	// 		case 'authorize':
	// 			tokenUtils.consume(req.body.token,req.body.domain)
	// 			.then(function (result) {
	// 				logger.info('token consumation result', result)
	// 				res.send((result == true) ? 200 : 403)
	// 			})
	// 			break
	// 		default:
	// 			res.send(400)
	// 	}
	// })

	return router;
}
