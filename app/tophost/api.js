var Q = require('q')
  , request = require('request-promise')
  , bodyParser = require('body-parser')

  , config = require('../config')
  , logger = require('../utils/logger')
  // , tokenUtils = require('./tokens')
  , mail = require('../utils/mail')

module.exports.validateCredentials = function (domain,token,action) {
	var req =  request.post(config.tophost_endpoint, { form: {domain:domain, token:token, action: 'authorize', requested_action:action}, json: true, resolveWithFullResponse: true })
	logger.info('Tophost:', 'validateCredentials', domain, token)
	// logger.info('Tophost full request', bodyParser.json(req))
	return req.then(function(res) { 
			logger.info('Tophost response:\n' + res.body)
			return res.statusCode == 200 && res.body == 'OK'
		})
		.catch(function(err) { 
			logger.error(err)
			return false 
		})
}

module.exports.confirmPage = function (domain,token) {
	logger.info('Tophost:', 'confirmPage')
	var deferred = Q.defer()
	  , maxAttempts = 10
	var recursion = function(domain,token,attempts) {
		if(attempts <= 0) {
			logger.error('Unable to notify tophost of page activation after ' + maxAttempts + ' attempts')
			mail.sendMailForTophostProblem("Dopo " + maxAttempts + " tentativi è risultato impossibile da parte del server Majeeko confermare l'avvenuta selezione della pagina da abbinare al dominio \"" + domain + "\". Si conferma che la pagina è online ed è possibile procedere con l'impostazione dei DNS.")
			return
		}
		logger.info('Communicating page activation to tophost, attempt ', maxAttempts - attempts + 1, 'token:', token)
		
		// logger.info('token generated', token)
		request.post(config.tophost_endpoint, { form: {domain:domain, token:token, action: 'build'}, json: true, resolveWithFullResponse: true })
			.then(function(res) { 
				if(res.statusCode != 200 && res.body != 'OK') {
					setTimeout(recursion,10000,domain,token,attempts-1)
				} else {
					logger.info('Succesfully confirmed page binding with Tophost')
					deferred.resolve(true)
				}
			})
			.catch(function(err) { 
				logger.error(err)
				setTimeout(recursion,10000,domain,token,attempts-1)
			})

	}
	// tokenUtils.create(domain)
	// .then(function (token) {
	recursion(domain,token,maxAttempts);
	// })
	// .catch(function (err) {
	// 	logger.error(err)
	// })

	return deferred.promise;
}