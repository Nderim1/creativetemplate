module.exports = function(req, res, next) {
	res.header('X-Powered-By', 'Majeeko');
	next();
};