var colors = require('colors'),
	async = require('async'),
	moment = require('moment'),
	R = require('ramda'),
	_ = require('lodash'),
	Q = require('q'),
	fs = require('fs'),

	config = require('./config'),
	logger = require('./utils/logger'), // winston
	facebook = require('./facebook'),
	facebookSync = require('./sync/facebook'),
	db = require('./db'),
	mail = require('./utils/mail'),
	pageUtils = require('./utils/pages');

function userCanImpersonate (profile) {
	var deferred = Q.defer();
	fs.readFile(__dirname + '/auth/admins.json', function (err, data) {
		if (err) {
			return deferred.reslove(false);
		}
		admins = JSON.parse(data);
		if(_.includes(admins.accounts, profile.id)){
			return deferred.resolve(true);
		} else {
			return deferred.resolve(false);
		}
	});
	return deferred.promise;
}

function onSuccessfulFacebookAuth (req, shortLiveAccessToken, refreshToken, profile, done){
	if (req.session.impersonate) {
		userCanImpersonate(profile)
		.then(function (can){
			if(can) {
				require('./utils/impersonate')(req.session.impersonate)(shortLiveAccessToken, refreshToken, profile, done);
			} else {
				done(null, false, {message:"Feature forbidden for the current user."});
			}
		});
	} else {
		_onSuccessfulFacebookAuth(shortLiveAccessToken, refreshToken, profile, done);
	}
}

function _onSuccessfulFacebookAuth (shortLiveAccessToken, refreshToken, profile, done) {
	var firstLogin = false;

	facebook.fetchUserData(profile.id, shortLiveAccessToken)
		.then(function(userData) {
			db.getUserByFacebookId(profile.id)
				.then(function(user) {
					if (!user) {
						firstLogin = true;
						logger.info('New user registered', profile.name, moment().toISOString());
						if (profile._json.email) {
							try {
								mail.sendWelcomeMessage(profile._json.email, profile.name.givenName, profile.name.familyName, profile.gender);
							} catch (err) {
								logger.error(err);
							}
						}
					}
					facebookSync.saveUserData(profile, userData, shortLiveAccessToken)
						.then(function() {
							facebook.exchangeUserToken(config, shortLiveAccessToken)
								.then(function(longLiveAccessToken) {
									facebookSync.saveUserData(profile, userData, longLiveAccessToken)
										.then(function() {
											db.updateUserReady(profile.id, true);
											if (!userData.accounts) userData.accounts = {};
											if (!userData.accounts.data) userData.accounts.data = [];
											var accounts = userData.accounts.data.filter(R.prop('is_published'));
											logger.log('before accounts'.red);
											async.each(accounts, function(account, callback) {
												pageUtils.createFromAccount(account);
												// facebookSync.createPage(account.id, account.access_token)
												//   .finally(function() {
												//     facebookSync.syncPage(account.id, account.access_token)
												//       .then(function() {
												//         logger.info('All page data downloaded for page "' + account.name + '"')
												//         callback();
												//       })
												//       .catch(function(err) {
												//         logger.error(err)
												//       })
												//   })
											}, function() {
												logger.info('All went well');
											}, function(err) {
												logger.error(err);
											});
										});
								})
								.catch(function(err) {
									logger.error(err);
								});

							process.nextTick(function() {
								logger.log('nextTick'.red);
								done(null, _.extend({
									firstLogin: firstLogin
								}, profile));
							});
						});
				});
		})
		.catch(function(err) {
			logger.error('Error on successfull facebook auth', {
				error: err,
				stack: err.stack,
				filename: __filename
			});
		});
}

module.exports = onSuccessfulFacebookAuth;
