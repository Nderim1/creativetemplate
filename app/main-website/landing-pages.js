var _ = require('lodash');
var fs = require('fs');
var router = require('express').Router();

var db = require('../db');
var logger = require('../utils/logger');

router.get('/custom/:id', function(req, res) {
	var example =  _.sample(require('../examples/websites'));
	var banner_bg = "/images/banner/default.jpg";
	example.banner_bg = banner_bg;
	db.getPageById(req.params.id)
		.then(function(page){
			if(!page) {
				throw 404;
			} else {
				res.render('landing/custom', {
					title: 'Majeeko | Custom Landing Page (' + req.params.id + ')'
		          , pageId: req.params.id
				  , example: example
				  , pageName: page.pageData.name
		        }, function(err, html) {
					if(err) {
						logger.error('Trying to get 404 page in template', err)
						res.redirect('/');
					} else {
						res.end(html)
					}
				});

			}
		})
		.catch(function(err) {
			if(err == 404) {
				res.render(404);
			} else {
				res.status(500).send();
			}
		})
})

router.get('/custom2/:id', function(req, res) {
	var example =  _.sample(require('../examples/websites'));
	var banner_bg = "/images/banner/default.jpg";
	example.banner_bg = banner_bg;
	db.getPageById(req.params.id)
		.then(function(page){
			if(!page) {
				throw 404;
			} else {
				res.render('landing/custom2', {
					title: 'Majeeko | Custom Landing Page (' + req.params.id + ')'
		          , pageId: req.params.id
				  , example: example
				  , pageName: page.pageData.name
		        }, function(err, html) {
					if(err) {
						logger.error('Trying to get 404 page in template', err)
						res.redirect('/');
					} else {
						res.end(html)
					}
				});

			}
		})
		.catch(function(err) {
			if(err == 404) {
				res.render(404);
			} else {
				res.status(500).send();
			}
		})
})

// router.get('/custom3/:id', function(req, res) {
// 	var example =  _.sample(require('../examples/websites'));
// 	var banner_bg = "/images/banner/default.jpg";
// 	example.banner_bg = banner_bg;
// 	db.getPageById(req.params.id)
// 		.then(function(page){
// 			if(!page) {
// 				throw 404;
// 			} else {
// 				res.render('landing/easy_landings/custom3', {
// 					title: 'Majeeko | Custom Landing Page (' + req.params.id + ')'
// 		          , pageId: req.params.id
// 				  , example: example
// 				  , pageName: page.pageData.name
// 		        }, function(err, html) {
// 					if(err) {
// 						logger.error('Trying to get 404 page in template', err)
// 						res.redirect('/');
// 					} else {
// 						res.end(html)
// 					}
// 				});

// 			}
// 		})
// 		.catch(function(err) {
// 			if(err == 404) {
// 				res.render(404);
// 			} else {
// 				res.status(500).send();
// 			}
// 		})
// })

router.get('/:page', function(req, res) {
	var example =  _.sample(require('../examples/websites'));

	var banner_bg = "/images/banner/" + example.category_id + ".jpg";
	try {
		fs.openSync(__dirname + '/../../public' + banner_bg, 'r')
	} catch (err) {
		banner_bg = "/images/banner/default.jpg";
	}

	example.banner_bg = banner_bg;
	var endUrl = /seeweb/gi.test(req.params.page) ? 'landing/'+req.params.page : 'landing/generica_'+req.params.page;
	res.render(endUrl, {
		title: 'Majeeko | Generic Landing Page (' + req.params.page + ')'
      , example: example
    }, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.redirect('/');
		} else {
			res.end(html)
		}
	});
})

router.get('/:name/:page', function(req, res) {
	var exampleId = (req.params.name == 'wellness') ? '410058729061867' : '294128740754220'
	var example =  _.findWhere(require('../examples/websites'), {id:exampleId});

	var banner_bg = "/images/banner/" + example.category_id + ".jpg";
	try {
		fs.openSync(__dirname + '/../../public' + banner_bg, 'r')
	} catch (err) {
		banner_bg = "/images/banner/default.jpg";
	}

	example.banner_bg = banner_bg;
	res.render('landing/' + req.params.name + '_' + req.params.page, {
		title: 'Majeeko | ' + (req.params.name == 'wellness' ? 'Wellness' : 'Food') + ' Landing Page (' + req.params.page + ')'
      , example: example
    }, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.redirect('/');
		} else {
			res.end(html)
		}
	});
})

module.exports = router;
