var router = require('express').Router();
var _ = require('lodash');

var logger = require('../utils/logger');

// router.get('/landing_new', function(req, res) {
// 	res.render('landing/easy_landings/landing_new.html', function(err, html) {
// 		if(err) {
// 			logger.error('Trying to get 404 page in template', err)
// 			res.render(404);
// 		} else {
// 			res.end(html)
// 		}
// 	})
// });

router.get('/mjk-e-commerce', function(req, res) {
	res.render('landing/easy_landings/mjk-e-commerce.html', {description: "Vendi online i tuoi prodotti dal sito web", accedi: "http://go.majeeko.com/login_mjk_ec"}, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.render(404);
		} else {
			res.end(html)
		}
	})
});

router.get('/e-commerce', function(req, res) {
	res.render('landing/easy_landings/e-commerce.html', {description: "Vendi online i tuoi prodotti dal sito web", accedi: "http://go.majeeko.com/login_ec"}, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.render(404);
		} else {
			res.end(html)
		}
	})
});

router.get('/passaapro', function(req, res) {
	res.render('landing/easy_landings/e-commerce-upgrade.html', {description: "Attiva ora il tuo e-commerce", accedi: "http://go.majeeko.com/login_to-pro"}, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.render(404);
		} else {
			res.end(html)
		}
	})
});

router.get('/reseller', function(req, res) {
	res.render('landing/easy_landings/reseller.html', {description: "Crea siti magici per i tuoi clienti", accedi: "http://go.majeeko.com/login_reseller"}, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.render(404);
		} else {
			res.end(html)
		}
	})
});

router.get('/enjore', function(req, res) {
	res.render('landing/easy_landings/enjore.html', {description: "Fai goal con il tuo sito", accedi: "http://go.majeeko.com/enjore_login"}, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.render(404);
		} else {
			res.end(html)
		}
	})
});

router.get('/carousel/:id?', function(req, res) {
	var carouselIndex = 0;
	var description = "hai un'attività?";
	var accedi = "http://go.majeeko.com/login_lc";
	// description = ""
	if("palestra" == req.params.id){carouselIndex = 0;}
	if("artigiano" == req.params.id){carouselIndex = 1;}
	if("ristorante" == req.params.id){carouselIndex = 2;}
	if("negozio" == req.params.id){carouselIndex = 3;}
	if("caffe" == req.params.id){carouselIndex = 4;}
	if("estetica" == req.params.id){carouselIndex = 5;}
	// if("wellness" == req.params.id){carouselIndex = 6;}
	// if("hotel" == req.params.id){carouselIndex = 7;}
	res.render('landing/easy_landings/carousel.html', {carouselIndex: carouselIndex, description:description, accedi:accedi}, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.render(404);
		} else {
			res.end(html)
		}
	})
});

router.get('/goo/:id?', function(req, res) {
	var carouselIndex = 0;
	var description = "hai un'attività?";
	var accedi = "http://go.majeeko.com/login_goo";

	if("palestra" == req.params.id){carouselIndex = 0;}
	if("artigiano" == req.params.id){carouselIndex = 1;}
	if("ristorante" == req.params.id){carouselIndex = 2;}
	if("negozio" == req.params.id){carouselIndex = 3;}
	if("caffe" == req.params.id){carouselIndex = 4;}
	if("estetica" == req.params.id){carouselIndex = 5;}
	
	res.render('landing/easy_landings/carousel.html', {carouselIndex: carouselIndex, description:description, accedi:accedi}, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.render(404);
		} else {
			res.end(html)
		}
	})
});

router.get('/:id?', function(req, res) {
	var carouselIndex = 0;
	var description = "Crea il tuo sito web in un click";
	var accedi = "http://go.majeeko.com/login_lc";
	// description = ""
	if("negozio" == req.params.id){carouselIndex = 0;}
	if("artigiano" == req.params.id){carouselIndex = 1;}
	if("ristorante" == req.params.id){carouselIndex = 2;}
	res.render('landing/easy_landings/category.html', {carouselIndex: carouselIndex, description:description, accedi:accedi}, function(err, html) {
		if(err) {
			logger.error('Trying to get 404 page in template', err)
			res.render(404);
		} else {
			res.end(html)
		}
	})
});

/*
---------------------------------------------------------
qui é da inserire lo script che apre il modale quando si chiude la tab
---------------------------------------------------------
*/

/*
---------------------------------------------------------
qui é da sistemare lo script per dinamicizzare le landing 
dello scraper con le preview dei siti generati
---------------------------------------------------------
*/





module.exports = router;
