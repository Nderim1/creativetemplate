module.exports = function(router) {
	var R = require('ramda'),
	_ = require('lodash'),
	fs = require('fs'),
	passport = require('passport'),
	session = require('express-session'),
	MongoStore = require('connect-mongo')(session),
	FacebookStrategy = require('passport-facebook').Strategy,
	config = require('../config'),
	db = require('../db'),
	auth = require('../auth'),
	purchase = require('../purchase'),
	logger = require('../utils/logger'),
	commonRoutes = require('../routes/common'),
	captcha = require('../utils/captcha'),
	mailService = require('../utils/mail'),
	facebook = require('../facebook'),
	postLoginSetup = require('../post-login-setup');

	router.use(function(req,res,next) {
		if(req.session.tophost && req.session.tophost.domain && req.session.tophost.token)
			return res.redirect(req.protocol + '://tophost.' + config.primary_domain);
		else
			next();
	});

	router.use(function (req,res,next) {
		// if(req.hostname == process.env.HOST) {
		// 	return res.redirect(req.protocol + '://www.' + process.env.HOST + req.originalUrl)
		// }
		if(req.hostname.substr(0,4) == 'www.') {
			return res.redirect(req.protocol + '://' + process.env.HOST + req.originalUrl);
		}
		return next();
	});

	if(process.env.NODE_ENV != 'production') {
		router.get('/uc', function(req, res) {
			var example =  _.sample(require('../examples/websites'));

			var banner_bg = "/images/banner/" + example.category_id + ".jpg";
			try {
				fs.openSync(__dirname + '/../../public' + banner_bg, 'r');
			} catch (err) {
				banner_bg = "/images/banner/default.jpg";
			}

			example.banner_bg = banner_bg;
			if(req.user) {
				db.getUserByFacebookId(req.user.id)
				    .then(function(user) {
				        res.render('courtesy', {
				            user: user,
				            example: example
				        });
				    });
			} else {
				res.render('courtesy', {
		            example: example
		        });
			}
		});

		router.get('/landing-tophost', function(req, res) {
			var example =  _.sample(require('../examples/websites'));

			var banner_bg = "/images/banner/" + example.category_id + ".jpg";
			try {
				fs.openSync(__dirname + '/../../public' + banner_bg, 'r');
			} catch (err) {
				banner_bg = "/images/banner/default.jpg";
			}

			example.banner_bg = banner_bg;
			if(req.user) {
				db.getUserByFacebookId(req.user.id)
				    .then(function(user) {
				        res.render('tophost/landing', {
				            user: user,
				            example: example
				        });
				    });
			} else {
				res.render('tophost/landing', {
		            example: example
		        });
			}
		});
	}

	router.use('/landing', require('./easy-landing-pages'));
	router.use('/landing', require('./landing-pages'));

	/**
	 *
	 *	LANDING PAGES ROUTES :: END
	 *
	 */

	router.get('/', function(req, res) {
		if(req.isAuthenticated())
			return res.redirect('/manager');

		var example =  _.sample(require('../examples/websites'));

		var banner_bg = "/images/banner/" + example.category_id + ".jpg";
		try {
			fs.openSync(__dirname + '/../../public' + banner_bg, 'r');
		} catch (err) {
			banner_bg = "/images/banner/default.jpg";
		}

		example.banner_bg = banner_bg;
		if(req.user) {
			db.getUserByFacebookId(req.user.id)
				.then(function(user) {
					res.render('homepage', {
						is_main_homepage: true,
						title: 'Majeeko | Crea il tuo sito web con i contenuti presenti nella tua Pagina Facebook!',
						user: user,
						example: example
					});
				});
		} else {
			res.render('homepage', {
				title: 'Majeeko | Crea il tuo sito web con i contenuti presenti nella tua Pagina Facebook!',
				example: example,
				is_main_homepage: true
			});
		}
	});

	router.get('/pages/privacy', function(req, res) {
		res.render('generic', {
			title: 'Majeeko | Privacy Policy',
			pageContent: "privacy",
			isAuthenticated: req.isAuthenticated()
		});
	});

	router.get('/pages/terms', function(req, res) {
		res.render('generic', {
			title: 'Majeeko | Terms',
			pageContent: "terms",
			isAuthenticated: req.isAuthenticated()
		});
	});

	router.get('/pages/cookies', function(req, res) {
		res.render('generic', {
			title: 'Majeeko | Cookies',
			pageContent: "cookies",
			isAuthenticated: req.isAuthenticated()
		});
	});

	var managerRoute = function(req, res) {
		db.getUserByFacebookId(req.user.id)
			.then(function(user) {
				user.facebook.accounts = user.facebook.accounts.filter(R.prop('is_published'));
				res.render('manager', {
					title: 'Majeeko | ' + (req.user.firstLogin ? 'Welcome Manager' : 'Manager'),
					user: user,
					first_time: req.user.firstLogin
				});
				delete req.user.firstLogin;
			});
	};

	router.get('/manager/welcome', auth.ensureAuthenticated, function (req, res, next) {
		if(!req.user.firstLogin)
			return res.redirect('/manager');
		else
			next();
	}, managerRoute);
	router.get('/manager', auth.ensureAuthenticated, managerRoute);

	router.get('/manager/grant-permission', function (req, res, next) {
		db.getUserByFacebookId(req.query.id)
			.then(function (user) {
				res.render('grant-permission', {
					title: 'Majeeko | Manager',
					user: user
				});
			});
	});

	router.get('/editor/:pageName', commonRoutes.editor);

	//BRAINTREE INTEGRATION

	router.get('/client_token', auth.ensureAuthenticated,function(req,res){
	    purchase.braintree.generateClientToken({},function(err,response) {
	    	if(!err) {
	    		res.send(response.clientToken);
	    	} else {
	    		logger.error(err);
	    		res.status(500).send();
	    	}
	    });
	});

	router.get('/thank-you', auth.ensureAuthenticated, function(req, res) {
		db.getUserByFacebookId(req.user.id).then(function(user) {
			// res.render('thank-you', {user:user})
			res.render('manager', {
					title: 'Majeeko | Manager',
					user: user,
					thankYou:true
				});
		});
	});

	router.get('/money-transfer-instructions', auth.ensureAuthenticated, function(req, res) {
		var order = req.session.order;
		if(!order) {
			return res.redirect('/');
		}
		db.getUserByFacebookId(req.user.id).then(function(user) {
			res.render('money-transfer-instructions', {user:user, order:order});
		});
	});

	router.post('/send-contact-form', function(req, res, next) {
		if(!req.body['g-recaptcha-response']) {
			console.log('Missing captcha in request (route: /send-contact-form)');
			res.json(400, { error: "Missing captcha in request!"});
		}
		captcha.verify(req.body['g-recaptcha-response'], req.ip)
			.then(function(verified) {
				if(!verified) {
					return res.json({ success: false, message: 'error-captcha' });
				}

				var domain = config.primary_domain;
				var to = "info@majeeko.com";

				mailService.sendContactForm(
					to,
					domain,
					req.body.name.content,
					req.body.surname.content,
					req.body.email.content,
					req.body.message.content,
					req.headers.host
				)
				.then(function (result) {
					console.log("mail sent to: " + to);
					if(result.status == "rejected") {
						logger.error("Mail rejected, reason: " + result.reject_reason);
						res.json({ success: false, message: 'error-email-address' });
					}
					res.json({ success: true, message: result });
				})
				.catch(function (err) {
					logger.error(err);
					res.json({ success: false, message: 'error-sending-mail' });
				});
			});
	});

	return router;
};
