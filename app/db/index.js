var config = require('../config'),
	R = require('ramda'),
	Q = require('q'),
	moment = require('moment'),
	_ = require('lodash'),
	logger = require('../utils/logger'),
	pwAnalytics = require('../utils/analytics/piwik'),
	pageUtils = require('../utils/pages'),
	siteMap = require('../utils/google-sitemap/api'),
	db = require('./db');

logger.debug('mongo connection', config.mongo_connection);

function updateUserReady(facebookId, ready) {
	return db.users.update({ 'facebook.profile.id': facebookId }, { $set:{ ready: ready } });
}

module.exports.updateUserReady = updateUserReady;

function checkPageUsernameFree(pageUsername) {
	return db.pages.count({ 'pageData.username': pageUsername }).then( R.equals(0) );
}

module.exports.checkPageUsernameFree = checkPageUsernameFree;

function updatePageConfiguration(pageUsername, configuration, action) {

	// this isjust work-around, wating for rewrite of the all configuration object and normalizing of the db

	if(action == "save") {
		return db.pages.update({ 'username': pageUsername }, { $set: { "configuration.editor.preview": configuration } });
	} else if(action == "ce") {
		return getPageByUsername(pageUsername)
			.then(function (page) {
				return pageUtils.setDefaultColors(page, configuration.graphics.template)
					.then(function (preview) {
						configuration.graphics.colors = preview.graphics.colors;
						configuration._css = preview._css;
						return db.pages.update({ 'username': pageUsername }, { $set: { "configuration.editor.preview": configuration } });
					})
					.catch(function (err) {
						logger.error('Error saving page with default colors', {
							error: err,
							stack: err.stack,
							filename: __filename,
							meta: {
								page_username: pageUsername,
							}
						});
					});
			});
	} else if(action == "reset") {
		return getPageByUsername(pageUsername).then(function (page) {
			return db.pages.update({ 'username': pageUsername }, { $set: { "configuration.editor.preview": _.omit(page.configuration, "editor") } });
		});
	}
	//publish
	return db.pages.update({ 'username': pageUsername }, { $set: { "configuration.settings": configuration.settings, "configuration.graphics": configuration.graphics,
																	"configuration.contents": configuration.contents, "configuration._css": configuration._css,
																	"configuration.editor.preview": configuration  }});
}

module.exports.updatePageConfiguration = updatePageConfiguration;

function getPageById(pageId) {
	return db.pages.findOne({ 'facebook_id': pageId });
}

module.exports.getPageById = getPageById;

function getPageByUsername(pageUsername) {
	return db.pages.findOne({ 'username': pageUsername });
}

module.exports.getPageByUsername = getPageByUsername;

function getPageByDomain(domain) {
	logger.debug('getting page for domain', domain);
	var other_domain = /www\./gi.test(domain) ? domain.replace('www.', '') : "www." + domain;
	var $inObject = {
		$in: [domain, other_domain]
	};
	return db.pages.findOne({domains: $inObject});
}

module.exports.getPageByDomain = getPageByDomain;

function getPageBySubdomain(subdomain) {
	return db.pages.findOne({
		$or: [{
			'username': { $regex : new RegExp('^' + subdomain + '$', "i") }
		}, {
			'facebook_id': subdomain
		}]
	});
}

module.exports.getPageBySubdomain = getPageBySubdomain;

function activatePageForDomains (pageFacebookId, domains, activator, isPro) {
	return getPageById(pageFacebookId)
		.then(function (page) {
			if(!page) {
				var deferred = Q.defer();
				deferred.reject({message:"page doesn't exist"});
				return deferred.promise;
			}
			return page;
		}).then(function(page){

			var pageObject = {
				paid_on: moment().toISOString(),
				isPro: isPro ? moment().toISOString() : false
			};

			var activations = domains.map(function(domain) {
				return {
					domain: /www\./gi.test(domain) ? domain : "www."+domain,
					created_at: moment().toISOString(),
					activator: activator || 'majeeko'
				};
			});

			pageObject.activations = (page.activations || []).concat(activations);
			pageObject.domains = _.uniq((page.domains || []).concat(pageObject.activations
				.filter(function(act) {
					return !act.deleted_at;
				})
				.map(function(act) {
					return act.domain;
				})));
			return db.pages.update({
				facebook_id: pageFacebookId
			}, {
				$set: pageObject
			});
		});
}

module.exports.activatePageForDomains = activatePageForDomains;

function getUserByFacebookId(facebookId) {
	return db.users.findOne({ 'facebook.profile.id': facebookId });
}

module.exports.getUserByFacebookId = getUserByFacebookId;

function getUserAccounts(facebookId) {
	return getUserByFacebookId( facebookId ).then( R.compose( R.filter(R.prop('is_published')), R.prop('accounts'), R.prop('facebook') ) );
}

module.exports.getUserAccounts = getUserAccounts;

function checkUserOwnsPage(facebookId, pageId) {
	return getUserAccounts( facebookId )
	.then(function(accounts) {
		logger.info('User accounts', {accounts:accounts});
		return _.contains(accounts.map(R.prop('id')), pageId);
	})
	.catch(function (err) {
		logger.error(err);
		return false;
	});
}

module.exports.checkUserOwnsPage = checkUserOwnsPage;

function getUserAccountsIds(userFacebookId) {
	return getUserAccounts(userFacebookId).then(function(accounts){
		return accounts.map(R.prop('id'));
	});
}

module.exports.getUserAccountsIds = getUserAccountsIds;

function getPagesByIds(ids) {
	return db.pages.find({ 'facebook_id': { $in: ids } }).toArray()
}

module.exports.getPagesByIds = getPagesByIds;

function getUserPages(userFacebookId) {
	return getUserAccountsIds(userFacebookId).then(getPagesByIds);
}

module.exports.getUserPages = getUserPages;

function bindPageToTophostDomain(pageId,domain) {
	return activatePageForDomains(pageId, [domain], 'tophost');
}

module.exports.bindPageToTophostDomain = bindPageToTophostDomain;

function unbindPageFromTophostDomain(page,domain) {
	var deferred = Q.defer();
	try{
		var activation = _.find(page.activations, function (act) { return act.domain == domain && act.activator == 'tophost' && !act.deleted_at;});
		if(activation) {
			activation.deleted_at = moment().toISOString();
			var pageObject = {
				activations: page.activations
			};
			pageObject.domains = _.uniq(_.without(page.domains,domain));
			if(!pageObject.domains.length) pageObject.paid_on = false;
			return db.pages.update({_id:page._id},{$set:pageObject});
		} else {
			logger.error('No activation found for requested TopHost destroy action', domain);
			deferred.reject();
			return deferred.promise;
		}
	} catch (err) {
		logger.error(err);
		deferred.reject();
		return deferred.promise;
	}
}

function activateSitemapsForId(id) {
	return db.pages.findOne({
			'facebook_id': id
		}, {
			'domains': 1
		})
		.then(function(page) {
			var promises = page.domains.filter(function(elem) {
				return !/www/gi.test(elem);
			}).map(function(elem) {
				return siteMap.activateSiteMap(elem, id);
			});
			return Q.allSettled(promises).then(function(result) {
					return result;
				})
				.catch(function(err) {
					logger.error(err);
					return err;
				});
		});
}

function activateSitemapsForPaidPages() {
	return db.pages.find({
			'domains': {
				$exists: true
			},
			'siteMap': {
				$exists: false
			}
		}, {
			'facebook_id': 1
		})
		.toArray()
		.then(function(result) {
			var ids = result.map(function(elem) {
				return elem.facebook_id;
			});
			return Q.allSettled(ids.map(function(elem) {
					return activateSitemapsForId(elem);
				})).then(function(result) {
					return result;
				})
				.catch(function(err) {
					logger.error(err);
					return err;
				});
		})
		.catch(function(err) {
			return err;
		});
}

function getAlbumById(albumId) {
	return db.albums.findOne({ 'id': albumId });
}

module.exports.getAlbumById = getAlbumById;

function getPostById(postId) {
	return db.posts.findOne({ 'id': postId });
}

module.exports.getPostById = getPostById;

function getEventById(eventId) {
	return db.events.findOne({ 'id': eventId });
}

module.exports.getEventById = getEventById;

function getStoreProducts(pageID) {
	return getPageById(pageID).then(function (page) {
		return _.get(page, 'modules.store.products');
	});
}

module.exports.getStoreProducts = getStoreProducts;

function getStoreProductByID(pageID, productID) {
	return getPageById(pageID).then(function (page) {
		var products = _.filter(_.flatten(_.map(_.get(page, 'modules.store.products.category'), function (category) {
					return category.products;
				})), Boolean);
		return _.findWhere(products, {productID: productID});
	});
}

module.exports.getStoreProductByID = getStoreProductByID;

function deleteStoreProduct(pageID, product) {
	return getPageById(pageID).then(function (page) {
		var categoryIndex = _.findIndex(_.get(page, 'modules.store.products.category'), {categoryName: product.productCategory});
		var objectToPull= {};
		objectToPull['modules.store.products.category.'+categoryIndex+'.products'] = {productID: product.productID};
		return  db.pages.update({ 'facebook_id': pageID }, { $pull: objectToPull});
	});
}

module.exports.deleteStoreProduct = deleteStoreProduct;

function saveStoreProduct(pageID, product) {
	return getPageById(pageID).then(function (page) {
			product.productID =  product.productID || generateID();
		var categories = _.get(page, 'modules.store.products.category'),
			products = _.filter(_.flatten(_.map(categories, function (category) {
					return category.products;
				})), Boolean),
			oldProductIndex = _.findIndex(products, {productID: product.productID}),
			oldProduct,
			updatePath,
			newProduct = {},
			newProductCategoryIndex = _.findIndex(categories, { categoryName: product.productCategory });
		if(oldProductIndex != -1) {
			oldProduct = products[oldProductIndex];
			var oldProductCategoryIndex = _.findIndex(categories, { categoryName: oldProduct.productCategory });
			// product category change
			if(product.productCategory !== oldProduct.productCategory) {
				var productToDelete = {};
				updatePath = "modules.store.products.category." + oldProductCategoryIndex + ".products";
				insertPath = "modules.store.products.category." + newProductCategoryIndex + ".products";
				newProduct[insertPath] = product;
				productToDelete[updatePath] = {
					productID: oldProduct.productID
				};
				return db.pages.update({ 'facebook_id': pageID }, {
					$addToSet: newProduct,
					$pull: productToDelete
				}).then(function (response) {
					return response;
				}).catch(function (response) {
					logger.error(error);
					return false;
				});
			} else {
				var oldProductCategory = categories[oldProductCategoryIndex];
				oldProductIndex = _.findIndex(oldProductCategory.products, { productID: oldProduct.productID});
				updatePath = "modules.store.products.category." + newProductCategoryIndex + ".products." + oldProductIndex;
				newProduct[updatePath] = product;
				return db.pages.update({ 'facebook_id': pageID }, {
					$set: newProduct
				}).then(function (response) {
					return response;
				}).catch(function (response) {
					logger.error(error);
					return false;
				});
			}
		} else {
			updatePath = "modules.store.products.category." + newProductCategoryIndex + ".products";
			newProduct[updatePath] = product;
			return db.pages.update({ 'facebook_id': pageID }, {
				$addToSet: newProduct
			}).then(function (response) {
				return response;
			}).catch(function (error) {
				logger.error(error);
				return false;
			});
		}
	});
}

module.exports.saveStoreProduct = saveStoreProduct;

function getStoreCategoryByID(pageID, categoryID) {
	return getPageById(pageID).then(function (page) {
		var categories = _.map(_.get(page, 'modules.store.products.category'), function (category) {
			return {
				categoryName: category.categoryName,
				categoryID: category.categoryID
			};
		});
		return _.findWhere(categories, {categoryID: categoryID});
	});
}

module.exports.getStoreCategoryByID = getStoreCategoryByID;

function getStoreCategories(pageID) {
	return getPageById(pageID).then(function (page) {
		var categories = _.map(_.get(page, 'modules.store.products.category'), function (category) {
			return {
				categoryName: category.categoryName,
				categoryID: category.categoryID
			};
		});
		return categories;
	});
}

module.exports.getStoreCategories = getStoreCategories;

function saveStoreCategory(pageID, category) {
	return getPageById(pageID).then(function (page) {
		var categories = _.get(page, 'modules.store.products.category');
		var savedCategoryIndex = _.findIndex(categories, {categoryID: category.categoryID});
		if(savedCategoryIndex != -1) {
			category = {
				categoryName: category.categoryName,
				categoryID: category.categoryID,
				products: categories[savedCategoryIndex].products || []
			};
			category.products.forEach(function (product) {
				product.productCategory = category.categoryName;
			});
			var itemToUpdate = "modules.store.products.category." + savedCategoryIndex;
			var newCategory = {};
			newCategory[itemToUpdate] = category;
			return db.pages.update({ 'facebook_id': pageID }, {
				$set: newCategory
			}).then(function (response) {
				return response;
			}).catch(function (response) {
				return response;
			});
		} else {
			category.categoryID = generateID('C');
			return db.pages.update({ 'facebook_id': pageID }, {
				$addToSet: {
					"modules.store.products.category": category
				}
			}).then(function (response) {
				return response;
			});
		}
	});
}

module.exports.saveStoreCategory = saveStoreCategory;

function deleteStoreCategory(pageID, category) {
	return getPageById(pageID).then(function (page) {
		var objectToPull= {};
		objectToPull['modules.store.products.category'] = {categoryID: category.categoryID};
		return  db.pages.update({ 'facebook_id': pageID }, { $pull: objectToPull});
	});
}

module.exports.deleteStoreCategory = deleteStoreCategory;

function getStoreOrders(pageID) {
	return getPageById(pageID).then(function (page) {
		return _.get(page, 'modules.store.orders');
	});
}

module.exports.getStoreOrders = getStoreOrders;

function saveStoreOrder(pageID, order) {
	return getPageById(pageID).then(function (page) {
		var orders = _.get(page, 'modules.store.orders') || [],
			oldOrderIndex = _.findIndex(orders, { orderID: order.orderID}),
			updatePath,
			updateObj = {};

			if(oldOrderIndex != -1) {
				updatePath = 'modules.store.orders.'+ oldOrderIndex;
				updateObj = {};
				updateObj[updatePath] = order;

				return db.pages.update({
					facebook_id: pageID
				}, {
					$set: updateObj
				});
			} else {
				return null;
			}
	});
}

module.exports.saveStoreOrder = saveStoreOrder;

function getStoreOrderByID(pageID, orderID) {
	return getPageById(pageID).then(function (page) {
		var orders = _.get(page, 'modules.store.orders');
		return _.findWhere(orders, {orderID: orderID});
	});
}

module.exports.getStoreOrderByID = getStoreOrderByID;

function saveNewOrder(id, order) {
	//need refactor ASAP
	var orders;
	order.date = new Date();
	order.orderID = generateID("O");
	// could be new, canceled, pending, paid, shipped
	order.status = order.status || "pending";
	return getPageById(id).then(function (page) {
		orders = _.get(page, "modules.store.orders");
		if(orders) {
			return db.pages.update({
					facebook_id: id
				}, {
					$push: {
						"modules.store.orders": order
					}
				}).then(function (){
					order.items.forEach(function (item) {
						page.modules.store.products.category.forEach(function (category) {
							if(Array.isArray(category.products)) {
								category.products.forEach(function (product) {
									if(item.productID == product.productID) {
										product.quantity = product.quantity - item.quantity;
										if(product.quantity < 0) {
											product.quantity = 0;
										}
									}
								});
							}
						});
					});
					console.log(page.modules.store.products.category);
					db.pages.update({
						facebook_id: id
					}, {
						$set: {
							"modules.store.products.category": page.modules.store.products.category
						}
					});
				});
		} else {
			orders = [order];
			return db.pages.update({
					facebook_id: id
				}, {
					$set: {
						"modules.store.orders": orders
					}
				}).then(function (){
					order.items.forEach(function (item) {
						page.modules.store.products.category.forEach(function (category) {
							if(Array.isArray(category.products)) {
								category.products.forEach(function (product) {
									if(item.productID == product.productID) {
										product.quantity = product.quantity - item.quantity;
										if(product.quantity < 0) {
											product.quantity = 0;
										}
									}
								});
							}
						});
					});
					db.pages.update({
						facebook_id: id
					}, {
						$set: {
							"modules.store.products.category": page.modules.store.products.category
						}
					});
				});
		}
	});

}

module.exports.saveNewOrder = saveNewOrder;

function getStoreConfiguration(pageID) {
	return getPageById(pageID).then(function (page) {
		return _.get(page, 'modules.store.configuration', {});
	});
}

module.exports.getStoreConfiguration = getStoreConfiguration;

function saveStoreConfiguration(pageID, configuration) {
	return db.pages.update({"facebook_id": pageID},
			{ $set: {'modules.store.configuration': configuration} })
		.then(function (response) {
			return response;
		}).catch(function (response) {
			return response;
		});
}

module.exports.saveStoreConfiguration = saveStoreConfiguration;

function generateID(prefix) {
	var ts = (+new Date()).toString();
	var parts = ts.split( "" ).reverse();
	var id = "";

	for( var i = 0; i < 8; ++i ) {
		var index = Math.floor(Math.random() * parts.length);
		id += parts[index];
	}
	prefix = prefix || "P";
	return id;
}

module.exports.activateSitemapsForId = activateSitemapsForId;

module.exports.activateSitemapsForPaidPages = activateSitemapsForPaidPages;

module.exports.unbindPageFromTophostDomain  = unbindPageFromTophostDomain;

module.exports._raw = db;
