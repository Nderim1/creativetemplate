var config = require('../config');
var db = require('promised-mongo')(config.mongo_connection, ['users', 'albums','pages','orders','sales','coupons','posts','events']);

module.exports = db;
