var db = require('../db'),
 	_ = require('lodash'),
 	R = require('ramda'),
	logger = require('../utils/logger'),
	defaultPageSlugs = ['news', 'events', 'albums', 'contacts'],
	postsMap = { news: 'posts', events: 'events', albums: 'albums'},
	fs = require('fs'),
	moment = require('moment'),

	captchaService = require('./captcha'),
	config = require('../config'),
	url = require('url'),
	piwik = require('../utils/analytics/piwik'),
  	Site = require('./models/site'),
  	Q = require('q');

/**
 * Analyze incoming url to add some proprieties for the next routers/controllers
 * Adds:
 * 		is_embedded
 * 		is_subdomain
 * @param {req} Request Object.
 * @param {res} Response Object
 * @param {next} Next middleware
 */

var analyzeURL = function (req, res, next) {
	//Assign the correct Lookup function
	req.site = req.site || {};
	req.site.is_subdomain = checkIsSubdomain(req.hostname) ? true : false;
	req.site.currentUrl = req.protocol + "://" + req.hostname + req.originalUrl;
	req.site.renderType = /.+\.embedded\..+/.test(req.hostname) ? "preview" : "active";
	req.site.url2png = req.query.url2png === "true" || false;
	req.site.isEditor = (req.site.renderType === 'preview') && !req.site.url2png;
	logger.debug("pageUtils.analyzeURL: "+ req.path);
	logger.debug('url2png',req.site.url2png);
	if(req.params){
		logger.debug('params',req.params);
	}
	if(req.query){
		logger.debug('query',req.query);
	}

	if(!req.headers.referer && req.site.renderType === "preview" && !req.query.url2png && req.originalUrl != '/css/dynamic.css') {
		return res.render(403);
	}
	next();
};

var templateExists = function templateExists(template) {
	try {
		return require('../templates/' + template + '/default-configuration');
	} catch (err) {
		return false;
	}
};

var pageAvailable = function(slug, site) {
	var menuItem = _.find(site.getConfiguration().settings.menu, {"slug": slug}) || _.find(site.getConfiguration().settings.menu, {"id": slug});
	var isStore = (_.get(_.first(_.filter(site.getConfiguration().contents.extra_pages, function(e){return e.id == menuItem.id;})), "type")) == "store";
	var renderExtraPage = site.get("paid_on") || (_.includes(defaultPageSlugs, menuItem.id)) || (site.renderType === "preview");
	var renderItem = menuItem.active && renderExtraPage;
	if(isStore && !site.get("isPro") && (site.renderType != "preview")) {
		renderItem = false;
	}
	if(!(menuItem && renderItem)) {
		return false;
	}


	switch(menuItem.id) {
		case 'news':
			return (site.posts.length > 0) ? site.posts : true;
		case 'events':
			return (site.events.length > 0) ? site.events : true;
		case 'albums':
			return (site.albums.length > 0) ? site.albums : true;
		case 'contacts':
			return site.getConfiguration().settings.mail || (site.get("pageData.location") && site.get("pageData.location.latitude") && site.get("pageData.location.longitude"));
	}
	return true;
};

var checkPageExists = function(req, res, next) {
	var pageData = req.pageData,
		requestedPageSlug = req.params.page,
		template = _.get(pageData, "configuration.graphics.template", "hellobrand"),
		defaultGraphics = require('../templates/' + template + '/default-configuration').graphics;

	if(pageAvailable(requestedPageSlug, pageData)) {
		next();
	} else {
		res.render('../templates/' + _.get(pageData, "configuration.graphics.template", defaultGraphics.template) + '/pages/404', {}, function(err, html) {
			if(err) {
				logger.error('Trying to get 404 page in template', {
					error: err,
					filename: __filename,
					meta: {
						defaultGraphics_template: defaultGraphics.template
					},
					stack: err.stack
				});
				res.redirect('/');
			} else {
				res.end(html);
			}
		});
	}
};

var slicePosts = function(postsArray, configColumns) {
	return postsArray.slice(0, calculateColumnNumber(postsArray.length, configColumns));
};

var calculateColumnNumber = function(postsCount, configColumns) {
	var num = configColumns ? Math.min(postsCount, configColumns) : postsCount;
	var options = [0,1,2,3,4].filter(function(opt) {
		return opt <= num;
	});
	return _.last(options);
};

var calculateColumnNumbersForPage = function (pageContext, site) {
	var template,
		defaultGraphics,
		columns = {};

	template = _.get(site, "configuration.graphics.template", "hellobrand");
	defaultGraphics = require('../templates/' + template + '/default-configuration').graphics;
	columns.news = Math.min(4, _.get(site, "configuration.graphics.columns.news", defaultGraphics.columns.news));
	columns.events = Math.min(4, _.get(site, "configuration.graphics.columns.events", defaultGraphics.columns.events));
	columns.albums = Math.min(4, _.get(site, "configuration.graphics.columns.albums", defaultGraphics.columns.albums));

	if(site.posts) pageContext.news_columns = calculateColumnNumber(site.posts.length, columns.news);
	if(site.events) pageContext.events_columns = calculateColumnNumber(site.events.length, columns.events);
	if(site.albums) pageContext.albums_columns = calculateColumnNumber(site.albums.length, columns.albums);
};

var numberElementsByCol = function(nr){
	switch(nr){
		case 2: return 10; break;
		case 3: return 12; break;
		case 4: return 12; break;
		default: return 6;
	}
}

var calculateItemsToDisplay = function (pageContext, pageData) {

	if (pageData.posts) {
		pageContext.posts_number = numberElementsByCol(pageContext.news_columns);
	}
	if (pageData.events) {
		pageContext.events_number = numberElementsByCol(pageContext.events_columns);
	}
	if (pageData.albums) {
		pageContext.albums_number = pageContext.albums_columns * 3;
	}
};

var calculatePagination = function(req, res, pageContext, pageData, pageItemId) {
	var show_page = pageContext.show_page ? pageContext.show_page : 0,
		elements_number,
		skip,
		elements_sliced,
		current_url,
		elements_number_skiped,
		numberOfPages;
	if (pageItemId === "news") {
		elements_number = parseInt(pageContext.posts_number);
		skip = show_page > 0 ? ((pageContext.show_page - 1) * elements_number) : 0;
		elements_sliced = pageContext.page.posts.slice(skip, elements_number + skip);
		current_url = (url.parse(req.url).pathname);
		numberOfPages = Math.ceil(pageContext.page.posts.length / elements_number);
		elements_number_skiped = pageContext.page.posts.slice(skip).length;
	} else if (pageItemId === "albums") {
		elements_number = parseInt(pageContext.albums_number);
		skip = show_page > 0 ? ((pageContext.show_page - 1) * elements_number) : 0;
		elements_sliced = pageContext.page.albums.slice(skip, elements_number + skip);
		current_url = (url.parse(req.url).pathname);
		numberOfPages = Math.ceil(pageContext.page.albums.length / elements_number);
		elements_number_skiped = pageContext.page.albums.slice(skip).length;
	} else if (pageItemId === "events"){
		elements_number = parseInt(pageContext.events_number);
		numberOfPages = Math.ceil(pageContext.page.posts.length / elements_number);
		skip = show_page > 0 ? ((pageContext.show_page - 1) * elements_number) : 0;
		elements_sliced = pageContext.page.events.slice(skip, elements_number + skip);
		current_url = (url.parse(req.url).pathname);
		numberOfPages = Math.ceil(pageContext.page.events.length / elements_number);
		elements_number_skiped = pageContext.page.events.slice(skip).length;
	}

	if(elements_sliced) {
		if (show_page > numberOfPages) {
			res.redirect(current_url);
		}
		pageContext.prev_page = show_page - 1 > 0 ? current_url + '?show_page=' + (show_page - 1) : undefined;

		if (elements_number_skiped > elements_sliced.length) {
			if (show_page === 0) {
				show_page = 1;
			}
			pageContext.next_page = current_url + '?show_page=' + (show_page + 1);
		} else {
			pageContext.next_page = undefined;
		}

		pageContext.pages = [];
		for (var i = 0; i < numberOfPages; i++) {
			pageContext.pages.push(current_url + '?show_page=' + (i + 1));
		}

		pageContext.active = show_page;
		if(pageItemId === "news") {
			pageContext.page.posts = elements_sliced;
		} else if (pageItemId === "albums") {
			pageContext.page.albums = elements_sliced;
		} else {
			pageContext.page.events = elements_sliced;
		}
	}
};

function getTrimmedEvents(site){
	var includePastEvents = _.get(site.configuration, "contents.homepage.showPastEvents",true);
	var sorted_raw_events = _.sortBy(site.events,function(e){
		return moment(e.start_time).unix();
	});
	var past_events = _.remove(sorted_raw_events, function(e){
		return moment(e.start_time).isBefore(moment(), "day");
	});
	var future_events = sorted_raw_events;

	if(includePastEvents){
		// getting selected option: events columns
		template = _.get(site, "configuration.graphics.template", "hellobrand");
		defaultGraphics = require('../templates/' + template + '/default-configuration').graphics;
		var number_events = _.get(site, "configuration.graphics.columns.events", defaultGraphics.columns.events);
		if(future_events.length>=number_events){
			return future_events;
		}else{
			var limited_past_events = _.takeRight(past_events, (number_events - future_events.length));
			return _.flatten([limited_past_events, future_events]).reverse();
			//return _.flatten([future_events, limited_past_events.reverse()]);
		}
	} else {
		return future_events;
	}
}

function assignConfiguration(site) {
	if(site.renderType == "active") {
		// website.majeeko.com
		site.configuration = _.omit(site.get("configuration"), ["_css", "__css"]);
	} else if(site.renderType == "preview") {
		// embedded.majeeko.com
		site.configuration = _.omit(site.get("configuration.editor.preview", site.get("configuration")), ["_css", "__css"]);
	} else {
		site.configuration = _.omit(site.get("configuration.editor.default", site.get("configuration")), ["_css", "__css"]);
	}
	_.set(site.configuration, "contents.homepage.showNews", _.get(site.configuration, "contents.homepage.showNews",true));
	_.set(site.configuration, "contents.homepage.showAlbums", _.get(site.configuration, "contents.homepage.showAlbums",true));
	_.set(site.configuration, "contents.homepage.showEvents", _.get(site.configuration, "contents.homepage.showEvents",true));
	_.set(site.configuration, "contents.homepage.showPastEvents", _.get(site.configuration, "contents.homepage.showPastEvents",true));
	_.set(site.configuration, "contents.homepage.showContacts", _.get(site.configuration, "contents.homepage.showContacts",true));
	_.get(site.configuration, "contents.extra_pages").forEach(function(extra_page){
		if(_.has(extra_page, "type")) {
			return;
		}
		if(_.isUndefined(extra_page.album)){
			_.set(extra_page,"type", "text");
		}else{
			_.set(extra_page,"type", "album");
		}
	});
	if(!_.has(site.configuration, "graphics.fonts")){
		_.set(site.configuration, "graphics.fonts", _.get(site.configuration, "graphics.template", "default"));
	}
	site.configuration.settings.phone = _.get(site,"configuration.settings.phone", _.get(site,"data.pageData.phone"));

	return site;
}

var filterPageMenu = function(site) {
	var template = site.get("configuration.graphics.template", "innovation"),
		defaultMenu = require('../templates/' + template + '/default-configuration').settings.menu;

	// need to copy all the menu items to have correct titles to use in homepage's sections
	site.configuration.settings.menu_all = site.configuration.settings.menu;
	site.configuration.settings.menu = site.configuration.settings.menu_all.filter(function (page) {
		var slug = page.slug || page.id;
		return pageAvailable(slug, site);
	});
	return site;
};

// returns db page (or null) based on subdomain
var getPageDataForSubdomain = function (subdomain) {
	return db._raw.pages.findOne({
		$or: [{
			'username': { $regex : new RegExp('^' + subdomain + '$', "i") }
		}, {
			'facebook_id': subdomain
		}]
	});
};


var getPageDataForDomain = function (domain) {
	//duplicato di getPageByDomain in db.js
	return db._raw.pages.findOne({
		'domains': { $in: [ domain ] }
	});
};

function checkIsSubdomain(hostname) {
	return (new RegExp(process.env.HOST)).test(hostname);
}

var getPageNews = function (site) {
	if(site.get("pageData.posts")) {
		var items = site.get("pageData.posts.data")
			.filter(function(post) {


				if(_.contains(['event'], post.type)){
					return false;
				}else if ((post.attachments && post.attachments.data[0].type === 'cover_photo') || ( post.hasOwnProperty('story') && /profile picture/gi.test(post.story))){
					//return false to filter cover photos and prevent them to be duplicated
					return false;
				} else if(_.get(post, "majeeko.hidden") === true) {
					return false;
				}
				return true;
					// return post.attachments
					// 	&& post.attachments.data.length
					// && post.type !== 'event'
					// && post.attachments.data[0].type !== 'event'
					// && post.attachments.data[0].type !== 'photo'
					// && post.attachments.data[0].type !== 'cover_photo'
			});
		return items.length ? items : null;
	}
	return null;
};

var getPageEvents = function (site) {
	if(site.get("pageData.events")) {
		var items = site.get("pageData.events.data");
		if(items.length) {
			items = items.filter(function(event) {
				if(_.get(event, "majeeko.hidden") === true) {
					return false;
				}
				return true;
			});
		}
		return items.length ? items : null;
	}
	return null;
};

var getPageAlbums = function (site) {
	if(site.get("pageData.albums")) {
		var items = site.get("pageData.albums.data")
			.filter(function(album) {
				return album.type == 'normal' && album.photos && (!_.get(album, "majeeko.hidden"));
			})
			.map(function(album) {
				return _.extend({}, album, {});
			});
		return items.length ? items : null;
	}
	return null;
};

var prepareSite = function(req, res) {
	var pageLookupFn,
		pageLookupCriteria;

	if(req.session.tophost) {
		if(req.session.tophost.currentPage) {
			pageLookupFn = Site.findById;
			pageLookupCriteria = req.session.tophost.currentPage.facebook_id;
		} else {
			logger.error('Tophost session active but no page available')
			return res.sendStatus(500)
		}
	} else if(req.site.is_subdomain && !req.site.editor) {
		pageLookupFn = Site.findBySubdomain;
		pageLookupCriteria = req.subdomains.slice(1).join('.');
	} else if(req.site.is_subdomain && req.site.editor){
		pageLookupFn = Site.findBySubdomain;
		pageLookupCriteria = req.path.split("/")[2];
	} else {
		pageLookupFn = Site.findByDomain;
		pageLookupCriteria = req.hostname;
	}

	return pageLookupFn(pageLookupCriteria)
		.then(function(site) {
			if (site) {
				site.set("configuration.settings.social", _.pick(site.get("configuration.settings.social"), R.compose(R.not, R.isNil)));

				// site.posts = getPageNews(site);
				// site.events = getPageEvents(site);
				// site.albums = getPageAlbums(site);
				req.site = _.extend(site, req.site);
				req.site = assignConfiguration(req.site);
				delete req.site.data.configuration;

				if(req.site.is_subdomain) {
					req.site.data.siteMap = null;
				} else {
					try {
						var url = req.site.currentUrl;
						url = url.split('/')[2];
						var first = '';
						var second = '';
						if(/www/gi.test(url)){
							second = url.split('.')[1];
							first = url.split('.')[2];
						} else {
							second = url.split('.')[0];
							first = url.split('.')[1];
						}
						req.site.data.siteMap = req.site.data.siteMap[second][first];
					} catch(e) {
						logger.error("Error assigning site map to " + url);
					}
				}

				var piwikSiteId = req.site.get('analytics.piwik.idSite');
				if(piwikSiteId) {
					try {
						var originalIp = (req.headers['x-forwarded-for']) ? req.headers['x-forwarded-for'].split(', ')[0] : req.ip;
						piwik.trackAction(piwikSiteId, req.hostname, req.originalUrl, originalIp);
					} catch(err) {
						logger.error('Error communicating action to Piwik', {
							error: err,
							filename: __filename,
							meta: {
								piwikSiteId: piwikSiteId,
								req_hostname: req.hostname,
								req_originalUrl:req.originalUrl,
								originalIp:originalIp
							},
							stack: err.stack
						});
					}
				} else if(req.site.get('paid_on')) {
					piwik.registerSiteToPiwik(site.data,site.get('domains'))
					.then(function(){
						// track action with piwik
						// piwik.trackAction(piwikSiteId, req.hostname, req.originalUrl, originalIp);
					});
					// logger.error({
					// 	message: 'Error tracking action with piwik: no Piwik siteId for current site',
					// 	info: {
					// 		piwikSiteId: piwikSiteId,
					// 		req_hostname: req.hostname,
					// 		req_originalUrl:req.originalUrl,
					// 		originalIp:req.ip
					// 	}
					// });
				}

			} else {
				throw new Error('Page not found!');
			}
		})
		.then(function(){
			return req.site;
		})
		.catch(function(err) {
			logger.debug(err);
			res.render('404');
		});
};

module.exports = {
	checkPageExists: checkPageExists,
	templateExists: templateExists,
	prepareSite: prepareSite,
	slicePosts: slicePosts,
	calculateColumnNumber: calculateColumnNumber,
	calculateColumnNumbersForPage: calculateColumnNumbersForPage,
	calculateItemsToDisplay: calculateItemsToDisplay,
	filterPageMenu: filterPageMenu,
	calculatePagination: calculatePagination,
	analyzeURL: analyzeURL,
	assignConfiguration: assignConfiguration,
	getTrimmedEvents: getTrimmedEvents,
};
