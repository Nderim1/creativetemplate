var _ = require('lodash'),
	Q = require('q'),
	captchapng = require('captchapng'),
	converter = require('hex-rgb-converter'),
	Site = require('./models/site');

exports.generateCaptcha = function (req, res) {
	var captchaNumber = parseInt(Math.random()*9000+1000);
	if (req.session) {
		req.session.captcha = captchaNumber;
	}
	var p = new captchapng(70,20, captchaNumber);
	if(req.site.is_subdomain) {
		getConfiguration = Site.getConfigurationBySubdomain;
		query = req.subdomains.slice(1).join('.');
	} else {
		getConfiguration = Site.getConfigurationByDomain;
		query = req.hostname;
	}
	return getConfiguration(query, req.site.renderType)
		.then(function (configuration) {
				var background = _.get(configuration, "graphics.colors.inner_background", "#ffffff"),
					text = _.get(configuration, "graphics.colors.texts", "#666666");
				background = converter.toRGB(background.slice(1).length == 3 ? background.slice(1) + background.slice(1) : background.slice(1));
				text = converter.toRGB(text.slice(1).length == 3 ? text.slice(1) + text.slice(1) : text.slice(1));
				p.color(background[0], background[1], background[2], 255);  // First color: background (red, green, blue, alpha)
				p.color(text[0], text[1], text[2], 255); // Second color: paint (red, green, blue, alpha)
				var img = p.getBase64();
				var imgbase64 = new Buffer(img,'base64');
				return imgbase64;
			})
		.catch (function (err) {
				logger.error("Can't create captcha code.", {
							error:err,
							stack:err.stack,
							filename: __filename
						});
				return;
			});
};

exports.checkCaptcha = function (submittedCaptcha, targetCaptcha) {
	return Q.Promise(function(resolve, reject) {
		if( submittedCaptcha == targetCaptcha) {
			resolve(true);
		} else {
			resolve(false);
		}
	});
};