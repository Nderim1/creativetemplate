var _ = require('lodash'),
	fs = require('fs'),
	db = require('../db'),
	R = require('ramda'),
	qs = require('qs'),
	logger = require('../utils/logger'),
	pageUtils = require('./page-utils'),
	templateUtils = require('../utils/templates'),
	Site = require('./models/site'),
	Sitemap = require('../utils/google-sitemap/api'),
	fonts = require('../utils/fonts'),
	config = require('../config');

function removeHiddenElements(site) {
	if(site.posts) {
		site.posts = site.posts.filter(function (post) {
			return !_.get(post, "majeeko.hidden");
		});
	}
	if(site.albums) {
		site.albums = site.albums.filter(function (album) {
			return !_.get(album, "majeeko.hidden");
		});
	}
	if(site.events) {
		site.events = site.events.filter(function (event) {
			return !_.get(event, "majeeko.hidden");
		});
	}
	return site;
}

function _renderTemplatePage (req, res, template, page, context) {
	context = _.assign({}, context, {configuration: req.site.getConfiguration()});

	if(process.env.NODE_ENV == 'development') {
		return res.render('../templates/' + template + '/pages/' + page, context);
	} else {
		return res.render('../templates/' + template + '/pages/' + page, context, function(err,html) {
			if(err) {
				logger.error('Trying to get page ' + page + ' in template ' + template, err);
				context.fallbackTemplate = true;
				res.render('../templates/hellobrand/pages/' + page, context, function (err,html) {
					if(err) {
						logger.error('Trying to get page ' + page + ' in template ' + template, err);
						res.redirect('/');
					} else {
						return res.end(html);
					}
				});
			} else {
				return res.end(html);
			}
		});
	}
}

function renderHomepage (req,res,next) {
	return pageUtils.prepareSite(req, res)
		.then(function () {
			if(!pageUtils.templateExists(req.site.configuration.graphics.template)) {
				req.site.configuration.graphics.template = "innovation";
				req.site.data.configuration.graphics.template = "innovation";
			}
			req.site = pageUtils.filterPageMenu(req.site);
			req.site =removeHiddenElements(req.site);
			var pageContext = {
					page: req.site,
					isHome: true,
					renderType: req.site.renderType,
					isEditor: req.site.isEditor,
					url2png: req.site.url2png
				},
			   	defaultGraphics = require('../templates/' + req.site.configuration.graphics.template + '/default-configuration').graphics; // !!!!
			if(req.site.getConfiguration().graphics.fonts){
				pageContext.fonts = fonts.getFontWithoutFallback(req.site.getConfiguration().graphics.fonts);
			}

			req.site.events = pageUtils.getTrimmedEvents(req.site);
			pageUtils.calculateColumnNumbersForPage(pageContext, req.site);
			if(req.site.posts) req.site.posts = pageUtils.slicePosts(req.site.posts, _.get(pageContext, "news_columns", defaultGraphics.columns.news));
			if(req.site.events) req.site.events = pageUtils.slicePosts(req.site.events, _.get(pageContext, "events_columns", defaultGraphics.columns.events));
			if(req.site.albums) req.site.albums = pageUtils.slicePosts(req.site.albums, _.get(pageContext, "albums_columns", defaultGraphics.columns.albums));

			return _renderTemplatePage(req, res, req.site.configuration.graphics.template, 'index', pageContext);
		}).catch(function (err) {
			logger.debug(err);
			res.status(404).render('404');
		});
}

function renderPage (req,res,next) {
	return pageUtils.prepareSite(req,res)
		.then(function (){
			if(!pageUtils.templateExists(req.site.configuration.graphics.template)) {
				req.site.configuration.graphics.template = "innovation";
				req.site.data.configuration.graphics.template = "innovation";
			}
			req.site = pageUtils.filterPageMenu(req.site);
			req.site =removeHiddenElements(req.site);
			var pageTemplate,
				// Need to check also ids for retrocompatibiliy
				pageItem = _.find(
						req.site.getConfiguration().settings.menu, { "slug": req.params.page }) ||
						_.find(req.site.getConfiguration().settings.menu, { "id": req.params.page }
					);
			 	if(!pageItem) {
			 		throw new Error("404 page not found");
			 	}
			 	pageContext = {
				  	page: req.site,
					title: _.find(req.site.getConfiguration().settings.menu, R.compose(R.equals(pageItem.id), R.prop('id'))).label,
					show_page: parseInt(req.query.show_page),
					renderType: req.site.renderType,
					isEditor: req.site.isEditor,
					url2png: req.site.url2png,
					pageItem: pageItem,
					slug: pageItem.slug
				};

				if(req.site.getConfiguration().graphics.fonts){
					pageContext.fonts = fonts.getFontWithoutFallback(req.site.getConfiguration().graphics.fonts);
				}

			if(_.contains(['news', 'events', 'albums', 'contacts'], pageItem.id)) {
				pageUtils.calculateColumnNumbersForPage(pageContext, req.site);
				pageUtils.calculateItemsToDisplay(pageContext, req.site);
				pageTemplate = pageItem.id;
			} else {
				pageTemplate = 'extra-page';
				var extraPageConfiguration = _.find(req.site.configuration.contents.extra_pages,{id: pageItem.id});
				_.extend(pageContext, {
					// title: _.find(pageData.configuration.settings.menu, R.compose(R.equals(req.params.page), R.prop('id'))).label,
					text: extraPageConfiguration.text || ""
				});
				if(extraPageConfiguration.type === "store") {
					pageContext.columns = req.site.configuration.graphics.columns.store || 2;
					pageContext.isStore = true;
					pageContext.storeData = _.pick(_.get(req.site, "data.modules.store"), ['configuration', 'products']);
					pageContext.storeID = _.get(req.site, "data.facebook_id");
					pageContext.notify_url = "http://" + req.site.data.username + ".website." + config.primary_domain + "/store/ipn";
					//pageContext.notify_url = "http://ipn.majeeko-test.com";
	          		pageContext.return_url = "http://" + req.hostname + "/store/thanks";
					return _renderTemplatePage(req, res, req.site.configuration.graphics.template, pageTemplate, pageContext);
				} else if(extraPageConfiguration.album) {
					var album = _.findWhere(req.site.albums, { id:extraPageConfiguration.album });
					if(album) {
						try {
							pageContext.album_id = album.id;
							pageContext.photos = album.photos;
						} catch(err) {
							logger.error('Album not found: ', extraPageConfiguration.album);
							logger.error(err);
							pageContext.photos = [];
						}
					} else {
						pageContext.photos = [];
					}
					pageContext.columns = pageUtils.calculateColumnNumber(pageContext.photos.length, req.site.configuration.graphics.columns.albums);
				}
			}
			pageUtils.calculatePagination(req, res, pageContext, req.site, pageItem.id);
			return _renderTemplatePage(req, res, req.site.configuration.graphics.template, pageTemplate, pageContext);
		})
		.catch(function (err) {
			logger.debug(err);
			res.redirect(302, "http://" + req.hostname);
		});
}

function renderPostDetailPage (req, res, template) {
	return pageUtils.prepareSite(req, res)
		.then(function (){
			if(!pageUtils.templateExists(req.site.configuration.graphics.template)) {
				req.site.configuration.graphics.template = "innovation";
				req.site.data.configuration.graphics.template = "innovation";
			}
			req.site = pageUtils.filterPageMenu(req.site);
			req.site =removeHiddenElements(req.site);
			var slug = req.path.split('/')[1];
			var real_slug = slug;

			if (slug === "news") slug = "posts";

			var	post = _.findWhere(req.site[slug], { id: req.params.id }),
				isModal = qs.parse(req.query).modal,
				customIndex = qs.parse(req.query).customIndex,
				configuration_template = isModal ? '__default': req.site.getConfiguration().graphics.template, // Modals content is the same for every template
				pageContext = {
					page: req.site,
				  	postData: post,
					renderType: req.site.renderType,
					isEditor: req.site.isEditor,
					url2png: req.site.url2png,
					customIndex: customIndex,
					slug: real_slug
				};

			if(req.site.getConfiguration().graphics.fonts){
				pageContext['fonts'] = fonts.getFontWithoutFallback(req.site.getConfiguration().graphics.fonts)
			}
			return _renderTemplatePage(req, res, configuration_template, template + (isModal ? '-content' : ''), pageContext);

		}).catch(function (err) {
			logger.debug(err);
			res.status(404).render('404');
		});
}

function renderDefaultCss (req,res, template) {
	var getConfiguration,
		query;
	if(req.site.is_subdomain) {
		getConfiguration = Site.getConfigurationBySubdomain;
		query = req.subdomains.slice(1).join('.');
	} else {
		getConfiguration = Site.getConfigurationByDomain;
		query = req.hostname;
	}

	return getConfiguration(query, req.site.renderType).then(function (configuration) {
			template = template || configuration.graphics.template;
			fs.readFile(__dirname + '/../../public/css/templates/' + template + '.css', 'utf8', function (err, content) {
				if(err) {
					logger.error(err);
					res.status(404).send();
				} else {
					logger.info(content);
					res.send(content);
				}
			});
	});
}

function renderDynamicCss (req,res) {
	res.header("Content-type", "text/css");
	if(req.site.is_subdomain) {
		return Site.getDynamicCssByUsername(req.subdomains.slice(1).join('.'), req.site.renderType).then(function(data) {
			if(!pageUtils.templateExists(data.template)) {
				return renderDefaultCss(req, res, "innovation");
			}
			res.send(data.css);
		})
		.catch(function (err) {
			logger.error(err);
			return renderDefaultCss(req, res);
		});
	} else {
		return Site.getDynamicCssByDomain(req.hostname, req.site.renderType).then(function(data) {
			if(!pageUtils.templateExists(data.template)) {
				return renderDefaultCss(req, res, "innovation");
			}
			res.send(data.css);
		})
		.catch(function (err) {
			logger.error(err);
			return renderDefaultCss(req, res);
		});
	}
}

function renderSitemap(req, res) {
	res.header("Content-type", "text/xml");
	return pageUtils.prepareSite(req, res)
		.then(function() {
			var urls = [];
			// urls[0] = 'http://'+req.site.data.username+'.website.'+process.env.HOST+'/';
			urls[0] = req.site.currentUrl.split('sitemap')[0];
			var menu = req.site.configuration.settings.menu.filter(function(elem) {
				return typeof elem === 'object';
			}).filter(function(elem) {
				return elem.active === true;
			}).map(function(elem) {
				if (elem.slug) return urls[0] + elem.slug;
				else return urls[0] + elem.id;
			}).forEach(function(elem) {
				urls.push(elem);
			});
			var sitemap = Sitemap.generateSiteMap(urls);
			res.send(sitemap);
		})
		.catch(function(err) {
			logger.error(err);
			res.send(500);
		});
}

module.exports = {
	renderHomepage: renderHomepage,
	renderPage: renderPage,
	renderPostDetailPage: renderPostDetailPage,
	renderDefaultCss: renderDefaultCss,
	renderDynamicCss: renderDynamicCss,
	renderSitemap:renderSitemap
};
