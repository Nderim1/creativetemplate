var db = require('../../db'),
	_ = require('lodash'),
	Q = require('q'),
	logger = require('../../utils/logger'),
	templateUtils = require('../../utils/templates'),
	postFactory = require('./post'),
	albumFactory = require('./album'),
	eventFactory = require('./event');

var sitePrototype = {
	updateConfiguration: function updateConfiguration(action) {
		return db.updatePageConfiguration(this.username).then();
	},
	get: function get(prop, default_val) {
		return _.get(this, "data." + prop, default_val);
	},
	getConfiguration: function getConfiguration() {
		return _.get(this, "configuration");
	},
	set: function set(prop, value) {
		_.set(this, "data." + prop, value);
	},
	has: function has(prop) {
		return _.has(this, prop);
	}
};

var siteFactory = function siteFactory(data) {
	logger.debug('siteFactory: start building page')
	var site,
		postsPromises = [],
		eventsPromises = [],
		profilePictureAlbum,
		tempAlbums,
		coverImageAlbum,
		wallImageAlbum,
		i,
		albumsPromise = Q([]),
		deferred = Q.defer();

	if (_.has(data, "pageData.posts.data")) {
		for (i = 0 ; i < data.pageData.posts.data.length;  i++) {
			var currentPost = postFactory(data.pageData.posts.data[i]);
			if (currentPost) {
				postsPromises.push(currentPost);
			}
		}
	}

	if (_.has(data, "pageData.albums.data")) {
		albumsPromise = albumFactory.createMultiple(data.pageData.albums.data);
		// for (i = 0 ; i < data.pageData.albums.data.length;  i++) {
		// 	albumsPromise.push(albumFactory(data.pageData.albums.data[i]));
		// }
	}

	if (_.has(data, "pageData.events.data")) {
		for (i = 0 ; i < data.pageData.events.data.length;  i++) {
			var currentEvent = eventFactory(data.pageData.events.data[i]);
			if (currentEvent) {
				eventsPromises.push(currentEvent);
			}
		}
	}

	try {
		if(_.has(data, "pageData.hours")){
			var hours = _.get(data, "pageData.hours");
			if(!_.has(data,'pageData.hours.mon')){
				var daysObj = {mon:[],	tue:[],	wed:[],	thu:[],	fri:[],	sat:[],	sun:[]};
				var openDays = _.pairs(hours);
				openDays.forEach(function(elem){
					daysObj[elem[0].slice(0,3)].push(elem);
				});
				Object.keys(daysObj).forEach(function(elem){
					daysObj[elem].sort(function(a,b){
						return a - b ;
					});
				});
				hours = daysObj;
			}
		}
	} catch (e) {
		logger.error('Error during hours creation of the ' + site.data.facebook_id, {
			error: err,
			stack: err.stack,
			filename: __filename
		});
	}

	var pageDataOmitted = _.omit(data.pageData, ["posts", "albums", "events"]);
	data.pageData = pageDataOmitted;

	Q.all([
		Q.all(postsPromises).then(function(posts){
			logger.debug('siteFactory: posts fetched')
			return posts
		}),
		albumsPromise.then(function(albums){
			logger.debug('siteFactory: albums fetched', albums.length)
			return albums
		}),
		Q.all(eventsPromises).then(function(events){
			logger.debug('siteFactory: events fetched')
			return events
		})
	])
		.spread(function (postsArray, albumsArray, eventsArray) {
			tempAlbums = [];
			for (i = 0 ; i < albumsArray.length;  i++) {
				var currentAlbum = albumsArray[i];
				if (currentAlbum && currentAlbum.count && (currentAlbum.type == "normal" || currentAlbum.type == "mobile")) {
					tempAlbums.push(currentAlbum);
				} else if(currentAlbum && currentAlbum.type == "cover" ) {
					coverImageAlbum = _.extend({}, currentAlbum);
				} else if(currentAlbum && currentAlbum.type == "profile" ) {
					profilePictureAlbum = _.extend({}, currentAlbum);
				} else if(currentAlbum && currentAlbum.type == "wall" ) {
					wallImageAlbum = _.extend({}, currentAlbum);
				}
			}
			postsArray = postsArray.filter( Boolean );

			eventsArray = eventsArray.filter( Boolean );

			site = {
				data: data,
				posts: postsArray ? postsArray.slice() : [],
				albums: tempAlbums ? tempAlbums.slice() : [],
				events: eventsArray ? eventsArray.slice() : [],
				profilePictureAlbum: profilePictureAlbum,
				coverImageAlbum: coverImageAlbum,
				wallImageAlbum: wallImageAlbum,
				hours: hours ? hours : false
			};
			logger.debug('siteFactory: pre Object.create')
			var resultSite = _.extend(Object.create(sitePrototype), site)
			logger.debug('siteFactory: page building complete')
			deferred.resolve(resultSite);
		})
		.catch(function (err) {
			logger.error('Error during site creation', {
				error: err,
				stack: err.stack,
				filename: __filename
			});
			deferred.reject();
		});
	return deferred.promise;
};

// Class methods that hit db to get props of the social pages
siteFactory.findById = function (pageId) {
	return db.getPageById(pageId).then(function (page) {
		return siteFactory(page);
	});
};

siteFactory.findByUsername = function (username) {
	return db.getPageBySubdomain(username).then(function (page) {
		return siteFactory(page);
	});
};

siteFactory.findByDomain = function (domain) {
	return db.getPageByDomain(domain).then(function (page) {
		logger.debug('Page found')
		return siteFactory(page);
	});
};

siteFactory.findBySubdomain = function (subdomain) {
	return db.getPageBySubdomain(subdomain).then(function (page) {
		return siteFactory(page);
	});
};

siteFactory.checkUsernameFree = function (username) {
	return db.checkPageUsernameFree(username);
};

/**
 * Get configuration obejct for the passed subdommain, hitting the database.
 * @param {string} Domain domain of the page.
 * @param {string} type Type of the configuration to get, can be: active, preview, default.
 * @return {object} Returns configuration object.
 */

siteFactory.getConfigurationBySubdomain = function (subdomain, type) {
	return siteFactory.findBySubdomain(subdomain).then( function (site) {
		if(type === "active") {
			return _.omit(site.get("configuration"), 'editor');
		}
		if(type === "preview") {
			return _.get(site.get("configuration"), "editor.preview", _.omit(site.get("configuration"), 'editor'));
		}

		return _.get(site.get("configuration"), "editor.default", _.omit(site.get("configuration"), 'editor'));

	});
};

/**
 * Get configuration obejct for the passed dommain, hitting the database.
 * @param {string} Domain domain of the page.
 * @param {string} type Type of the configuration to get, can be: active, preview, default.
 * @return {object} Returns configuration object.
 *
 * TODO fix code duplication!!!
 */

siteFactory.getConfigurationByDomain = function (domain, type) {
	return siteFactory.findByDomain(domain).then( function (site) {
		if(type === "active") {
			return _.omit(site.get("configuration"), 'editor');
		}
		if(type === "preview") {
			return _.get(site.get("configuration"), "editor.preview", _.omit(site.get("configuration"), 'editor'));
		}

		return _.get(site.get("configuration"), "editor.default", _.omit(site.get("configuration"), 'editor'));

	});
};

/**
 * Get correct dynamic css for the passed dommain, hitting the database.
 * @param {string} Username of the page.
 * @param {string} type Type of the css to get, can be: active, preview, default.
 * @return {string} Dynamic CSS file.
 */

siteFactory.getDynamicCssByUsername = function (username, type) {
	return siteFactory.findByUsername(username).then(function (site) {
		return getDynamicCss(site, type);
	});
};

siteFactory.getDynamicCssByDomain = function (domain, type) {
	return siteFactory.findByDomain(domain).then(function (site) {
		return getDynamicCss(site, type);
	});
};

function getDynamicCss (site, type) {
	var has_css = site.has("data.configuration._css");
	if (!has_css) {
		return templateUtils.compileSiteDynamicCss(site.get("facebook_id"))
		.then(function (mongoResult) {
			return db._raw.pages.findOne({facebook_id:site.get("facebook_id")},{'configuration':1})
   				.then(function(page){
    				if(type === "active") {
    					return { css: _.get(page, 'configuration._css'), template: _.get(page, 'configuration.graphics.template') };
    				}

    				if(type === "preview") {
    					return { css: _.get(page, 'configuration.editor.preview._css', _.get(page, 'configuration._css')), template: _.get(page, 'configuration.editor.graphics.template', _.get(page, 'configuration.graphics.template')) };
    				}

    				return { css: _.get(page, "configuration.editor.default._css", _.get(page, 'configuration._css')), template: _.get(page, "configuration.editor.default.graphics.template", _.get(page, 'configuration.graphics.template')) };
    			});
		})
		.catch(function(err){
	   		logger.error(err);
			res.status(500).send();
	  	});
	}

	if(type === "active") {
		return { css: _.get(site, 'data.configuration._css'), template: _.get(site, 'data.configuration.graphics.template') };
	}

	if(type === "preview") {
		return { css: _.get(site, 'data.configuration.editor.preview._css', _.get(site, 'data.configuration._css')), template: _.get(site, 'data.configuration.editor.graphics.template', _.get(site, 'data.configuration.graphics.template')) };
	}

	return { css: _.get(site, 'data.configuration.editor.default._css', _.get(site, 'data.configuration._css')), template: _.get(site, 'data.configuration.editor.default.graphics.template', _.get(site, 'data.configuration.graphics.template')) };
}
module.exports =  siteFactory;
