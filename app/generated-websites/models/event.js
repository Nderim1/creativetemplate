var _ = require('lodash'),
	db = require('../../db'),
	Q = require('q');

var eventFactory = function eventFactory(eventData, options) {
	options = options || {};
	var deferred = Q.defer();
	if (Object.keys(eventData).length < 5) {
		return eventFactory.findById(eventData);
	} else {
		var event = {
			id: eventData.id,
			title: eventData.name,
			description: eventData.description,
			maybe_count: eventData.maybe_count,
			invited_count: eventData.invited_count,
			start_time: eventData.start_time,
			end_time: eventData.end_time,
			ticket_uri: eventData.ticket_uri,
			place: eventData.place,
			majeeko: eventData.majeeko || options.majeeko,
			cover_image: {
				source: _.get(eventData, "cover.source", "/images/event-standard.jpg"),
				id: _.get(eventData, "cover.cover_id"),
				is_fallback: ! _.has(eventData, "cover.source")
			}
		};
		deferred.resolve(event);
	}
	return deferred.promise;
};

eventFactory.findById = function findById(event) {
	return db.getEventById(event.id).then(function (eventData) {
		if(eventData) {
			return eventFactory(eventData, {majeeko: _.get(event, "majeeko")});
		} else {
			return undefined;
		}
	});
};

module.exports = eventFactory;
