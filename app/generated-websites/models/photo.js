var _ = require('lodash');

var photoFactory = function photoFactory(photoData, album) {
	var title = album.title;
	if(photoData.name){
		title = photoData.name + "\n\n" + album.title;
	}
	return {
		id: photoData.id,
		description: photoData.name,
		title: title,
		//source: "http://graph.facebook.com/"+ photoData.id +"/picture?type=normal",
		source: photoData.images ? _.get(photoData.images[0], "source"): undefined,
		//miniature: "http://graph.facebook.com/"+ photoData.id +"/picture?type=album",
		miniature: photoData.images ? _.get(photoData.images[0], "source"): undefined,
		image_array: photoData.images ? photoData.images.slice() : [],
		height: photoData.height,
		width: photoData.width,
		place: photoData.place
	};

};
module.exports = photoFactory;
