var _ = require('lodash'),
	db = require('../../db'),
	Q = require('q');

var postFactory = function postFactory(postData, options) {
	options = options || {};
	var deferred = Q.defer();
	if (Object.keys(postData).length < 5) {
		return postFactory.findById(postData);
	} else {
		var post,
			postPrototype = {
				id: postData.id,
				created_time: postData.created_time,
				description: postData.message,
				cover_image: _.get(postData, "attachments.data[0].media.image.src") ||
							((_.has(postData, "attachments.data[0].subattachments.data") &&
							postData.attachments.data[0].subattachments.data.length > 1) ? _.get(postData.attachments.data[0].subattachments.data[0], "media.image.src") : undefined),
				cover_image_is_fallback: false,
				cover_image_properties: _.get(postData,"attachments.data[0].media.image",false),
				title: "",
				abstract: "",
				caption:"",
				source: "",
				link: "",
				image_array: [],
				type: "",
				majeeko: postData.majeeko || options.majeeko
			};

		if (_.get(postData, "type") == "link") {
			post = _.assign(postPrototype, {
				title: postData.name,
				abstract: postData.description,
				caption: postData.caption,
				link: postData.link,
				type: "link"
			});
			if(!post.description) {
				post.description = postData.description;
			}
		} else if (_.get(postData, "type") == "status") {
			post = _.assign(postPrototype, {
				title: "Aggiornamento di stato",
				type: "status"
			});
		} else if (_.get(postData, "type") == "photo" && !(/cover photo/i.test(postData.name)) && !(/Profile Pictures/i.test(postData.name))) {
			var title;
			if(/Photos From/i.test(postData.name)) {
				title = "Nuove foto aggiunte";
			} else if (/Timeline Photos/i.test(postData.name)) {
				title = "Foto dalla bacheca";
			} else {
				title = postData.name;
			}

			post = _.assign(postPrototype, {
				link: postData.link,
				caption: postData.caption,
				image_array: _.has(postData, "attachments.data[0].subattachments.data") && postData.attachments.data[0].subattachments.data.length > 1 ? postData.attachments.data[0].subattachments.data.slice() : [],
				title: title,
				type: "photo"
			});
			if(!post.description) {
				post.description = postData.description || "Abbiamo pubblicato una nuova foto";
			}
		} else if (_.get(postData, "type") == "video") {
			post = _.assign(postPrototype, {
				title: postData.name,
				abstract: postData.description,
				source: postData.source,
				type: "video",
				caption: postData.caption,
				link: postData.link
			});
			if(!post.description) {
				post.description = postData.description || "Abbiamo pubblicato un nuovo video";
			}
		} else if (_.get(postData, "type") == "offer") {
			post = _.assign(postPrototype, {
				title: postData.name,
				abstract: postData.description,
				type: "offer",
				caption: postData.caption,
				link: postData.link
			});
		}
		if (post && !post.cover_image) {
			post.cover_image = '/images/news-standard.jpg';
			post.cover_image_is_fallback = true;
		}
		deferred.resolve(post);
	}
	return deferred.promise;
};

postFactory.findById = function findById(post) {
	return db.getPostById(post.id).then(function (postData) {
		if(postData) {
			return postFactory(postData, {majeeko: _.get(post, "majeeko")});
		} else {
			return undefined;
		}
	});
};

module.exports = postFactory;
