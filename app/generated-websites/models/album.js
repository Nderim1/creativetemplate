var _ = require('lodash'),
	photoFactory = require('./photo'),
	db = require('../../db'),
	Q = require('q'),
	logger = require('../../utils/logger');

var albumFactory = function albumFactory(albumData, options) {
	options = options || {};
	var deferred = Q.defer();
	if (Object.keys(albumData).length < 5) {
		return albumFactory.findById(albumData);
	} else {
		deferred.resolve(albumFactory.init(album,options));
	}
	return deferred.promise;
};

albumFactory.createMultiple = function createMultiple(albums, options) {
	options = options || {};
	var sync = [];
	var async = [];
	var albumsMap = {};
	albums.forEach(function(albumData) {
		if (Object.keys(albumData).length < 5) {
			async.push(albumData);
			albumsMap[albumData.id] = albumData;
		} else {
			sync.push(albumData);
		}
	});
	return db._raw.albums.find({
		id: {
			$in: async.map( a => a.id )
		}
	})
	.toArray()
	.then(albums => {
		return sync.concat(albums)
			.sort( (a,b) => new Date(a.created_time) < new Date(b.created_time) ? 1 : -1 )
			.map( album => {
				var ref = albumsMap[album.id];
				if(ref && ref.majeeko) album.majeeko = ref.majeeko;
				return albumFactory.init(album, options);
			});
	});
};

albumFactory.init = function init(albumData, options) {
	options = options || {};
	var cover_photo_id,
		cover_photo_obj;
	var album,
		albumPrototype  = {
			id: albumData.id,
			title: albumData.name,
			type: "",
			count: albumData.count,
			created_time: albumData.created_time,
			description: albumData.description,
			photos: [],
			majeeko: albumData.majeeko || options.majeeko
		};

	album = _.assign(albumPrototype, {
		type: albumData.type
	});
	if (_.has(albumData, "photos.data")) {
		for (var i = 0; i < albumData.photos.data.length; i++) {
			album.photos.push(photoFactory(albumData.photos.data[i], album));
		}
	}
	if (_.has(albumData, "cover_photo.id")){
		cover_photo_id = _.get(albumData, "cover_photo.id");
		cover_photo_obj = _.findWhere(album.photos, {id: cover_photo_id});
		album.cover_image = _.get(cover_photo_obj, "source");
	} else if(_.has(albumData, "cover_photo")){
		cover_photo_id = _.get(albumData, "cover_photo");
		cover_photo_obj = _.findWhere(album.photos, {id: cover_photo_id});
		album.cover_image = _.get(cover_photo_obj, "source");
	}
	album.cover_image = album.cover_image || _.get(album.photos[0], "source");
	return album;
};

albumFactory.findById = function findById(album) {
	return db.getAlbumById(album.id).then(function (albumData) {
		if(albumData) {
			return albumFactory.init(albumData, {majeeko: _.get(album, "majeeko")});
		} else if (album.pageID) {
			return db.getPageById(album.pageID).then(function (page) {
				var albums = _.get(page, 'pageData.albums.data');
				var albumData = _.findWhere(albums, { id: album.id});
				if(albumData) {
					return albumFactory.init(albumData);
				} else {
					return undefined;
				}
			});
		} else {
			return undefined;
		}
	});
};
module.exports = albumFactory;
