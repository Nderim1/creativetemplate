var logger = require('../utils/logger'),
	captchaService = require('./captcha'),
	// mailService = require('../utils/mail'),
	mandrillUtils = require('../utils/mail/mandrill'),
	config = require('../config'),
	pageUtils = require('./page-utils')

function sendContactForm (req,res,next) {
	if(!req.body['captcha_result']) {
		logger.error('Missing captcha in request');
		return res.json(400,{ error: 'Missing captcha in request!' });
	}

	captchaService.checkCaptcha(req.body['captcha_result'], req.session.captcha)
		.then(function(verified) {
			if(!verified) {
				return res.json({ success: false, error: 'Captcha not confirmed!' });
			}
			pageUtils.prepareSite(req, res)
			.then(function () {
				var domain = config.primary_domain;
				var to = req.site.getConfiguration().settings.mail;
				var options = {
					template: req.site.data.hasOwnProperty('activations') ? "email-form-siti-acquistati" : "email-form-siti-registrati",
					subject:"Nuovo messaggio dal sito " + req.rawHeaders[1],
					toAddr:to,
					fromAddr: "no-reply@" + config.primary_domain,
					merge_vars:{
						URL:req.rawHeaders[1],
						MAIL:req.body.email,
						MESSAGE:req.body.message,
						NOME:req.body.firstname || "-",
						COGNOME:req.body.lastname || "-"
					},
					replyTo: req.body.email
				}
				mandrillUtils.sendTemplate(options)
				.then(function (result) {
					res.json({ success: true, message: 'Contact form correctly sent!' });
				})
				.catch(function (err) {
					logger.error(err);
					res.json({ success: false, error: 'Error sending contact form!' });
				});
			});
		});
}

module.exports = {
	sendContactForm: sendContactForm
};
