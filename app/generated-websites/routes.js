var router = require('express').Router(),
	pageUtils = require('./page-utils'),
	_ = require('lodash'),
	renderer = require('./renderer'),
	contacts = require('./contacts'),
	captcha = require('./captcha'),
	session = require('express-session'),
	MongoStore = require('connect-mongo')(session),
	config = require('../config'),
	db = require('../db'),
	mail = require('../utils/mail'),
	Q = require('q');

router.use(
	session({
		resave: false,
		saveUninitialized: false,
		secret: 'THE_big_MAJEEKO_s3gr3t0',
		cookie: {
			// path: '/',
			// domain: (req.hostname.indexOf(config.primary_domain) != -1 ? '.' + config.primary_domain : req.hostname),
			maxAge: 1000 * 60 * 60 * 24 // 24 hours
		},
		store: new MongoStore({
			url: 'mongodb://' + config.mongo_connection
		})
	})
);
router.use(function (req,res,next){
	req.session.cookie.domain = (req.hostname.indexOf(config.primary_domain) != -1 ? '.' + config.primary_domain : req.hostname);
	next();
});
router.use(pageUtils.analyzeURL);

router.get('/css/dynamic.css', renderer.renderDynamicCss);

router.get('/', renderer.renderHomepage);

router.get('/sitemap',function (req,res,next){
	console.log('sitemap requested');
	return renderer.renderSitemap(req,res);
});

router.get('/captcha.png', function (req, res) {
	return captcha.generateCaptcha(req, res).then(function (image) {
		res.header('content-type', 'image/gif');
   		return res.send(image);
	});
});

router.get('/:page', renderer.renderPage);

router.get('/news/:id', function (req,res,next) {
	return renderer.renderPostDetailPage(req, res, 'news-detail');
});
router.get('/albums/:id', function (req,res,next) {
	return renderer.renderPostDetailPage(req, res, 'album-detail');
});
router.get('/events/:id', function (req,res,next) {
	return renderer.renderPostDetailPage(req, res, 'event-detail');
});

router.post('/send-contact-form', contacts.sendContactForm);

router.post('/submit-order', function (req, res) {

	//
	// Add company name to the contacts
	//
	if(req.body.customer && req.body.cart) {

		var customer = {
				name: req.body.customer.fname,
				last_name: req.body.customer.lname,
				email: req.body.customer.sof_email,
				phone: req.body.customer.sof_ph,
				address: req.body.customer.sof_add,
				city: req.body.customer.sof_city,
				province: req.body.customer.sof_province,
				zip: req.body.customer.sof_zip,
				country: req.body.customer.sof_country
			},
			order = {
				customer: customer,
				currency: req.query.currency_code,
				shipping: parseFloat(req.query.handling_cart),
				message: req.body.customer.sof_message,
				total: 0,
				items: []
			};
		// req.body.cart
		for(var i = 0; i < req.body.cart.length; i++) {
			order.total += (parseFloat(req.body.cart[i].price) * parseInt(req.body.cart[i].quantity));
			order.items.push(req.body.cart[i]);
		}

		order.total += parseFloat(order.shipping);

		db.getPageById(req.query.storeID).then(function (page) {
			if(page) {
				db.saveNewOrder(req.query.storeID, order).then(function (data) {
						Q.all([
							mail.sendNewOrderMessage(customer.email, page.modules.store.configuration.ownerEmail, order, page, "money_transfer", customer),
							mail.sendNewOrderMessage(page.modules.store.configuration.ownerEmail, customer.email, order, page,  "money_transfer")
							])
						.then(function (response) {
							return res.json({adminMailSent: true, customerMailSent: true});
						}).catch(function (error) {
							logger.error(error);
							return res.status(501).send();
						});
				});
			} else {
				res.status(501).send();
			}
		});
	}
});

router.all('/store/thanks', function (req, res, next) {
	var pageLookupFn,
		pageLookupCriteria;

	if(req.site.is_subdomain && !req.site.editor) {
		pageLookupFn = db.getPageByUsername;
		pageLookupCriteria = req.subdomains.slice(1).join('.');
	} else if(req.site.is_subdomain && req.site.editor){
		pageLookupFn = db.getPageByUsername;
		pageLookupCriteria = req.path.split("/")[2];
	} else {
		pageLookupFn = db.getPageByDomain;
		pageLookupCriteria = req.hostname;
	}
	pageLookupFn(pageLookupCriteria).then(function (page) {
		if(page) {
			var storeID = _.get(_.findWhere(page.configuration.contents.extra_pages, {type: "store"}), "id");
			var storeSlug = _.get(_.findWhere(page.configuration.settings.menu, {id: storeID}), "slug");
			res.redirect(301, "/" + storeSlug + "#/thanks/uid-show");
		}
	});
});

router.post('/store/ipn', function (req, res, next) {
	var customer = {
			name: req.body.first_name,
			last_name: req.body.last_name,
			email: req.body.payer_email,
			phone: "",
			address: req.body.address_street,
			city: req.body.address_city,
			province: req.body.address_state,
			zip: req.body.address_zip,
			country: req.body.address_country
		},
		paypal = {
			txn_id: "1V9246871Y168640M",
			txn_type: "cart",
			verify_sign: "AIKG13BPLgEqdnatIVeRNqKu2lbDAdaSRlFmMRZ7zoIRi43LFY0rihsu",
			payment_status: "Completed",
			payment_date: "07:04:11 Jan 19, 2016 PST",
			"ipn_track_id": "50e4225a81f86",
			"payer_id": "YFUFAE28LD3RQ",
			"payer_status": "verified",
		},
		order = {
			customer: customer,
			paypal: paypal,
			shipping: req.body.mc_handling,
			total: req.body.mc_gross,
			status: "paid",
			items: []
		};

	for(var i = 1; i <= req.body.num_cart_items; i++) {
		var item = {};
		item.name = req.body["item_name"+i];
		item.productID = req.body["item_number"+i];
		item.quantity = req.body["quantity"+i];
		item.price = req.body["mc_gross_"+i] / item.quantity ;
		order.items.push(item);
	}

	db.getPageByUsername(req.subdomains.slice(1).join('.')).then(function (page) {
		if(page) {
			if(req.body.receiver_email == page.modules.store.configuration.businessEmail) {
				db.saveNewOrder(page.facebook_id, order).then(function (data) {
					Q.all([
						mail.sendNewOrderMessage(customer.email, page.modules.store.configuration.ownerEmail, order, page, "paypal", customer),
						mail.sendNewOrderMessage(page.modules.store.configuration.ownerEmail, customer.email, order, page, "paypal")
						])
					.then(function (response) {
						return res.json({adminMailSent: true, customerMailSent: true});
					}).catch(function (error) {
						logger.error(error);
						return res.status(501).send();
					});
				});
			}
		} else {
			res.status(501).send();
		}
	});
});

module.exports = router;
