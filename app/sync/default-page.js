var _ = require('lodash');

module.exports = function(pageId, pageAccessToken, template) {
	var templateConfiguration = _.cloneDeep(require('../templates/' + template + '/default-configuration'));
	if(!templateConfiguration.contents.extra_pages || !templateConfiguration.contents.extra_pages.length) {
		templateConfiguration.contents.extra_pages = [
	        {
	            "id": "extra-page-1",
	            "real_name": "Pagina extra 1",
	            "text": "",
	            "type": "text"
	        },
	        {
	            "id": "extra-page-2",
	            "real_name": "Pagina extra 2",
	            "text": "",
	            "type": "text"
	        }
	    ];
	}
	return {
		facebook_id: pageId,
		fb_access_token: pageAccessToken,
		needs_resync: false,
		status: 'unsynced',
		configuration: templateConfiguration
	};
};
