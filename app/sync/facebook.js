var config = require('../config'),
	request = require('request-promise'),
	db = require('../db'),
	facebookGraphAPI = "https://graph.facebook.com",
	colors = require('colors'),
	async = require('async'),
	qs = require('qs'),
	Q = require('q'),
	R = require('ramda'),

	facebook = require('../facebook'),
	logger = require('../utils/logger'),

	moment = require('moment'),

	_ = require('lodash'),
	less = require('less'),
	fs = require('fs'),

	templateUtils = require('../utils/templates'),
	pageUtils = require('../utils/pages'),

	rx = require('rx');

	_.defaults = require('merge-defaults');

var appAccessToken = config.facebook.clientID + "|" + config.facebook.clientSecret;

var saveUserData = function(profile, data, accessToken, callback) {
	var deferred = Q.defer();
	try {
		var user = {
			$set: {
				facebook: {
					profile: profile,
					authorized: true,
					access_token: accessToken,
					accounts: (data.accounts && data.accounts.data) ? data.accounts.data : [],
					profile_picture: data.picture.data.url
				}
			},
			$setOnInsert: {
				first_login: moment().toISOString()
			}
		};
		db._raw.users.update({
				'facebook.profile.id': profile.id
			}, user, {
				upsert: true
			})
			.then(function(user) {
				logger.info('Successfully saved user data');
				deferred.resolve(user);
			})
			.catch(function(err) {
				logger.error('Error while saving user data', err);
				deferred.reject(err);
			});
	} catch (err) {
		deferred.reject(err);
		logger.error('Error while saving user data', err);
	}
	return deferred.promise;
};

var usernameReplaceCharacters = function(username) {
	return username.replace(/[^\d\w]+/g, '-').toLowerCase().replace(/^[\-]*([^\-].+[^\-])[\-]*$/g, '$1');
};

var getFreeUsername = function(pageData) {

	logger.info('searching free username');

	var deferred = Q.defer();

	var possibleUsernames = [],
		city;
	if (pageData.location && pageData.location.city) {
		city = usernameReplaceCharacters(pageData.location.city.toLowerCase());
	}
	if (pageData.username) {
		possibleUsernames.push(usernameReplaceCharacters(pageData.username).slice(0,63));
		if (city) {
			possibleUsernames.push(usernameReplaceCharacters(pageData.username).slice(0,63 - city.length - 1) + '-' + city);
		}
	}
	if (pageData.name) {
		possibleUsernames.push((usernameReplaceCharacters(pageData.name)).slice(0,63));
		if (city) {
			possibleUsernames.push(usernameReplaceCharacters(pageData.name).slice(0,63 - city.length - 1) + '-' + city);
		}
	}

	logger.info('usernames taken into account:', possibleUsernames.join(','));

	var usernames = rx.Observable.from(possibleUsernames);

	usernames
		.concatMap(function(username) {
			return rx.Observable.fromPromise(db.checkPageUsernameFree(username))
				.filter(function(free) {
					return free;
				})
				.map(function() {
					return username;
				});
		})
		.first()
		.subscribe(function(username) {
			logger.info('picked username:', username);
			deferred.resolve(username);
		}, function(e) {
			deferred.resolve(pageData.id);
			console.error(e);
		}, function() {
			deferred.resolve(pageData.id);
			console.log('completed free username search');
		});

	return deferred.promise;

};

var mergePosts = function(olds, news) {
	olds = olds || [];
	news = news || [];
	var postNoOverwriteProperties = ['exclude'];
	var ids = _.map(news, R.prop('id'));
	return _.map(ids, function(id) {
		var oldOne = _.findWhere(olds, {
				id: id
			}) || {},
			newOne = _.findWhere(news, {
				id: id
			});
		return _.extend({}, oldOne, _.omit(newOne, postNoOverwriteProperties));
	});
};

var syncPage = function(pageId, pageAccessToken, options) {

	return request({
		method: 'POST',
		json: true,
		uri: config.site_sync_url,
		body: {
			id: pageId,
			options: options
		}
	})
	.then(function() {
		return { message: "done" };
	})
	.catch(function(err) {
		logger.error('Error syncing page ', err);
	});
};

var syncAllUserPages = function(userFacebookId) {
	return db.getUserAccounts(userFacebookId)
		.then(function(accounts) {
			async.each(accounts, function(account, callback) {
				logger.info(account.id, account.fb_access_token);
				syncPage(account.id, account.fb_access_token)
					.then(function() {
						logger.info('ok', account.id);
						callback(null, account.id);
					})
					.catch(function(err) {
						logger.error(err);
						callback(err);
					});
			}, function() {
				logger.info('All pages synced for user', userFacebookId);
			});
		})
		.catch(function(err) {
			logger.error(err);
			return err;
		});
};

var syncAllPages = function() {
	var deferred = Q.defer();
	db._raw.pages.find()
		.toArray()
		.then(function(err, pages) {
			async
			.each(pages, 
				function(page, callback) {
					logger.info(page.pageData.id, page.fb_access_token);
					syncPage(page.pageData.id, page.fb_access_token)
						.then(function() {
							logger.info('ok', page.pageData.id);
							callback(null, page.pageData.id);
						})
						.catch(function(err) {
							logger.error(err);
							callback(err);
						});
				},
				function() {
					logger.info('All pages synced');
					deferred.resolve();
				}
			);
		})
		.catch(function(err) {
			logger.error(err);
			deferred.reject(err);
		})
	return deferred.promise;
};


module.exports = {
	saveUserData: saveUserData,

	usernameReplaceCharacters: usernameReplaceCharacters,

	syncPage: syncPage,
	syncAllPages: syncAllPages,
	syncAllUserPages: syncAllUserPages,

	getFreeUsername: getFreeUsername
};
