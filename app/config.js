module.exports = {
    defaultTemplate: 'hellobrand'
  , root_url: process.env.ROOT_URL || 'http://www.majeeko-test.com'
  , primary_domain: process.env.HOST || 'majeeko-test.com'

  , URL2PNG_APIKEY: 'P54EDB1E126D89'
  , URL2PNG_SECRET: 'SEA9D2B51D6379'

  , AWS_ACCESS_KEY_ID: process.env.AWS_KEY_ID || ''
  , AWS_SECRET_ACCESS_KEY: process.env.AWS_SECRET_ACCESS_KEY || ''

// paypal sandbox
  , paypal: {
	    // host: process.env.PAYPAL_HOST || 'api.sandbox.paypal.com'
      mode: process.env.PAYPAL_MODE || 'sandbox'
	  , client_id: process.env.PAYPAL_CLIENT_ID
	  , client_secret: process.env.PAYPAL_CLIENT_SECRET
	  // , account: 'karel291180-facilitator@gmail.com'
  }

  , facebook: {
     clientID: process.env.FACEBOOK_APP_ID
    , clientSecret: process.env.FACEBOOK_APP_SECRET
    // , callbackURL: (process.env.ROOT_URL || 'http://www.majeeko-test.com') + '/auth/facebook/callback'
    , callbackURL: 'http://' + (process.env.HOST || 'majeeko-test.com') + '/auth/facebook/callback'
  }

  , mandrill: {
      api_key: process.env.MANDRILL_API_KEY || ''
  }

  // , recaptcha: {
  //   secret: '6LfRpAYTAAAAAK-EFSQv9q-iEHF1Z4S3EOfsvmt7'
  // }

  , mongo_connection: (process.env.MONGO_HOST || 'localhost') + 
                      ':' + (process.env.MONGO_PORT || '27017') + 
                      '/' + (process.env.MONGO_NAME || 'synky')
  , tophost_endpoint: process.env.TOPHOST_API_ENDPOINT 
      // || 'http://localhost:3000' 
      || 'http://tophost-test-api-vje4npqs5n.elasticbeanstalk.com'
  , tophost_redirect: process.env.TOPHOST_REDIRECT_URL

  , tophost_tokens_redis_host: process.env.TOPHOST_TOKENS_REDIS_HOST || 'localhost'
  , tophost_tokens_redis_port: process.env.TOPHOST_TOKENS_REDIS_PORT || '6379'
  , site_sync_url: process.env.SERVICE_SITE_SYNC_URL || 'http://localhost:1337/api/sync'
}
