var db = require('../db'),
	_ = require('lodash');
	router = require('express').Router();

function userOwnsStore(req, res, next) {
	db.checkUserOwnsPage(req.user.id, req.params.pageID).then(function (owns) {
		if (owns) {
			next();
		} else {
			res.send(403);
		}
	});
}

// products routes
router.route('/products/:pageID')
.all(userOwnsStore)
.get(function (req, res, next) {
	db.getStoreProducts(req.params.pageID).then(function (categoryList) {
		var products = [];
		if(categoryList) {
			if (req.query && req.query.filter) {
				var singleCategoryProducts = _.findWhere(categoryList.category, {categoryName: req.query.filter});

				if(singleCategoryProducts) {
					products = singleCategoryProducts.products;
				}
			} else {
				products = _.filter(_.flatten(_.map(categoryList.category, function (category) {
					return category.products;
				})), Boolean);
			}
		}
		res.json(products);
	});
})
.post(function (req, res, next) {
	if(req.body && req.body.product) {
		if(req.body.action == "delete") {
			db.deleteStoreProduct(req.params.pageID, req.body.product).then(function (response) {
				res.json(response);
			});
		} else {
			db.saveStoreProduct(req.params.pageID, req.body.product).then(function (response) {
				res.json(response);
			});
		}
	} else {
		res.status(403).json({"message": "send a vald category object"});
	}
});

router.get('/products/:pageID/:id', userOwnsStore, function (req, res, next) {
	db.getStoreProductByID(req.params.pageID, req.params.id).then(function (product) {
		if(product) {
			res.json(product);
		} else {
			res.json(404);
		}
	});
});

// categories routes
router.route('/categories/:pageID')
.all(userOwnsStore)
.get(function (req, res, next) {
	db.getStoreCategories(req.params.pageID).then(function (categories) {
		if(categories) {
			categories.sort(function(a, b){
			    if(a.categoryName < b.categoryName) return -1;
			    if(a.categoryName > b.categoryName) return 1;
			    return 0;
			});
			res.json(categories);
		} else {
			res.json([]);
		}
	});
})
.post(function (req, res, next) {
	if(req.body && req.body.category) {
		if(req.body.action == "delete") {
			db.deleteStoreCategory(req.params.pageID, req.body.category).then(function (response) {
				res.json(response);
			});
		} else {
			db.saveStoreCategory(req.params.pageID, req.body.category).then(function (response) {
				res.json(response);
			});
		}
	} else {
		res.status(403).json({"message": "send a vald category object"});
	}
});

router.get('/categories/:pageID/:id', userOwnsStore, function (req, res, next) {
	db.getStoreCategoryByID(req.params.pageID, req.params.id).then(function (category) {
		if (category) {
			res.json(category);
		} else {
			res.status(404).json({message: 404});
		}
	});
});

// configuration routes
router.route('/configuration/:pageID')
.all(userOwnsStore)
.get(function (req, res, next) {
	db.getStoreConfiguration(req.params.pageID).then(function (configuration) {
		if(configuration) {
			res.json(configuration);
		} else {
			res.status(404).json({});
		}
	});
})
.post(function (req, res, next) {
	if(req.body && req.body.configuration) {
		db.saveStoreConfiguration(req.params.pageID, req.body.configuration).then(function (response) {
			res.json(response);
		});
	} else {
		res.status(404).json({});
	}
});

// orders routes
router.route('/orders/:pageID')
.all(userOwnsStore)
.get(function (req, res, next) {
	db.getStoreOrders(req.params.pageID).then(function (orders) {
		if(orders) {
			orders.sort(function(a,b){
	 			return new Date(b.date) - new Date(a.date);
			});
			res.json(orders);
		} else {
			res.json([]);
		}
	});
})
.post(function (req, res, next) {
	if(req.body && req.body.order) {
		db.saveStoreOrder(req.params.pageID, req.body.order).then(function (response) {
			res.json(response);
		});
	} else {
		res.status(404).json({message: 404});
	}
});

router.get('/orders/:pageID/:id', function (req, res, next) {
	db.getStoreOrderByID(req.params.pageID, req.params.id).then(function (order) {
		if(order) {
			res.json(order);
		} else {
			res.status(404).json({message: 404});
		}
	});
});

module.exports = router;
