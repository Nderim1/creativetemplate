module.exports = function(router) {

	var db = require('../db'),
		auth = require('../auth'),
		R = require('ramda'),
		_ = require('lodash'),
		facebookSync = require('../sync/facebook'),
		facebook = require('../facebook'),
		previewManager = require('../website-preview'),
		logger = require('../utils/logger'),

		domainService = require('../utils/domain'),
		qs = require('qs'),

		coupons = require('../utils/coupons'),
		Q = require('q'),

		templateUtils = require('../utils/templates'),

		basicAuth = require('basic-auth'),
		pageUtils = require('../utils/pages'),
		sanitizePage = require('../../scripts/sanitize-page'),
		slugify = require('slug'),
		bodyParser = require('body-parser'),
		siteFactory = require('../generated-websites/models/site'),
		albumFactory = require('../generated-websites/models/album'),
		storeAdminRouter = require('./store'),
		purchase = require('../purchase/routes'),
		Order = require('../purchase/orders');


	router.use('/store', auth.ensureAuthenticated, storeAdminRouter);
	router.use('/purchase', auth.ensureAuthenticated, purchase);

	router.post('/orders/complete/:id', auth.requireBasicAuth, function (req, res, next) {
		if(req.params && req.params.id) {
			Order.getById(req.params.id).then(function (order) {
				if(!_.has(order, "status")){
					res.status(400).send();
				} else {
					switch(order.status){
						case "pending":
						// change order status to pending_billing
							order.status = "pending_billing";
							order.payment_info.payment_received_on = moment().toISOString();
							order.save().then(function (order) {
								res.json({ status: "pending_billing" });
							}).catch(function (err) {
								logger.error(err);
								res.status(400).json({ error: err});
							});
							break;
						case "pending_payment":
						// chang order status to completed
							order.complete().then(function (order) {
								db._raw.pages.findOne({facebook_id: order.facebook_page_id}).then(function (page) {
									var service = _.findWhere(order.cart, { type: "service" });
									var isPro = (service.value == "pro" || service.value == "upgrade") ? true : false;
									pageUtils.activatePageForDomains(page.facebook_id, order.domains, undefined, isPro);
									res.json({ status: "completed" });
								});
							});
							break;
						default:
					}
				}
			});
		}
	});

	var pwAnalytics = require('../utils/analytics/piwik');

	var getUserAccountsIds = function(user) {
		return user.facebook.accounts.filter(R.prop('is_published')).map(R.prop('id'));
	};

	router.get('/api/me/status', auth.ensureAuthenticated, function(req, res) {
		db.getUserPages(req.user.id)
			.then(function (pages) {
				res.json({
					ready: (pages.length == getUserAccountsIds(user).length) && user.ready
				});
			});
	});

	var getUserPages = function (user, attempts) {
		var deferred = Q.defer();
		if(attempts === null || attempts === undefined) attempts = 10;
		logger.info('getUserPages'.red, user.facebook.profile.id, attempts);
		var _pages;
		try {
			var recursion = function recursion(user, attempts) {
				logger.info('getUserPagesUntilComplete'.red, user.facebook.profile.id, attempts);
				if(attempts <= 0) {
					if(_pages) {
						deferred.resolve(_pages);
					} else {
						deferred.reject('to many attempts');
					}
					return;
				}
				try {
					db.getUserPages(user.facebook.profile.id)
						.then(function(pages) {
							_pages = pages;
							if(pages.length == getUserAccountsIds(user).length) {
								deferred.resolve(pages);
							} else {
								setTimeout(recursion, 1000, user, attempts - 1);
							}
						})
						.catch(function (err) {
							logger.error(err);
							setTimeout(recursion, 1000, user, attempts - 1);
						});
				} catch (err) {
					logger.error(err);
					deferred.reject(err);
				}
			};
			recursion(user, attempts);
		} catch (err) {
			logger.error(err);
			deferred.reject(err);
		}
		return deferred.promise;
	};

	var getUserUntilReady = function (userFacebookId, attempts) {
		var deferred = Q.defer();
		if(attempts === null || attempts === undefined) attempts = 10;
		logger.info('getUserUntilReady'.red, userFacebookId, attempts);
		try {
			var recursion = function(userFacebookId, attempts) {
				logger.info('getUserUntilReady recursion'.red, userFacebookId, attempts);
				if(attempts <= 0) {
					logger.error('user never ready');
					deferred.reject('user never ready');
					return;
				}
				db.getUserByFacebookId(userFacebookId)
					.then(function (user) {
						if(user.ready) {
							deferred.resolve(user);
						} else {
							setTimeout(recursion, 1000, userFacebookId, attempts - 1);
						}
					})
					.catch(function (err) {
						logger.error(err);
						deferred.reject(err);
					});
			};
			recursion(userFacebookId, attempts);
		} catch (err) {
			logger.error(err);
			deferred.reject(err);
		}

		return deferred.promise;
	};

	var getPages = function(userFacebookId) {
		return getUserUntilReady(userFacebookId) // returns user
			.then(getUserPages); // returns pages
	};

	var composePageData = function (site) {
		var posts_count = site.posts ? site.posts.length : 0,
			events_count = site.events ? site.events.length : 0,
			albums_count = site.albums ? site.albums.length : 0;

		var page = site.data || site;
		logger.debug('composePageData');
		var _page = _.omit(page, [
			"_id",
			"fb_access_token",
			"template",
			"pageData",
			"configuration"
		]);
		if(page.pageData) {
			_page = _.extend(_page, {
				id: page.pageData.id,
				picture: (page.pageData.picture && page.pageData.picture.data) ? page.pageData.picture.data.url : '/images/brand-standard.jpg',
				name: page.pageData.name,
				username: page.username,
				category: page.pageData.category,
				likes: page.pageData.likes,
				posts_count: posts_count,
				events_count: events_count,
				albums_count: albums_count,
				domain: pageUtils.getGeneratedWebsiteDomain(page),
				url: pageUtils.getGeneratedWebsiteUrl(page)
			});
		}
		if (page.paid_on) {
			_page.status = 'paid';
			if (page.hasOwnProperty('analytics')) {
				logger.info('idSite',page.analytics.piwik.idSite);
				return pwAnalytics.getWidgetUrl(page.analytics.piwik.username, page.analytics.piwik.password, page.analytics.piwik.idSite)
					.then(function(data) {
						logger.info('composePageData', data);
						return _.extend(_page, {
								piwikWidgetUrl: data
							});
							//return _page;
					})
					.catch(function(err) {
						logger.error('Error getting Piwik widget in composePageData', {
							error:err,
							stack:err.stack,
							filename: __filename,
							meta: {
								page_analytics_piwik_username: page.analytics.piwik.username,
								page_analytics_piwik_password: page.analytics.piwik.password,
								page_analytics_piwik_idSite: page.analytics.piwik.idSite
							}
						})
						return _page;
					});
			} else {
				if(page.domains && page.domains.length > 0) {
					pwAnalytics.registerSiteToPiwik(page.facebook_id,page.domains);
				}
			}
			var deferred = Q.defer();
			deferred.resolve(_page);
			return deferred.promise;
			// return _page;
		} else {
			var deferred = Q.defer();
			deferred.resolve(_page);
			return deferred.promise;
		}
	};

	router.get('/api/me/pages', auth.ensureAuthenticated, function(req, res) {
		getPages(req.user.id)
			.then(function(pages) {
				var sitesPromise = [];
				for (var i = 0; i < pages.length; i++) {
					sitesPromise.push(siteFactory(pages[i]));
				}

				return Q.all(sitesPromise).then(function (sites) {
					return Q.all(sites.map(function(site) {
							return composePageData(site);
						}))
						.then(function(data) {
							res.json(data);
						});
				});
			})
			.catch(function(err) {
				console.log(err);
				res.status(500).send();
			});
	});

	router.post('/api/editor', auth.ensureAuthenticated, function(req, res) {
		db.getPageByUsername(req.body.username)
			.then(function(page) {
				if(page) {
					db.checkUserOwnsPage(req.user.id,page.facebook_id)
						.then(function (owns) {
							if(owns) {
								var template = req.body.configuration.graphics.template;
								var action = req.body.action;

								return templateUtils.renderTemplateStyles(template, req.body.configuration.graphics)
								    .then(function(output) {
								        var configuration = req.body.configuration;
								        configuration._css = output.css;

								        // Create slugs for all links
								        _.forEach(configuration.settings.menu, function (item) {
								        	var slug = slugify(item.label, { lower: true});
								        	var slug_item = _.find(configuration.settings.menu, {slug:slug});
								        	if(!slug_item) {
								        		item.slug = slug;
								        	} else if(slug_item && (_.get(slug_item, "id") === _.get(item, "id"))){
								        		item.slug = slug;
								        	} else {
								        		item.slug = item.id;
								        		item.label = _.capitalize(item.id);
								        	}
								        });

								        // restore lost menu items
								        var defaultConfiguration = require('../templates/' + template + '/default-configuration');
								        defaultConfiguration.settings.menu.forEach(function (item) {
								        	if(!_.findWhere(configuration.settings.menu, {id:item.id})) {
								        		configuration.settings.menu.push(item);
								        	}
								        });

								        return db.updatePageConfiguration(req.body.username, configuration, action)
									        .then(function() {
									        	logger.info('Successfully saved editor changes for: ' + action);
									        	return res.json({ message: 'done'});
									        });
									})
									.catch(function(err) {
										logger.error(err);
										throw err;
									});
							} else {
								return res.send(403);
							}
						});
				} else {
					res.send(404);
				}
			}).catch(function (err) {
				logger.error('error', err);
				return res.json({ message: 'error'});
			});
	});

	router.get('/api/album/:pageID/:id', auth.ensureAuthenticated, function getAlbumbyID(req, res) {
		var album = {
			id: req.params.id || null,
			pageID: req.params.pageID || null
		};
		return albumFactory.findById(album).then(function (albumObject) {
			if(albumObject) {
				res.json(albumObject);
			} else {
				res.send(404);
			}
		}).catch(function () {
			res.send(404);
		});
	});

	router.post('/api/sync', auth.ensureAuthenticated, function(req, res) {
		if(!req.body.id) res.status(400).end();
		db.checkUserOwnsPage(req.user.id, req.body.id).then(function(owns) {
			if(owns) {
				db.getPageById( req.body.id ).then(function(page) {
					return facebookSync.syncPage( page.facebook_id, page.fb_access_token, { limitedSync: !page.paid_on })
					.then(function (page) {
						logger.info('Sync complete');
						return siteFactory.findById(req.body.id).then(function (site) {
							return composePageData(site).then(function(site){
								return res.json({ message: 'done', page: site});
							});
						});
					})
					.catch(function (err) {
						logger.info('Sync error', err);
						return res.status(500).json({ message: err});
					} );
				});

			} else {
				return res.status(403).json({ message: 'Not Authorized!'});
			}
		});
	});

	router.get('/api/toggle-post/:id', auth.ensureAuthenticated, function(req, res) {
		var pageId = req.params.id;
		if(!pageId) res.status(400).end();
		return db.checkUserOwnsPage(req.user.id, pageId).then(function(owns) {
			if(owns) {
				return siteFactory.findById(pageId).then(function(site) {
					var posts = [],
						albums = [],
						events = [];
					if(_.has(site, "posts")) {
						posts = _.filter(site.posts, function(element) {
							return _.get(element, "majeeko.hidden");
						});
					}
					if(_.has(site, "albums")) {
						albums = _.filter(site.albums, function(element) {
							return _.get(element, "majeeko.hidden");
						});
					}
					if(_.has(site, "events")) {
						events = _.filter(site.events, function(element) {
							return _.get(element, "majeeko.hidden");
						});
					}
					return res.status(200).json({ message: 'done', data: {news: posts, album: albums, event: events}});
				}).catch(function(err) {
					logger.error('Error during getting hidden elements', {
						error: err,
						stack: err.stack,
						filename: __filename,
						meta: {
							page_id: pageId,
							user_id: req.user.id
						}
					});
					return res.status(500).end();
				});
			}
		});
	});

	router.post('/api/toggle-post', auth.ensureAuthenticated, function(req, res) {
		var pageId = req.body.pageId;
		if(!pageId) res.status(400).end();

		function _toggleValues(page, collection, type){
			_.forEach(collection, function(element) {
				var collection_element = _.findWhere(page.pageData[type].data, {id:element});
				var hidden_value = _.get(collection_element, "majeeko.hidden", false);
				if(hidden_value === false) {
					_.set(collection_element, "majeeko.hidden", true);
				} else {
					_.set(collection_element, "majeeko.hidden", false);
				}
			});
		}

		return db.checkUserOwnsPage(req.user.id, pageId).then(function(owns) {
			if(owns) {
				db.getPageById(pageId).then(function(page) {
					if(page) {
						if(_.has(page, "pageData.posts.data")) {
							_toggleValues(page, req.body.news, "posts");
						}

						if(_.has(page, "pageData.albums.data")) {
							_toggleValues(page, req.body.album, "albums");
						}

						if(_.has(page, "pageData.events.data")) {
							_toggleValues(page, req.body.event, "events");
						}

						return db._raw.pages.update({ 'facebook_id': pageId }, page)
							.then(function(data) {
								return res.json({ message: 'done'});
							}).catch(function(err) {
								return res.status(304).json({ message: err});
							});
					} else {
						throw new Error("Facebook Page not found!");
					}
				}).catch(function(err) {
					logger.error('Error during hiding element/s', {
						error: err,
						stack: err.stack,
						filename: __filename,
						meta: {
							page_id: pageId,
							user_id: req.user.id
						}
					});
					return res.status(500).end();
				});
			}
		});
	});

	/**
	 * Facebook page search
	 * requires "q" queryString parameter with the string to search for with Facebook graph API
	 */
	router.get('/api/page-search', function (req,res) {
		if(!req.query.q) return res.send(400, 'Missing query parameter');
		facebook
			.searchPages(req.query.q)
			.then(function (results) {
				res.json(results);
			}, function (err) {
				res.status(500).send();
			});
	});

	router.post('/api/generate-preview', function (req,res) {
		if(!req.body.id) {
			logger.error('Preview requested for page but missing id in request');
			return res.send(400, "Missing id in request");
		}
		logger.info('Preview requested for page with ID', req.body.id);
		previewManager.createPreview(req.body.id)
			.then(function (previewUrl) {
				res.json({ status: 'success', message: 'Preview created', url:previewUrl});
			})
			.catch(function (err) {
				logger.error('Error while generating preview', err);
				res.status(500).send();
			});
	});

	router.post('/api/fetch-preview', function (req,res) {
		if(!req.body.id) {
			logger.error('Preview requested for page but missing id in request');
			return res.send(400, "Missing id in request");
		}
		logger.info('Preview requested for page with ID', req.body.id);
		previewManager.fetchPreview(req.body.id)
			.then(function (previewUrl) {
				res.json({ status: 'success', message: 'Preview fetched', url:previewUrl});
			})
			.catch(function (err) {
				logger.error('Error fetching preview', err);
				res.status(500).send();
			});
	});

	router.get('/api/domain/suggest', function (req,res) {
		var query = qs.parse(req.query);
		if(!query.q) {
			return res.send(400, 'Missing query parameter');
		}
		domainService.suggest(query.q)
			.then(function (suggestions) {

				if(!suggestions){
					return res.send('ERROR:NO_SUGGESTIONS');
				}
				if(_.some(suggestions, _.has("error"))){
					return res.send('ERROR:DOMAIN_GENERIC_ERROR');
				}
				for(var i in suggestions){
					if(_.contains(suggestions[i].error, "InvalidInput")) {
						return res.send('ERROR:INVALID_DOMAIN');
					}
					if(_.has(suggestions[i],"error")){
						return res.send('ERROR:DOMAIN_GENERIC_ERROR');
					}
				}
				res.send(suggestions.filter(function(suggestion) {
					return _.contains(['AVAILABLE','UNAVAILABLE'], suggestion.availability);
				}).map(function (suggestion) {
					var matches = suggestion.domainName.match(/(.+)\.([^.]+)/);
					return _.extend(suggestion, {
						mld: matches[1],
						tld: matches[2]
					});
				}));
			})
			.catch(function (err) {
				logger.error('Error getting domain suggestions', err);
				res.send('ERROR:UNABLE_TO_SERVE_SUGGESTIONS');
			});
	});

	router.get('/api/coupon', function (req,res) {
		var coupon = qs.parse(req.query).coupon;
		if(coupon) {
			// res.json(coupons.getDiscount(coupon))
			coupons.getDiscount(coupon)
			.then(function(discount){
				res.json(discount);
			});
		} else {
			res.json(0);
		}
	});

	router.post('/api/sync-all-pages', auth.requireBasicAuth, function (req,res) {
		facebookSync.syncAllPages()
		.then(function() {
			res.send(200);
		})
		.catch(function (err) {
			res.send(500,err);
		});
	});

	router.post('/api/sync-all-user-pages', auth.requireBasicAuth, function (req,res) {
		facebookSync.syncAllUserPages(req.body.id)
		.then(function() {
			res.send(200);
		})
		.catch(function (err) {
			res.send(500,err);
		});
	});

	router.post('/api/sync-pages', auth.requireBasicAuth, function (req,res) {
		if(!req.body.ids) {
			res.send(400);
			return;
		}
		var options = req.body.options || {};
		db.getPagesByIds(req.body.ids)
		.then(function (pages) {
			return Q.all(pages.map(function (page) {
				return facebookSync
					.syncPage(page.facebook_id, page.fb_access_token,options);
			}))
			.then(function() {
				res.send(200);
			});
		})
		.catch(function (err) {
			res.send(500,err);
		});
	});

	router.post('/api/sync-paid-pages', auth.requireBasicAuth, function (req,res) {
		var t = (new Date()).getTime();
		var options = req.body.options || {};
		db._raw.pages.find({paid_on:{$exists:true}},{'facebook_id':1,'fb_access_token':1})
		.toArray()
		.then(function (pages) {
			return Q.all(pages.map(function (page) {
				return facebookSync
					.syncPage(page.facebook_id, page.fb_access_token,options);
			}))
			.then(function() {
				logger.info('All paid pages synced successfully in', ((new Date()).getTime() - t)/1000, 'seconds');
				res.send(200);
			});
		})
		.catch(function (err) {
			res.send(500,err);
		});
	});

	router.post('/api/sync-page', auth.requireBasicAuth, function (req,res) {
		var options = req.body.options || {};
		db.getPageById(req.body.id)
		.then(function (page) {
			return facebookSync
				.syncPage(req.body.id, page.fb_access_token,options)
				.then(function() {
					res.send(200);
				});
		})
		.catch(function (err) {
			res.send(500,err);
		});
	});

	router.post('/api/sanitize-page', auth.requireBasicAuth, function (req, res) {
		if(!req.body.id) res.status(400).send("Provide an ID");
		db.getPageById(req.body.id)
		.then(function (page) {
			return sanitizePage.sanitizePageConfigById(req.body.id)
					.then(function(page) {
						res.status(200).send(page.username + "'s configuration sanitized");
					});
		})
		.catch(function (err) {
			res.status(500).send(err);
		});
	});

	router.post('/api/recompile-website-less', auth.requireBasicAuth, function (req, res) {
		if(!req.body.id) res.status(400).send("Provide an ID");
		templateUtils.compileSiteDynamicCss(req.body.id)
		.then(function() {
			res.status(200).json({message:'done'});
		})
		.catch(function(err) {
			logger.error(err);
			res.status(500).send({message: 'error'});
		});
	});

	/**
	 * Method for handling frontend javascript errors
	 * req.body could be javaascript frontend generic error or CSP report from the browser
	 * {
	 *	  "csp-report": {
	 *	    "document-uri": "http://example.org/page.html",
	 *	    "referrer": "http://evil.example.com/",
	 *	    "blocked-uri": "http://evil.example.com/evil.js",
	 *	    "violated-directive": "script-src 'self' https://apis.google.com",
	 *	    "original-policy": "script-src 'self' https://apis.google.com; report-uri http://example.org/my_amazing_csp_report_parser"
	 *	  }
	 *	}
	 * @param  {req object} req
	 * @param  {res object} res
	 */
	router.post('/api/report-violation', bodyParser.json(), function (req, res) {
		if (req.body["csp-report"]) {
		    logger.error('Frontend CSP report', {
				"blocked-uri": req.body["csp-report"]["blocked-uri"],
				"document-uri": req.body["csp-report"]["document-uri"],
				"effective-directive": req.body["csp-report"]["effective-directive"],
				"original-policy": req.body["csp-report"]["original-policy"],
				"referrer": req.body["csp-report"].referrer,
				"status-code": req.body["csp-report"]["status-code"],
				"violated-directive": req.body["csp-report"]["violated-directive"]
			});
		} else if(req.body["error-report"]){
			logger.error('Frontend Javascript report', {
				"error-type": req.body["error-report"].message,
				file: req.body["error-report"].file,
				line: req.body["error-report"].line
			});
		}
		res.status(200).end();
	});

	router.post('/api/activate-sitemap',auth.requireBasicAuth,function(req,res){
		var id = req.body.id;
		db.activateSitemapsForId(id)
		.then(function(data){
			res.send(data);
		})
		.catch(function(err){
			res.status(502).send(err);
		})
	})

	router.post('/api/activate-sitemap-for-paid',auth.requireBasicAuth,function(req,res){
		db.activateSitemapsForPaidPages().then(function(result){
			res.send(result);
		})
		.catch(function(err){
			res.status(502).send(err);
		})
	})

	router.post('/activate-page',auth.requireBasicAuth,function(req,res){
		var domains = req.body.domains;
		var activator = req.body.activator;
		var id = req.body.id;

		pageUtils.activatePageForDomains(id,domains,activator)
		.then(function(data){
			res.send({status:true})
		})
	})

	router.post('/api/generate-preview-with-screenshot', auth.requireBasicAuth, function (req, res) {
		previewManager.createPreview(req.body.id)
			.then(function (previewUrl) {
				return previewManager.takeScreenshot(previewUrl,req.body.id + '.png')
			})
			.then(function (screenshotUrl) {
				res.json({ status: 'success', message: 'Preview created', url:screenshotUrl});
			})
			.catch(function (err) {
				logger.error('Error while generating preview', err);
				res.send(500, err);
			});
	});

	return router;
};
