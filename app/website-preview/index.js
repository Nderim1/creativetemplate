var config = require('../config')

  , url2png = require('url2png')(config.URL2PNG_APIKEY,config.URL2PNG_SECRET)

  , fs = require('fs')
  , facebookSync = require('../sync/facebook')
  , Q = require('q')
  , colors = require('colors')
  , logger = require('../utils/logger')
  , AWS = require('aws-sdk')

  , stream = require('stream')

  , moment = require('moment')

  , pageUtils = require('../utils/pages')
  , db = require('../db')

AWS.config.update({
	accessKeyId: config.AWS_ACCESS_KEY_ID,
	secretAccessKey: config.AWS_SECRET_ACCESS_KEY
});

var createPreview = function(facebookPageId) {
	logger.info('Creating preview for page with ID', facebookPageId)

	return Q.promise(function(res,rej) {
		// create blank page in db
		pageUtils.createRaw(facebookPageId)
			.then(function(page) {
				// sync page
				return facebookSync.syncPage(facebookPageId, null, {minimal: true})
					.then(function () {
						setTimeout(function() {
							var websiteUrl = pageUtils.getEmbeddedWebsiteUrlByPageId(facebookPageId) + '/?url2png=true';
							res(websiteUrl);
						}, 500);
					})
					.catch(function (err) {
						logger.error('Error while syncing page', err);
						rej(err);
					});
			})
			.catch(function (err) {
				logger.error('Error while creating preview', err);
				rej(err);
			});
	})
}

function fetchPreview(facebookPageId) {
	return db.getPageById(facebookPageId)
		.then(function(page) {
			if(!page) pageUtils.getEmbeddedWebsiteUrlByPageId('not-found') + '/?url2png=true';
				// throw "Page with id " + facebookPageId + " not found in db!";
			return pageUtils.getEmbeddedWebsiteUrlByPageId(facebookPageId) + '/?url2png=true';
		})
}

var takeScreenshot = function (url,fileName) {
	if(!url) throw new Error('URL missing');
	if(!fileName) throw new Error('File name missing');

	var path = 'screenshots/' + fileName;
	var s3 = new AWS.S3({
		params: {
			Bucket: 'majeeko',
			Key: path
		}
	});

	logger.info('Screenshotting page', url, path)

	var deferred = Q.defer();

	var url2pngOptions = { fullpage:true, thumbnail_max_width: 600, protocol: 'http', unique: moment().toISOString() }
	var screenshotStream = url2png.readURL(url, url2pngOptions).pipe(new stream.PassThrough());
	s3.upload({
		Body: screenshotStream
	})
	.send(function (err,data) {
		logger.info('Screenshot saved at "' + data.Location + '"');
		deferred.resolve(data.Location)
	});

	return deferred.promise;
}

module.exports.takeScreenshot = takeScreenshot;
module.exports.createPreview = createPreview;
module.exports.fetchPreview = fetchPreview;
