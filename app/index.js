var env = process.env.NODE_ENV || 'development',
	config = require('./config'),
	colors = require('colors'),
	express = require('express'),
	logger = require('./utils/logger'), // winston
	cookieParser = require('cookie-parser'),
	bodyParser = require('body-parser'),
	methodOverride = require('method-override'),
	errorHandler = require('errorhandler'),
	session = require('express-session'),
	MongoStore = require('connect-mongo')(session),
	favicon = require('serve-favicon'),
	routes = require('./routes'),

	passport = require('passport'),
	FacebookStrategy = require('passport-facebook').Strategy,

	request = require('request-promise'),

	facebook = require('./facebook'),
	facebookSync = require('./sync/facebook'),
	swig = require('swig'),

	async = require('async'),

	db = require('./db'),

	i18n = require('i18next'),

	_ = require('lodash'),

	mail = require('./utils/mail'),

	moment = require('moment'),

	pageUtils = require('./utils/pages'),

	R = require('ramda'),

	compress = require('compression'),

	csp = require('helmet-csp');
// serialize and deserialize



// global config

var app = express();
app.use(bodyParser.json({
	type: ['json', 'application/csp-report']
}));

// app.use(csp({
// 	// Specify directives as normal
// 	defaultSrc: ["'self'", process.env.HOST],
// 	scriptSrc: ["'self'", process.env.HOST, "'unsafe-inline'", "'unsafe-eval'", "*.google-analytics.com", "*.googleapis.com",
// 		"*.google.com", "*.gstatic.com", "*.facebook.net", "*.cloudflare.com", "*.jsdelivr.net"
// 	],
// 	styleSrc: ["'self'", process.env.HOST, "'unsafe-inline'", "*.cloudflare.com", "*.bootstrapcdn.com", "*.googleapis.com"],
// 	fontSrc: ["'self", process.env.HOST, "*.bootstrapcdn.com", "*.gstatic.com"],
// 	imgSrc: ["'self", process.env.HOST, "'unsafe-inline'", "*.akamaihd.net", "*.gstatic.com",
// 		"*.google.com", "*.google-analytics.com", "*.fbcdn.net", "data:", "*.facebook.com",
// 		"*.googleapis.com"
// 	],
// mediaSrc:["'self'",process.env.HOST,"*.fbcdn.net"],
// 	frameSrc: ["*"],
// 	reportUri: "http://" + process.env.HOST + '/api/report-violation',

// 	// Set to true if you only want browsers to report errors, not block them
// 	reportOnly: false
// }));

i18n.init({
	fallbackLng: 'en',
	localStorageExpirationTime: 7200000,
	fallbackOnEmpty: "",
	debug: false,
	//saveMissing: true,
	ns: {
		namespaces: ['translation', 'template'],
		defaultNs: 'translation'
	}
});
i18n.registerAppHelper(app);

app.use(compress());

app.use(i18n.handle);

app.use(require('./powered-by-majeeko'));



app.set('port', process.env.PORT || 80);
app.set('views', __dirname + '/views');
app.use(require('morgan')('combined', {
	'stream': logger.stream
})); // logger
app.use(cookieParser());

app.use(favicon('public/favicon.ico'));
app.use(bodyParser({
	extended: false
}));
app.use(methodOverride());
// app.get('/fake-cookie', function (req,res) {
//   res.cookie('connect.sid','sdahdjk')
//   res.cookie('connect.sid','sdahdjk',{domain:'.' + config.primary_domain})
//   res.send('done')
// })

app.use(express.static(__dirname + '/../public'));

require('./swig-filters')(swig);

var swigGlobals = {
	ROOT_URL: config.root_url,
	PRIMARY_DOMAIN: config.primary_domain,
	NODE_ENV: env
};

function render_template(pathName, locals, cb) {
	return swig.renderFile(pathName, _.extend(locals, swigGlobals), cb);
}

app.engine('html', render_template);
app.set('view engine', 'html');

swig.setDefaults({
	cache: (process.env.NODE_ENV == 'development') ? false : 'memory',
	varControls: ['{=', '=}']
});

// env config

switch (env) {
	default:
		case 'staging':
		case 'development':
		app.use(errorHandler({
		dumpExceptions: true,
		showStack: true
	}));
	break;
	case 'production':
			app.use(errorHandler());
		break;
}

// routes




app.use(routes)

app.use(function(req, res, next) {
	res.status(404);
	res.render('404');
	return;
})

module.exports = app;
