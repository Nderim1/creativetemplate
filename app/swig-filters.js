var _ = require('lodash'),
	logger = require('./utils/logger'),
	accounting = require('accounting'),
	moment = require('moment'),
	pageUtils = require('./utils/pages');

moment.locale("it_IT");

module.exports = function(swig) {


	swig.setFilter('currencyEUR', function (amount) {
		return "&euro; "+ parseFloat(amount).toFixed(2);
	});


	swig.setFilter('countPaidAccounts', function (accounts) {
		var count = 0;
		for (var i = 0; i < accounts.length; i++) {
			if(accounts[i].paid_on) count++;
		}
		return count;
	});

    swig.setFilter('countFreeAccounts', function (accounts) {
        var count = 0;
        for (var i = 0; i < accounts.length; i++) {
            if(!accounts[i].paid_on) count++;
        }
        return count;
    });

    swig.setFilter('getAccountStatus', function (account) {
        return account.paid_on ? 'synced' : 'pending';
    });

    swig.setFilter('getProfilePicture', function (page) {
        try {
        	var logo_id = page.getConfiguration().graphics.logoId;
        	var logoImage =  _.findWhere(page.profilePictureAlbum.photos, { id: logo_id })
            return logoImage.source;
        } catch(err) {
            logger.info(err);
			return page.get("pageData.picture.data.url", _.get(page, "data.pageData.picture", '/images/brand-standard.jpg'));
        }
    });

	function getBannerPictureObjectProperties(page, banner_id){
		var available_images = _.findWhere(page.coverImageAlbum.photos, { id: banner_id }).image_array;
		available_images = available_images.sort(function(a,b){return a.width < b.width;});
		var out_img = _.first(available_images); // output: highest res banner
		out_img.offset_x = 50;
		out_img.offset_y = 50;

		if (page.get("pageData.cover.cover_id") == banner_id) {
			out_img.offset_x = page.get("pageData.cover.offset_x");
			out_img.offset_y = page.get("pageData.cover.offset_y");
		}

		return out_img;
	}

	function getBannerPictures(page) {
		try{
			if(page.getConfiguration().graphics.bannerId){ // --> bannerID setted
				var banner_ids = page.getConfiguration().graphics.bannerId;
				var banners = banner_ids.split(",");
				var imgs_array = [];
				for(i in banners){
					imgs_array.push(getBannerPictureObjectProperties(page, banners[i]));
				}
				return imgs_array;
			}
		} catch(err) {}
		try{
			if(page.get("pageData.cover.cover_id")){
				return [getBannerPictureObjectProperties(page, page.get("pageData.cover.cover_id"))];
			}
		} catch(err) {}
		try{
			if(page.get("pageData.cover")){
				return [page.get("pageData.cover")];
			}
		} catch(err) {}
		return [];
	}

	swig.setFilter('getBannerStyle', function(page) {
		var banner = _.first(getBannerPictures(page));
		var banner_src = _.get(banner,'source');

		var selectedCoverPhotoId = page.getConfiguration().graphics.bannerId;
		var facebookCoverPhotoId = page.get("pageData.cover.cover_id");

		var print = '';
		if (banner && banner_src) {
			print = ' style="';
			print += "background-image: url('" + banner_src + "'); ";
			print += "background-size: cover; ";
			print += "background-position: " + banner.offset_x + "% " + banner.offset_y + "%; ";
			print += '" ';
		}

		return print;
	});

	swig.setFilter('hasBanner', function (page) {
		return _.has(_.first(getBannerPictures(page)),'source');
	});

	swig.setFilter('getBannerPictures', function (page) {
		return getBannerPictures(page);
	});




/*




	swig.setFilter('getBannerPicture', function (page) {

		return  _.get(getBannerPictures(page)[0],'source','');
	});

	swig.setFilter('getBannerPictures', function (page) {
		return getBannerPictures(page);
	});

	swig.setFilter('getBannerPictureOffset', function (page) {

		try {
			/* if the selected cover image is the actual cover of the FB page,
			 do return the right offset
			 if the selected cover image is not the actual cover of the FB page,
			 do return {'error': 'not_available'}
			 ///
			var selectedCoverPhotoId = page.getConfiguration().graphics.bannerId;
			var facebookCoverPhotoId = page.get("pageData.cover.cover_id");

			if (facebookCoverPhotoId === selectedCoverPhotoId) {
				r = {};
				r["offset_x"] = page.get("pageData.cover.offset_x");
				r["offset_y"] = page.get("pageData.cover.offset_y");
				return r;
			} else {
				r = {};
				r['error'] = "not_available";
				return r;
			}
		} catch (err) {
			logger.error(err);
			return '';
		}
	});
*/

	swig.setFilter('getPostUrl', function (post,kindOfPost) {
		if(kindOfPost == 'event'){
			return "/events/" + post.id;
		}
		if(kindOfPost == 'album'){
			return "/albums/" + post.id;
		}
		if(kindOfPost == 'news'){
			return "/news/" + post.id;
		}
		return "/";
	});

	function getImageObjLowestResolution(images){
		return _.first(_.sortBy(images, function(n){return n.width;}));
	}
	function getImageObjHighestResolution(images){
		return _.last(_.sortBy(images, function(n){return n.width;}));
	}

	function getImageOfSize(images,size){

		if(size == "low"){
			//get the lowest resolution available
			return _.get(getImageObjLowestResolution(images),'source');
		}
		else if(size == "high"){
			//get the highest resolution
			return _.get(getImageObjHighestResolution(images),'source');
		}else{
			//get the highest resolution below the size specified
			var removedImages = _.remove(images,function(e){return e.width > size;});
			if(images.length>0){
				var highestResImg = getImageObjHighestResolution(images);
			}else{
				var highestResImg = getImageObjHighestResolution(removedImages);
			}

			var url = _.get(highestResImg,'source');

			return url;
		}
	}

	swig.setFilter('getImageOfSize', function(images,size){return getImageOfSize(images,size)});

	swig.setFilter('truncateText', function(text, amount){
		if(text.length > amount){
			return text.substring(0,amount) + "...";
		}
		return text;
	});

	swig.setFilter('displayTimePost', function(post) {
		if(!post.created_time){
			return "";
		}
		return moment(post.created_time).format('l');
	});

	swig.setFilter('displayTimeEvent', function(post, length) {

		if(!post.start_time){
			return "";
		}

		var custom_format = "short";
		if (length){
			custom_format = length;
		}

		/* Detecting single or multiple days/times in a day */
		var startTime = moment(post.start_time);
		var endTime = false;
		if (post.end_time){
			endTime = moment(post.end_time);
		}

		if(!endTime){
			// single time, single day
			if(custom_format == "short"){
				return moment(startTime).format('l');
				//return moment(startTime).format('l LT');
			}else{
				return moment(startTime).format('LLLL');
			}
		}
		if(moment(startTime).dayOfYear() === moment(endTime).dayOfYear() && moment(startTime).year() === moment(endTime).year() ){
			// multiple times - same day
			if(custom_format == "short"){
				return moment(startTime).format('l LT') + " - " + moment(endTime).format('LT');
			}else{
				return moment(startTime).format('LLLL') + " - " + moment(endTime).format('LT');
			}
		}else{
			// multiple times - different days
			if(custom_format == "short"){
				return moment(startTime).format('l') + " - " + moment(endTime).format('l');
			}else{
				return moment(startTime).format('LLL') + " - " + moment(endTime).format('LLL');
			}
		}
	});

	var getFormattedAddress = function(location){
		var str = "";
		if("street" in location)
			str += location.street + ", ";
		if("zip" in location)
			str += location.zip + " ";
		if("city" in location)
			str += location.city + " ";
		if("country" in location)
			str += location.country;
		return str;
	};

	swig.setFilter('getLocationAddress', function (post) {
		if(! ("place" in post) || ! ("location" in post.place)){
			return "";
		}
		return getFormattedAddress(post.place.location);
	});

	swig.setFilter('getLocationUrl', function (post) {
		if(! ("place" in post) || ! ("location" in post.place)){
			return "";
		}
		var str = "https://maps.google.com?q=";
		str += getFormattedAddress(post.place.location);

		return encodeURI(str);
	});

	swig.setFilter('nl2br', function (text) {
		return text.replace(/(?:\r\n|\r|\n)/g, '<br>');
	});

	swig.setFilter('generateBootstrapColumn', function (numColumns,breakpoint) {
		breakpoint = breakpoint || 'md';
		switch(numColumns) {
			case 1:
				return 'col-' + breakpoint + '-12';
			case 2:
				return 'col-' + breakpoint + '-6';
			case 3:
			default:
				return 'col-' + breakpoint + '-4';
			case 4:
				return 'col-' + breakpoint + '-3';
			case 5:
			case 6:
				return 'col-' + breakpoint + '-2';
		}
		try {
			return post.attachments.data[0].title || post.attachments.data[0].subattachments.data[0].title;
		} catch (e) {
			logger.error(e);
			return '';
		}
	});

	swig.setFilter('currency', function (value, currency) {
		return accounting.formatNumber(value, {
			decimal : ",",
			thousand: ".",
			precision : 2
		});
	});

	swig.setFilter('isString', function(value) {
		if(value.constructor == String)
			return true;
		else
			return false
	});

	swig.setFilter('getItemSlug', function(value) {
		return value.slug || value.id;
	});

	swig.setFilter('localDate', function(date,date_format) {

		return moment(date).format(date_format);
	});

	swig.setFilter('renderHidePostIcon', function(page) {
		if(page.renderType === "preview" && !page.url2png) {
			return true;
		}
		return false;
	});

	swig.setFilter('videoType',function(post){
		if(/soundcloud/gi.test(post.caption) && post.link) return 'soundcloud';
		else if(/youtube/gi.test(post.caption) && post.source) return 'youtube';
		else if(/vimeo/gi.test(post.caption) && post.source) return 'vimeo';
		else if(post.source) return 'no_provider';
		else return 'no_video';
	});

	swig.setFilter('getSectionTitle',function(page, sectionId){
		var elements = page['configuration']['settings']['menu_all'];
		for(var i = 0; i < elements.length; i++){
			var menuElement = elements[i];
			if(menuElement.id == sectionId){
				return menuElement.label;
			}
		}
		return "";
	});

	swig.setFilter('elementsGridColumns',function(value, print){

		var isHome = false;
		if(value[0]){
			isHome = value[0];
		}

		var layoutColumns = value[1] || 2;
		var kindOfPost = value[2] || 'news';
		var minimumColumns = 3;
		if(value[3] >=1){
			minimumColumns = value[3];
		}

		var out = {};

		out.colSmImg = 12;
		out.colMdImg = 12;
		out.colSmContent = 12;
		out.colMdContent = 12;

		if (kindOfPost == 'event' || kindOfPost == 'news'){
			if(isHome){
				out.colSmImg = 6;
				out.colSmContent = 6;
				if(layoutColumns >= minimumColumns){
					out.colMdImg = 12;
					out.colMdContent = 12;
				}else{
					out.colMdImg = 6;
					out.colMdContent = 6;
				}
			}else{
				out.colSmImg = 7;
				out.colSmContent = 5;
				if(layoutColumns >= minimumColumns){
					out.colMdImg = 12;
					out.colMdContent = 12;
				}else{
					out.colMdImg = 7;
					out.colMdContent = 5;
				}
			}
		}

		if (kindOfPost == 'album'){
			out.colSmImg = 12;
			out.colMdImg = 12;
			out.colSmContent = 12;
			out.colMdContent = 12;
		}

		if(print && print == "img"){
			return "col-sm-" + out.colSmImg + " col-md-" + out.colMdImg;
		}
		if(print && print == "content"){
			return "col-sm-" + out.colSmContent + " col-md-" + out.colMdContent;
		}

		return out;
	});


	swig.setFilter('juxtaposeClass',function(value){
		var isHome = false;
		if(value[0]){
			isHome = value[0];
		}

		var layoutColumns = value[1] || 2;
		var kindOfPost = value[2] || 'news';
		var minimumColumns = 3;
		if(value[3] >=1){
			minimumColumns = value[3];
		}

		var juxtaposedSm = true;
		var juxtaposedMd = true;

		if (kindOfPost == 'event' || kindOfPost == 'news'){
			if(layoutColumns >= minimumColumns){
				juxtaposedSm = false;
				juxtaposedMd = false;
			}
		}else{
			juxtaposedSm = false;
			juxtaposedMd = false;
		}

		out = "";

		if(juxtaposedSm){
			out += "juxtaposed-sm-true ";
		}else{
			out += "juxtaposed-sm-false ";
		}
		if(juxtaposedMd){
			out += "juxtaposed-md-true ";
		}else{
			out += "juxtaposed-md-false ";
		}

		return out;

	});



	swig.setFilter('getColor',function(page, kind, raw){
		if(!kind){
			kind = 'titles';
		}
		if(typeof raw == "undefined"){
			raw = false;
		}

		var color = page.configuration.graphics.colors[kind];

		if(typeof color == "undefined"){
			color = "#ffffff";
		}

		if(raw){
			return color.replace("#","");
		}

		return color;
	});

	swig.setFilter('getHours',function(page, type){
		// type: 'now-status' returns "open" if now is open
		// type: 'today-hours' return a string like "10:30 - 12:30, 18:50 - 21:20"
		// type: 'week-hours' return a weekly status
		var hours, k;

		var dayNumber = {'mon':1,'tue':2,'wed':3,'thu':4,'fri':5,'sat':6,'sun':0};
		var numberDay = {1: 'mon', 2: 'tue', 3: 'wed', 4: 'thu', 5: 'fri', 6: 'sat', 7: 'sun'};


		if(_.has(page,"hours")){
			hours = page.hours;
		}else{
			return false;
		}

		if(!_.has(hours, "mon")){
			return false;
		}

		if(typeof type === "undefined" || ! type){
			return true;
		}

		else if(type == "now-status"){
			// returning "open" if it is open now, "closed" otherwise
			for(k in hours){ // looping all the days
				if(dayNumber[k] == moment().format('d')){ // k is the actual day
					// i.e. k = "mon"

					if(	hours[k].length > 1  && hours[k][0] &&
						moment().isBetween( 	moment(hours[k][0][1], "HH:mm"),
												moment(hours[k][1][1], "HH:mm") )
						||
						hours[k].length > 2  && hours[k][2] &&
						moment().isBetween( 	moment(hours[k][2][1], "HH:mm"),
												moment(hours[k][3][1], "HH:mm") ) ){
						return "open";
					}
					return "closed";
				}
			}
			return "closed";
		}

		else if(type == "today-hours"){
			// return a string to display, containing the hours for the actual day
			for(k in hours) {
				if (dayNumber[k] == moment().format('d')) {
					var displayHour = "";
					if(	hours[k].length > 1  && hours[k][0] && hours[k][1]){
						displayHour += moment(hours[k][0][1], "HH:mm").format("LT");
						displayHour += " - ";
						displayHour += moment(hours[k][1][1], "HH:mm").format("LT");
					}
					if(	hours[k].length > 2  && hours[k][2] && hours[k][3]){
						displayHour += ", ";
						displayHour += moment(hours[k][2][1], "HH:mm").format("LT");
						displayHour += " - ";
						displayHour += moment(hours[k][3][1], "HH:mm").format("LT");
					}
					return displayHour;
				}
			}
			return "";
		}

		else if (type == "week-hours") {

			var dateGroups = [];

			dateGroups.push({ // init dateGroups
				'start_day': false,
				'end_day': false,
				'open_1': true,
				'close_1': true,
				'open_2': true,
				'close_2': true
			});

			for (var i in numberDay) {
				k = numberDay[i];
				var latestDateGroup = _.last(dateGroups);
				if (_.get(hours[k], "[0][1]", false) != latestDateGroup['open_1'] ||
					_.get(hours[k], "[1][1]", false) != latestDateGroup['close_1'] ||
					_.get(hours[k], "[2][1]", false) != latestDateGroup['open_2'] ||
					_.get(hours[k], "[3][1]", false) != latestDateGroup['close_2']) {

					var hours_label = "";

					if(_.get(hours[k], "[0][1]", false) ){
						hours_label += moment(_.get(hours[k], "[0][1]", false),"HH:mm").format("LT");
						hours_label += " - ";
						hours_label += moment(_.get(hours[k], "[1][1]", false),"HH:mm").format("LT");
					}
					if(_.get(hours[k], "[2][1]", false) ){
						hours_label += ", ";
						hours_label += moment(_.get(hours[k], "[2][1]", false),"HH:mm").format("LT");
						hours_label += " - ";
						hours_label += moment(_.get(hours[k], "[3][1]", false),"HH:mm").format("LT");
					}

					dateGroups.push({
						'start_day': k,
						'end_day': k,
						'open_1': _.get(hours[k], "[0][1]", false),
						'close_1': _.get(hours[k], "[1][1]", false),
						'open_2': _.get(hours[k], "[2][1]", false),
						'close_2': _.get(hours[k], "[3][1]", false),
						//Labels
						'start_day_label': moment(i - 1, "e").format("dddd"),
						'end_day_label': moment(i -1, "e").format("dddd"),
						'hours_label': hours_label
					});
				} else {
					// creating new dateGroup
					latestDateGroup['end_day'] = k;
					latestDateGroup['end_day_label'] = moment(i - 1, "e").format("dddd");
				}
			}
			return _.filter(dateGroups,function(e){
				return e['start_day'] && (e['open_1'] || e['open_2']);
			});
		}
		return false;
	});

	/* 				Payment url 				*/
	swig.setFilter('getApiUrl',function(params){
		if(params.action == "init"){
			return "/purchase/orders/";
		}
		return "/purchase/orders/";
	});


	swig.setFilter('getGeneratedWebsiteDomain', pageUtils.getGeneratedWebsiteDomain);

	swig.setFilter('getGeneratedWebsiteUrl', pageUtils.getGeneratedWebsiteUrl);

	swig.setFilter('getEmbeddedWebsiteUrl', pageUtils.getEmbeddedWebsiteUrl);

};
