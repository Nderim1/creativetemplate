module.exports = {
	"settings": {
		"menu": [{
			"id": "news",
			"real_name": "News",
			"active": true,
			"label": "News"
		}, {
			"id": "events",
			"real_name": "Eventi",
			"active": true,
			"label": "Eventi"
		}, {
			"id": "albums",
			"real_name": "Gallery",
			"active": true,
			"label": "Gallery"
		}, {
			"id": "contacts",
			"real_name": "Contatti",
			"active": true,
			"label": "Contatti"
		}, {
			"id": "extra-page-1",
			"real_name": "Pagina extra 1",
			"active": false,
			"label": "Pagina extra 1"
		}, {
			"id": "extra-page-2",
			"real_name": "Pagina extra 2",
			"active": false,
			"label": "Pagina extra 2"
		}]
	},
	"graphics": {
		"template": "fashion",
		"category": "local bisness",
		"columns": {
			"news": 3,
			"events": 1,
			"albums": 4
		},
		"colors": {
			"outer_background": "#333",
			"inner_background": "#FFF",
			"titles": "#ccc",
			"texts": "#666"
		},
		"mjk": {
			"mjk_text": 		"#CCC",
			"mjk_nav_bg":       "#CCC",
			"mjk_link_text":    "#CCC",
			"mjk_nav_text":     "#CCC",
			"mjk_well_bg":      "#CCC",
			"mjk_jumbotron_bg": "#CCC",
			"mjk_bg":           "#CCC"
		}
	},

	"contents": {
		"extra_pages": []
	}
}