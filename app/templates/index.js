var templates = [
		{ label: 'Harry', value: 'innovation'},
		{ label: 'Circe', value: 'parallelo'},
		{ label: 'Merlin', value: 'opus'},
		{ label: 'Salazar', value: 'salazar'},
		{ label: 'HelloBrand!', value: 'hellobrand'},
		{ label: 'Temple', value: 'temple'},
		{ label: 'Portfolio', value: 'portfolio'},
		{ label: 'Simposio', value: 'simposio'},
		{ label: 'NewTemplate', value: 'newTemplate'},
		{ label: 'LessTemplate', value: 'lessTemplate'},
		{ label: 'Fashion', value: 'fashion'},
		{ label: 'Creative', value: 'creative'}
];

module.exports = templates;
