module.exports = {
	"settings": {
		"menu": [{
			"id": "news",
			"real_name": "News",
			"active": true,
			"label": "News"
		}, {
			"id": "events",
			"real_name": "Eventi",
			"active": true,
			"label": "Eventi"
		}, {
			"id": "albums",
			"real_name": "Gallery",
			"active": true,
			"label": "Gallery"
		}, {
			"id": "contacts",
			"real_name": "Contatti",
			"active": true,
			"label": "Contatti"
		}, {
			"id": "extra-page-1",
			"real_name": "Pagina extra 1",
			"active": false,
			"label": "Pagina extra 1",
	        "type": "text"
		}, {
			"id": "extra-page-2",
			"real_name": "Pagina extra 2",
			"active": false,
			"label": "Pagina extra 2",
	        "type": "text"
		}]
	},
	"graphics": {
		"template": "innovation",
		"category": "local bisness",
		"columns": {
			"news": 3,
			"events": 1,
			"albums": 4
		},
		"colors": {
			"outer_background": "#333",
			"inner_background": "#FFF",
			"titles": "#ccc",
			"texts": "#666"
		},
		"fonts": "innovation"
	},
	"contents": {
		"extra_pages": []
	}
}
