var websites = [
	{
		id: "284351312453",
		page_name: "otticaincentro.it",
		category: "Shopping/Retail",
		category_id: "2521",
		picture: "/images/example-websites/284351312453.min.jpg"
	},
	{
		id: "149817485170564",
		page_name: "Apollo Bar Ristorante",
		category: "Restaurant/cafe",
		category_id: "1900",
		picture: "/images/example-websites/149817485170564.min.jpg"
	},
	{
		id: "167123673471149",
		page_name: "Oh My Dog Feletto Umberto",
		category: "Pet services",
		category_id: "2516",
		picture: "/images/example-websites/167123673471149.min.jpg"
	},
	{
		id: "294128740754220",
		page_name: "Trattoria del Sole",
		category: "Restaurant/cafe",
		category_id: "1900",
		picture: "/images/example-websites/294128740754220.min.jpg"
	},
	{
		id: "298264243625463",
		page_name: "Vanillà Gelateria",
		category: "Food/grocery",
		category_id: "2513",
		picture: "/images/example-websites/298264243625463.min.jpg"
	},
	{
		id: "340072379354853",
		page_name: "HelloBrand",
		category: "Consulting/business services",
		category_id: "2248",
		picture: "/images/example-websites/340072379354853.min.jpg"
	},
	{
		id: "409610329199906",
		page_name: "Las Brisas Restaurant Ibiza",
		category: "Restaurant/cafe",
		category_id: "1900",
		picture: "/images/example-websites/409610329199906.min.jpg"
	},
	{
		id: "410058729061867",
		page_name: "YogaMarga",
		category: "Health/beauty",
		category_id: "2214",
		picture: "/images/example-websites/410058729061867.min.jpg"
	},
	{
		id: "537250046396306",
		page_name: "GARBO Italy",
		category: "Food/beverages",
		category_id: "2252",
		picture: "/images/example-websites/537250046396306.min.jpg"
	},
	{
		id: "719978998039600",
		page_name: "Adile",
		category: "Home decor",
		category_id: "2218",
		picture: "/images/example-websites/719978998039600.min.jpg"
	},
	{
		id: "853638027986263",
		page_name: "Simone lo stile del fiore",
		category: "Event planning/event services",
		category_id: "2511",
		picture: "/images/example-websites/853638027986263.min.jpg"
	},
	{
		id: "52497915917",
		page_name: "Casale Marretti",
		category: "Restaurant/cafe",
		category_id: "1900",
		picture: "/images/example-websites/52497915917.min.jpg"
	},
	{
		id: "178658181811",
		page_name: "Nicola Malaguti Jazz Photographer",
		category: "Consulting/business services",
		category_id: "2248",
		picture: "/images/example-websites/178658181811.min.jpg"
	},
	{
		id: "237645282991097",
		page_name: "Bartolini Studio Immobiliare",
		category: "Home decor",
		category_id: "2218",
		picture: "/images/example-websites/237645282991097.min.jpg"
	},
	{
		id: "388316877850895",
		page_name: "Armonicorpo Studio Massaggi",
		category: "Health/beauty",
		category_id: "2214",
		picture: "/images/example-websites/388316877850895.min.jpg"
	},
	{
		id: "412284632163205",
		page_name: "I Sogni di Cocò",
		category: "Shopping/Retail",
		category_id: "2521",
		picture: "/images/example-websites/412284632163205.min.jpg"
	},
	{
		id: "627284400741016",
		page_name: "kia tappeti",
		category: "Shopping/Retail",
		category_id: "2521",
		picture: "/images/example-websites/627284400741016.min.jpg"
	},
	{
		id: "798199303581408",
		page_name: "JoinT Studio",
		category: "Consulting/business services",
		category_id: "2248",
		picture: "/images/example-websites/798199303581408.min.jpg"
	},
	{
		id: "1516998775222545",
		page_name: "BeMySpot",
		category: "Health/beauty",
		category_id: "2214",
		picture: "/images/example-websites/1516998775222545.min.jpg"
	},
	{
		id: "1562243574022165",
		page_name: "VERY n'ICE - Idee Golose",
		category: "",
		category_id: "2511",
		picture: "/images/example-websites/1562243574022165.min.jpg"
	},
	{
		id: "1602232839997932",
		page_name: "Ottica Evo",
		ccategory: "Shopping/Retail",
		category_id: "2521",
		picture: "/images/example-websites/1602232839997932.min.jpg"
	}
];

module.exports = websites;
