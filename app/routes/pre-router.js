var express = require('express');
var R = require('ramda')
  , _ = require('lodash')
  , fs = require('fs')
  , passport = require('passport')
  , session = require('express-session')
  , MongoStore = require('connect-mongo')(session)
  , FacebookStrategy = require('passport-facebook').Strategy

  , config = require('../config')
  , db = require('../db')
  , auth = require('../auth')
  , purchase = require('../purchase')
  , logger = require('../utils/logger')
  , commonRoutes = require('../routes/common')
  , captcha = require('../utils/captcha')
  , mailService = require('../utils/mail')
  , Braintree = require('../purchase/braintree')
  , facebook = require('../facebook')
  , postLoginSetup = require('../post-login-setup')

module.exports = function() {
	var router = express.Router();

	passport.serializeUser(function(user, done) {
		done(null, user);
	});
	passport.deserializeUser(function(user, done) {
		done(null, user);
	});

	// config

	passport.use(new FacebookStrategy({
			clientID: config.facebook.clientID,
			clientSecret: config.facebook.clientSecret,
			callbackURL: config.facebook.callbackURL,
			// profileFields: ["id", "birthday","first_name","email", "last_name", "gender", "picture.width(200).height(200)"],
			passReqToCallback: true
		},
		postLoginSetup
		// require('./post-login-setup')
	));
	router.use(session({
		resave: false,
		saveUninitialized: false,
		secret: 'THE_big_MAJEEKO_s3gr3t0',
		cookie: {
			path: '/',
			domain: '.' + config.primary_domain,
			maxAge: 1000 * 60 * 60 * 24 // 24 hours
		},
		store: new MongoStore({
			url: 'mongodb://' + config.mongo_connection
		})
	}));

	var sanitizeSessionCookie = function(req, res, next) {
		if (!req.cookies['cookies_sanitized'] && req.method == 'GET') {
			if (req.session) req.session.destroy();
			if (req.cookies) delete req.cookies['connect.sid'];
			res.cookie('cookies_sanitized', true);
			res.clearCookie('connect.sid', {
				path: '/'
			});
			res.redirect((/\/auth\/facebook\/callback\/*/).test(req.path) ? '/auth/facebook' : req.originalUrl);
			return;
		}
		next();
	};
	router.use(sanitizeSessionCookie);

	// router.use(function(req,res,next) {
	// 	if(req.session.tophost && req.session.tophost.domain && req.session.tophost.token)
	// 		return res.redirect(req.protocol + '://tophost.' + config.primary_domain);
	// 	else
	// 		next();
	// });

	router.use(passport.initialize());
	router.use(passport.session());
	router.get('/auth/facebook',
		function(req, res, next) {
			console.log('/auth/facebook'.toUpperCase().red);
			if (req.query.impersonate) {
				req.session.impersonate = req.query.impersonate;
			}
			next();
		},
		passport.authenticate('facebook', {
			scope: ['email', 'manage_pages']
		}));

	router.get('/auth/facebook/callback',
		passport.authenticate('facebook', {
			failureRedirect: '/auth/failure'
		}),
		function(req, res) {
	  		if(req.user && req.user.id) {
	  			return facebook.checkMajeekoPermissions(req.user)
					.then(function (granted) {
						if (granted) {
							if (req.user.firstLogin) {
								return res.redirect('/manager/welcome');
							} else {
								return res.redirect('/manager');
							}
						} else {
							var _user = req.user;
							req.session.destroy();
							return res.redirect('/manager/grant-permission/?id=' + _user.id);
						}
					})
					.catch(function (err) {
						logger.error('Error during user login', {
							error:err,
							stack:err.stack,
							filename: __filename
						});
					});
	  		}
		});

	router.get('/auth/failure', function(req, res) {
		var redirectUrl = !req.session.tophost ? '/' : config.tophost_redirect
		req.session.destroy()
		res.redirect(redirectUrl)
	})

	router.get('/logout', function(req, res) {
		req.logout();
		req.session.destroy()
		res.redirect('/');
	});

	// router.use(function (req,res,next) {
	// 	// if(req.hostname == process.env.HOST) {
	// 	// 	return res.redirect(req.protocol + '://www.' + process.env.HOST + req.originalUrl)
	// 	// }
	// 	if(req.hostname.substr(0,4) == 'www.') {
	// 		return res.redirect(req.protocol + '://' + process.env.HOST + req.originalUrl)
	// 	}
	// 	return next()
	// })

	return router;
}
