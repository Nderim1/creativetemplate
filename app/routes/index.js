var express = require('express'),
    router = express.Router(),
    mainSiteRouter = require('./pre-router')(),
    tophostRouter = require('./pre-router')(),
	generatedSiteRouter = require('../generated-websites/routes'),
	_ = require('lodash'),
  	config = require('../config'),
  	logger = require('../utils/logger');

require('../main-website/routes')(mainSiteRouter);
require('../api/routes')(mainSiteRouter);
require('../tophost/routes')(tophostRouter);
require('../api/routes')(tophostRouter);

var isSubdomain = function isSubdomain(hostname) {
	return (new RegExp(process.env.HOST)).test(hostname);
};

function isTophost(req) {
	return req.subdomains && req.subdomains[0] == 'tophost';
}

function isMain(req) {
	return _.contains(['www.' + config.primary_domain, config.primary_domain],req.hostname);
}

router.use(function(req, res, next) {
	// logger.info('main router')
    if(isMain(req)) {
        mainSiteRouter.handle(req, res, next);
    } else if(isTophost(req)) {
        tophostRouter.handle(req, res, next);
    } else if(!isSubdomain(req.hostname) || req.subdomains && (req.subdomains[0] == 'website' || req.subdomains[0] == 'embedded')) {
        generatedSiteRouter.handle(req, res, next);
    } else {
    	return res.render(404);
    }
});

module.exports = router;
