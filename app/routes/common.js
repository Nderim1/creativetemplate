var R = require('ramda'),
  _ = require('lodash'),
  fs = require('fs'),
  fonts = require('../utils/fonts'),
  config = require('../config'),
  db = require('../db'),
  logger = require('../utils/logger'),
  pageUtils = require('../utils/pages'),
  siteController = require('../generated-websites/page-utils'),
  templates = require('../templates'),
  pageNames = {
    	'news': 'News',
    	'events': 'Eventi',
    	'albums': 'Album',
    	'contacts': 'Contatti',
    	'extra-page-1': 'Pagina Extra 1',
    	'extra-page-2': 'Pagina Extra 2'
	};

var displayEditor = function (req,res,page,args) {
	args = args || {};
	db.getUserByFacebookId(req.user.id)
	    .then(function(user) {
			var configuration = _.omit(page.configuration, ['_css','__css', 'editor']);
			var username;
			if(_.has(page,'data.username') && _.get(page,'data.username') != req.params.pageName) {
				return res.redirect('/editor/' + _.get(page,'data.username'));
			}
			var pageData = {
	        	configuration: configuration,
	          	id: page.data.facebook_id,
	          	username: req.params.pageName,
	          	num_posts: page.posts ? page.posts.length : 0,
	          	num_events: page.events ? page.events.length : 0,
	          	paid: !!page.data.paid_on,
	          	isPro: !!page.data.isPro,
	          	pageName: _.get(page, "data.pageData.name"),
	          	mail: configuration.settings.mail || _.head(page.data.emails)
	        };
	        if(page.albums) {
	        	pageData.albums = page.albums
	        		.filter(function(album) {
	        			return album.type == 'normal' && album.photos;
	        		})
	        		.map(function(album, index) {
		        		return { label: album.title, value: album.id, description: album.description };
		        	});

	        	try {
		        	pageData.profile_pictures = page.profilePictureAlbum.photos.map(function (photo) {
		        		return {
		        			id: photo.id,
		        			src: photo.source
		        		};
		        	});
	        	} catch(err) {
	        		pageData.profile_pictures = [];
	        	}
	        	try{
	        		pageData.configuration.settings.phone = page.configuration.settings.phone || page.pageData.phone;
	        	} catch(err){
	        		pageData.configuration.settings.phone = '';
	        	}
	        	try {
		        	pageData.cover_pictures = page.coverImageAlbum.photos.map(function (photo) {
		        		return {
		        			id: photo.id,
		        			src: photo.source
		        		};
		        	});
	        	} catch(err) {
	        		pageData.cover_pictures = [];
	        	}
	        }
			if(page.get(pageData, "cover")){
				pageData.cover = page.get(pageData, "cover");
			}
		    pageData.num_albums = pageData.albums ? pageData.albums.length : 0;

		    _.forEach(pageData.configuration.settings.menu, function (element, key) {
		    	_.set(element, "real_name", _.get(pageNames, element.id));
		    });
		    _.forEach(pageData.configuration.contents.extra_pages, function (element, key) {
		    	_.set(element, "real_name", _.get(pageNames, element.id));
		    });
		    pageData.hiddenData = {};
		    if(page.posts) {
			    pageData.hiddenData.posts = page.posts.filter(function(post) {
			    	return _.get(post, "majeeko.hidden");
			    });
			}
			if(page.albums) {
			   	pageData.hiddenData.albums = page.albums.filter(function(album) {
			    	return _.get(album, "majeeko.hidden");
			    });
		   	}
		   	if(page.events) {
			    pageData.hiddenData.events = page.events.filter(function(event) {
			    	return _.get(event, "majeeko.hidden");
			    });
			}
		    res.render('editor', {
		    	title: 'Majeeko | Editor (' + pageData.username + ')',
		    	user: _.extend({},user,{facebook:_.omit(user.facebook, ['access_token'])}),
		    	pageData: pageData,
		    	fonts: fonts.getAllFontsWithoutFallback() ,
		    	allFonts: fonts.getAllFonts(),
		    	is_tophost: args.is_tophost,
		    	templates: templates,
		    	embeddedWebsiteURL: pageUtils.getEmbeddedWebsiteUrlByPageId(pageData.id)
		    });
	    }).catch(function(error) {
	    	logger.error(error);
	    });
};

module.exports.editor = function (req,res) {
	// return res.send(req.session)
	req.site = {
		is_subdomain: true,
		renderType: "preview",
		editor: true
	};

	if(!req.isAuthenticated()) return res.redirect(pageUtils.getGeneratedWebsiteUrlByPageUsername(req.params.pageName, req.protocol))
	siteController.prepareSite(req, res)
		.then(function(site) {
			if(site) {
				site.data.configuration = _.omit(site.get("configuration.editor.preview", site.get("configuration")), ["_css", "__css"]);
				db.checkUserOwnsPage(req.user.id, site.get("facebook_id"))
					.then(function (owns) {
						if(owns) {
							displayEditor(req, res, site,{is_tophost:_.has(req, 'session.tophost.currentPage')});
						} else {
							throw new Error("403 Forbidden");
						}
					})
			} else {
				throw new Error('Site not found!');
				res.render(404,'404');
			}
	})
	.catch(function (err) {
		logger.error(err);
	})
};
