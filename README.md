# Majeeko

A website builder which takes contents from your Facebook pages and allows a small business to create their website within a few clicks ;)

### Translation
Translations are made with [i18next](http://i18next.com/).

#### Setting translation strings
All the texts are included in `translation.json` (for the main website) and in `template.json` (for the generated websites). These files are placed in the `locales/dev` folder: `dev` is the fallback language folder (Italian); English translations are placed in `locales/en`.

All the texts is hierarchically organized using a flexible structure. Every text string has its own label. Always check that the resulting file is a valid JSON (don't forget commas).

Texts strings can contain *placeholders* like this: 

```
 "hello": "Hello, __user_name__!",
```

In the template `__user_name__` will be replaced with the right string.

#### Translation interface
The actual translation of all the strings is performed online using [webtranslateit](https://webtranslateit.com/en/projects/12541-Majeeko-Website/files) (login account in GoogleDrive). Here we upload the json files that have to be translated. Than we download the resulting translation JSON file and place it into `locales/en`.

#### Updating translations

All the updates done in the fallback language (Italian) should be done directly in the json files in `locales/dev`. These updates should be regularly uploaded to webtranslated. 

**!! Attention !!** Always be careful when uploading an updated version of a JSON file to the site: there could be some changes to the fallback language (Italian) done online (by a translator) that could be overwritten. In case, please check the *diff*erences between the online version and the local updated one.

**English translations have to be done exclusively online**. All the updates can be downloaded locally in order to overwrite the preceding local file.

#### Printing translated text
In the templates, translated texts can be printed like this:

```
{= t('translation:editor.you_are_editing') =}
```


where `translation` refers to the JSON file where the translation is placed - the remaining part refers to the path in the JSON tree.

##### Handling placeholders
The content of placeholders is injected like this:

```
{= t( 'translation:manager.user.hello', { 'user_name': "Mr. Smith"}) =}
```


### Sessions

Sessions are stored in **MongoDB** using the [connect-mongo](https://github.com/motdotla/dotenv) package.
Should eventually switch to **Redis**?

### Templates

Using [swig](http://paularmstrong.github.io/swig/) for both the main website and generated templates. Reasons:

- fast
- automatic partials
- caching

### Generated pages usernames

All usernames from facebook are stripped of ' ', '.', '!', '?', which are replaced with '-'.

### Purchase

The service will offer 2 purchase methods:

- PayPal (need to setup and test API) **TODO**
- bank money transfer (need to setup purchase workflow) **TODO**

After purchase domain registration and DNS routing will be handled via Amazon Route53 (workflow draft already set up).

### Sync

Sync works as soon as requested. Will be resource intensive on the server(s). Need to set up queueing strategy ( could be [Kue](http://automattic.github.io/kue/) ).

**TODO** Need to setup sync status/progress feedback.

### Manager state update

Manager updates work by polling for pages every 5s via [angular-poller](http://emmaguo.github.io/angular-poller/). **Could be resource consuming. Need to think a strategy to avoid to much load on server.**

For the future use of websockets ( [*socket.io*](http://socket.io/) ) could be a better option.

### Logging [todo]

Need to setup logging for server activities (syncs, template renders, ...).

Considering using [winston](https://github.com/winstonjs/winston).

Resources:

- [Advanced logging with NodeJs](http://tostring.it/2014/06/23/advanced-logging-with-nodejs/)
- [Express-winston](http://bithavoc.io/express-winston/)
- [Comparing Winston and Bunyan Node.js Logging (StrongLoop)](https://strongloop.com/strongblog/compare-node-js-logging-winston-bunyan/)

### Editor

When user saves editor configuration less files ( *app/templates/{template_name}/styles/style.less* ) are recompiled and stored in the *_css* variable in MongoDB.

##### Idea:

could enable as many color fields as are the props in template's *default-configuration.js*, each one translated into a less variable.

### Generated website preview [todo]

Set up homepage search box and automatic website preview. The system requests screenshot to *url2png* and stores it in Amazon S3.

- url2png
- angular-typeahead
	
	- [https://angular-ui.github.io/bootstrap/](https://angular-ui.github.io/bootstrap/)
	- [https://github.com/angular-ui/bootstrap/blob/master/template/typeahead/typeahead-popup.html](https://github.com/angular-ui/bootstrap/blob/master/template/typeahead/typeahead-popup.html)

Facebook query is
	
	https://graph.facebook.com/v2.3/search?q={search_string}&type=page

Read from S3 is enabled for * on bucket/object *majeeko/screenshots* (required generation of a policy file). Also CORS are enabled from * on S3 bucket.

References:

- [https://developers.facebook.com/docs/graph-api/using-graph-api/v2.3](https://developers.facebook.com/docs/graph-api/using-graph-api/v2.3)
- [sfTypeahead: A Twitter Typeahead directive](https://github.com/Siyfion/angular-typeahead)
- [SyackOverflow: How to make all Objects in AWS S3 bucket public by default](http://stackoverflow.com/a/23102551/1440302)
- [AWS Policy Generator](http://awspolicygen.s3.amazonaws.com/policygen.html)
- [Direct to S3 File Uploads in Node.js](https://devcenter.heroku.com/articles/s3-upload-node)

# Setup

- creata Elastic Beanstalk application
- create Key Pairs

#### Change EC2 timezone

Change each instance timezone especially for logging purposes (need to automate it).

	sudo rm /etc/localtime && sudo ln -s /usr/share/zoneinfo/Europe/Rome /etc/localtime

Ref. [http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/set-time.html](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/set-time.html)

# Development

### Docker

*majeeko-test.com* should point (via */etc/hosts* or DNS) to Docker host machine (eg. `echo $DOCKER_HOST`).

Docker command to start dev session:

	docker run --rm -it -v `pwd`:/usr/src/myapp --link {MONGO_CONTAINER}:mongodb -e MONGO_HOST=mongodb -e MONGO_NAME=synky -p 80:80 -w /usr/src/myapp mettjus/node-dev gulp
	
It assumes your working dir is project root dir (eg. *../../majeeko*, `pwd` is used to determine working dir) and you have a mongodb Docker container named *majeeko_mongodb_1* already running.

It runs a container based on the *mettjus/node-dev* image, which bundles *gulp*, *nodemon* and *node-inspector*.

Container details:

- removes the container after detaching (`--rm`)
- creates a *mongodb* alias inside the container pointing to the mongodb container
- passes in required environment variables
- maps port 80 to 80 on Docker host machine
- sets working dir inside container to */usr/src/myapp*, also mapped to current external working dir

To attach to a running Docker container avoiding transmission of signals (eg. CTRL+C) to the container run:

	docker attach --sig-proxy=false {CONTAINER}

**NOTE**

All stuff stored in the dev container gets wiped once the container stops. To avoid such behaviour don't use `--rm` flag, which will let the container hanging around after stop (retrieve via `docker ps -a`).

### Environment variables

Using [dotenv](https://github.com/motdotla/dotenv) to load environment variables from **.env** file.

**Update**

Supplying environment variables via *Gulp*.

### Auto-restarting Node.js

Using [nodemon](http://nodemon.io/) to automatically restart Node.js on file changes (need to check out [gulp-nodemon](https://github.com/JacksonGariety/gulp-nodemon))

# Deployment 

## Codeship

Include `--skip-ci` in the commit summary to avoid build.

## Elastic Beanstalk

`zip` command for manual deployment (includes *.ebextensions*):

	zip -r -X ../majeeko.zip ./* .ebextensions -x node_modules/**\* .git/**\*

### Node modules deployment

There could be the need for setting up EB to `bower install` after deploy. Need to figure out best stratecgy. In the meantime leaving a snippet:

	// package.json
	"scripts": {
		...
		"postinstall": "node_modules/bower/bin/bower install -F"
	}

Right now *bower_components* are committed in Git.

## Server monitoring

Using following commands to read logs (need to have *ccze* installed for colorization, see below):

### Web App Server

Logs to *stdout*, need to attach to that:

	tail -f /proc/{pid}/fd/1 | ccze

##### New Relic Server

[https://docs.newrelic.com/docs/accounts-partnerships/partnerships/partner-based-installation/amazon-web-services-aws-users#lsm](https://docs.newrelic.com/docs/accounts-partnerships/partnerships/partner-based-installation/amazon-web-services-aws-users#lsm)

### Mongo

Logs only to file:

	tail -f /var/log/mongodb/mongod.log | ccze

##### *ccze* installation:

To enable *ccze* repo modify */etc/yum.repos.d/epel.repo*. Under the section marked [epel], change `enabled=0` to `enabled=1`.

##### MMS

# Modules

##### [merge-defaults](https://www.npmjs.com/package/merge-defaults)

Enable recursive `_.defaults` for Facebook sync operations.

Refs:

- [https://lodash.com/docs#defaults](https://lodash.com/docs#defaults)
- [mikermcneil / _.recursive.js](https://gist.github.com/mikermcneil/3862932)
- [Add deep support to _.defaults. #154](https://github.com/lodash/lodash/issues/154)
- [https://www.npmjs.com/package/underscore.deep](https://www.npmjs.com/package/underscore.deep)
- [http://stackoverflow.com/questions/14843815/recursive-extend-in-underscore-js](http://stackoverflow.com/questions/14843815/recursive-extend-in-underscore-js)

# TODOs

- take a look at [wiredep](https://github.com/taptapship/wiredep) for frontend dependencies concatenation

- decide how to manage dependencies installation and assets compilation (eg: `bower install` or `gulp less`, right now *bower_components*, *node_modules* and */public/css* are committed in Git)

- manage user token expiration

Should manage Facebook API request errors and require new token in case old one has expired.

- ensure new downloaded data satisfies page rendering requirements

Idea: create a Schema validation mechanism based on JSON to specify necessary data; in case necessary data isn't available in update old data should be kept. Example:

    "pageData" : {
        "picture" : {
            "data" : {
                "url" : "$string" # "https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xpf1/v/t1.0-1/p50x50/...=1438194476_1b1499354a313627d2cd536cca1af1da"
            }
        },
        "cover" : {
            "cover_id" : "/[0-9]+/", # "321941698009242"
            "offset_x" : "$number", # 0
            "offset_y" : "$number", # 58
            "source" : "$string", # "https://scontent.xx.fbcdn.net/hphotos-.../10860829_321941698009242_2890869105231331558_o.jpg"
            "id" : "$string", # "321941698009242"
        }
        ...

- better module structure
- find a solution for Facebook image download/caching

AWS ElastiCache? S3?

- create prototype for template customization control interface

Use `<iframe>` live html/css injection via AngularJS.


- implement other templates
- create backend to read/analyze db data

There's need for Fabio (and possibly others) to see what pages have been created, what category they belong to, and other data.