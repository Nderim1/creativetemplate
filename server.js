if(process.env.DEBUG)
	require('dotenv').load();

if(process.env.NODE_ENV == 'production')  {
	console.log('NEW RELIC HERE')
	require('newrelic')
}

var app = require('./app'),
    server = require('http').Server(app);

server.listen(app.get('port'), function() {
    console.log('\nExpress server listening on port ' + app.get('port'));
});