describe("app", function() {
    it("should have tests", function() {
        expect(true).toBe(true);
    });
});

describe("db page creation procedure", function() {
    it("should create a valid username", function() {
        var facebookSync = require('../app/facebook-sync')

        expect(facebookSync.usernameReplaceCharacters('hello,world')).toBe('hello-world')
        expect(facebookSync.usernameReplaceCharacters('hello,,,world')).toBe('hello-world')
        expect(facebookSync.usernameReplaceCharacters('hello,,,  world')).toBe('hello-world')
        expect(facebookSync.usernameReplaceCharacters('hello,,,  --world')).toBe('hello-world')
        expect(facebookSync.usernameReplaceCharacters('Hello,,,  --world')).toBe('hello-world')
    })
})

describe("configuration", function() {
    it("should return well formed mongo db connection string", function() {
        var config = require('../app/config')

        expect(config.mongo_connection).toMatch(/\w+\/\w+/)
    })
})