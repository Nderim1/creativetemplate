db.pages.find().forEach(function (page) { 
    //print(page)
    var template
    switch(page.configuration.graphics.template) {

        case 'hiroshima':
        case 'portfolio':
            template = 'portfolio'
            break;
        case 'nagasaki':
        case 'temple':
            template = 'temple'
            break;
        case 'tetris':
        case 'simposio':
            template = 'simposio'
            break;
        case 'pippo':
        case 'fashion_magazine':
            template = 'fashion_magazine'
            break;
        default:
            template = 'hellobrand'
    }
    db.pages.update({_id:page._id},{$set:{'configuration.graphics.template':template}})
    //print(page.configuration.graphics.template)
})