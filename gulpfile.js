require('dotenv').load();
var gulp = require('gulp')
  , nodemon = require('gulp-nodemon')
  , less = require('gulp-less')
  , path = require('path')
  , plumber = require('gulp-plumber')
  , rename = require('gulp-rename')
  , autoprefixer = require('gulp-autoprefixer')
  , concat = require('gulp-concat')
  , ngAnnotate = require('gulp-ng-annotate')
  , uglify = require('gulp-uglify')
  , useref = require('gulp-useref')
  , cssnano = require('gulp-cssnano')

/**
 * Run nodemon.
 */

gulp.task('nodemon', function(cb) {
	runNodemon(cb)
})

gulp.task('nodemon-debug', function(cb) {
	runNodemon(cb,true)
})

function runNodemon(cb,debug) {
	debug = debug || false;
	var called = false;
	return nodemon({
		// ext: 'html js',
		script: 'server.js',
		nodeArgs: debug ? '--debug' : '',
		watch: [
			'server.js',
			'app/**/*.*',
			'locales/**/*.*'
		],
		// delay: 1000,
		ignore: [
			// 'public/**/*.*',
			'app/views/**/*.*',
			'app/templates/**/*.*'
		],
		env: {
			'PORT': 80
		}
	}).
	on('start', function() {
		if (!called) {
			called = true;
			cb();
		}
	})
	.on('restart', function(files) {
		console.log('Nodemon restarted after these files changed',files)
	})
}

/**
 * Compile less files.
 */

gulp.task('less', function () {
  return gulp.src('./assets/less/styles.less')
  	.pipe(plumber())
    .pipe(less({
      // paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(autoprefixer({
        browsers: ['> 1%'],
        cascade: false
    }))
    .pipe(gulp.dest('./public/css'));
});

gulp.task('less-order', function () {
	return gulp.src('./assets/less/order-page.less')
		.pipe(plumber())
		.pipe(less())
		.pipe(autoprefixer({
			browsers: ['> 1%'],
			cascade: false
		}))
		.pipe(gulp.dest('./public/css'));
});

gulp.task('less-editor', function () {
	return gulp.src('./assets/less/editor.less')
		.pipe(plumber())
		.pipe(less({
			paths: [ path.join(__dirname, 'less', 'includes') ]
		}))
		.pipe(autoprefixer({
			browsers: ['> 1%'],
			cascade: false
		}))
		.pipe(gulp.dest('./public/css'));
});

gulp.task('less-landing', function () {
	return gulp.src('./public/landing/less/*.less')
		.pipe(plumber())
		.pipe(less())
		.pipe(autoprefixer({
			browsers: ['> 1%'],
			cascade: false
		}))
		.pipe(cssnano({compatibility: 'ie8'}))
		.pipe(gulp.dest('./public/landing/css/'));
});

/**
 * Compile less files for templates.
 */

gulp.task('template-less', function () {
  return gulp.src('./app/templates/*/styles/style.less')
  	.pipe(plumber())
    .pipe(less({
      paths: [ path.join(__dirname, 'assets/bootstrap-less') ]
    }))
    .pipe(autoprefixer({
        browsers: ['> 1%'],
        cascade: false
    }))
    .pipe(rename(function(path) {
    	path.basename = path.dirname.split('/')[0]
    	path.dirname = ''
    }))
    .pipe(gulp.dest('./public/css/templates'));
});


/**
 * Combine js files for main website
 */
gulp.task('build-js', function() {
	var assets = useref.assets({
		searchPath: 'public'
	});
	return gulp.src('app/views/partials/scripts/main.html')
		.pipe(plumber())
		.pipe(assets)
		.pipe(ngAnnotate())
		.pipe(uglify({mangle:false}))
		.pipe(gulp.dest('./public/js/concat/'));
});

/**
 * Combine js files for generated websites
 */
gulp.task('concat-js-templates', function() {
	return gulp.src([
		'./public/components/lodash/lodash.min.js',
		'./public/components/rxjs/dist/rx.all.min.js',
		'./public/components/swiper/dist/js/swiper.jquery.min.js',
		'./public/components/angular-rx/dist/rx.angular.js',
		'./public/components/angular-cookies/angular-cookies.min.js',
		'./public/components/angular-bootstrap/ui-bootstrap.min.js',
		'./public/components/angular-bootstrap/ui-bootstrap-tpls.min.js',
		'./public/components/angular-clamp/ng-clamp.js',
		'./public/components/ramda/dist/ramda.min.js',
		'./public/components/bootstrap/dist/js/bootstrap.min.js',
		'./public/components/moment/min/moment.min.js',
		'./public/components/moment/locale/it.js'
	])
		.pipe(concat('template.js'))
		.pipe(ngAnnotate())
		.pipe(uglify({mangle:false}))
		.pipe(gulp.dest('./public/js/concat/'));
});

gulp.task('build',['less', 'less-editor', 'less-landing', 'template-less', 'build-js', 'concat-js-templates'])

/**
 * Watch for changes in less/js files.
 */

gulp.task('watch',['less', 'less-editor', 'template-less'], function () {
	//gulp.watch(['public/js/**/*.js','!./public/js/concat/*.js'], ['concat-js-templates','concat-js-main-homepage']);
	gulp.watch(['./assets/**/*.less', './app/templates/*/styles/*'], ['less', 'less-editor', 'template-less']);
});

gulp.task('default', ['watch', 'nodemon']);
gulp.task('debug', ['watch', 'nodemon-debug']);
